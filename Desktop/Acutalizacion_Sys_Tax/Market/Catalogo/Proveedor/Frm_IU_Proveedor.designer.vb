﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_IU_Proveedor
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_IU_Proveedor))
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.lbl_rotulo = New DevExpress.XtraEditors.LabelControl()
        Me.Frm_IU_AgenteConvertedLayout = New DevExpress.XtraLayout.LayoutControl()
        Me.chk_ir = New DevExpress.XtraEditors.CheckEdit()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.btn_guardar1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem8 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarDockControl1 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl2 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl3 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl4 = New DevExpress.XtraBars.BarDockControl()
        Me.btn_quitar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_agregar = New DevExpress.XtraEditors.SimpleButton()
        Me.Grid_Imagenes = New DevExpress.XtraGrid.GridControl()
        Me.VGrid_Imagen = New DevExpress.XtraGrid.Views.WinExplorer.WinExplorerView()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.lbl_cedula = New System.Windows.Forms.Label()
        Me.lbl_ruc = New System.Windows.Forms.Label()
        Me.txt_celular = New DevExpress.XtraEditors.TextEdit()
        Me.chk_Activo = New DevExpress.XtraEditors.CheckEdit()
        Me.Txt_Ruc = New DevExpress.XtraEditors.TextEdit()
        Me.Txt_Retenido = New DevExpress.XtraEditors.TextEdit()
        Me.TxtID = New DevExpress.XtraEditors.TextEdit()
        Me.txt_cedula = New DevExpress.XtraEditors.TextEdit()
        Me.btn_ver = New DevExpress.XtraEditors.SimpleButton()
        Me.chk_imi = New DevExpress.XtraEditors.CheckEdit()
        Me.BarManager2 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.BarDockControl5 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl6 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl7 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl8 = New DevExpress.XtraBars.BarDockControl()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.chk_iva = New DevExpress.XtraEditors.CheckEdit()
        Me.BarManager3 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.BarDockControl13 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl14 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl15 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl16 = New DevExpress.XtraBars.BarDockControl()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem5 = New DevExpress.XtraBars.BarButtonItem()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.TabbedControlGroup1 = New DevExpress.XtraLayout.TabbedControlGroup()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.TxtDesBancoitem = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TxtBancoIDitem = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem37 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.Txtsiglasitem = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem40 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.Txtsiglasitem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem34 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem35 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem45 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem88 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem101 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.BarManager11 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.BarDockControl37 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl38 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl39 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl40 = New DevExpress.XtraBars.BarDockControl()
        Me.btn_guardar = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem20 = New DevExpress.XtraBars.BarButtonItem()
        Me.LayoutConverter1 = New DevExpress.XtraLayout.Converter.LayoutConverter(Me.components)
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit5 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit6 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControlGroup5 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.TabbedControlGroup2 = New DevExpress.XtraLayout.TabbedControlGroup()
        Me.LayoutControlGroup6 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem6 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem7 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem8 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem9 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem10 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup7 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit8 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit9 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit10 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit11 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit12 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControlGroup8 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.TabbedControlGroup3 = New DevExpress.XtraLayout.TabbedControlGroup()
        Me.LayoutControlGroup9 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem11 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem12 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem13 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem14 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem15 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup10 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControl3 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit3 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit14 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit15 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit16 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit17 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit18 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControlGroup11 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.TabbedControlGroup4 = New DevExpress.XtraLayout.TabbedControlGroup()
        Me.LayoutControlGroup12 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem16 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem17 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem18 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem19 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem20 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup13 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControl4 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit4 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit20 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit21 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit22 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit23 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit24 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControlGroup14 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.TabbedControlGroup5 = New DevExpress.XtraLayout.TabbedControlGroup()
        Me.LayoutControlGroup15 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem21 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem22 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem23 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem24 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem25 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup16 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControl5 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit5 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit26 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit27 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit28 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit29 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit30 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControlGroup17 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.TabbedControlGroup6 = New DevExpress.XtraLayout.TabbedControlGroup()
        Me.LayoutControlGroup18 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem34 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem35 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem36 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem37 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem26 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem27 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem28 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem29 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem30 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem39 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup19 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControl6 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit6 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit32 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit33 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit34 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit35 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit36 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit37 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit38 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit39 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit40 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit41 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup20 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.TabbedControlGroup7 = New DevExpress.XtraLayout.TabbedControlGroup()
        Me.LayoutControlGroup21 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem38 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem41 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem42 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem43 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem44 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem31 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem32 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem46 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem47 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem48 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem49 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem50 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem51 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem33 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlGroup22 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControl7 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit7 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit7 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit13 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit19 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit25 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit31 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit42 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit43 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit44 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit45 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit46 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit47 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit48 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit49 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit50 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit51 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEdit8 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit52 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit53 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit54 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit55 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit56 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControlGroup23 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.TabbedControlGroup8 = New DevExpress.XtraLayout.TabbedControlGroup()
        Me.LayoutControlGroup24 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem62 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem63 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem64 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem65 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem40 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem66 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem67 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem68 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem69 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem70 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem71 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem41 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem42 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem72 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem43 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem73 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem74 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem44 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem75 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem45 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem46 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem76 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem47 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem78 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup25 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem79 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem80 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem81 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem48 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem82 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem49 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem83 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem84 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControl8 = New DevExpress.XtraLayout.LayoutControl()
        Me.CheckEdit12 = New DevExpress.XtraEditors.CheckEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.WinExplorerView1 = New DevExpress.XtraGrid.Views.WinExplorer.WinExplorerView()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEdit13 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit57 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit58 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit59 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit60 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.LayoutControlGroup26 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.TabbedControlGroup9 = New DevExpress.XtraLayout.TabbedControlGroup()
        Me.LayoutControlGroup27 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem52 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem53 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem36 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem38 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem54 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem55 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem56 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem39 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem50 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem51 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem52 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem57 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem58 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem59 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem60 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup28 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem77 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem85 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem86 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem53 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem87 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.BarDockControl9 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl10 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl11 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl12 = New DevExpress.XtraBars.BarDockControl()
        Me.LayoutControl9 = New DevExpress.XtraLayout.LayoutControl()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton5 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.WinExplorerView2 = New DevExpress.XtraGrid.Views.WinExplorer.WinExplorerView()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextEdit61 = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEdit10 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit62 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit63 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit64 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit65 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton6 = New DevExpress.XtraEditors.SimpleButton()
        Me.CheckEdit11 = New DevExpress.XtraEditors.CheckEdit()
        Me.BarManager4 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.BarDockControl17 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl18 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl19 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl20 = New DevExpress.XtraBars.BarDockControl()
        Me.BarButtonItem6 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem7 = New DevExpress.XtraBars.BarButtonItem()
        Me.LayoutControlGroup29 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.TabbedControlGroup10 = New DevExpress.XtraLayout.TabbedControlGroup()
        Me.LayoutControlGroup30 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem33 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem61 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem54 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem55 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem89 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem90 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem91 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem56 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem57 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem58 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem59 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem92 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem93 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem94 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem96 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup31 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem97 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem98 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem99 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem60 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem100 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.BarDockControl21 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl22 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl23 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl24 = New DevExpress.XtraBars.BarDockControl()
        CType(Me.Frm_IU_AgenteConvertedLayout, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Frm_IU_AgenteConvertedLayout.SuspendLayout()
        CType(Me.chk_ir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Grid_Imagenes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VGrid_Imagen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_celular.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk_Activo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_Ruc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_Retenido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_cedula.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk_imi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk_iva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabbedControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtDesBancoitem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtBancoIDitem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txtsiglasitem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txtsiglasitem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem88, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem101, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabbedControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl2.SuspendLayout()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabbedControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl3.SuspendLayout()
        CType(Me.CheckEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit14.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit15.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit16.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit17.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit18.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabbedControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl4.SuspendLayout()
        CType(Me.CheckEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit20.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit21.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit22.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit23.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit24.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabbedControlGroup5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl5.SuspendLayout()
        CType(Me.CheckEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit26.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit27.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit28.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit29.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit30.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabbedControlGroup6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl6.SuspendLayout()
        CType(Me.CheckEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit32.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit33.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit34.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit35.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit36.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit37.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit38.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit39.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit40.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit41.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabbedControlGroup7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl7.SuspendLayout()
        CType(Me.CheckEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit19.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit25.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit31.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit42.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit43.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit44.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit45.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit46.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit47.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit48.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit49.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit50.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit51.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit52.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit53.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit54.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit55.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit56.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabbedControlGroup8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem66, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem67, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem68, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem69, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem70, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem71, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem74, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem75, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem76, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem78, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem79, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem80, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem81, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem82, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem84, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl8.SuspendLayout()
        CType(Me.CheckEdit12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WinExplorerView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit57.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit58.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit59.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit60.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabbedControlGroup9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem77, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem85, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem86, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem87, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl9.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WinExplorerView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit61.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit62.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit63.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit64.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit65.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabbedControlGroup10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem89, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem90, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem91, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem92, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem93, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem94, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem96, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem97, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem98, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem99, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem100, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbl_rotulo
        '
        Me.lbl_rotulo.Location = New System.Drawing.Point(12, 12)
        Me.lbl_rotulo.Name = "lbl_rotulo"
        Me.lbl_rotulo.Size = New System.Drawing.Size(66, 13)
        Me.lbl_rotulo.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.lbl_rotulo.TabIndex = 1
        '
        'Frm_IU_AgenteConvertedLayout
        '
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.chk_ir)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.btn_quitar)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.btn_agregar)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.Grid_Imagenes)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.lbl_cedula)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.lbl_ruc)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.txt_celular)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.chk_Activo)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.Txt_Ruc)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.Txt_Retenido)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.TxtID)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.lbl_rotulo)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.txt_cedula)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.btn_ver)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.chk_imi)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.chk_iva)
        Me.Frm_IU_AgenteConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Frm_IU_AgenteConvertedLayout.Location = New System.Drawing.Point(0, 40)
        Me.Frm_IU_AgenteConvertedLayout.Name = "Frm_IU_AgenteConvertedLayout"
        Me.Frm_IU_AgenteConvertedLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(70, 113, 250, 501)
        Me.Frm_IU_AgenteConvertedLayout.Root = Me.LayoutControlGroup1
        Me.Frm_IU_AgenteConvertedLayout.Size = New System.Drawing.Size(1174, 543)
        Me.Frm_IU_AgenteConvertedLayout.TabIndex = 0
        '
        'chk_ir
        '
        Me.chk_ir.Location = New System.Drawing.Point(106, 250)
        Me.chk_ir.MenuManager = Me.BarManager1
        Me.chk_ir.Name = "chk_ir"
        Me.chk_ir.Properties.Caption = ""
        Me.chk_ir.Size = New System.Drawing.Size(1044, 19)
        Me.chk_ir.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.chk_ir.TabIndex = 61
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.BarDockControl1)
        Me.BarManager1.DockControls.Add(Me.BarDockControl2)
        Me.BarManager1.DockControls.Add(Me.BarDockControl3)
        Me.BarManager1.DockControls.Add(Me.BarDockControl4)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btn_guardar1, Me.BarButtonItem2, Me.BarButtonItem8})
        Me.BarManager1.MainMenu = Me.Bar1
        Me.BarManager1.MaxItemId = 4
        '
        'Bar1
        '
        Me.Bar1.BarName = "Menú principal"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btn_guardar1), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem8), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem2)})
        Me.Bar1.OptionsBar.MultiLine = True
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Menú principal"
        '
        'btn_guardar1
        '
        Me.btn_guardar1.Caption = "Guardar"
        Me.btn_guardar1.Id = 0
        Me.btn_guardar1.ImageOptions.Image = CType(resources.GetObject("btn_guardar1.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_guardar1.ImageOptions.LargeImage = CType(resources.GetObject("btn_guardar1.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.btn_guardar1.Name = "btn_guardar1"
        Me.btn_guardar1.Tag = ""
        '
        'BarButtonItem8
        '
        Me.BarButtonItem8.Caption = "BarButtonItem8"
        Me.BarButtonItem8.Id = 3
        Me.BarButtonItem8.ImageOptions.Image = CType(resources.GetObject("BarButtonItem8.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem8.Name = "BarButtonItem8"
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Ayuda"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Explicación de Terminos"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.BarButtonItem8.SuperTip = SuperToolTip1
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "Regresar"
        Me.BarButtonItem2.Id = 1
        Me.BarButtonItem2.ImageOptions.Image = CType(resources.GetObject("BarButtonItem2.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem2.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem2.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarDockControl1
        '
        Me.BarDockControl1.CausesValidation = False
        Me.BarDockControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.BarDockControl1.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl1.Manager = Me.BarManager1
        Me.BarDockControl1.Size = New System.Drawing.Size(1174, 40)
        '
        'BarDockControl2
        '
        Me.BarDockControl2.CausesValidation = False
        Me.BarDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BarDockControl2.Location = New System.Drawing.Point(0, 583)
        Me.BarDockControl2.Manager = Me.BarManager1
        Me.BarDockControl2.Size = New System.Drawing.Size(1174, 0)
        '
        'BarDockControl3
        '
        Me.BarDockControl3.CausesValidation = False
        Me.BarDockControl3.Dock = System.Windows.Forms.DockStyle.Left
        Me.BarDockControl3.Location = New System.Drawing.Point(0, 40)
        Me.BarDockControl3.Manager = Me.BarManager1
        Me.BarDockControl3.Size = New System.Drawing.Size(0, 543)
        '
        'BarDockControl4
        '
        Me.BarDockControl4.CausesValidation = False
        Me.BarDockControl4.Dock = System.Windows.Forms.DockStyle.Right
        Me.BarDockControl4.Location = New System.Drawing.Point(1174, 40)
        Me.BarDockControl4.Manager = Me.BarManager1
        Me.BarDockControl4.Size = New System.Drawing.Size(0, 543)
        '
        'btn_quitar
        '
        Me.btn_quitar.ImageOptions.Image = CType(resources.GetObject("btn_quitar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_quitar.Location = New System.Drawing.Point(54, 49)
        Me.btn_quitar.Name = "btn_quitar"
        Me.btn_quitar.Size = New System.Drawing.Size(26, 22)
        Me.btn_quitar.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.btn_quitar.TabIndex = 59
        Me.btn_quitar.Text = "Quitar Imagen"
        '
        'btn_agregar
        '
        Me.btn_agregar.ImageOptions.Image = CType(resources.GetObject("btn_agregar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_agregar.Location = New System.Drawing.Point(24, 49)
        Me.btn_agregar.Name = "btn_agregar"
        Me.btn_agregar.Size = New System.Drawing.Size(26, 22)
        Me.btn_agregar.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.btn_agregar.TabIndex = 58
        Me.btn_agregar.Tag = "Agregar"
        Me.btn_agregar.Text = "Agregar Imagen"
        '
        'Grid_Imagenes
        '
        Me.Grid_Imagenes.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Grid_Imagenes.Location = New System.Drawing.Point(24, 75)
        Me.Grid_Imagenes.MainView = Me.VGrid_Imagen
        Me.Grid_Imagenes.MenuManager = Me.BarManager1
        Me.Grid_Imagenes.Name = "Grid_Imagenes"
        Me.Grid_Imagenes.Size = New System.Drawing.Size(1126, 444)
        Me.Grid_Imagenes.TabIndex = 57
        Me.Grid_Imagenes.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.VGrid_Imagen})
        '
        'VGrid_Imagen
        '
        Me.VGrid_Imagen.Appearance.ItemDescriptionHovered.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VGrid_Imagen.Appearance.ItemDescriptionHovered.ForeColor = System.Drawing.Color.Red
        Me.VGrid_Imagen.Appearance.ItemDescriptionHovered.Options.UseFont = True
        Me.VGrid_Imagen.Appearance.ItemDescriptionHovered.Options.UseForeColor = True
        Me.VGrid_Imagen.Appearance.ItemDescriptionNormal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VGrid_Imagen.Appearance.ItemDescriptionNormal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.VGrid_Imagen.Appearance.ItemDescriptionNormal.Options.UseFont = True
        Me.VGrid_Imagen.Appearance.ItemDescriptionNormal.Options.UseForeColor = True
        Me.VGrid_Imagen.Appearance.ItemHovered.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VGrid_Imagen.Appearance.ItemHovered.ForeColor = System.Drawing.Color.Blue
        Me.VGrid_Imagen.Appearance.ItemHovered.Options.UseFont = True
        Me.VGrid_Imagen.Appearance.ItemHovered.Options.UseForeColor = True
        Me.VGrid_Imagen.Appearance.ItemNormal.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VGrid_Imagen.Appearance.ItemNormal.ForeColor = System.Drawing.Color.Blue
        Me.VGrid_Imagen.Appearance.ItemNormal.Options.UseFont = True
        Me.VGrid_Imagen.Appearance.ItemNormal.Options.UseForeColor = True
        Me.VGrid_Imagen.Appearance.ItemPressed.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VGrid_Imagen.Appearance.ItemPressed.ForeColor = System.Drawing.Color.Blue
        Me.VGrid_Imagen.Appearance.ItemPressed.Options.UseFont = True
        Me.VGrid_Imagen.Appearance.ItemPressed.Options.UseForeColor = True
        Me.VGrid_Imagen.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn14, Me.GridColumn15, Me.GridColumn16, Me.GridColumn1})
        Me.VGrid_Imagen.ColumnSet.DescriptionColumn = Me.GridColumn16
        Me.VGrid_Imagen.ColumnSet.ExtraLargeImageColumn = Me.GridColumn14
        Me.VGrid_Imagen.ColumnSet.ExtraLargeImageIndexColumn = Me.GridColumn14
        Me.VGrid_Imagen.ColumnSet.LargeImageColumn = Me.GridColumn14
        Me.VGrid_Imagen.ColumnSet.LargeImageIndexColumn = Me.GridColumn14
        Me.VGrid_Imagen.ColumnSet.MediumImageColumn = Me.GridColumn14
        Me.VGrid_Imagen.ColumnSet.MediumImageIndexColumn = Me.GridColumn14
        Me.VGrid_Imagen.ColumnSet.SmallImageColumn = Me.GridColumn14
        Me.VGrid_Imagen.ColumnSet.SmallImageIndexColumn = Me.GridColumn14
        Me.VGrid_Imagen.ColumnSet.TextColumn = Me.GridColumn16
        Me.VGrid_Imagen.GridControl = Me.Grid_Imagenes
        Me.VGrid_Imagen.Name = "VGrid_Imagen"
        Me.VGrid_Imagen.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateAllContent
        Me.VGrid_Imagen.OptionsView.Style = DevExpress.XtraGrid.Views.WinExplorer.WinExplorerViewStyle.ExtraLarge
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "Imagen"
        Me.GridColumn14.FieldName = "Imagen"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 0
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "ProveedorID"
        Me.GridColumn15.FieldName = "ProveedorID"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = True
        Me.GridColumn15.VisibleIndex = 0
        '
        'GridColumn16
        '
        Me.GridColumn16.Caption = "Documento"
        Me.GridColumn16.FieldName = "Documento"
        Me.GridColumn16.Name = "GridColumn16"
        Me.GridColumn16.Visible = True
        Me.GridColumn16.VisibleIndex = 0
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "TipoID"
        Me.GridColumn1.FieldName = "TipoID"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'lbl_cedula
        '
        Me.lbl_cedula.BackColor = System.Drawing.Color.Transparent
        Me.lbl_cedula.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cedula.ForeColor = System.Drawing.Color.Red
        Me.lbl_cedula.Location = New System.Drawing.Point(389, 152)
        Me.lbl_cedula.Name = "lbl_cedula"
        Me.lbl_cedula.Size = New System.Drawing.Size(50, 30)
        Me.lbl_cedula.TabIndex = 55
        Me.lbl_cedula.Text = "0"
        Me.lbl_cedula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl_ruc
        '
        Me.lbl_ruc.BackColor = System.Drawing.Color.Transparent
        Me.lbl_ruc.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ruc.ForeColor = System.Drawing.Color.Red
        Me.lbl_ruc.Location = New System.Drawing.Point(389, 118)
        Me.lbl_ruc.Name = "lbl_ruc"
        Me.lbl_ruc.Size = New System.Drawing.Size(50, 30)
        Me.lbl_ruc.TabIndex = 54
        Me.lbl_ruc.Text = "0"
        Me.lbl_ruc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt_celular
        '
        Me.txt_celular.EditValue = ""
        Me.txt_celular.EnterMoveNextControl = True
        Me.txt_celular.Location = New System.Drawing.Point(106, 186)
        Me.txt_celular.Name = "txt_celular"
        Me.txt_celular.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_celular.Properties.Appearance.Options.UseFont = True
        Me.txt_celular.Properties.MaxLength = 10
        Me.txt_celular.Size = New System.Drawing.Size(279, 30)
        Me.txt_celular.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.txt_celular.TabIndex = 6
        '
        'chk_Activo
        '
        Me.chk_Activo.EditValue = True
        Me.chk_Activo.Location = New System.Drawing.Point(249, 49)
        Me.chk_Activo.Name = "chk_Activo"
        Me.chk_Activo.Properties.Caption = "Activo"
        Me.chk_Activo.Size = New System.Drawing.Size(215, 19)
        Me.chk_Activo.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.chk_Activo.TabIndex = 1
        '
        'Txt_Ruc
        '
        Me.Txt_Ruc.EditValue = ""
        Me.Txt_Ruc.EnterMoveNextControl = True
        Me.Txt_Ruc.Location = New System.Drawing.Point(106, 118)
        Me.Txt_Ruc.Name = "Txt_Ruc"
        Me.Txt_Ruc.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Ruc.Properties.Appearance.Options.UseFont = True
        Me.Txt_Ruc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_Ruc.Properties.MaxLength = 14
        Me.Txt_Ruc.Size = New System.Drawing.Size(279, 30)
        Me.Txt_Ruc.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.Txt_Ruc.TabIndex = 4
        '
        'Txt_Retenido
        '
        Me.Txt_Retenido.EditValue = ""
        Me.Txt_Retenido.EnterMoveNextControl = True
        Me.Txt_Retenido.Location = New System.Drawing.Point(106, 84)
        Me.Txt_Retenido.Name = "Txt_Retenido"
        Me.Txt_Retenido.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Retenido.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Txt_Retenido.Properties.Appearance.Options.UseFont = True
        Me.Txt_Retenido.Properties.Appearance.Options.UseForeColor = True
        Me.Txt_Retenido.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_Retenido.Properties.MaxLength = 100
        Me.Txt_Retenido.Size = New System.Drawing.Size(1044, 30)
        Me.Txt_Retenido.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.Txt_Retenido.TabIndex = 3
        '
        'TxtID
        '
        Me.TxtID.Enabled = False
        Me.TxtID.EnterMoveNextControl = True
        Me.TxtID.Location = New System.Drawing.Point(106, 49)
        Me.TxtID.Name = "TxtID"
        Me.TxtID.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtID.Properties.Appearance.Options.UseFont = True
        Me.TxtID.Size = New System.Drawing.Size(139, 30)
        Me.TxtID.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.TxtID.TabIndex = 2
        '
        'txt_cedula
        '
        Me.txt_cedula.EditValue = ""
        Me.txt_cedula.EnterMoveNextControl = True
        Me.txt_cedula.Location = New System.Drawing.Point(106, 152)
        Me.txt_cedula.Name = "txt_cedula"
        Me.txt_cedula.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_cedula.Properties.Appearance.Options.UseFont = True
        Me.txt_cedula.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_cedula.Properties.MaxLength = 14
        Me.txt_cedula.Size = New System.Drawing.Size(279, 30)
        Me.txt_cedula.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.txt_cedula.TabIndex = 5
        '
        'btn_ver
        '
        Me.btn_ver.ImageOptions.Image = CType(resources.GetObject("btn_ver.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_ver.Location = New System.Drawing.Point(84, 49)
        Me.btn_ver.Name = "btn_ver"
        Me.btn_ver.Size = New System.Drawing.Size(26, 22)
        Me.btn_ver.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.btn_ver.TabIndex = 58
        Me.btn_ver.Text = "Vista Imagen"
        '
        'chk_imi
        '
        Me.chk_imi.Location = New System.Drawing.Point(106, 280)
        Me.chk_imi.MenuManager = Me.BarManager2
        Me.chk_imi.Name = "chk_imi"
        Me.chk_imi.Properties.Caption = ""
        Me.chk_imi.Size = New System.Drawing.Size(1044, 19)
        Me.chk_imi.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.chk_imi.TabIndex = 61
        '
        'BarManager2
        '
        Me.BarManager2.DockControls.Add(Me.BarDockControl5)
        Me.BarManager2.DockControls.Add(Me.BarDockControl6)
        Me.BarManager2.DockControls.Add(Me.BarDockControl7)
        Me.BarManager2.DockControls.Add(Me.BarDockControl8)
        Me.BarManager2.Form = Me
        Me.BarManager2.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItem1, Me.BarButtonItem3})
        Me.BarManager2.MaxItemId = 3
        '
        'BarDockControl5
        '
        Me.BarDockControl5.CausesValidation = False
        Me.BarDockControl5.Dock = System.Windows.Forms.DockStyle.Top
        Me.BarDockControl5.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl5.Manager = Me.BarManager2
        Me.BarDockControl5.Size = New System.Drawing.Size(1174, 0)
        '
        'BarDockControl6
        '
        Me.BarDockControl6.CausesValidation = False
        Me.BarDockControl6.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BarDockControl6.Location = New System.Drawing.Point(0, 583)
        Me.BarDockControl6.Manager = Me.BarManager2
        Me.BarDockControl6.Size = New System.Drawing.Size(1174, 0)
        '
        'BarDockControl7
        '
        Me.BarDockControl7.CausesValidation = False
        Me.BarDockControl7.Dock = System.Windows.Forms.DockStyle.Left
        Me.BarDockControl7.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl7.Manager = Me.BarManager2
        Me.BarDockControl7.Size = New System.Drawing.Size(0, 583)
        '
        'BarDockControl8
        '
        Me.BarDockControl8.CausesValidation = False
        Me.BarDockControl8.Dock = System.Windows.Forms.DockStyle.Right
        Me.BarDockControl8.Location = New System.Drawing.Point(1174, 0)
        Me.BarDockControl8.Manager = Me.BarManager2
        Me.BarDockControl8.Size = New System.Drawing.Size(0, 583)
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Guardar"
        Me.BarButtonItem1.Id = 0
        Me.BarButtonItem1.ImageOptions.Image = CType(resources.GetObject("BarButtonItem1.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem1.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem1.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "Regresar"
        Me.BarButtonItem3.Id = 1
        Me.BarButtonItem3.ImageOptions.Image = CType(resources.GetObject("BarButtonItem3.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem3.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem3.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'chk_iva
        '
        Me.chk_iva.Location = New System.Drawing.Point(106, 220)
        Me.chk_iva.MenuManager = Me.BarManager3
        Me.chk_iva.Name = "chk_iva"
        Me.chk_iva.Properties.Caption = ""
        Me.chk_iva.Size = New System.Drawing.Size(1044, 19)
        Me.chk_iva.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.chk_iva.TabIndex = 61
        '
        'BarManager3
        '
        Me.BarManager3.DockControls.Add(Me.BarDockControl13)
        Me.BarManager3.DockControls.Add(Me.BarDockControl14)
        Me.BarManager3.DockControls.Add(Me.BarDockControl15)
        Me.BarManager3.DockControls.Add(Me.BarDockControl16)
        Me.BarManager3.Form = Me
        Me.BarManager3.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItem4, Me.BarButtonItem5})
        Me.BarManager3.MaxItemId = 3
        '
        'BarDockControl13
        '
        Me.BarDockControl13.CausesValidation = False
        Me.BarDockControl13.Dock = System.Windows.Forms.DockStyle.Top
        Me.BarDockControl13.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl13.Manager = Me.BarManager3
        Me.BarDockControl13.Size = New System.Drawing.Size(1174, 0)
        '
        'BarDockControl14
        '
        Me.BarDockControl14.CausesValidation = False
        Me.BarDockControl14.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BarDockControl14.Location = New System.Drawing.Point(0, 583)
        Me.BarDockControl14.Manager = Me.BarManager3
        Me.BarDockControl14.Size = New System.Drawing.Size(1174, 0)
        '
        'BarDockControl15
        '
        Me.BarDockControl15.CausesValidation = False
        Me.BarDockControl15.Dock = System.Windows.Forms.DockStyle.Left
        Me.BarDockControl15.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl15.Manager = Me.BarManager3
        Me.BarDockControl15.Size = New System.Drawing.Size(0, 583)
        '
        'BarDockControl16
        '
        Me.BarDockControl16.CausesValidation = False
        Me.BarDockControl16.Dock = System.Windows.Forms.DockStyle.Right
        Me.BarDockControl16.Location = New System.Drawing.Point(1174, 0)
        Me.BarDockControl16.Manager = Me.BarManager3
        Me.BarDockControl16.Size = New System.Drawing.Size(0, 583)
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "Guardar"
        Me.BarButtonItem4.Id = 0
        Me.BarButtonItem4.ImageOptions.Image = CType(resources.GetObject("BarButtonItem4.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem4.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem4.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem4.Name = "BarButtonItem4"
        '
        'BarButtonItem5
        '
        Me.BarButtonItem5.Caption = "Regresar"
        Me.BarButtonItem5.Id = 1
        Me.BarButtonItem5.ImageOptions.Image = CType(resources.GetObject("BarButtonItem5.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem5.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem5.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem5.Name = "BarButtonItem5"
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.TabbedControlGroup1})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1174, 543)
        Me.LayoutControlGroup1.Text = "Generales"
        Me.LayoutControlGroup1.TextVisible = False
        '
        'TabbedControlGroup1
        '
        Me.TabbedControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.TabbedControlGroup1.Name = "TabbedControlGroup1"
        Me.TabbedControlGroup1.SelectedTabPage = Me.LayoutControlGroup3
        Me.TabbedControlGroup1.SelectedTabPageIndex = 1
        Me.TabbedControlGroup1.Size = New System.Drawing.Size(1154, 523)
        Me.TabbedControlGroup1.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup2, Me.LayoutControlGroup3})
        Me.TabbedControlGroup1.Text = "Generales"
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.CaptionImage = CType(resources.GetObject("LayoutControlGroup3.CaptionImage"), System.Drawing.Image)
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem12, Me.LayoutControlItem19, Me.LayoutControlItem26, Me.EmptySpaceItem2, Me.LayoutControlItem6})
        Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(1130, 474)
        Me.LayoutControlGroup3.Text = "Soportes"
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.Grid_Imagenes
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(1130, 448)
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextVisible = False
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.btn_agregar
        Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem19.MaxSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem19.MinSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem19.TextVisible = False
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.btn_quitar
        Me.LayoutControlItem26.Location = New System.Drawing.Point(30, 0)
        Me.LayoutControlItem26.MaxSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem26.MinSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem26.TextVisible = False
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(90, 0)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(1040, 26)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.btn_ver
        Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem19"
        Me.LayoutControlItem6.Location = New System.Drawing.Point(60, 0)
        Me.LayoutControlItem6.MaxSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem6.MinSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem6.Text = "LayoutControlItem19"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.CaptionImage = CType(resources.GetObject("LayoutControlGroup2.CaptionImage"), System.Drawing.Image)
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.TxtDesBancoitem, Me.TxtBancoIDitem, Me.EmptySpaceItem1, Me.EmptySpaceItem37, Me.Txtsiglasitem, Me.LayoutControlItem40, Me.Txtsiglasitem1, Me.EmptySpaceItem34, Me.EmptySpaceItem4, Me.EmptySpaceItem35, Me.LayoutControlItem7, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem45, Me.LayoutControlItem88, Me.LayoutControlItem101})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(1130, 474)
        Me.LayoutControlGroup2.Text = "Generales"
        '
        'TxtDesBancoitem
        '
        Me.TxtDesBancoitem.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtDesBancoitem.AppearanceItemCaption.Options.UseFont = True
        Me.TxtDesBancoitem.Control = Me.Txt_Retenido
        Me.TxtDesBancoitem.Image = CType(resources.GetObject("TxtDesBancoitem.Image"), System.Drawing.Image)
        Me.TxtDesBancoitem.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.TxtDesBancoitem.Location = New System.Drawing.Point(0, 35)
        Me.TxtDesBancoitem.MaxSize = New System.Drawing.Size(0, 34)
        Me.TxtDesBancoitem.MinSize = New System.Drawing.Size(136, 34)
        Me.TxtDesBancoitem.Name = "TxtDesBancoitem"
        Me.TxtDesBancoitem.Size = New System.Drawing.Size(1130, 34)
        Me.TxtDesBancoitem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.TxtDesBancoitem.Text = "Proveedor"
        Me.TxtDesBancoitem.TextLocation = DevExpress.Utils.Locations.Left
        Me.TxtDesBancoitem.TextSize = New System.Drawing.Size(79, 16)
        '
        'TxtBancoIDitem
        '
        Me.TxtBancoIDitem.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBancoIDitem.AppearanceItemCaption.Options.UseFont = True
        Me.TxtBancoIDitem.Control = Me.TxtID
        Me.TxtBancoIDitem.Location = New System.Drawing.Point(0, 0)
        Me.TxtBancoIDitem.MaxSize = New System.Drawing.Size(0, 35)
        Me.TxtBancoIDitem.MinSize = New System.Drawing.Size(136, 35)
        Me.TxtBancoIDitem.Name = "TxtBancoIDitem"
        Me.TxtBancoIDitem.Size = New System.Drawing.Size(225, 35)
        Me.TxtBancoIDitem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.TxtBancoIDitem.Text = "ID"
        Me.TxtBancoIDitem.TextLocation = DevExpress.Utils.Locations.Left
        Me.TxtBancoIDitem.TextSize = New System.Drawing.Size(79, 16)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(444, 0)
        Me.EmptySpaceItem1.MinSize = New System.Drawing.Size(104, 24)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(686, 35)
        Me.EmptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem37
        '
        Me.EmptySpaceItem37.AllowHotTrack = False
        Me.EmptySpaceItem37.Location = New System.Drawing.Point(0, 261)
        Me.EmptySpaceItem37.Name = "EmptySpaceItem37"
        Me.EmptySpaceItem37.Size = New System.Drawing.Size(1130, 213)
        Me.EmptySpaceItem37.TextSize = New System.Drawing.Size(0, 0)
        '
        'Txtsiglasitem
        '
        Me.Txtsiglasitem.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txtsiglasitem.AppearanceItemCaption.Options.UseFont = True
        Me.Txtsiglasitem.Control = Me.Txt_Ruc
        Me.Txtsiglasitem.Image = CType(resources.GetObject("Txtsiglasitem.Image"), System.Drawing.Image)
        Me.Txtsiglasitem.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.Txtsiglasitem.Location = New System.Drawing.Point(0, 69)
        Me.Txtsiglasitem.MaxSize = New System.Drawing.Size(0, 34)
        Me.Txtsiglasitem.MinSize = New System.Drawing.Size(136, 34)
        Me.Txtsiglasitem.Name = "Txtsiglasitem"
        Me.Txtsiglasitem.Size = New System.Drawing.Size(365, 34)
        Me.Txtsiglasitem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.Txtsiglasitem.Text = "RUC"
        Me.Txtsiglasitem.TextLocation = DevExpress.Utils.Locations.Left
        Me.Txtsiglasitem.TextSize = New System.Drawing.Size(79, 16)
        '
        'LayoutControlItem40
        '
        Me.LayoutControlItem40.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem40.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem40.Control = Me.txt_celular
        Me.LayoutControlItem40.Image = CType(resources.GetObject("LayoutControlItem40.Image"), System.Drawing.Image)
        Me.LayoutControlItem40.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem40.Location = New System.Drawing.Point(0, 137)
        Me.LayoutControlItem40.MaxSize = New System.Drawing.Size(0, 34)
        Me.LayoutControlItem40.MinSize = New System.Drawing.Size(136, 34)
        Me.LayoutControlItem40.Name = "LayoutControlItem40"
        Me.LayoutControlItem40.Size = New System.Drawing.Size(365, 34)
        Me.LayoutControlItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem40.Text = "Telefono"
        Me.LayoutControlItem40.TextSize = New System.Drawing.Size(79, 16)
        '
        'Txtsiglasitem1
        '
        Me.Txtsiglasitem1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txtsiglasitem1.AppearanceItemCaption.Options.UseFont = True
        Me.Txtsiglasitem1.Control = Me.txt_cedula
        Me.Txtsiglasitem1.CustomizationFormText = "Ruc"
        Me.Txtsiglasitem1.Image = CType(resources.GetObject("Txtsiglasitem1.Image"), System.Drawing.Image)
        Me.Txtsiglasitem1.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.Txtsiglasitem1.Location = New System.Drawing.Point(0, 103)
        Me.Txtsiglasitem1.MaxSize = New System.Drawing.Size(0, 34)
        Me.Txtsiglasitem1.MinSize = New System.Drawing.Size(136, 34)
        Me.Txtsiglasitem1.Name = "Txtsiglasitem1"
        Me.Txtsiglasitem1.Size = New System.Drawing.Size(365, 34)
        Me.Txtsiglasitem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.Txtsiglasitem1.Text = "No Cedula"
        Me.Txtsiglasitem1.TextLocation = DevExpress.Utils.Locations.Left
        Me.Txtsiglasitem1.TextSize = New System.Drawing.Size(79, 16)
        '
        'EmptySpaceItem34
        '
        Me.EmptySpaceItem34.AllowHotTrack = False
        Me.EmptySpaceItem34.Location = New System.Drawing.Point(365, 137)
        Me.EmptySpaceItem34.MaxSize = New System.Drawing.Size(0, 34)
        Me.EmptySpaceItem34.MinSize = New System.Drawing.Size(104, 34)
        Me.EmptySpaceItem34.Name = "EmptySpaceItem34"
        Me.EmptySpaceItem34.Size = New System.Drawing.Size(765, 34)
        Me.EmptySpaceItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem34.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = False
        Me.EmptySpaceItem4.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmptySpaceItem4.AppearanceItemCaption.Options.UseFont = True
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(419, 103)
        Me.EmptySpaceItem4.MaxSize = New System.Drawing.Size(0, 34)
        Me.EmptySpaceItem4.MinSize = New System.Drawing.Size(183, 34)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(711, 34)
        Me.EmptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem4.Text = "Ejemplo : 0012701820003Q ( 14 Caracteres )"
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(79, 0)
        Me.EmptySpaceItem4.TextVisible = True
        '
        'EmptySpaceItem35
        '
        Me.EmptySpaceItem35.AllowHotTrack = False
        Me.EmptySpaceItem35.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmptySpaceItem35.AppearanceItemCaption.Options.UseFont = True
        Me.EmptySpaceItem35.Location = New System.Drawing.Point(419, 69)
        Me.EmptySpaceItem35.MinSize = New System.Drawing.Size(183, 24)
        Me.EmptySpaceItem35.Name = "EmptySpaceItem35"
        Me.EmptySpaceItem35.Size = New System.Drawing.Size(711, 34)
        Me.EmptySpaceItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem35.Text = "Ejemplo : J0310000252904  ( 14 Caracteres )"
        Me.EmptySpaceItem35.TextSize = New System.Drawing.Size(79, 0)
        Me.EmptySpaceItem35.TextVisible = True
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.chk_Activo
        Me.LayoutControlItem7.Location = New System.Drawing.Point(225, 0)
        Me.LayoutControlItem7.MaxSize = New System.Drawing.Size(0, 35)
        Me.LayoutControlItem7.MinSize = New System.Drawing.Size(56, 35)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(219, 35)
        Me.LayoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem4.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem4.Control = Me.lbl_ruc
        Me.LayoutControlItem4.Location = New System.Drawing.Point(365, 69)
        Me.LayoutControlItem4.MinSize = New System.Drawing.Size(24, 24)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(54, 34)
        Me.LayoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.lbl_cedula
        Me.LayoutControlItem5.Location = New System.Drawing.Point(365, 103)
        Me.LayoutControlItem5.MinSize = New System.Drawing.Size(24, 24)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(54, 34)
        Me.LayoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = False
        '
        'LayoutControlItem45
        '
        Me.LayoutControlItem45.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem45.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem45.Control = Me.chk_ir
        Me.LayoutControlItem45.Location = New System.Drawing.Point(0, 201)
        Me.LayoutControlItem45.MaxSize = New System.Drawing.Size(0, 30)
        Me.LayoutControlItem45.MinSize = New System.Drawing.Size(140, 30)
        Me.LayoutControlItem45.Name = "LayoutControlItem45"
        Me.LayoutControlItem45.Size = New System.Drawing.Size(1130, 30)
        Me.LayoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem45.Text = "Aplicar IR"
        Me.LayoutControlItem45.TextSize = New System.Drawing.Size(79, 16)
        Me.LayoutControlItem45.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'LayoutControlItem88
        '
        Me.LayoutControlItem88.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem88.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem88.Control = Me.chk_imi
        Me.LayoutControlItem88.CustomizationFormText = "Exoneración IR"
        Me.LayoutControlItem88.Location = New System.Drawing.Point(0, 231)
        Me.LayoutControlItem88.MaxSize = New System.Drawing.Size(0, 30)
        Me.LayoutControlItem88.MinSize = New System.Drawing.Size(140, 30)
        Me.LayoutControlItem88.Name = "LayoutControlItem88"
        Me.LayoutControlItem88.Size = New System.Drawing.Size(1130, 30)
        Me.LayoutControlItem88.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem88.Text = "Aplicar IMI"
        Me.LayoutControlItem88.TextSize = New System.Drawing.Size(79, 16)
        Me.LayoutControlItem88.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'LayoutControlItem101
        '
        Me.LayoutControlItem101.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem101.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem101.Control = Me.chk_iva
        Me.LayoutControlItem101.CustomizationFormText = "Aplicar IR"
        Me.LayoutControlItem101.Location = New System.Drawing.Point(0, 171)
        Me.LayoutControlItem101.MaxSize = New System.Drawing.Size(0, 30)
        Me.LayoutControlItem101.MinSize = New System.Drawing.Size(140, 30)
        Me.LayoutControlItem101.Name = "LayoutControlItem101"
        Me.LayoutControlItem101.Size = New System.Drawing.Size(1130, 30)
        Me.LayoutControlItem101.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem101.Text = "Aplicar IVA"
        Me.LayoutControlItem101.TextSize = New System.Drawing.Size(79, 16)
        Me.LayoutControlItem101.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'BarManager11
        '
        Me.BarManager11.DockControls.Add(Me.BarDockControl37)
        Me.BarManager11.DockControls.Add(Me.BarDockControl38)
        Me.BarManager11.DockControls.Add(Me.BarDockControl39)
        Me.BarManager11.DockControls.Add(Me.BarDockControl40)
        Me.BarManager11.Form = Me
        Me.BarManager11.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btn_guardar, Me.BarButtonItem20})
        Me.BarManager11.MaxItemId = 3
        '
        'BarDockControl37
        '
        Me.BarDockControl37.CausesValidation = False
        Me.BarDockControl37.Dock = System.Windows.Forms.DockStyle.Top
        Me.BarDockControl37.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl37.Manager = Me.BarManager11
        Me.BarDockControl37.Size = New System.Drawing.Size(1174, 0)
        '
        'BarDockControl38
        '
        Me.BarDockControl38.CausesValidation = False
        Me.BarDockControl38.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BarDockControl38.Location = New System.Drawing.Point(0, 583)
        Me.BarDockControl38.Manager = Me.BarManager11
        Me.BarDockControl38.Size = New System.Drawing.Size(1174, 0)
        '
        'BarDockControl39
        '
        Me.BarDockControl39.CausesValidation = False
        Me.BarDockControl39.Dock = System.Windows.Forms.DockStyle.Left
        Me.BarDockControl39.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl39.Manager = Me.BarManager11
        Me.BarDockControl39.Size = New System.Drawing.Size(0, 583)
        '
        'BarDockControl40
        '
        Me.BarDockControl40.CausesValidation = False
        Me.BarDockControl40.Dock = System.Windows.Forms.DockStyle.Right
        Me.BarDockControl40.Location = New System.Drawing.Point(1174, 0)
        Me.BarDockControl40.Manager = Me.BarManager11
        Me.BarDockControl40.Size = New System.Drawing.Size(0, 583)
        '
        'btn_guardar
        '
        Me.btn_guardar.Caption = "Guardar"
        Me.btn_guardar.Id = 0
        Me.btn_guardar.ImageOptions.Image = CType(resources.GetObject("btn_guardar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_guardar.ImageOptions.LargeImage = CType(resources.GetObject("btn_guardar.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.btn_guardar.Name = "btn_guardar"
        '
        'BarButtonItem20
        '
        Me.BarButtonItem20.Caption = "Regresar"
        Me.BarButtonItem20.Id = 1
        Me.BarButtonItem20.ImageOptions.Image = CType(resources.GetObject("BarButtonItem20.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem20.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem20.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem20.Name = "BarButtonItem20"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.CheckEdit1)
        Me.LayoutControl1.Controls.Add(Me.TextEdit2)
        Me.LayoutControl1.Controls.Add(Me.TextEdit3)
        Me.LayoutControl1.Controls.Add(Me.TextEdit4)
        Me.LayoutControl1.Controls.Add(Me.TextEdit5)
        Me.LayoutControl1.Controls.Add(Me.TextEdit6)
        Me.LayoutControl1.Controls.Add(Me.LabelControl1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 40)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup5
        Me.LayoutControl1.Size = New System.Drawing.Size(843, 357)
        Me.LayoutControl1.TabIndex = 52
        '
        'CheckEdit1
        '
        Me.CheckEdit1.EditValue = True
        Me.CheckEdit1.Location = New System.Drawing.Point(193, 46)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Caption = "Activo"
        Me.CheckEdit1.Size = New System.Drawing.Size(87, 19)
        Me.CheckEdit1.StyleController = Me.LayoutControl1
        Me.CheckEdit1.TabIndex = 1
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(100, 166)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit2.Properties.Appearance.Options.UseFont = True
        Me.TextEdit2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit2.Properties.Mask.EditMask = "n0"
        Me.TextEdit2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit2.Size = New System.Drawing.Size(278, 26)
        Me.TextEdit2.StyleController = Me.LayoutControl1
        Me.TextEdit2.TabIndex = 5
        '
        'TextEdit3
        '
        Me.TextEdit3.Location = New System.Drawing.Point(100, 136)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit3.Properties.Appearance.Options.UseFont = True
        Me.TextEdit3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit3.Properties.MaxLength = 200
        Me.TextEdit3.Size = New System.Drawing.Size(719, 26)
        Me.TextEdit3.StyleController = Me.LayoutControl1
        Me.TextEdit3.TabIndex = 4
        '
        'TextEdit4
        '
        Me.TextEdit4.Location = New System.Drawing.Point(100, 106)
        Me.TextEdit4.Name = "TextEdit4"
        Me.TextEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit4.Properties.Appearance.Options.UseFont = True
        Me.TextEdit4.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit4.Properties.MaxLength = 50
        Me.TextEdit4.Size = New System.Drawing.Size(278, 26)
        Me.TextEdit4.StyleController = Me.LayoutControl1
        Me.TextEdit4.TabIndex = 3
        '
        'TextEdit5
        '
        Me.TextEdit5.Location = New System.Drawing.Point(100, 76)
        Me.TextEdit5.Name = "TextEdit5"
        Me.TextEdit5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit5.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextEdit5.Properties.Appearance.Options.UseFont = True
        Me.TextEdit5.Properties.Appearance.Options.UseForeColor = True
        Me.TextEdit5.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit5.Properties.MaxLength = 100
        Me.TextEdit5.Size = New System.Drawing.Size(638, 26)
        Me.TextEdit5.StyleController = Me.LayoutControl1
        Me.TextEdit5.TabIndex = 2
        '
        'TextEdit6
        '
        Me.TextEdit6.Enabled = False
        Me.TextEdit6.Location = New System.Drawing.Point(100, 46)
        Me.TextEdit6.Name = "TextEdit6"
        Me.TextEdit6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit6.Properties.Appearance.Options.UseFont = True
        Me.TextEdit6.Size = New System.Drawing.Size(89, 26)
        Me.TextEdit6.StyleController = Me.LayoutControl1
        Me.TextEdit6.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl1.StyleController = Me.LayoutControl1
        Me.LabelControl1.TabIndex = 47
        '
        'LayoutControlGroup5
        '
        Me.LayoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup5.GroupBordersVisible = False
        Me.LayoutControlGroup5.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.TabbedControlGroup2})
        Me.LayoutControlGroup5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup5.Name = "Root"
        Me.LayoutControlGroup5.Size = New System.Drawing.Size(843, 357)
        Me.LayoutControlGroup5.Text = "Generales"
        Me.LayoutControlGroup5.TextVisible = False
        '
        'TabbedControlGroup2
        '
        Me.TabbedControlGroup2.Location = New System.Drawing.Point(0, 0)
        Me.TabbedControlGroup2.Name = "TabbedControlGroup1"
        Me.TabbedControlGroup2.SelectedTabPage = Me.LayoutControlGroup6
        Me.TabbedControlGroup2.SelectedTabPageIndex = 0
        Me.TabbedControlGroup2.Size = New System.Drawing.Size(823, 337)
        Me.TabbedControlGroup2.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup6, Me.LayoutControlGroup7})
        Me.TabbedControlGroup2.Text = "Generales"
        '
        'LayoutControlGroup6
        '
        Me.LayoutControlGroup6.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.EmptySpaceItem6, Me.EmptySpaceItem7, Me.EmptySpaceItem8, Me.EmptySpaceItem9, Me.EmptySpaceItem10, Me.LayoutControlItem11})
        Me.LayoutControlGroup6.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup6.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup6.Size = New System.Drawing.Size(799, 291)
        Me.LayoutControlGroup6.Text = "Generales"
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem1.Control = Me.TextEdit5
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 30)
        Me.LayoutControlItem1.MaxSize = New System.Drawing.Size(718, 30)
        Me.LayoutControlItem1.MinSize = New System.Drawing.Size(718, 30)
        Me.LayoutControlItem1.Name = "TxtDesBancoitem"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(799, 30)
        Me.LayoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem1.Text = "Razon Social"
        Me.LayoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.TextEdit6
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.MaxSize = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem2.MinSize = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem2.Name = "TxtBancoIDitem"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem2.Text = "ID"
        Me.LayoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem3.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem3.Control = Me.TextEdit4
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 60)
        Me.LayoutControlItem3.MaxSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem3.MinSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem3.Name = "Txtsiglasitem"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem3.Text = "Ruc"
        Me.LayoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem8.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem8.Control = Me.TextEdit3
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 90)
        Me.LayoutControlItem8.MaxSize = New System.Drawing.Size(0, 30)
        Me.LayoutControlItem8.MinSize = New System.Drawing.Size(130, 30)
        Me.LayoutControlItem8.Name = "LayoutControlItem4"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(799, 30)
        Me.LayoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem8.Text = "Dirección"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem9.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem9.Control = Me.TextEdit2
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem9.MaxSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem9.MinSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem9.Name = "LayoutControlItem5"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem9.Text = "Telefono"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(73, 16)
        '
        'EmptySpaceItem6
        '
        Me.EmptySpaceItem6.AllowHotTrack = False
        Me.EmptySpaceItem6.Location = New System.Drawing.Point(0, 182)
        Me.EmptySpaceItem6.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem6.Size = New System.Drawing.Size(799, 109)
        Me.EmptySpaceItem6.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem7
        '
        Me.EmptySpaceItem7.AllowHotTrack = False
        Me.EmptySpaceItem7.Location = New System.Drawing.Point(0, 150)
        Me.EmptySpaceItem7.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem7.Size = New System.Drawing.Size(799, 32)
        Me.EmptySpaceItem7.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem8
        '
        Me.EmptySpaceItem8.AllowHotTrack = False
        Me.EmptySpaceItem8.Location = New System.Drawing.Point(358, 120)
        Me.EmptySpaceItem8.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem8.Size = New System.Drawing.Size(441, 30)
        Me.EmptySpaceItem8.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem9
        '
        Me.EmptySpaceItem9.AllowHotTrack = False
        Me.EmptySpaceItem9.Location = New System.Drawing.Point(358, 60)
        Me.EmptySpaceItem9.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem9.Size = New System.Drawing.Size(441, 30)
        Me.EmptySpaceItem9.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem10
        '
        Me.EmptySpaceItem10.AllowHotTrack = False
        Me.EmptySpaceItem10.Location = New System.Drawing.Point(260, 0)
        Me.EmptySpaceItem10.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem10.Size = New System.Drawing.Size(539, 30)
        Me.EmptySpaceItem10.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.CheckEdit1
        Me.LayoutControlItem11.Location = New System.Drawing.Point(169, 0)
        Me.LayoutControlItem11.Name = "LayoutControlItem7"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(91, 30)
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlGroup7
        '
        Me.LayoutControlGroup7.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup7.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup7.Size = New System.Drawing.Size(799, 291)
        Me.LayoutControlGroup7.Text = "Contactos"
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.CheckEdit2)
        Me.LayoutControl2.Controls.Add(Me.TextEdit8)
        Me.LayoutControl2.Controls.Add(Me.TextEdit9)
        Me.LayoutControl2.Controls.Add(Me.TextEdit10)
        Me.LayoutControl2.Controls.Add(Me.TextEdit11)
        Me.LayoutControl2.Controls.Add(Me.TextEdit12)
        Me.LayoutControl2.Controls.Add(Me.LabelControl2)
        Me.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl2.Location = New System.Drawing.Point(0, 40)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.Root = Me.LayoutControlGroup8
        Me.LayoutControl2.Size = New System.Drawing.Size(843, 357)
        Me.LayoutControl2.TabIndex = 52
        '
        'CheckEdit2
        '
        Me.CheckEdit2.EditValue = True
        Me.CheckEdit2.Location = New System.Drawing.Point(193, 46)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Caption = "Activo"
        Me.CheckEdit2.Size = New System.Drawing.Size(87, 19)
        Me.CheckEdit2.StyleController = Me.LayoutControl2
        Me.CheckEdit2.TabIndex = 1
        '
        'TextEdit8
        '
        Me.TextEdit8.Location = New System.Drawing.Point(100, 166)
        Me.TextEdit8.Name = "TextEdit8"
        Me.TextEdit8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit8.Properties.Appearance.Options.UseFont = True
        Me.TextEdit8.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit8.Properties.Mask.EditMask = "n0"
        Me.TextEdit8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit8.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit8.Size = New System.Drawing.Size(278, 26)
        Me.TextEdit8.StyleController = Me.LayoutControl2
        Me.TextEdit8.TabIndex = 5
        '
        'TextEdit9
        '
        Me.TextEdit9.Location = New System.Drawing.Point(100, 136)
        Me.TextEdit9.Name = "TextEdit9"
        Me.TextEdit9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit9.Properties.Appearance.Options.UseFont = True
        Me.TextEdit9.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit9.Properties.MaxLength = 200
        Me.TextEdit9.Size = New System.Drawing.Size(719, 26)
        Me.TextEdit9.StyleController = Me.LayoutControl2
        Me.TextEdit9.TabIndex = 4
        '
        'TextEdit10
        '
        Me.TextEdit10.Location = New System.Drawing.Point(100, 106)
        Me.TextEdit10.Name = "TextEdit10"
        Me.TextEdit10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit10.Properties.Appearance.Options.UseFont = True
        Me.TextEdit10.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit10.Properties.MaxLength = 50
        Me.TextEdit10.Size = New System.Drawing.Size(278, 26)
        Me.TextEdit10.StyleController = Me.LayoutControl2
        Me.TextEdit10.TabIndex = 3
        '
        'TextEdit11
        '
        Me.TextEdit11.Location = New System.Drawing.Point(100, 76)
        Me.TextEdit11.Name = "TextEdit11"
        Me.TextEdit11.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit11.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextEdit11.Properties.Appearance.Options.UseFont = True
        Me.TextEdit11.Properties.Appearance.Options.UseForeColor = True
        Me.TextEdit11.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit11.Properties.MaxLength = 100
        Me.TextEdit11.Size = New System.Drawing.Size(638, 26)
        Me.TextEdit11.StyleController = Me.LayoutControl2
        Me.TextEdit11.TabIndex = 2
        '
        'TextEdit12
        '
        Me.TextEdit12.Enabled = False
        Me.TextEdit12.Location = New System.Drawing.Point(100, 46)
        Me.TextEdit12.Name = "TextEdit12"
        Me.TextEdit12.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit12.Properties.Appearance.Options.UseFont = True
        Me.TextEdit12.Size = New System.Drawing.Size(89, 26)
        Me.TextEdit12.StyleController = Me.LayoutControl2
        Me.TextEdit12.TabIndex = 0
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl2.StyleController = Me.LayoutControl2
        Me.LabelControl2.TabIndex = 47
        '
        'LayoutControlGroup8
        '
        Me.LayoutControlGroup8.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup8.GroupBordersVisible = False
        Me.LayoutControlGroup8.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.TabbedControlGroup3})
        Me.LayoutControlGroup8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup8.Name = "Root"
        Me.LayoutControlGroup8.Size = New System.Drawing.Size(843, 357)
        Me.LayoutControlGroup8.Text = "Generales"
        Me.LayoutControlGroup8.TextVisible = False
        '
        'TabbedControlGroup3
        '
        Me.TabbedControlGroup3.Location = New System.Drawing.Point(0, 0)
        Me.TabbedControlGroup3.Name = "TabbedControlGroup1"
        Me.TabbedControlGroup3.SelectedTabPage = Me.LayoutControlGroup9
        Me.TabbedControlGroup3.SelectedTabPageIndex = 0
        Me.TabbedControlGroup3.Size = New System.Drawing.Size(823, 337)
        Me.TabbedControlGroup3.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup9, Me.LayoutControlGroup10})
        Me.TabbedControlGroup3.Text = "Generales"
        '
        'LayoutControlGroup9
        '
        Me.LayoutControlGroup9.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem10, Me.LayoutControlItem13, Me.LayoutControlItem14, Me.LayoutControlItem15, Me.LayoutControlItem16, Me.EmptySpaceItem11, Me.EmptySpaceItem12, Me.EmptySpaceItem13, Me.EmptySpaceItem14, Me.EmptySpaceItem15, Me.LayoutControlItem18})
        Me.LayoutControlGroup9.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup9.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup9.Size = New System.Drawing.Size(799, 291)
        Me.LayoutControlGroup9.Text = "Generales"
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem10.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem10.Control = Me.TextEdit11
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 30)
        Me.LayoutControlItem10.MaxSize = New System.Drawing.Size(718, 30)
        Me.LayoutControlItem10.MinSize = New System.Drawing.Size(718, 30)
        Me.LayoutControlItem10.Name = "TxtDesBancoitem"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(799, 30)
        Me.LayoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem10.Text = "Razon Social"
        Me.LayoutControlItem10.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem13.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem13.Control = Me.TextEdit12
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem13.MaxSize = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem13.MinSize = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem13.Name = "TxtBancoIDitem"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem13.Text = "ID"
        Me.LayoutControlItem13.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem14.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem14.Control = Me.TextEdit10
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 60)
        Me.LayoutControlItem14.MaxSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem14.MinSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem14.Name = "Txtsiglasitem"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem14.Text = "Ruc"
        Me.LayoutControlItem14.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem15.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem15.Control = Me.TextEdit9
        Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 90)
        Me.LayoutControlItem15.MaxSize = New System.Drawing.Size(0, 30)
        Me.LayoutControlItem15.MinSize = New System.Drawing.Size(130, 30)
        Me.LayoutControlItem15.Name = "LayoutControlItem4"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(799, 30)
        Me.LayoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem15.Text = "Dirección"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem16.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem16.Control = Me.TextEdit8
        Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem16.MaxSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem16.MinSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem16.Name = "LayoutControlItem5"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem16.Text = "Telefono"
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(73, 16)
        '
        'EmptySpaceItem11
        '
        Me.EmptySpaceItem11.AllowHotTrack = False
        Me.EmptySpaceItem11.Location = New System.Drawing.Point(0, 182)
        Me.EmptySpaceItem11.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem11.Size = New System.Drawing.Size(799, 109)
        Me.EmptySpaceItem11.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem12
        '
        Me.EmptySpaceItem12.AllowHotTrack = False
        Me.EmptySpaceItem12.Location = New System.Drawing.Point(0, 150)
        Me.EmptySpaceItem12.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem12.Size = New System.Drawing.Size(799, 32)
        Me.EmptySpaceItem12.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem13
        '
        Me.EmptySpaceItem13.AllowHotTrack = False
        Me.EmptySpaceItem13.Location = New System.Drawing.Point(358, 120)
        Me.EmptySpaceItem13.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem13.Size = New System.Drawing.Size(441, 30)
        Me.EmptySpaceItem13.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem14
        '
        Me.EmptySpaceItem14.AllowHotTrack = False
        Me.EmptySpaceItem14.Location = New System.Drawing.Point(358, 60)
        Me.EmptySpaceItem14.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem14.Size = New System.Drawing.Size(441, 30)
        Me.EmptySpaceItem14.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem15
        '
        Me.EmptySpaceItem15.AllowHotTrack = False
        Me.EmptySpaceItem15.Location = New System.Drawing.Point(260, 0)
        Me.EmptySpaceItem15.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem15.Size = New System.Drawing.Size(539, 30)
        Me.EmptySpaceItem15.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.CheckEdit2
        Me.LayoutControlItem18.Location = New System.Drawing.Point(169, 0)
        Me.LayoutControlItem18.Name = "LayoutControlItem7"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(91, 30)
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem18.TextVisible = False
        '
        'LayoutControlGroup10
        '
        Me.LayoutControlGroup10.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup10.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup10.Size = New System.Drawing.Size(799, 291)
        Me.LayoutControlGroup10.Text = "Contactos"
        '
        'LayoutControl3
        '
        Me.LayoutControl3.Controls.Add(Me.CheckEdit3)
        Me.LayoutControl3.Controls.Add(Me.TextEdit14)
        Me.LayoutControl3.Controls.Add(Me.TextEdit15)
        Me.LayoutControl3.Controls.Add(Me.TextEdit16)
        Me.LayoutControl3.Controls.Add(Me.TextEdit17)
        Me.LayoutControl3.Controls.Add(Me.TextEdit18)
        Me.LayoutControl3.Controls.Add(Me.LabelControl3)
        Me.LayoutControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl3.Location = New System.Drawing.Point(0, 40)
        Me.LayoutControl3.Name = "LayoutControl3"
        Me.LayoutControl3.Root = Me.LayoutControlGroup11
        Me.LayoutControl3.Size = New System.Drawing.Size(843, 357)
        Me.LayoutControl3.TabIndex = 52
        '
        'CheckEdit3
        '
        Me.CheckEdit3.EditValue = True
        Me.CheckEdit3.Location = New System.Drawing.Point(193, 46)
        Me.CheckEdit3.Name = "CheckEdit3"
        Me.CheckEdit3.Properties.Caption = "Activo"
        Me.CheckEdit3.Size = New System.Drawing.Size(87, 19)
        Me.CheckEdit3.StyleController = Me.LayoutControl3
        Me.CheckEdit3.TabIndex = 1
        '
        'TextEdit14
        '
        Me.TextEdit14.Location = New System.Drawing.Point(100, 166)
        Me.TextEdit14.Name = "TextEdit14"
        Me.TextEdit14.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit14.Properties.Appearance.Options.UseFont = True
        Me.TextEdit14.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit14.Properties.Mask.EditMask = "n0"
        Me.TextEdit14.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit14.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit14.Size = New System.Drawing.Size(278, 26)
        Me.TextEdit14.StyleController = Me.LayoutControl3
        Me.TextEdit14.TabIndex = 5
        '
        'TextEdit15
        '
        Me.TextEdit15.Location = New System.Drawing.Point(100, 136)
        Me.TextEdit15.Name = "TextEdit15"
        Me.TextEdit15.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit15.Properties.Appearance.Options.UseFont = True
        Me.TextEdit15.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit15.Properties.MaxLength = 200
        Me.TextEdit15.Size = New System.Drawing.Size(719, 26)
        Me.TextEdit15.StyleController = Me.LayoutControl3
        Me.TextEdit15.TabIndex = 4
        '
        'TextEdit16
        '
        Me.TextEdit16.Location = New System.Drawing.Point(100, 106)
        Me.TextEdit16.Name = "TextEdit16"
        Me.TextEdit16.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit16.Properties.Appearance.Options.UseFont = True
        Me.TextEdit16.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit16.Properties.MaxLength = 50
        Me.TextEdit16.Size = New System.Drawing.Size(278, 26)
        Me.TextEdit16.StyleController = Me.LayoutControl3
        Me.TextEdit16.TabIndex = 3
        '
        'TextEdit17
        '
        Me.TextEdit17.Location = New System.Drawing.Point(100, 76)
        Me.TextEdit17.Name = "TextEdit17"
        Me.TextEdit17.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit17.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextEdit17.Properties.Appearance.Options.UseFont = True
        Me.TextEdit17.Properties.Appearance.Options.UseForeColor = True
        Me.TextEdit17.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit17.Properties.MaxLength = 100
        Me.TextEdit17.Size = New System.Drawing.Size(638, 26)
        Me.TextEdit17.StyleController = Me.LayoutControl3
        Me.TextEdit17.TabIndex = 2
        '
        'TextEdit18
        '
        Me.TextEdit18.Enabled = False
        Me.TextEdit18.Location = New System.Drawing.Point(100, 46)
        Me.TextEdit18.Name = "TextEdit18"
        Me.TextEdit18.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit18.Properties.Appearance.Options.UseFont = True
        Me.TextEdit18.Size = New System.Drawing.Size(89, 26)
        Me.TextEdit18.StyleController = Me.LayoutControl3
        Me.TextEdit18.TabIndex = 0
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl3.StyleController = Me.LayoutControl3
        Me.LabelControl3.TabIndex = 47
        '
        'LayoutControlGroup11
        '
        Me.LayoutControlGroup11.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup11.GroupBordersVisible = False
        Me.LayoutControlGroup11.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.TabbedControlGroup4})
        Me.LayoutControlGroup11.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup11.Name = "Root"
        Me.LayoutControlGroup11.Size = New System.Drawing.Size(843, 357)
        Me.LayoutControlGroup11.Text = "Generales"
        Me.LayoutControlGroup11.TextVisible = False
        '
        'TabbedControlGroup4
        '
        Me.TabbedControlGroup4.Location = New System.Drawing.Point(0, 0)
        Me.TabbedControlGroup4.Name = "TabbedControlGroup1"
        Me.TabbedControlGroup4.SelectedTabPage = Me.LayoutControlGroup12
        Me.TabbedControlGroup4.SelectedTabPageIndex = 0
        Me.TabbedControlGroup4.Size = New System.Drawing.Size(823, 337)
        Me.TabbedControlGroup4.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup12, Me.LayoutControlGroup13})
        Me.TabbedControlGroup4.Text = "Generales"
        '
        'LayoutControlGroup12
        '
        Me.LayoutControlGroup12.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem17, Me.LayoutControlItem20, Me.LayoutControlItem21, Me.LayoutControlItem22, Me.LayoutControlItem23, Me.EmptySpaceItem16, Me.EmptySpaceItem17, Me.EmptySpaceItem18, Me.EmptySpaceItem19, Me.EmptySpaceItem20, Me.LayoutControlItem25})
        Me.LayoutControlGroup12.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup12.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup12.Size = New System.Drawing.Size(799, 291)
        Me.LayoutControlGroup12.Text = "Generales"
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem17.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem17.Control = Me.TextEdit17
        Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 30)
        Me.LayoutControlItem17.MaxSize = New System.Drawing.Size(718, 30)
        Me.LayoutControlItem17.MinSize = New System.Drawing.Size(718, 30)
        Me.LayoutControlItem17.Name = "TxtDesBancoitem"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(799, 30)
        Me.LayoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem17.Text = "Razon Social"
        Me.LayoutControlItem17.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem20.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem20.Control = Me.TextEdit18
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem20.MaxSize = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem20.MinSize = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem20.Name = "TxtBancoIDitem"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem20.Text = "ID"
        Me.LayoutControlItem20.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem21.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem21.Control = Me.TextEdit16
        Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 60)
        Me.LayoutControlItem21.MaxSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem21.MinSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem21.Name = "Txtsiglasitem"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem21.Text = "Ruc"
        Me.LayoutControlItem21.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem22.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem22.Control = Me.TextEdit15
        Me.LayoutControlItem22.Location = New System.Drawing.Point(0, 90)
        Me.LayoutControlItem22.MaxSize = New System.Drawing.Size(0, 30)
        Me.LayoutControlItem22.MinSize = New System.Drawing.Size(130, 30)
        Me.LayoutControlItem22.Name = "LayoutControlItem4"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(799, 30)
        Me.LayoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem22.Text = "Dirección"
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem23.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem23.Control = Me.TextEdit14
        Me.LayoutControlItem23.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem23.MaxSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem23.MinSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem23.Name = "LayoutControlItem5"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem23.Text = "Telefono"
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(73, 16)
        '
        'EmptySpaceItem16
        '
        Me.EmptySpaceItem16.AllowHotTrack = False
        Me.EmptySpaceItem16.Location = New System.Drawing.Point(0, 182)
        Me.EmptySpaceItem16.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem16.Size = New System.Drawing.Size(799, 109)
        Me.EmptySpaceItem16.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem17
        '
        Me.EmptySpaceItem17.AllowHotTrack = False
        Me.EmptySpaceItem17.Location = New System.Drawing.Point(0, 150)
        Me.EmptySpaceItem17.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem17.Size = New System.Drawing.Size(799, 32)
        Me.EmptySpaceItem17.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem18
        '
        Me.EmptySpaceItem18.AllowHotTrack = False
        Me.EmptySpaceItem18.Location = New System.Drawing.Point(358, 120)
        Me.EmptySpaceItem18.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem18.Size = New System.Drawing.Size(441, 30)
        Me.EmptySpaceItem18.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem19
        '
        Me.EmptySpaceItem19.AllowHotTrack = False
        Me.EmptySpaceItem19.Location = New System.Drawing.Point(358, 60)
        Me.EmptySpaceItem19.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem19.Size = New System.Drawing.Size(441, 30)
        Me.EmptySpaceItem19.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem20
        '
        Me.EmptySpaceItem20.AllowHotTrack = False
        Me.EmptySpaceItem20.Location = New System.Drawing.Point(260, 0)
        Me.EmptySpaceItem20.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem20.Size = New System.Drawing.Size(539, 30)
        Me.EmptySpaceItem20.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.CheckEdit3
        Me.LayoutControlItem25.Location = New System.Drawing.Point(169, 0)
        Me.LayoutControlItem25.Name = "LayoutControlItem7"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(91, 30)
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem25.TextVisible = False
        '
        'LayoutControlGroup13
        '
        Me.LayoutControlGroup13.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup13.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup13.Size = New System.Drawing.Size(799, 291)
        Me.LayoutControlGroup13.Text = "Contactos"
        '
        'LayoutControl4
        '
        Me.LayoutControl4.Controls.Add(Me.CheckEdit4)
        Me.LayoutControl4.Controls.Add(Me.TextEdit20)
        Me.LayoutControl4.Controls.Add(Me.TextEdit21)
        Me.LayoutControl4.Controls.Add(Me.TextEdit22)
        Me.LayoutControl4.Controls.Add(Me.TextEdit23)
        Me.LayoutControl4.Controls.Add(Me.TextEdit24)
        Me.LayoutControl4.Controls.Add(Me.LabelControl4)
        Me.LayoutControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl4.Location = New System.Drawing.Point(0, 40)
        Me.LayoutControl4.Name = "LayoutControl4"
        Me.LayoutControl4.Root = Me.LayoutControlGroup14
        Me.LayoutControl4.Size = New System.Drawing.Size(843, 357)
        Me.LayoutControl4.TabIndex = 52
        '
        'CheckEdit4
        '
        Me.CheckEdit4.EditValue = True
        Me.CheckEdit4.Location = New System.Drawing.Point(193, 46)
        Me.CheckEdit4.Name = "CheckEdit4"
        Me.CheckEdit4.Properties.Caption = "Activo"
        Me.CheckEdit4.Size = New System.Drawing.Size(87, 19)
        Me.CheckEdit4.StyleController = Me.LayoutControl4
        Me.CheckEdit4.TabIndex = 1
        '
        'TextEdit20
        '
        Me.TextEdit20.Location = New System.Drawing.Point(100, 166)
        Me.TextEdit20.Name = "TextEdit20"
        Me.TextEdit20.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit20.Properties.Appearance.Options.UseFont = True
        Me.TextEdit20.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit20.Properties.Mask.EditMask = "n0"
        Me.TextEdit20.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit20.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit20.Size = New System.Drawing.Size(278, 26)
        Me.TextEdit20.StyleController = Me.LayoutControl4
        Me.TextEdit20.TabIndex = 5
        '
        'TextEdit21
        '
        Me.TextEdit21.Location = New System.Drawing.Point(100, 136)
        Me.TextEdit21.Name = "TextEdit21"
        Me.TextEdit21.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit21.Properties.Appearance.Options.UseFont = True
        Me.TextEdit21.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit21.Properties.MaxLength = 200
        Me.TextEdit21.Size = New System.Drawing.Size(719, 26)
        Me.TextEdit21.StyleController = Me.LayoutControl4
        Me.TextEdit21.TabIndex = 4
        '
        'TextEdit22
        '
        Me.TextEdit22.Location = New System.Drawing.Point(100, 106)
        Me.TextEdit22.Name = "TextEdit22"
        Me.TextEdit22.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit22.Properties.Appearance.Options.UseFont = True
        Me.TextEdit22.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit22.Properties.MaxLength = 50
        Me.TextEdit22.Size = New System.Drawing.Size(278, 26)
        Me.TextEdit22.StyleController = Me.LayoutControl4
        Me.TextEdit22.TabIndex = 3
        '
        'TextEdit23
        '
        Me.TextEdit23.Location = New System.Drawing.Point(100, 76)
        Me.TextEdit23.Name = "TextEdit23"
        Me.TextEdit23.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit23.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextEdit23.Properties.Appearance.Options.UseFont = True
        Me.TextEdit23.Properties.Appearance.Options.UseForeColor = True
        Me.TextEdit23.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit23.Properties.MaxLength = 100
        Me.TextEdit23.Size = New System.Drawing.Size(638, 26)
        Me.TextEdit23.StyleController = Me.LayoutControl4
        Me.TextEdit23.TabIndex = 2
        '
        'TextEdit24
        '
        Me.TextEdit24.Enabled = False
        Me.TextEdit24.Location = New System.Drawing.Point(100, 46)
        Me.TextEdit24.Name = "TextEdit24"
        Me.TextEdit24.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit24.Properties.Appearance.Options.UseFont = True
        Me.TextEdit24.Size = New System.Drawing.Size(89, 26)
        Me.TextEdit24.StyleController = Me.LayoutControl4
        Me.TextEdit24.TabIndex = 0
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl4.StyleController = Me.LayoutControl4
        Me.LabelControl4.TabIndex = 47
        '
        'LayoutControlGroup14
        '
        Me.LayoutControlGroup14.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup14.GroupBordersVisible = False
        Me.LayoutControlGroup14.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.TabbedControlGroup5})
        Me.LayoutControlGroup14.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup14.Name = "Root"
        Me.LayoutControlGroup14.Size = New System.Drawing.Size(843, 357)
        Me.LayoutControlGroup14.Text = "Generales"
        Me.LayoutControlGroup14.TextVisible = False
        '
        'TabbedControlGroup5
        '
        Me.TabbedControlGroup5.Location = New System.Drawing.Point(0, 0)
        Me.TabbedControlGroup5.Name = "TabbedControlGroup1"
        Me.TabbedControlGroup5.SelectedTabPage = Me.LayoutControlGroup15
        Me.TabbedControlGroup5.SelectedTabPageIndex = 0
        Me.TabbedControlGroup5.Size = New System.Drawing.Size(823, 337)
        Me.TabbedControlGroup5.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup15, Me.LayoutControlGroup16})
        Me.TabbedControlGroup5.Text = "Generales"
        '
        'LayoutControlGroup15
        '
        Me.LayoutControlGroup15.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem24, Me.LayoutControlItem27, Me.LayoutControlItem28, Me.LayoutControlItem29, Me.LayoutControlItem30, Me.EmptySpaceItem21, Me.EmptySpaceItem22, Me.EmptySpaceItem23, Me.EmptySpaceItem24, Me.EmptySpaceItem25, Me.LayoutControlItem32})
        Me.LayoutControlGroup15.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup15.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup15.Size = New System.Drawing.Size(799, 291)
        Me.LayoutControlGroup15.Text = "Generales"
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem24.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem24.Control = Me.TextEdit23
        Me.LayoutControlItem24.Location = New System.Drawing.Point(0, 30)
        Me.LayoutControlItem24.MaxSize = New System.Drawing.Size(718, 30)
        Me.LayoutControlItem24.MinSize = New System.Drawing.Size(718, 30)
        Me.LayoutControlItem24.Name = "TxtDesBancoitem"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(799, 30)
        Me.LayoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem24.Text = "Razon Social"
        Me.LayoutControlItem24.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem27.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem27.Control = Me.TextEdit24
        Me.LayoutControlItem27.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem27.MaxSize = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem27.MinSize = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem27.Name = "TxtBancoIDitem"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem27.Text = "ID"
        Me.LayoutControlItem27.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem28.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem28.Control = Me.TextEdit22
        Me.LayoutControlItem28.Location = New System.Drawing.Point(0, 60)
        Me.LayoutControlItem28.MaxSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem28.MinSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem28.Name = "Txtsiglasitem"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem28.Text = "Ruc"
        Me.LayoutControlItem28.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem29.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem29.Control = Me.TextEdit21
        Me.LayoutControlItem29.Location = New System.Drawing.Point(0, 90)
        Me.LayoutControlItem29.MaxSize = New System.Drawing.Size(0, 30)
        Me.LayoutControlItem29.MinSize = New System.Drawing.Size(130, 30)
        Me.LayoutControlItem29.Name = "LayoutControlItem4"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(799, 30)
        Me.LayoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem29.Text = "Dirección"
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem30.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem30.Control = Me.TextEdit20
        Me.LayoutControlItem30.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem30.MaxSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem30.MinSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem30.Name = "LayoutControlItem5"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem30.Text = "Telefono"
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(73, 16)
        '
        'EmptySpaceItem21
        '
        Me.EmptySpaceItem21.AllowHotTrack = False
        Me.EmptySpaceItem21.Location = New System.Drawing.Point(0, 182)
        Me.EmptySpaceItem21.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem21.Size = New System.Drawing.Size(799, 109)
        Me.EmptySpaceItem21.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem22
        '
        Me.EmptySpaceItem22.AllowHotTrack = False
        Me.EmptySpaceItem22.Location = New System.Drawing.Point(0, 150)
        Me.EmptySpaceItem22.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem22.Size = New System.Drawing.Size(799, 32)
        Me.EmptySpaceItem22.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem23
        '
        Me.EmptySpaceItem23.AllowHotTrack = False
        Me.EmptySpaceItem23.Location = New System.Drawing.Point(358, 120)
        Me.EmptySpaceItem23.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem23.Size = New System.Drawing.Size(441, 30)
        Me.EmptySpaceItem23.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem24
        '
        Me.EmptySpaceItem24.AllowHotTrack = False
        Me.EmptySpaceItem24.Location = New System.Drawing.Point(358, 60)
        Me.EmptySpaceItem24.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem24.Size = New System.Drawing.Size(441, 30)
        Me.EmptySpaceItem24.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem25
        '
        Me.EmptySpaceItem25.AllowHotTrack = False
        Me.EmptySpaceItem25.Location = New System.Drawing.Point(260, 0)
        Me.EmptySpaceItem25.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem25.Size = New System.Drawing.Size(539, 30)
        Me.EmptySpaceItem25.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.Control = Me.CheckEdit4
        Me.LayoutControlItem32.Location = New System.Drawing.Point(169, 0)
        Me.LayoutControlItem32.Name = "LayoutControlItem7"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(91, 30)
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem32.TextVisible = False
        '
        'LayoutControlGroup16
        '
        Me.LayoutControlGroup16.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup16.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup16.Size = New System.Drawing.Size(799, 291)
        Me.LayoutControlGroup16.Text = "Contactos"
        '
        'LayoutControl5
        '
        Me.LayoutControl5.Controls.Add(Me.CheckEdit5)
        Me.LayoutControl5.Controls.Add(Me.TextEdit26)
        Me.LayoutControl5.Controls.Add(Me.TextEdit27)
        Me.LayoutControl5.Controls.Add(Me.TextEdit28)
        Me.LayoutControl5.Controls.Add(Me.TextEdit29)
        Me.LayoutControl5.Controls.Add(Me.TextEdit30)
        Me.LayoutControl5.Controls.Add(Me.LabelControl5)
        Me.LayoutControl5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl5.Location = New System.Drawing.Point(0, 40)
        Me.LayoutControl5.Name = "LayoutControl5"
        Me.LayoutControl5.Root = Me.LayoutControlGroup17
        Me.LayoutControl5.Size = New System.Drawing.Size(843, 357)
        Me.LayoutControl5.TabIndex = 52
        '
        'CheckEdit5
        '
        Me.CheckEdit5.EditValue = True
        Me.CheckEdit5.Location = New System.Drawing.Point(193, 46)
        Me.CheckEdit5.Name = "CheckEdit5"
        Me.CheckEdit5.Properties.Caption = "Activo"
        Me.CheckEdit5.Size = New System.Drawing.Size(87, 19)
        Me.CheckEdit5.StyleController = Me.LayoutControl5
        Me.CheckEdit5.TabIndex = 1
        '
        'TextEdit26
        '
        Me.TextEdit26.Location = New System.Drawing.Point(100, 166)
        Me.TextEdit26.Name = "TextEdit26"
        Me.TextEdit26.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit26.Properties.Appearance.Options.UseFont = True
        Me.TextEdit26.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit26.Properties.Mask.EditMask = "n0"
        Me.TextEdit26.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit26.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit26.Size = New System.Drawing.Size(278, 26)
        Me.TextEdit26.StyleController = Me.LayoutControl5
        Me.TextEdit26.TabIndex = 5
        '
        'TextEdit27
        '
        Me.TextEdit27.Location = New System.Drawing.Point(100, 136)
        Me.TextEdit27.Name = "TextEdit27"
        Me.TextEdit27.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit27.Properties.Appearance.Options.UseFont = True
        Me.TextEdit27.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit27.Properties.MaxLength = 200
        Me.TextEdit27.Size = New System.Drawing.Size(719, 26)
        Me.TextEdit27.StyleController = Me.LayoutControl5
        Me.TextEdit27.TabIndex = 4
        '
        'TextEdit28
        '
        Me.TextEdit28.Location = New System.Drawing.Point(100, 106)
        Me.TextEdit28.Name = "TextEdit28"
        Me.TextEdit28.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit28.Properties.Appearance.Options.UseFont = True
        Me.TextEdit28.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit28.Properties.MaxLength = 50
        Me.TextEdit28.Size = New System.Drawing.Size(278, 26)
        Me.TextEdit28.StyleController = Me.LayoutControl5
        Me.TextEdit28.TabIndex = 3
        '
        'TextEdit29
        '
        Me.TextEdit29.Location = New System.Drawing.Point(100, 76)
        Me.TextEdit29.Name = "TextEdit29"
        Me.TextEdit29.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit29.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextEdit29.Properties.Appearance.Options.UseFont = True
        Me.TextEdit29.Properties.Appearance.Options.UseForeColor = True
        Me.TextEdit29.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit29.Properties.MaxLength = 100
        Me.TextEdit29.Size = New System.Drawing.Size(638, 26)
        Me.TextEdit29.StyleController = Me.LayoutControl5
        Me.TextEdit29.TabIndex = 2
        '
        'TextEdit30
        '
        Me.TextEdit30.Enabled = False
        Me.TextEdit30.Location = New System.Drawing.Point(100, 46)
        Me.TextEdit30.Name = "TextEdit30"
        Me.TextEdit30.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit30.Properties.Appearance.Options.UseFont = True
        Me.TextEdit30.Size = New System.Drawing.Size(89, 26)
        Me.TextEdit30.StyleController = Me.LayoutControl5
        Me.TextEdit30.TabIndex = 0
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl5.StyleController = Me.LayoutControl5
        Me.LabelControl5.TabIndex = 47
        '
        'LayoutControlGroup17
        '
        Me.LayoutControlGroup17.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup17.GroupBordersVisible = False
        Me.LayoutControlGroup17.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.TabbedControlGroup6})
        Me.LayoutControlGroup17.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup17.Name = "Root"
        Me.LayoutControlGroup17.Size = New System.Drawing.Size(843, 357)
        Me.LayoutControlGroup17.Text = "Generales"
        Me.LayoutControlGroup17.TextVisible = False
        '
        'TabbedControlGroup6
        '
        Me.TabbedControlGroup6.Location = New System.Drawing.Point(0, 0)
        Me.TabbedControlGroup6.Name = "TabbedControlGroup1"
        Me.TabbedControlGroup6.SelectedTabPage = Me.LayoutControlGroup18
        Me.TabbedControlGroup6.SelectedTabPageIndex = 0
        Me.TabbedControlGroup6.Size = New System.Drawing.Size(823, 337)
        Me.TabbedControlGroup6.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup18, Me.LayoutControlGroup19})
        Me.TabbedControlGroup6.Text = "Generales"
        '
        'LayoutControlGroup18
        '
        Me.LayoutControlGroup18.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem31, Me.LayoutControlItem34, Me.LayoutControlItem35, Me.LayoutControlItem36, Me.LayoutControlItem37, Me.EmptySpaceItem26, Me.EmptySpaceItem27, Me.EmptySpaceItem28, Me.EmptySpaceItem29, Me.EmptySpaceItem30, Me.LayoutControlItem39})
        Me.LayoutControlGroup18.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup18.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup18.Size = New System.Drawing.Size(799, 291)
        Me.LayoutControlGroup18.Text = "Generales"
        '
        'LayoutControlItem31
        '
        Me.LayoutControlItem31.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem31.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem31.Control = Me.TextEdit29
        Me.LayoutControlItem31.Location = New System.Drawing.Point(0, 30)
        Me.LayoutControlItem31.MaxSize = New System.Drawing.Size(718, 30)
        Me.LayoutControlItem31.MinSize = New System.Drawing.Size(718, 30)
        Me.LayoutControlItem31.Name = "TxtDesBancoitem"
        Me.LayoutControlItem31.Size = New System.Drawing.Size(799, 30)
        Me.LayoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem31.Text = "Razon Social"
        Me.LayoutControlItem31.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem31.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem34
        '
        Me.LayoutControlItem34.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem34.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem34.Control = Me.TextEdit30
        Me.LayoutControlItem34.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem34.MaxSize = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem34.MinSize = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem34.Name = "TxtBancoIDitem"
        Me.LayoutControlItem34.Size = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem34.Text = "ID"
        Me.LayoutControlItem34.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem34.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem35
        '
        Me.LayoutControlItem35.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem35.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem35.Control = Me.TextEdit28
        Me.LayoutControlItem35.Location = New System.Drawing.Point(0, 60)
        Me.LayoutControlItem35.MaxSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem35.MinSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem35.Name = "Txtsiglasitem"
        Me.LayoutControlItem35.Size = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem35.Text = "Ruc"
        Me.LayoutControlItem35.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem35.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem36
        '
        Me.LayoutControlItem36.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem36.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem36.Control = Me.TextEdit27
        Me.LayoutControlItem36.Location = New System.Drawing.Point(0, 90)
        Me.LayoutControlItem36.MaxSize = New System.Drawing.Size(0, 30)
        Me.LayoutControlItem36.MinSize = New System.Drawing.Size(130, 30)
        Me.LayoutControlItem36.Name = "LayoutControlItem4"
        Me.LayoutControlItem36.Size = New System.Drawing.Size(799, 30)
        Me.LayoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem36.Text = "Dirección"
        Me.LayoutControlItem36.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem37
        '
        Me.LayoutControlItem37.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem37.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem37.Control = Me.TextEdit26
        Me.LayoutControlItem37.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem37.MaxSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem37.MinSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem37.Name = "LayoutControlItem5"
        Me.LayoutControlItem37.Size = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem37.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem37.Text = "Telefono"
        Me.LayoutControlItem37.TextSize = New System.Drawing.Size(73, 16)
        '
        'EmptySpaceItem26
        '
        Me.EmptySpaceItem26.AllowHotTrack = False
        Me.EmptySpaceItem26.Location = New System.Drawing.Point(0, 182)
        Me.EmptySpaceItem26.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem26.Size = New System.Drawing.Size(799, 109)
        Me.EmptySpaceItem26.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem27
        '
        Me.EmptySpaceItem27.AllowHotTrack = False
        Me.EmptySpaceItem27.Location = New System.Drawing.Point(0, 150)
        Me.EmptySpaceItem27.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem27.Size = New System.Drawing.Size(799, 32)
        Me.EmptySpaceItem27.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem28
        '
        Me.EmptySpaceItem28.AllowHotTrack = False
        Me.EmptySpaceItem28.Location = New System.Drawing.Point(358, 120)
        Me.EmptySpaceItem28.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem28.Size = New System.Drawing.Size(441, 30)
        Me.EmptySpaceItem28.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem29
        '
        Me.EmptySpaceItem29.AllowHotTrack = False
        Me.EmptySpaceItem29.Location = New System.Drawing.Point(358, 60)
        Me.EmptySpaceItem29.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem29.Size = New System.Drawing.Size(441, 30)
        Me.EmptySpaceItem29.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem30
        '
        Me.EmptySpaceItem30.AllowHotTrack = False
        Me.EmptySpaceItem30.Location = New System.Drawing.Point(260, 0)
        Me.EmptySpaceItem30.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem30.Size = New System.Drawing.Size(539, 30)
        Me.EmptySpaceItem30.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem39
        '
        Me.LayoutControlItem39.Control = Me.CheckEdit5
        Me.LayoutControlItem39.Location = New System.Drawing.Point(169, 0)
        Me.LayoutControlItem39.Name = "LayoutControlItem7"
        Me.LayoutControlItem39.Size = New System.Drawing.Size(91, 30)
        Me.LayoutControlItem39.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem39.TextVisible = False
        '
        'LayoutControlGroup19
        '
        Me.LayoutControlGroup19.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup19.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup19.Size = New System.Drawing.Size(799, 291)
        Me.LayoutControlGroup19.Text = "Contactos"
        '
        'LayoutControl6
        '
        Me.LayoutControl6.Controls.Add(Me.CheckEdit6)
        Me.LayoutControl6.Controls.Add(Me.TextEdit32)
        Me.LayoutControl6.Controls.Add(Me.TextEdit33)
        Me.LayoutControl6.Controls.Add(Me.TextEdit34)
        Me.LayoutControl6.Controls.Add(Me.TextEdit35)
        Me.LayoutControl6.Controls.Add(Me.TextEdit36)
        Me.LayoutControl6.Controls.Add(Me.LabelControl6)
        Me.LayoutControl6.Controls.Add(Me.TextEdit37)
        Me.LayoutControl6.Controls.Add(Me.TextEdit38)
        Me.LayoutControl6.Controls.Add(Me.TextEdit39)
        Me.LayoutControl6.Controls.Add(Me.TextEdit40)
        Me.LayoutControl6.Controls.Add(Me.TextEdit41)
        Me.LayoutControl6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl6.Location = New System.Drawing.Point(0, 40)
        Me.LayoutControl6.Name = "LayoutControl6"
        Me.LayoutControl6.Root = Me.LayoutControlGroup20
        Me.LayoutControl6.Size = New System.Drawing.Size(843, 402)
        Me.LayoutControl6.TabIndex = 52
        '
        'CheckEdit6
        '
        Me.CheckEdit6.EditValue = True
        Me.CheckEdit6.Location = New System.Drawing.Point(193, 46)
        Me.CheckEdit6.Name = "CheckEdit6"
        Me.CheckEdit6.Properties.Caption = "Activo"
        Me.CheckEdit6.Size = New System.Drawing.Size(71, 19)
        Me.CheckEdit6.StyleController = Me.LayoutControl6
        Me.CheckEdit6.TabIndex = 1
        '
        'TextEdit32
        '
        Me.TextEdit32.EditValue = "22334375"
        Me.TextEdit32.Location = New System.Drawing.Point(100, 170)
        Me.TextEdit32.Name = "TextEdit32"
        Me.TextEdit32.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit32.Properties.Appearance.Options.UseFont = True
        Me.TextEdit32.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit32.Properties.Mask.EditMask = "0000-0000"
        Me.TextEdit32.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.TextEdit32.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit32.Size = New System.Drawing.Size(106, 26)
        Me.TextEdit32.StyleController = Me.LayoutControl6
        Me.TextEdit32.TabIndex = 5
        '
        'TextEdit33
        '
        Me.TextEdit33.Location = New System.Drawing.Point(100, 140)
        Me.TextEdit33.Name = "TextEdit33"
        Me.TextEdit33.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit33.Properties.Appearance.Options.UseFont = True
        Me.TextEdit33.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit33.Properties.MaxLength = 200
        Me.TextEdit33.Size = New System.Drawing.Size(719, 26)
        Me.TextEdit33.StyleController = Me.LayoutControl6
        Me.TextEdit33.TabIndex = 4
        '
        'TextEdit34
        '
        Me.TextEdit34.Location = New System.Drawing.Point(100, 110)
        Me.TextEdit34.Name = "TextEdit34"
        Me.TextEdit34.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit34.Properties.Appearance.Options.UseFont = True
        Me.TextEdit34.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit34.Properties.MaxLength = 50
        Me.TextEdit34.Size = New System.Drawing.Size(278, 26)
        Me.TextEdit34.StyleController = Me.LayoutControl6
        Me.TextEdit34.TabIndex = 3
        '
        'TextEdit35
        '
        Me.TextEdit35.Location = New System.Drawing.Point(100, 76)
        Me.TextEdit35.Name = "TextEdit35"
        Me.TextEdit35.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit35.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextEdit35.Properties.Appearance.Options.UseFont = True
        Me.TextEdit35.Properties.Appearance.Options.UseForeColor = True
        Me.TextEdit35.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit35.Properties.MaxLength = 100
        Me.TextEdit35.Size = New System.Drawing.Size(719, 26)
        Me.TextEdit35.StyleController = Me.LayoutControl6
        Me.TextEdit35.TabIndex = 2
        '
        'TextEdit36
        '
        Me.TextEdit36.Enabled = False
        Me.TextEdit36.Location = New System.Drawing.Point(100, 46)
        Me.TextEdit36.Name = "TextEdit36"
        Me.TextEdit36.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit36.Properties.Appearance.Options.UseFont = True
        Me.TextEdit36.Size = New System.Drawing.Size(89, 26)
        Me.TextEdit36.StyleController = Me.LayoutControl6
        Me.TextEdit36.TabIndex = 0
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl6.StyleController = Me.LayoutControl6
        Me.LabelControl6.TabIndex = 47
        '
        'TextEdit37
        '
        Me.TextEdit37.Location = New System.Drawing.Point(100, 208)
        Me.TextEdit37.Name = "TextEdit37"
        Me.TextEdit37.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit37.Properties.Appearance.Options.UseFont = True
        Me.TextEdit37.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit37.Size = New System.Drawing.Size(106, 26)
        Me.TextEdit37.StyleController = Me.LayoutControl6
        Me.TextEdit37.TabIndex = 6
        '
        'TextEdit38
        '
        Me.TextEdit38.EditValue = "2233"
        Me.TextEdit38.Location = New System.Drawing.Point(286, 170)
        Me.TextEdit38.Name = "TextEdit38"
        Me.TextEdit38.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit38.Properties.Appearance.Options.UseFont = True
        Me.TextEdit38.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit38.Size = New System.Drawing.Size(76, 26)
        Me.TextEdit38.StyleController = Me.LayoutControl6
        Me.TextEdit38.TabIndex = 6
        '
        'TextEdit39
        '
        Me.TextEdit39.Location = New System.Drawing.Point(286, 208)
        Me.TextEdit39.Name = "TextEdit39"
        Me.TextEdit39.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit39.Properties.Appearance.Options.UseFont = True
        Me.TextEdit39.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit39.Size = New System.Drawing.Size(76, 26)
        Me.TextEdit39.StyleController = Me.LayoutControl6
        Me.TextEdit39.TabIndex = 6
        '
        'TextEdit40
        '
        Me.TextEdit40.Location = New System.Drawing.Point(442, 170)
        Me.TextEdit40.MenuManager = Me.BarManager11
        Me.TextEdit40.Name = "TextEdit40"
        Me.TextEdit40.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit40.Properties.Appearance.Options.UseFont = True
        Me.TextEdit40.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit40.Size = New System.Drawing.Size(76, 26)
        Me.TextEdit40.StyleController = Me.LayoutControl6
        Me.TextEdit40.TabIndex = 6
        '
        'TextEdit41
        '
        Me.TextEdit41.Location = New System.Drawing.Point(442, 208)
        Me.TextEdit41.Name = "TextEdit41"
        Me.TextEdit41.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit41.Properties.Appearance.Options.UseFont = True
        Me.TextEdit41.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit41.Size = New System.Drawing.Size(76, 26)
        Me.TextEdit41.StyleController = Me.LayoutControl6
        Me.TextEdit41.TabIndex = 6
        '
        'LayoutControlGroup20
        '
        Me.LayoutControlGroup20.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup20.GroupBordersVisible = False
        Me.LayoutControlGroup20.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.TabbedControlGroup7})
        Me.LayoutControlGroup20.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup20.Name = "Root"
        Me.LayoutControlGroup20.Size = New System.Drawing.Size(843, 402)
        Me.LayoutControlGroup20.Text = "Generales"
        Me.LayoutControlGroup20.TextVisible = False
        '
        'TabbedControlGroup7
        '
        Me.TabbedControlGroup7.Location = New System.Drawing.Point(0, 0)
        Me.TabbedControlGroup7.Name = "TabbedControlGroup1"
        Me.TabbedControlGroup7.SelectedTabPage = Me.LayoutControlGroup21
        Me.TabbedControlGroup7.SelectedTabPageIndex = 0
        Me.TabbedControlGroup7.Size = New System.Drawing.Size(823, 382)
        Me.TabbedControlGroup7.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup21, Me.LayoutControlGroup22})
        Me.TabbedControlGroup7.Text = "Generales"
        '
        'LayoutControlGroup21
        '
        Me.LayoutControlGroup21.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem38, Me.LayoutControlItem41, Me.LayoutControlItem42, Me.LayoutControlItem43, Me.LayoutControlItem44, Me.EmptySpaceItem3, Me.EmptySpaceItem31, Me.EmptySpaceItem32, Me.LayoutControlItem46, Me.LayoutControlItem47, Me.LayoutControlItem48, Me.LayoutControlItem49, Me.LayoutControlItem50, Me.LayoutControlItem51, Me.EmptySpaceItem33})
        Me.LayoutControlGroup21.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup21.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup21.Size = New System.Drawing.Size(799, 336)
        Me.LayoutControlGroup21.Text = "Generales"
        '
        'LayoutControlItem38
        '
        Me.LayoutControlItem38.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem38.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem38.Control = Me.TextEdit35
        Me.LayoutControlItem38.Location = New System.Drawing.Point(0, 30)
        Me.LayoutControlItem38.MinSize = New System.Drawing.Size(130, 30)
        Me.LayoutControlItem38.Name = "TxtDesBancoitem"
        Me.LayoutControlItem38.Size = New System.Drawing.Size(799, 34)
        Me.LayoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem38.Text = "Razon Social"
        Me.LayoutControlItem38.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem38.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem41
        '
        Me.LayoutControlItem41.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem41.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem41.Control = Me.TextEdit36
        Me.LayoutControlItem41.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem41.MaxSize = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem41.MinSize = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem41.Name = "TxtBancoIDitem"
        Me.LayoutControlItem41.Size = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem41.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem41.Text = "ID"
        Me.LayoutControlItem41.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem41.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem42
        '
        Me.LayoutControlItem42.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem42.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem42.Control = Me.TextEdit34
        Me.LayoutControlItem42.Location = New System.Drawing.Point(0, 64)
        Me.LayoutControlItem42.MaxSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem42.MinSize = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem42.Name = "Txtsiglasitem"
        Me.LayoutControlItem42.Size = New System.Drawing.Size(358, 30)
        Me.LayoutControlItem42.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem42.Text = "Ruc"
        Me.LayoutControlItem42.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem42.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem43
        '
        Me.LayoutControlItem43.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem43.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem43.Control = Me.TextEdit33
        Me.LayoutControlItem43.Location = New System.Drawing.Point(0, 94)
        Me.LayoutControlItem43.MaxSize = New System.Drawing.Size(0, 30)
        Me.LayoutControlItem43.MinSize = New System.Drawing.Size(130, 30)
        Me.LayoutControlItem43.Name = "LayoutControlItem4"
        Me.LayoutControlItem43.Size = New System.Drawing.Size(799, 30)
        Me.LayoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem43.Text = "Dirección"
        Me.LayoutControlItem43.TextSize = New System.Drawing.Size(73, 16)
        '
        'LayoutControlItem44
        '
        Me.LayoutControlItem44.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem44.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem44.Control = Me.TextEdit32
        Me.LayoutControlItem44.Location = New System.Drawing.Point(0, 124)
        Me.LayoutControlItem44.MinSize = New System.Drawing.Size(130, 30)
        Me.LayoutControlItem44.Name = "LayoutControlItem5"
        Me.LayoutControlItem44.Size = New System.Drawing.Size(186, 38)
        Me.LayoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem44.Text = "Telefono"
        Me.LayoutControlItem44.TextSize = New System.Drawing.Size(73, 16)
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = False
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 210)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(799, 126)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem31
        '
        Me.EmptySpaceItem31.AllowHotTrack = False
        Me.EmptySpaceItem31.Location = New System.Drawing.Point(358, 64)
        Me.EmptySpaceItem31.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem31.Size = New System.Drawing.Size(441, 30)
        Me.EmptySpaceItem31.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem32
        '
        Me.EmptySpaceItem32.AllowHotTrack = False
        Me.EmptySpaceItem32.Location = New System.Drawing.Point(244, 0)
        Me.EmptySpaceItem32.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem32.Size = New System.Drawing.Size(555, 30)
        Me.EmptySpaceItem32.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem46
        '
        Me.LayoutControlItem46.Control = Me.CheckEdit6
        Me.LayoutControlItem46.Location = New System.Drawing.Point(169, 0)
        Me.LayoutControlItem46.Name = "LayoutControlItem7"
        Me.LayoutControlItem46.Size = New System.Drawing.Size(75, 30)
        Me.LayoutControlItem46.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem46.TextVisible = False
        '
        'LayoutControlItem47
        '
        Me.LayoutControlItem47.Control = Me.TextEdit37
        Me.LayoutControlItem47.CustomizationFormText = "Cuenta"
        Me.LayoutControlItem47.Location = New System.Drawing.Point(0, 162)
        Me.LayoutControlItem47.MinSize = New System.Drawing.Size(130, 30)
        Me.LayoutControlItem47.Name = "LayoutControlItem12"
        Me.LayoutControlItem47.Size = New System.Drawing.Size(186, 48)
        Me.LayoutControlItem47.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem47.Text = "Telefono"
        Me.LayoutControlItem47.TextSize = New System.Drawing.Size(73, 13)
        '
        'LayoutControlItem48
        '
        Me.LayoutControlItem48.Control = Me.TextEdit38
        Me.LayoutControlItem48.CustomizationFormText = "Cuenta"
        Me.LayoutControlItem48.Location = New System.Drawing.Point(186, 124)
        Me.LayoutControlItem48.MinSize = New System.Drawing.Size(130, 30)
        Me.LayoutControlItem48.Name = "LayoutControlItem19"
        Me.LayoutControlItem48.Size = New System.Drawing.Size(156, 38)
        Me.LayoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem48.Text = "Extensión 1 "
        Me.LayoutControlItem48.TextSize = New System.Drawing.Size(73, 13)
        '
        'LayoutControlItem49
        '
        Me.LayoutControlItem49.Control = Me.TextEdit39
        Me.LayoutControlItem49.CustomizationFormText = "Cuenta"
        Me.LayoutControlItem49.Location = New System.Drawing.Point(186, 162)
        Me.LayoutControlItem49.MinSize = New System.Drawing.Size(130, 30)
        Me.LayoutControlItem49.Name = "LayoutControlItem26"
        Me.LayoutControlItem49.Size = New System.Drawing.Size(156, 48)
        Me.LayoutControlItem49.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem49.Text = "Cuenta"
        Me.LayoutControlItem49.TextSize = New System.Drawing.Size(73, 13)
        '
        'LayoutControlItem50
        '
        Me.LayoutControlItem50.Control = Me.TextEdit40
        Me.LayoutControlItem50.CustomizationFormText = "Cuenta"
        Me.LayoutControlItem50.Location = New System.Drawing.Point(342, 124)
        Me.LayoutControlItem50.MinSize = New System.Drawing.Size(130, 30)
        Me.LayoutControlItem50.Name = "LayoutControlItem33"
        Me.LayoutControlItem50.Size = New System.Drawing.Size(156, 38)
        Me.LayoutControlItem50.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem50.Text = "Extension 2"
        Me.LayoutControlItem50.TextSize = New System.Drawing.Size(73, 13)
        '
        'LayoutControlItem51
        '
        Me.LayoutControlItem51.Control = Me.TextEdit41
        Me.LayoutControlItem51.CustomizationFormText = "Cuenta"
        Me.LayoutControlItem51.Location = New System.Drawing.Point(342, 162)
        Me.LayoutControlItem51.MinSize = New System.Drawing.Size(130, 30)
        Me.LayoutControlItem51.Name = "LayoutControlItem40"
        Me.LayoutControlItem51.Size = New System.Drawing.Size(156, 48)
        Me.LayoutControlItem51.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem51.Text = "Cuenta"
        Me.LayoutControlItem51.TextSize = New System.Drawing.Size(73, 13)
        '
        'EmptySpaceItem33
        '
        Me.EmptySpaceItem33.AllowHotTrack = False
        Me.EmptySpaceItem33.Location = New System.Drawing.Point(498, 124)
        Me.EmptySpaceItem33.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem33.Size = New System.Drawing.Size(301, 86)
        Me.EmptySpaceItem33.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlGroup22
        '
        Me.LayoutControlGroup22.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup22.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup22.Size = New System.Drawing.Size(799, 336)
        Me.LayoutControlGroup22.Text = "Contactos"
        '
        'LayoutControl7
        '
        Me.LayoutControl7.Controls.Add(Me.CheckEdit7)
        Me.LayoutControl7.Controls.Add(Me.TextEdit7)
        Me.LayoutControl7.Controls.Add(Me.TextEdit13)
        Me.LayoutControl7.Controls.Add(Me.TextEdit19)
        Me.LayoutControl7.Controls.Add(Me.TextEdit25)
        Me.LayoutControl7.Controls.Add(Me.TextEdit31)
        Me.LayoutControl7.Controls.Add(Me.TextEdit42)
        Me.LayoutControl7.Controls.Add(Me.TextEdit43)
        Me.LayoutControl7.Controls.Add(Me.TextEdit44)
        Me.LayoutControl7.Controls.Add(Me.TextEdit45)
        Me.LayoutControl7.Controls.Add(Me.TextEdit46)
        Me.LayoutControl7.Controls.Add(Me.TextEdit47)
        Me.LayoutControl7.Controls.Add(Me.TextEdit48)
        Me.LayoutControl7.Controls.Add(Me.TextEdit49)
        Me.LayoutControl7.Controls.Add(Me.TextEdit50)
        Me.LayoutControl7.Controls.Add(Me.TextEdit51)
        Me.LayoutControl7.Controls.Add(Me.CheckEdit8)
        Me.LayoutControl7.Controls.Add(Me.TextEdit52)
        Me.LayoutControl7.Controls.Add(Me.TextEdit53)
        Me.LayoutControl7.Controls.Add(Me.TextEdit54)
        Me.LayoutControl7.Controls.Add(Me.TextEdit55)
        Me.LayoutControl7.Controls.Add(Me.TextEdit56)
        Me.LayoutControl7.Controls.Add(Me.LabelControl7)
        Me.LayoutControl7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl7.Location = New System.Drawing.Point(0, 40)
        Me.LayoutControl7.Name = "LayoutControl7"
        Me.LayoutControl7.Root = Me.LayoutControlGroup23
        Me.LayoutControl7.Size = New System.Drawing.Size(1127, 375)
        Me.LayoutControl7.TabIndex = 52
        '
        'CheckEdit7
        '
        Me.CheckEdit7.Location = New System.Drawing.Point(24, 289)
        Me.CheckEdit7.MenuManager = Me.BarManager1
        Me.CheckEdit7.Name = "CheckEdit7"
        Me.CheckEdit7.Properties.Caption = "Comprobante"
        Me.CheckEdit7.Size = New System.Drawing.Size(282, 19)
        Me.CheckEdit7.StyleController = Me.LayoutControl7
        Me.CheckEdit7.TabIndex = 66
        '
        'TextEdit7
        '
        Me.TextEdit7.Location = New System.Drawing.Point(121, 49)
        Me.TextEdit7.MenuManager = Me.BarManager1
        Me.TextEdit7.Name = "TextEdit7"
        Me.TextEdit7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit7.Properties.Appearance.Options.UseFont = True
        Me.TextEdit7.Size = New System.Drawing.Size(122, 26)
        Me.TextEdit7.StyleController = Me.LayoutControl7
        Me.TextEdit7.TabIndex = 65
        Me.TextEdit7.Visible = False
        '
        'TextEdit13
        '
        Me.TextEdit13.Location = New System.Drawing.Point(121, 79)
        Me.TextEdit13.MenuManager = Me.BarManager1
        Me.TextEdit13.Name = "TextEdit13"
        Me.TextEdit13.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit13.Properties.Appearance.Options.UseFont = True
        Me.TextEdit13.Size = New System.Drawing.Size(122, 26)
        Me.TextEdit13.StyleController = Me.LayoutControl7
        Me.TextEdit13.TabIndex = 64
        Me.TextEdit13.Visible = False
        '
        'TextEdit19
        '
        Me.TextEdit19.Location = New System.Drawing.Point(743, 79)
        Me.TextEdit19.MenuManager = Me.BarManager1
        Me.TextEdit19.Name = "TextEdit19"
        Me.TextEdit19.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit19.Properties.Appearance.Options.UseFont = True
        Me.TextEdit19.Size = New System.Drawing.Size(267, 26)
        Me.TextEdit19.StyleController = Me.LayoutControl7
        Me.TextEdit19.TabIndex = 62
        Me.TextEdit19.Visible = False
        '
        'TextEdit25
        '
        Me.TextEdit25.Location = New System.Drawing.Point(344, 79)
        Me.TextEdit25.MenuManager = Me.BarManager1
        Me.TextEdit25.Name = "TextEdit25"
        Me.TextEdit25.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit25.Properties.Appearance.Options.UseFont = True
        Me.TextEdit25.Size = New System.Drawing.Size(298, 26)
        Me.TextEdit25.StyleController = Me.LayoutControl7
        Me.TextEdit25.TabIndex = 61
        Me.TextEdit25.Visible = False
        '
        'TextEdit31
        '
        Me.TextEdit31.Location = New System.Drawing.Point(344, 49)
        Me.TextEdit31.MenuManager = Me.BarManager1
        Me.TextEdit31.Name = "TextEdit31"
        Me.TextEdit31.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit31.Properties.Appearance.Options.UseFont = True
        Me.TextEdit31.Size = New System.Drawing.Size(298, 26)
        Me.TextEdit31.StyleController = Me.LayoutControl7
        Me.TextEdit31.TabIndex = 60
        Me.TextEdit31.Visible = False
        '
        'TextEdit42
        '
        Me.TextEdit42.Location = New System.Drawing.Point(743, 49)
        Me.TextEdit42.MenuManager = Me.BarManager1
        Me.TextEdit42.Name = "TextEdit42"
        Me.TextEdit42.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit42.Properties.Appearance.Options.UseFont = True
        Me.TextEdit42.Size = New System.Drawing.Size(267, 26)
        Me.TextEdit42.StyleController = Me.LayoutControl7
        Me.TextEdit42.TabIndex = 59
        Me.TextEdit42.Visible = False
        '
        'TextEdit43
        '
        Me.TextEdit43.Location = New System.Drawing.Point(121, 109)
        Me.TextEdit43.MenuManager = Me.BarManager1
        Me.TextEdit43.Name = "TextEdit43"
        Me.TextEdit43.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit43.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextEdit43.Properties.Appearance.Options.UseFont = True
        Me.TextEdit43.Properties.Appearance.Options.UseForeColor = True
        Me.TextEdit43.Properties.MaxLength = 20
        Me.TextEdit43.Size = New System.Drawing.Size(265, 26)
        Me.TextEdit43.StyleController = Me.LayoutControl7
        Me.TextEdit43.TabIndex = 56
        '
        'TextEdit44
        '
        Me.TextEdit44.Location = New System.Drawing.Point(121, 259)
        Me.TextEdit44.Name = "TextEdit44"
        Me.TextEdit44.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit44.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.TextEdit44.Properties.Appearance.Options.UseFont = True
        Me.TextEdit44.Properties.Appearance.Options.UseForeColor = True
        Me.TextEdit44.Size = New System.Drawing.Size(268, 26)
        Me.TextEdit44.StyleController = Me.LayoutControl7
        Me.TextEdit44.TabIndex = 55
        '
        'TextEdit45
        '
        Me.TextEdit45.Location = New System.Drawing.Point(313, 229)
        Me.TextEdit45.Name = "TextEdit45"
        Me.TextEdit45.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit45.Properties.Appearance.Options.UseFont = True
        Me.TextEdit45.Properties.MaxLength = 50
        Me.TextEdit45.Size = New System.Drawing.Size(350, 26)
        Me.TextEdit45.StyleController = Me.LayoutControl7
        Me.TextEdit45.TabIndex = 54
        '
        'TextEdit46
        '
        Me.TextEdit46.EditValue = ""
        Me.TextEdit46.Location = New System.Drawing.Point(121, 229)
        Me.TextEdit46.Name = "TextEdit46"
        Me.TextEdit46.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit46.Properties.Appearance.Options.UseFont = True
        Me.TextEdit46.Size = New System.Drawing.Size(91, 26)
        Me.TextEdit46.StyleController = Me.LayoutControl7
        Me.TextEdit46.TabIndex = 53
        '
        'TextEdit47
        '
        Me.TextEdit47.Location = New System.Drawing.Point(490, 199)
        Me.TextEdit47.Name = "TextEdit47"
        Me.TextEdit47.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit47.Properties.Appearance.Options.UseFont = True
        Me.TextEdit47.Properties.MaxLength = 5
        Me.TextEdit47.Size = New System.Drawing.Size(76, 26)
        Me.TextEdit47.StyleController = Me.LayoutControl7
        Me.TextEdit47.TabIndex = 52
        '
        'TextEdit48
        '
        Me.TextEdit48.Location = New System.Drawing.Point(313, 199)
        Me.TextEdit48.Name = "TextEdit48"
        Me.TextEdit48.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit48.Properties.Appearance.Options.UseFont = True
        Me.TextEdit48.Properties.MaxLength = 5
        Me.TextEdit48.Size = New System.Drawing.Size(76, 26)
        Me.TextEdit48.StyleController = Me.LayoutControl7
        Me.TextEdit48.TabIndex = 51
        '
        'TextEdit49
        '
        Me.TextEdit49.EditValue = ""
        Me.TextEdit49.Location = New System.Drawing.Point(121, 199)
        Me.TextEdit49.Name = "TextEdit49"
        Me.TextEdit49.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit49.Properties.Appearance.Options.UseFont = True
        Me.TextEdit49.Size = New System.Drawing.Size(91, 26)
        Me.TextEdit49.StyleController = Me.LayoutControl7
        Me.TextEdit49.TabIndex = 50
        '
        'TextEdit50
        '
        Me.TextEdit50.Location = New System.Drawing.Point(490, 169)
        Me.TextEdit50.Name = "TextEdit50"
        Me.TextEdit50.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit50.Properties.Appearance.Options.UseFont = True
        Me.TextEdit50.Properties.MaxLength = 5
        Me.TextEdit50.Size = New System.Drawing.Size(76, 26)
        Me.TextEdit50.StyleController = Me.LayoutControl7
        Me.TextEdit50.TabIndex = 49
        '
        'TextEdit51
        '
        Me.TextEdit51.Location = New System.Drawing.Point(313, 169)
        Me.TextEdit51.Name = "TextEdit51"
        Me.TextEdit51.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit51.Properties.Appearance.Options.UseFont = True
        Me.TextEdit51.Properties.MaxLength = 5
        Me.TextEdit51.Size = New System.Drawing.Size(76, 26)
        Me.TextEdit51.StyleController = Me.LayoutControl7
        Me.TextEdit51.TabIndex = 48
        '
        'CheckEdit8
        '
        Me.CheckEdit8.EditValue = True
        Me.CheckEdit8.Location = New System.Drawing.Point(196, 49)
        Me.CheckEdit8.Name = "CheckEdit8"
        Me.CheckEdit8.Properties.Caption = "Activo"
        Me.CheckEdit8.Size = New System.Drawing.Size(66, 19)
        Me.CheckEdit8.StyleController = Me.LayoutControl7
        Me.CheckEdit8.TabIndex = 1
        '
        'TextEdit52
        '
        Me.TextEdit52.EditValue = ""
        Me.TextEdit52.Location = New System.Drawing.Point(121, 169)
        Me.TextEdit52.Name = "TextEdit52"
        Me.TextEdit52.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit52.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextEdit52.Properties.Appearance.Options.UseFont = True
        Me.TextEdit52.Properties.Appearance.Options.UseForeColor = True
        Me.TextEdit52.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit52.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit52.Size = New System.Drawing.Size(91, 26)
        Me.TextEdit52.StyleController = Me.LayoutControl7
        Me.TextEdit52.TabIndex = 5
        '
        'TextEdit53
        '
        Me.TextEdit53.Location = New System.Drawing.Point(121, 139)
        Me.TextEdit53.Name = "TextEdit53"
        Me.TextEdit53.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit53.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextEdit53.Properties.Appearance.Options.UseFont = True
        Me.TextEdit53.Properties.Appearance.Options.UseForeColor = True
        Me.TextEdit53.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit53.Properties.MaxLength = 200
        Me.TextEdit53.Size = New System.Drawing.Size(1151, 26)
        Me.TextEdit53.StyleController = Me.LayoutControl7
        Me.TextEdit53.TabIndex = 4
        '
        'TextEdit54
        '
        Me.TextEdit54.Location = New System.Drawing.Point(487, 109)
        Me.TextEdit54.Name = "TextEdit54"
        Me.TextEdit54.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit54.Properties.Appearance.Options.UseFont = True
        Me.TextEdit54.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit54.Properties.MaxLength = 50
        Me.TextEdit54.Size = New System.Drawing.Size(265, 26)
        Me.TextEdit54.StyleController = Me.LayoutControl7
        Me.TextEdit54.TabIndex = 3
        '
        'TextEdit55
        '
        Me.TextEdit55.Location = New System.Drawing.Point(121, 79)
        Me.TextEdit55.Name = "TextEdit55"
        Me.TextEdit55.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit55.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextEdit55.Properties.Appearance.Options.UseFont = True
        Me.TextEdit55.Properties.Appearance.Options.UseForeColor = True
        Me.TextEdit55.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit55.Properties.MaxLength = 100
        Me.TextEdit55.Size = New System.Drawing.Size(1151, 26)
        Me.TextEdit55.StyleController = Me.LayoutControl7
        Me.TextEdit55.TabIndex = 2
        '
        'TextEdit56
        '
        Me.TextEdit56.Enabled = False
        Me.TextEdit56.Location = New System.Drawing.Point(121, 49)
        Me.TextEdit56.Name = "TextEdit56"
        Me.TextEdit56.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit56.Properties.Appearance.Options.UseFont = True
        Me.TextEdit56.Size = New System.Drawing.Size(71, 26)
        Me.TextEdit56.StyleController = Me.LayoutControl7
        Me.TextEdit56.TabIndex = 0
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl7.StyleController = Me.LayoutControl7
        Me.LabelControl7.TabIndex = 47
        '
        'LayoutControlGroup23
        '
        Me.LayoutControlGroup23.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup23.GroupBordersVisible = False
        Me.LayoutControlGroup23.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.TabbedControlGroup8})
        Me.LayoutControlGroup23.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup23.Name = "Root"
        Me.LayoutControlGroup23.Size = New System.Drawing.Size(1296, 358)
        Me.LayoutControlGroup23.Text = "Generales"
        Me.LayoutControlGroup23.TextVisible = False
        '
        'TabbedControlGroup8
        '
        Me.TabbedControlGroup8.Location = New System.Drawing.Point(0, 0)
        Me.TabbedControlGroup8.Name = "TabbedControlGroup1"
        Me.TabbedControlGroup8.SelectedTabPage = Me.LayoutControlGroup24
        Me.TabbedControlGroup8.SelectedTabPageIndex = 0
        Me.TabbedControlGroup8.Size = New System.Drawing.Size(1276, 338)
        Me.TabbedControlGroup8.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup24, Me.LayoutControlGroup25})
        Me.TabbedControlGroup8.Text = "Generales"
        '
        'LayoutControlGroup24
        '
        Me.LayoutControlGroup24.CaptionImage = CType(resources.GetObject("LayoutControlGroup24.CaptionImage"), System.Drawing.Image)
        Me.LayoutControlGroup24.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem62, Me.LayoutControlItem63, Me.LayoutControlItem64, Me.LayoutControlItem65, Me.EmptySpaceItem40, Me.LayoutControlItem66, Me.LayoutControlItem67, Me.LayoutControlItem68, Me.LayoutControlItem69, Me.LayoutControlItem70, Me.LayoutControlItem71, Me.EmptySpaceItem41, Me.EmptySpaceItem42, Me.LayoutControlItem72, Me.EmptySpaceItem43, Me.LayoutControlItem73, Me.LayoutControlItem74, Me.EmptySpaceItem44, Me.LayoutControlItem75, Me.EmptySpaceItem45, Me.EmptySpaceItem46, Me.LayoutControlItem76, Me.EmptySpaceItem47, Me.LayoutControlItem78})
        Me.LayoutControlGroup24.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup24.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup24.Size = New System.Drawing.Size(1252, 289)
        Me.LayoutControlGroup24.Text = "Generales"
        '
        'LayoutControlItem62
        '
        Me.LayoutControlItem62.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem62.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem62.Control = Me.TextEdit55
        Me.LayoutControlItem62.Image = CType(resources.GetObject("LayoutControlItem62.Image"), System.Drawing.Image)
        Me.LayoutControlItem62.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem62.Location = New System.Drawing.Point(0, 30)
        Me.LayoutControlItem62.MaxSize = New System.Drawing.Size(0, 30)
        Me.LayoutControlItem62.MinSize = New System.Drawing.Size(151, 30)
        Me.LayoutControlItem62.Name = "TxtDesBancoitem"
        Me.LayoutControlItem62.Size = New System.Drawing.Size(1252, 30)
        Me.LayoutControlItem62.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem62.Text = "Razon Social"
        Me.LayoutControlItem62.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem62.TextSize = New System.Drawing.Size(94, 16)
        '
        'LayoutControlItem63
        '
        Me.LayoutControlItem63.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem63.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem63.Control = Me.TextEdit56
        Me.LayoutControlItem63.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem63.MaxSize = New System.Drawing.Size(172, 30)
        Me.LayoutControlItem63.MinSize = New System.Drawing.Size(172, 30)
        Me.LayoutControlItem63.Name = "TxtBancoIDitem"
        Me.LayoutControlItem63.Size = New System.Drawing.Size(172, 30)
        Me.LayoutControlItem63.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem63.Text = "ID"
        Me.LayoutControlItem63.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem63.TextSize = New System.Drawing.Size(94, 16)
        '
        'LayoutControlItem64
        '
        Me.LayoutControlItem64.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem64.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem64.Control = Me.TextEdit53
        Me.LayoutControlItem64.Image = CType(resources.GetObject("LayoutControlItem64.Image"), System.Drawing.Image)
        Me.LayoutControlItem64.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem64.Location = New System.Drawing.Point(0, 90)
        Me.LayoutControlItem64.MaxSize = New System.Drawing.Size(0, 30)
        Me.LayoutControlItem64.MinSize = New System.Drawing.Size(151, 30)
        Me.LayoutControlItem64.Name = "LayoutControlItem4"
        Me.LayoutControlItem64.Size = New System.Drawing.Size(1252, 30)
        Me.LayoutControlItem64.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem64.Text = "Dirección"
        Me.LayoutControlItem64.TextSize = New System.Drawing.Size(94, 16)
        '
        'LayoutControlItem65
        '
        Me.LayoutControlItem65.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem65.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem65.Control = Me.TextEdit52
        Me.LayoutControlItem65.Image = CType(resources.GetObject("LayoutControlItem65.Image"), System.Drawing.Image)
        Me.LayoutControlItem65.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem65.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem65.MaxSize = New System.Drawing.Size(192, 30)
        Me.LayoutControlItem65.MinSize = New System.Drawing.Size(192, 30)
        Me.LayoutControlItem65.Name = "LayoutControlItem5"
        Me.LayoutControlItem65.Size = New System.Drawing.Size(192, 30)
        Me.LayoutControlItem65.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem65.Text = "Telefono"
        Me.LayoutControlItem65.TextSize = New System.Drawing.Size(94, 16)
        '
        'EmptySpaceItem40
        '
        Me.EmptySpaceItem40.AllowHotTrack = False
        Me.EmptySpaceItem40.Location = New System.Drawing.Point(242, 0)
        Me.EmptySpaceItem40.MaxSize = New System.Drawing.Size(644, 30)
        Me.EmptySpaceItem40.MinSize = New System.Drawing.Size(644, 30)
        Me.EmptySpaceItem40.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem40.Size = New System.Drawing.Size(1010, 30)
        Me.EmptySpaceItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem40.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem66
        '
        Me.LayoutControlItem66.Control = Me.CheckEdit8
        Me.LayoutControlItem66.Location = New System.Drawing.Point(172, 0)
        Me.LayoutControlItem66.MaxSize = New System.Drawing.Size(70, 30)
        Me.LayoutControlItem66.MinSize = New System.Drawing.Size(70, 30)
        Me.LayoutControlItem66.Name = "LayoutControlItem7"
        Me.LayoutControlItem66.Size = New System.Drawing.Size(70, 30)
        Me.LayoutControlItem66.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem66.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem66.TextVisible = False
        '
        'LayoutControlItem67
        '
        Me.LayoutControlItem67.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem67.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem67.Control = Me.TextEdit51
        Me.LayoutControlItem67.Location = New System.Drawing.Point(192, 120)
        Me.LayoutControlItem67.MaxSize = New System.Drawing.Size(177, 30)
        Me.LayoutControlItem67.MinSize = New System.Drawing.Size(177, 30)
        Me.LayoutControlItem67.Name = "LayoutControlItem6"
        Me.LayoutControlItem67.Size = New System.Drawing.Size(177, 30)
        Me.LayoutControlItem67.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem67.Text = "Ext"
        Me.LayoutControlItem67.TextSize = New System.Drawing.Size(94, 16)
        '
        'LayoutControlItem68
        '
        Me.LayoutControlItem68.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem68.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem68.Control = Me.TextEdit50
        Me.LayoutControlItem68.Location = New System.Drawing.Point(369, 120)
        Me.LayoutControlItem68.MaxSize = New System.Drawing.Size(177, 30)
        Me.LayoutControlItem68.MinSize = New System.Drawing.Size(177, 30)
        Me.LayoutControlItem68.Name = "LayoutControlItem12"
        Me.LayoutControlItem68.Size = New System.Drawing.Size(177, 30)
        Me.LayoutControlItem68.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem68.Text = "Ext"
        Me.LayoutControlItem68.TextSize = New System.Drawing.Size(94, 16)
        '
        'LayoutControlItem69
        '
        Me.LayoutControlItem69.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem69.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem69.Control = Me.TextEdit49
        Me.LayoutControlItem69.Image = CType(resources.GetObject("LayoutControlItem69.Image"), System.Drawing.Image)
        Me.LayoutControlItem69.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem69.Location = New System.Drawing.Point(0, 150)
        Me.LayoutControlItem69.MaxSize = New System.Drawing.Size(192, 30)
        Me.LayoutControlItem69.MinSize = New System.Drawing.Size(192, 30)
        Me.LayoutControlItem69.Name = "LayoutControlItem19"
        Me.LayoutControlItem69.Size = New System.Drawing.Size(192, 30)
        Me.LayoutControlItem69.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem69.Text = "Telefono"
        Me.LayoutControlItem69.TextSize = New System.Drawing.Size(94, 16)
        '
        'LayoutControlItem70
        '
        Me.LayoutControlItem70.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem70.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem70.Control = Me.TextEdit48
        Me.LayoutControlItem70.Location = New System.Drawing.Point(192, 150)
        Me.LayoutControlItem70.MaxSize = New System.Drawing.Size(177, 30)
        Me.LayoutControlItem70.MinSize = New System.Drawing.Size(177, 30)
        Me.LayoutControlItem70.Name = "LayoutControlItem26"
        Me.LayoutControlItem70.Size = New System.Drawing.Size(177, 30)
        Me.LayoutControlItem70.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem70.Text = "Ext"
        Me.LayoutControlItem70.TextSize = New System.Drawing.Size(94, 16)
        '
        'LayoutControlItem71
        '
        Me.LayoutControlItem71.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem71.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem71.Control = Me.TextEdit47
        Me.LayoutControlItem71.Location = New System.Drawing.Point(369, 150)
        Me.LayoutControlItem71.MaxSize = New System.Drawing.Size(177, 30)
        Me.LayoutControlItem71.MinSize = New System.Drawing.Size(177, 30)
        Me.LayoutControlItem71.Name = "LayoutControlItem33"
        Me.LayoutControlItem71.Size = New System.Drawing.Size(177, 30)
        Me.LayoutControlItem71.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem71.Text = "Ext"
        Me.LayoutControlItem71.TextSize = New System.Drawing.Size(94, 16)
        '
        'EmptySpaceItem41
        '
        Me.EmptySpaceItem41.AllowHotTrack = False
        Me.EmptySpaceItem41.Location = New System.Drawing.Point(546, 120)
        Me.EmptySpaceItem41.MaxSize = New System.Drawing.Size(340, 30)
        Me.EmptySpaceItem41.MinSize = New System.Drawing.Size(340, 30)
        Me.EmptySpaceItem41.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem41.Size = New System.Drawing.Size(706, 30)
        Me.EmptySpaceItem41.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem41.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem42
        '
        Me.EmptySpaceItem42.AllowHotTrack = False
        Me.EmptySpaceItem42.Location = New System.Drawing.Point(546, 150)
        Me.EmptySpaceItem42.MaxSize = New System.Drawing.Size(340, 30)
        Me.EmptySpaceItem42.MinSize = New System.Drawing.Size(340, 30)
        Me.EmptySpaceItem42.Name = "EmptySpaceItem34"
        Me.EmptySpaceItem42.Size = New System.Drawing.Size(706, 30)
        Me.EmptySpaceItem42.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem42.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem72
        '
        Me.LayoutControlItem72.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem72.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem72.Control = Me.TextEdit46
        Me.LayoutControlItem72.Image = CType(resources.GetObject("LayoutControlItem72.Image"), System.Drawing.Image)
        Me.LayoutControlItem72.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem72.Location = New System.Drawing.Point(0, 180)
        Me.LayoutControlItem72.MaxSize = New System.Drawing.Size(192, 30)
        Me.LayoutControlItem72.MinSize = New System.Drawing.Size(192, 30)
        Me.LayoutControlItem72.Name = "LayoutControlItem40"
        Me.LayoutControlItem72.Size = New System.Drawing.Size(192, 30)
        Me.LayoutControlItem72.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem72.Text = "Celular"
        Me.LayoutControlItem72.TextSize = New System.Drawing.Size(94, 16)
        '
        'EmptySpaceItem43
        '
        Me.EmptySpaceItem43.AllowHotTrack = False
        Me.EmptySpaceItem43.Location = New System.Drawing.Point(643, 180)
        Me.EmptySpaceItem43.MaxSize = New System.Drawing.Size(243, 30)
        Me.EmptySpaceItem43.MinSize = New System.Drawing.Size(243, 30)
        Me.EmptySpaceItem43.Name = "EmptySpaceItem35"
        Me.EmptySpaceItem43.Size = New System.Drawing.Size(609, 30)
        Me.EmptySpaceItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem43.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem73
        '
        Me.LayoutControlItem73.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem73.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem73.Control = Me.TextEdit45
        Me.LayoutControlItem73.Image = CType(resources.GetObject("LayoutControlItem73.Image"), System.Drawing.Image)
        Me.LayoutControlItem73.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem73.Location = New System.Drawing.Point(192, 180)
        Me.LayoutControlItem73.MaxSize = New System.Drawing.Size(451, 30)
        Me.LayoutControlItem73.MinSize = New System.Drawing.Size(451, 30)
        Me.LayoutControlItem73.Name = "LayoutControlItem45"
        Me.LayoutControlItem73.Size = New System.Drawing.Size(451, 30)
        Me.LayoutControlItem73.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem73.Text = "Email"
        Me.LayoutControlItem73.TextSize = New System.Drawing.Size(94, 16)
        '
        'LayoutControlItem74
        '
        Me.LayoutControlItem74.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem74.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem74.Control = Me.TextEdit44
        Me.LayoutControlItem74.Image = CType(resources.GetObject("LayoutControlItem74.Image"), System.Drawing.Image)
        Me.LayoutControlItem74.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem74.Location = New System.Drawing.Point(0, 210)
        Me.LayoutControlItem74.MaxSize = New System.Drawing.Size(369, 30)
        Me.LayoutControlItem74.MinSize = New System.Drawing.Size(369, 30)
        Me.LayoutControlItem74.Name = "LayoutControlItem52"
        Me.LayoutControlItem74.Size = New System.Drawing.Size(369, 30)
        Me.LayoutControlItem74.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem74.Text = "Cuenta"
        Me.LayoutControlItem74.TextSize = New System.Drawing.Size(94, 16)
        '
        'EmptySpaceItem44
        '
        Me.EmptySpaceItem44.AllowHotTrack = False
        Me.EmptySpaceItem44.Location = New System.Drawing.Point(369, 210)
        Me.EmptySpaceItem44.MaxSize = New System.Drawing.Size(517, 30)
        Me.EmptySpaceItem44.MinSize = New System.Drawing.Size(517, 30)
        Me.EmptySpaceItem44.Name = "EmptySpaceItem36"
        Me.EmptySpaceItem44.Size = New System.Drawing.Size(883, 30)
        Me.EmptySpaceItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem44.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem75
        '
        Me.LayoutControlItem75.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem75.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem75.Control = Me.TextEdit43
        Me.LayoutControlItem75.Location = New System.Drawing.Point(0, 60)
        Me.LayoutControlItem75.MaxSize = New System.Drawing.Size(366, 30)
        Me.LayoutControlItem75.MinSize = New System.Drawing.Size(366, 30)
        Me.LayoutControlItem75.Name = "LayoutControlItem53"
        Me.LayoutControlItem75.Size = New System.Drawing.Size(366, 30)
        Me.LayoutControlItem75.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem75.Text = "Siglas"
        Me.LayoutControlItem75.TextSize = New System.Drawing.Size(94, 16)
        '
        'EmptySpaceItem45
        '
        Me.EmptySpaceItem45.AllowHotTrack = False
        Me.EmptySpaceItem45.Location = New System.Drawing.Point(0, 263)
        Me.EmptySpaceItem45.Name = "EmptySpaceItem37"
        Me.EmptySpaceItem45.Size = New System.Drawing.Size(1252, 26)
        Me.EmptySpaceItem45.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem46
        '
        Me.EmptySpaceItem46.AllowHotTrack = False
        Me.EmptySpaceItem46.Location = New System.Drawing.Point(732, 60)
        Me.EmptySpaceItem46.MaxSize = New System.Drawing.Size(520, 30)
        Me.EmptySpaceItem46.MinSize = New System.Drawing.Size(520, 30)
        Me.EmptySpaceItem46.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem46.Size = New System.Drawing.Size(520, 30)
        Me.EmptySpaceItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem46.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem76
        '
        Me.LayoutControlItem76.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem76.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem76.Control = Me.TextEdit54
        Me.LayoutControlItem76.Image = CType(resources.GetObject("LayoutControlItem76.Image"), System.Drawing.Image)
        Me.LayoutControlItem76.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem76.Location = New System.Drawing.Point(366, 60)
        Me.LayoutControlItem76.MaxSize = New System.Drawing.Size(366, 30)
        Me.LayoutControlItem76.MinSize = New System.Drawing.Size(366, 30)
        Me.LayoutControlItem76.Name = "Txtsiglasitem"
        Me.LayoutControlItem76.Size = New System.Drawing.Size(366, 30)
        Me.LayoutControlItem76.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem76.Text = "Ruc"
        Me.LayoutControlItem76.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem76.TextSize = New System.Drawing.Size(94, 16)
        '
        'EmptySpaceItem47
        '
        Me.EmptySpaceItem47.AllowHotTrack = False
        Me.EmptySpaceItem47.Location = New System.Drawing.Point(286, 240)
        Me.EmptySpaceItem47.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem47.Size = New System.Drawing.Size(966, 23)
        Me.EmptySpaceItem47.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem78
        '
        Me.LayoutControlItem78.Control = Me.CheckEdit7
        Me.LayoutControlItem78.Location = New System.Drawing.Point(0, 240)
        Me.LayoutControlItem78.Name = "LayoutControlItem61"
        Me.LayoutControlItem78.Size = New System.Drawing.Size(286, 23)
        Me.LayoutControlItem78.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem78.TextVisible = False
        '
        'LayoutControlGroup25
        '
        Me.LayoutControlGroup25.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup25.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlGroup25.CaptionImage = CType(resources.GetObject("LayoutControlGroup25.CaptionImage"), System.Drawing.Image)
        Me.LayoutControlGroup25.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem79, Me.LayoutControlItem80, Me.LayoutControlItem81, Me.EmptySpaceItem48, Me.LayoutControlItem82, Me.EmptySpaceItem49, Me.LayoutControlItem83, Me.LayoutControlItem84})
        Me.LayoutControlGroup25.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup25.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup25.Size = New System.Drawing.Size(1252, 289)
        Me.LayoutControlGroup25.Text = "Contactos"
        '
        'LayoutControlItem79
        '
        Me.LayoutControlItem79.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem79.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem79.Control = Me.TextEdit42
        Me.LayoutControlItem79.Location = New System.Drawing.Point(622, 0)
        Me.LayoutControlItem79.MinSize = New System.Drawing.Size(195, 30)
        Me.LayoutControlItem79.Name = "LayoutControlItem56"
        Me.LayoutControlItem79.Size = New System.Drawing.Size(368, 30)
        Me.LayoutControlItem79.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem79.Text = "Apellidos"
        Me.LayoutControlItem79.TextSize = New System.Drawing.Size(94, 16)
        '
        'LayoutControlItem80
        '
        Me.LayoutControlItem80.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem80.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem80.Control = Me.TextEdit25
        Me.LayoutControlItem80.Location = New System.Drawing.Point(223, 30)
        Me.LayoutControlItem80.MaxSize = New System.Drawing.Size(399, 30)
        Me.LayoutControlItem80.MinSize = New System.Drawing.Size(399, 30)
        Me.LayoutControlItem80.Name = "LayoutControlItem58"
        Me.LayoutControlItem80.Size = New System.Drawing.Size(399, 30)
        Me.LayoutControlItem80.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem80.Text = "Nombres "
        Me.LayoutControlItem80.TextSize = New System.Drawing.Size(94, 16)
        '
        'LayoutControlItem81
        '
        Me.LayoutControlItem81.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem81.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem81.Control = Me.TextEdit31
        Me.LayoutControlItem81.Location = New System.Drawing.Point(223, 0)
        Me.LayoutControlItem81.MinSize = New System.Drawing.Size(195, 30)
        Me.LayoutControlItem81.Name = "LayoutControlItem57"
        Me.LayoutControlItem81.Size = New System.Drawing.Size(399, 30)
        Me.LayoutControlItem81.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem81.Text = "Nombres"
        Me.LayoutControlItem81.TextSize = New System.Drawing.Size(94, 16)
        '
        'EmptySpaceItem48
        '
        Me.EmptySpaceItem48.AllowHotTrack = False
        Me.EmptySpaceItem48.Location = New System.Drawing.Point(0, 60)
        Me.EmptySpaceItem48.Name = "EmptySpaceItem38"
        Me.EmptySpaceItem48.Size = New System.Drawing.Size(1252, 229)
        Me.EmptySpaceItem48.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem82
        '
        Me.LayoutControlItem82.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem82.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem82.Control = Me.TextEdit19
        Me.LayoutControlItem82.Location = New System.Drawing.Point(622, 30)
        Me.LayoutControlItem82.MinSize = New System.Drawing.Size(195, 30)
        Me.LayoutControlItem82.Name = "LayoutControlItem55"
        Me.LayoutControlItem82.Size = New System.Drawing.Size(368, 30)
        Me.LayoutControlItem82.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem82.Text = "Apellidos"
        Me.LayoutControlItem82.TextSize = New System.Drawing.Size(94, 16)
        '
        'EmptySpaceItem49
        '
        Me.EmptySpaceItem49.AllowHotTrack = False
        Me.EmptySpaceItem49.Location = New System.Drawing.Point(990, 0)
        Me.EmptySpaceItem49.Name = "EmptySpaceItem39"
        Me.EmptySpaceItem49.Size = New System.Drawing.Size(262, 60)
        Me.EmptySpaceItem49.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem83
        '
        Me.LayoutControlItem83.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem83.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem83.Control = Me.TextEdit13
        Me.LayoutControlItem83.Location = New System.Drawing.Point(0, 30)
        Me.LayoutControlItem83.MinSize = New System.Drawing.Size(195, 24)
        Me.LayoutControlItem83.Name = "LayoutControlItem60"
        Me.LayoutControlItem83.Size = New System.Drawing.Size(223, 30)
        Me.LayoutControlItem83.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem83.Text = " "
        Me.LayoutControlItem83.TextSize = New System.Drawing.Size(94, 16)
        '
        'LayoutControlItem84
        '
        Me.LayoutControlItem84.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem84.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem84.Control = Me.TextEdit7
        Me.LayoutControlItem84.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem84.Name = "LayoutControlItem59"
        Me.LayoutControlItem84.Size = New System.Drawing.Size(223, 30)
        Me.LayoutControlItem84.Text = " "
        Me.LayoutControlItem84.TextSize = New System.Drawing.Size(94, 16)
        '
        'LayoutControl8
        '
        Me.LayoutControl8.Controls.Add(Me.CheckEdit12)
        Me.LayoutControl8.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl8.Controls.Add(Me.SimpleButton2)
        Me.LayoutControl8.Controls.Add(Me.GridControl1)
        Me.LayoutControl8.Controls.Add(Me.Label1)
        Me.LayoutControl8.Controls.Add(Me.Label2)
        Me.LayoutControl8.Controls.Add(Me.TextEdit1)
        Me.LayoutControl8.Controls.Add(Me.CheckEdit13)
        Me.LayoutControl8.Controls.Add(Me.TextEdit57)
        Me.LayoutControl8.Controls.Add(Me.TextEdit58)
        Me.LayoutControl8.Controls.Add(Me.TextEdit59)
        Me.LayoutControl8.Controls.Add(Me.LabelControl8)
        Me.LayoutControl8.Controls.Add(Me.TextEdit60)
        Me.LayoutControl8.Controls.Add(Me.SimpleButton3)
        Me.LayoutControl8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl8.Location = New System.Drawing.Point(0, 40)
        Me.LayoutControl8.Name = "LayoutControl8"
        Me.LayoutControl8.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(70, 113, 250, 501)
        Me.LayoutControl8.Root = Me.LayoutControlGroup26
        Me.LayoutControl8.Size = New System.Drawing.Size(849, 392)
        Me.LayoutControl8.TabIndex = 0
        '
        'CheckEdit12
        '
        Me.CheckEdit12.Location = New System.Drawing.Point(120, 219)
        Me.CheckEdit12.MenuManager = Me.BarManager2
        Me.CheckEdit12.Name = "CheckEdit12"
        Me.CheckEdit12.Properties.Caption = "CheckEdit9"
        Me.CheckEdit12.Size = New System.Drawing.Size(705, 19)
        Me.CheckEdit12.StyleController = Me.LayoutControl8
        Me.CheckEdit12.TabIndex = 60
        '
        'SimpleButton1
        '
        Me.SimpleButton1.ImageOptions.Image = CType(resources.GetObject("SimpleButton1.ImageOptions.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(54, 49)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(26, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl8
        Me.SimpleButton1.TabIndex = 59
        Me.SimpleButton1.Visible = False
        '
        'SimpleButton2
        '
        Me.SimpleButton2.ImageOptions.Image = CType(resources.GetObject("SimpleButton2.ImageOptions.Image"), System.Drawing.Image)
        Me.SimpleButton2.Location = New System.Drawing.Point(24, 49)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(26, 22)
        Me.SimpleButton2.StyleController = Me.LayoutControl8
        Me.SimpleButton2.TabIndex = 58
        Me.SimpleButton2.Visible = False
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(24, 75)
        Me.GridControl1.MainView = Me.WinExplorerView1
        Me.GridControl1.MenuManager = Me.BarManager2
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(801, 293)
        Me.GridControl1.TabIndex = 57
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.WinExplorerView1})
        Me.GridControl1.Visible = False
        '
        'WinExplorerView1
        '
        Me.WinExplorerView1.Appearance.ItemDescriptionHovered.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WinExplorerView1.Appearance.ItemDescriptionHovered.ForeColor = System.Drawing.Color.Red
        Me.WinExplorerView1.Appearance.ItemDescriptionHovered.Options.UseFont = True
        Me.WinExplorerView1.Appearance.ItemDescriptionHovered.Options.UseForeColor = True
        Me.WinExplorerView1.Appearance.ItemDescriptionNormal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WinExplorerView1.Appearance.ItemDescriptionNormal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.WinExplorerView1.Appearance.ItemDescriptionNormal.Options.UseFont = True
        Me.WinExplorerView1.Appearance.ItemDescriptionNormal.Options.UseForeColor = True
        Me.WinExplorerView1.Appearance.ItemHovered.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WinExplorerView1.Appearance.ItemHovered.ForeColor = System.Drawing.Color.Blue
        Me.WinExplorerView1.Appearance.ItemHovered.Options.UseFont = True
        Me.WinExplorerView1.Appearance.ItemHovered.Options.UseForeColor = True
        Me.WinExplorerView1.Appearance.ItemNormal.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WinExplorerView1.Appearance.ItemNormal.ForeColor = System.Drawing.Color.Blue
        Me.WinExplorerView1.Appearance.ItemNormal.Options.UseFont = True
        Me.WinExplorerView1.Appearance.ItemNormal.Options.UseForeColor = True
        Me.WinExplorerView1.Appearance.ItemPressed.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WinExplorerView1.Appearance.ItemPressed.ForeColor = System.Drawing.Color.Blue
        Me.WinExplorerView1.Appearance.ItemPressed.Options.UseFont = True
        Me.WinExplorerView1.Appearance.ItemPressed.Options.UseForeColor = True
        Me.WinExplorerView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5})
        Me.WinExplorerView1.ColumnSet.DescriptionColumn = Me.GridColumn4
        Me.WinExplorerView1.ColumnSet.ExtraLargeImageColumn = Me.GridColumn2
        Me.WinExplorerView1.ColumnSet.ExtraLargeImageIndexColumn = Me.GridColumn2
        Me.WinExplorerView1.ColumnSet.LargeImageColumn = Me.GridColumn2
        Me.WinExplorerView1.ColumnSet.LargeImageIndexColumn = Me.GridColumn2
        Me.WinExplorerView1.ColumnSet.MediumImageColumn = Me.GridColumn2
        Me.WinExplorerView1.ColumnSet.MediumImageIndexColumn = Me.GridColumn2
        Me.WinExplorerView1.ColumnSet.SmallImageColumn = Me.GridColumn2
        Me.WinExplorerView1.ColumnSet.SmallImageIndexColumn = Me.GridColumn2
        Me.WinExplorerView1.ColumnSet.TextColumn = Me.GridColumn4
        Me.WinExplorerView1.GridControl = Me.GridControl1
        Me.WinExplorerView1.Name = "WinExplorerView1"
        Me.WinExplorerView1.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateAllContent
        Me.WinExplorerView1.OptionsView.Style = DevExpress.XtraGrid.Views.WinExplorer.WinExplorerViewStyle.ExtraLarge
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Imagen"
        Me.GridColumn2.FieldName = "Imagen"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 0
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "ProveedorID"
        Me.GridColumn3.FieldName = "ProveedorID"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 0
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Documento"
        Me.GridColumn4.FieldName = "Documento"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 0
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "TipoID"
        Me.GridColumn5.FieldName = "TipoID"
        Me.GridColumn5.Name = "GridColumn5"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(414, 151)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 30)
        Me.Label1.TabIndex = 55
        Me.Label1.Text = "0"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(414, 117)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 30)
        Me.Label2.TabIndex = 54
        Me.Label2.Text = "0"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextEdit1
        '
        Me.TextEdit1.EditValue = ""
        Me.TextEdit1.EnterMoveNextControl = True
        Me.TextEdit1.Location = New System.Drawing.Point(120, 185)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.MaxLength = 10
        Me.TextEdit1.Size = New System.Drawing.Size(125, 30)
        Me.TextEdit1.StyleController = Me.LayoutControl8
        Me.TextEdit1.TabIndex = 6
        '
        'CheckEdit13
        '
        Me.CheckEdit13.EditValue = True
        Me.CheckEdit13.Location = New System.Drawing.Point(249, 49)
        Me.CheckEdit13.Name = "CheckEdit13"
        Me.CheckEdit13.Properties.Caption = "Activo"
        Me.CheckEdit13.Size = New System.Drawing.Size(52, 19)
        Me.CheckEdit13.StyleController = Me.LayoutControl8
        Me.CheckEdit13.TabIndex = 1
        '
        'TextEdit57
        '
        Me.TextEdit57.EditValue = ""
        Me.TextEdit57.EnterMoveNextControl = True
        Me.TextEdit57.Location = New System.Drawing.Point(120, 117)
        Me.TextEdit57.Name = "TextEdit57"
        Me.TextEdit57.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit57.Properties.Appearance.Options.UseFont = True
        Me.TextEdit57.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit57.Properties.MaxLength = 14
        Me.TextEdit57.Size = New System.Drawing.Size(290, 30)
        Me.TextEdit57.StyleController = Me.LayoutControl8
        Me.TextEdit57.TabIndex = 4
        '
        'TextEdit58
        '
        Me.TextEdit58.EditValue = ""
        Me.TextEdit58.EnterMoveNextControl = True
        Me.TextEdit58.Location = New System.Drawing.Point(120, 83)
        Me.TextEdit58.Name = "TextEdit58"
        Me.TextEdit58.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit58.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextEdit58.Properties.Appearance.Options.UseFont = True
        Me.TextEdit58.Properties.Appearance.Options.UseForeColor = True
        Me.TextEdit58.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit58.Properties.MaxLength = 100
        Me.TextEdit58.Size = New System.Drawing.Size(680, 30)
        Me.TextEdit58.StyleController = Me.LayoutControl8
        Me.TextEdit58.TabIndex = 3
        '
        'TextEdit59
        '
        Me.TextEdit59.Enabled = False
        Me.TextEdit59.EnterMoveNextControl = True
        Me.TextEdit59.Location = New System.Drawing.Point(120, 49)
        Me.TextEdit59.Name = "TextEdit59"
        Me.TextEdit59.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit59.Properties.Appearance.Options.UseFont = True
        Me.TextEdit59.Size = New System.Drawing.Size(125, 30)
        Me.TextEdit59.StyleController = Me.LayoutControl8
        Me.TextEdit59.TabIndex = 2
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl8.StyleController = Me.LayoutControl8
        Me.LabelControl8.TabIndex = 1
        '
        'TextEdit60
        '
        Me.TextEdit60.EditValue = ""
        Me.TextEdit60.EnterMoveNextControl = True
        Me.TextEdit60.Location = New System.Drawing.Point(120, 151)
        Me.TextEdit60.Name = "TextEdit60"
        Me.TextEdit60.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit60.Properties.Appearance.Options.UseFont = True
        Me.TextEdit60.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit60.Properties.MaxLength = 14
        Me.TextEdit60.Size = New System.Drawing.Size(290, 30)
        Me.TextEdit60.StyleController = Me.LayoutControl8
        Me.TextEdit60.TabIndex = 5
        '
        'SimpleButton3
        '
        Me.SimpleButton3.ImageOptions.Image = CType(resources.GetObject("SimpleButton3.ImageOptions.Image"), System.Drawing.Image)
        Me.SimpleButton3.Location = New System.Drawing.Point(84, 49)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(26, 22)
        Me.SimpleButton3.StyleController = Me.LayoutControl8
        Me.SimpleButton3.TabIndex = 58
        Me.SimpleButton3.Visible = False
        '
        'LayoutControlGroup26
        '
        Me.LayoutControlGroup26.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup26.GroupBordersVisible = False
        Me.LayoutControlGroup26.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.TabbedControlGroup9})
        Me.LayoutControlGroup26.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup26.Name = "Root"
        Me.LayoutControlGroup26.Size = New System.Drawing.Size(849, 392)
        Me.LayoutControlGroup26.Text = "Generales"
        Me.LayoutControlGroup26.TextVisible = False
        '
        'TabbedControlGroup9
        '
        Me.TabbedControlGroup9.Location = New System.Drawing.Point(0, 0)
        Me.TabbedControlGroup9.Name = "TabbedControlGroup1"
        Me.TabbedControlGroup9.SelectedTabPage = Me.LayoutControlGroup27
        Me.TabbedControlGroup9.SelectedTabPageIndex = 0
        Me.TabbedControlGroup9.Size = New System.Drawing.Size(829, 372)
        Me.TabbedControlGroup9.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup27, Me.LayoutControlGroup28})
        Me.TabbedControlGroup9.Text = "Generales"
        '
        'LayoutControlGroup27
        '
        Me.LayoutControlGroup27.CaptionImage = CType(resources.GetObject("LayoutControlGroup27.CaptionImage"), System.Drawing.Image)
        Me.LayoutControlGroup27.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem52, Me.LayoutControlItem53, Me.EmptySpaceItem36, Me.EmptySpaceItem38, Me.LayoutControlItem54, Me.LayoutControlItem55, Me.LayoutControlItem56, Me.EmptySpaceItem39, Me.EmptySpaceItem50, Me.EmptySpaceItem51, Me.EmptySpaceItem52, Me.LayoutControlItem57, Me.LayoutControlItem58, Me.LayoutControlItem59, Me.LayoutControlItem60})
        Me.LayoutControlGroup27.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup27.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup27.Size = New System.Drawing.Size(805, 323)
        Me.LayoutControlGroup27.Text = "Generales"
        '
        'LayoutControlItem52
        '
        Me.LayoutControlItem52.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem52.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem52.Control = Me.TextEdit58
        Me.LayoutControlItem52.Image = CType(resources.GetObject("LayoutControlItem52.Image"), System.Drawing.Image)
        Me.LayoutControlItem52.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem52.Location = New System.Drawing.Point(0, 34)
        Me.LayoutControlItem52.MaxSize = New System.Drawing.Size(780, 34)
        Me.LayoutControlItem52.MinSize = New System.Drawing.Size(780, 34)
        Me.LayoutControlItem52.Name = "TxtDesBancoitem"
        Me.LayoutControlItem52.Size = New System.Drawing.Size(780, 34)
        Me.LayoutControlItem52.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem52.Text = "Retenido"
        Me.LayoutControlItem52.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem52.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem53
        '
        Me.LayoutControlItem53.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem53.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem53.Control = Me.TextEdit59
        Me.LayoutControlItem53.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem53.MaxSize = New System.Drawing.Size(225, 34)
        Me.LayoutControlItem53.MinSize = New System.Drawing.Size(225, 34)
        Me.LayoutControlItem53.Name = "TxtBancoIDitem"
        Me.LayoutControlItem53.Size = New System.Drawing.Size(225, 34)
        Me.LayoutControlItem53.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem53.Text = "ID"
        Me.LayoutControlItem53.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem53.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem36
        '
        Me.EmptySpaceItem36.AllowHotTrack = False
        Me.EmptySpaceItem36.Location = New System.Drawing.Point(281, 0)
        Me.EmptySpaceItem36.MinSize = New System.Drawing.Size(104, 24)
        Me.EmptySpaceItem36.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem36.Size = New System.Drawing.Size(499, 34)
        Me.EmptySpaceItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem36.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem38
        '
        Me.EmptySpaceItem38.AllowHotTrack = False
        Me.EmptySpaceItem38.Location = New System.Drawing.Point(0, 193)
        Me.EmptySpaceItem38.Name = "EmptySpaceItem37"
        Me.EmptySpaceItem38.Size = New System.Drawing.Size(805, 130)
        Me.EmptySpaceItem38.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem54
        '
        Me.LayoutControlItem54.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem54.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem54.Control = Me.TextEdit57
        Me.LayoutControlItem54.Image = CType(resources.GetObject("LayoutControlItem54.Image"), System.Drawing.Image)
        Me.LayoutControlItem54.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem54.Location = New System.Drawing.Point(0, 68)
        Me.LayoutControlItem54.MaxSize = New System.Drawing.Size(390, 34)
        Me.LayoutControlItem54.MinSize = New System.Drawing.Size(390, 34)
        Me.LayoutControlItem54.Name = "Txtsiglasitem"
        Me.LayoutControlItem54.Size = New System.Drawing.Size(390, 34)
        Me.LayoutControlItem54.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem54.Text = "RUC"
        Me.LayoutControlItem54.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem54.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem55
        '
        Me.LayoutControlItem55.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem55.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem55.Control = Me.TextEdit1
        Me.LayoutControlItem55.Image = CType(resources.GetObject("LayoutControlItem55.Image"), System.Drawing.Image)
        Me.LayoutControlItem55.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem55.Location = New System.Drawing.Point(0, 136)
        Me.LayoutControlItem55.MaxSize = New System.Drawing.Size(225, 34)
        Me.LayoutControlItem55.MinSize = New System.Drawing.Size(225, 34)
        Me.LayoutControlItem55.Name = "LayoutControlItem40"
        Me.LayoutControlItem55.Size = New System.Drawing.Size(225, 34)
        Me.LayoutControlItem55.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem55.Text = "Telefono"
        Me.LayoutControlItem55.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlItem56
        '
        Me.LayoutControlItem56.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem56.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem56.Control = Me.TextEdit60
        Me.LayoutControlItem56.CustomizationFormText = "Ruc"
        Me.LayoutControlItem56.Image = CType(resources.GetObject("LayoutControlItem56.Image"), System.Drawing.Image)
        Me.LayoutControlItem56.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem56.Location = New System.Drawing.Point(0, 102)
        Me.LayoutControlItem56.MaxSize = New System.Drawing.Size(390, 34)
        Me.LayoutControlItem56.MinSize = New System.Drawing.Size(390, 34)
        Me.LayoutControlItem56.Name = "Txtsiglasitem1"
        Me.LayoutControlItem56.Size = New System.Drawing.Size(390, 34)
        Me.LayoutControlItem56.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem56.Text = "No Cedula"
        Me.LayoutControlItem56.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem56.TextSize = New System.Drawing.Size(93, 16)
        '
        'EmptySpaceItem39
        '
        Me.EmptySpaceItem39.AllowHotTrack = False
        Me.EmptySpaceItem39.Location = New System.Drawing.Point(780, 0)
        Me.EmptySpaceItem39.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem39.Size = New System.Drawing.Size(25, 170)
        Me.EmptySpaceItem39.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem50
        '
        Me.EmptySpaceItem50.AllowHotTrack = False
        Me.EmptySpaceItem50.Location = New System.Drawing.Point(225, 136)
        Me.EmptySpaceItem50.MinSize = New System.Drawing.Size(104, 24)
        Me.EmptySpaceItem50.Name = "EmptySpaceItem34"
        Me.EmptySpaceItem50.Size = New System.Drawing.Size(555, 34)
        Me.EmptySpaceItem50.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem50.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem51
        '
        Me.EmptySpaceItem51.AllowHotTrack = False
        Me.EmptySpaceItem51.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmptySpaceItem51.AppearanceItemCaption.Options.UseFont = True
        Me.EmptySpaceItem51.Location = New System.Drawing.Point(448, 102)
        Me.EmptySpaceItem51.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem51.Size = New System.Drawing.Size(332, 34)
        Me.EmptySpaceItem51.Text = "Ejemplo : 0012701820003Q ( 14 Caracteres )"
        Me.EmptySpaceItem51.TextSize = New System.Drawing.Size(93, 0)
        Me.EmptySpaceItem51.TextVisible = True
        '
        'EmptySpaceItem52
        '
        Me.EmptySpaceItem52.AllowHotTrack = False
        Me.EmptySpaceItem52.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmptySpaceItem52.AppearanceItemCaption.Options.UseFont = True
        Me.EmptySpaceItem52.Location = New System.Drawing.Point(448, 68)
        Me.EmptySpaceItem52.Name = "EmptySpaceItem35"
        Me.EmptySpaceItem52.Size = New System.Drawing.Size(332, 34)
        Me.EmptySpaceItem52.Text = "Ejemplo : J0310000252904  ( 14 Caracteres )"
        Me.EmptySpaceItem52.TextSize = New System.Drawing.Size(93, 0)
        Me.EmptySpaceItem52.TextVisible = True
        '
        'LayoutControlItem57
        '
        Me.LayoutControlItem57.Control = Me.CheckEdit13
        Me.LayoutControlItem57.Location = New System.Drawing.Point(225, 0)
        Me.LayoutControlItem57.MinSize = New System.Drawing.Size(56, 23)
        Me.LayoutControlItem57.Name = "LayoutControlItem7"
        Me.LayoutControlItem57.Size = New System.Drawing.Size(56, 34)
        Me.LayoutControlItem57.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem57.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem57.TextVisible = False
        '
        'LayoutControlItem58
        '
        Me.LayoutControlItem58.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem58.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem58.Control = Me.Label2
        Me.LayoutControlItem58.Location = New System.Drawing.Point(390, 68)
        Me.LayoutControlItem58.Name = "LayoutControlItem4"
        Me.LayoutControlItem58.Size = New System.Drawing.Size(58, 34)
        Me.LayoutControlItem58.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem58.TextVisible = False
        '
        'LayoutControlItem59
        '
        Me.LayoutControlItem59.Control = Me.Label1
        Me.LayoutControlItem59.Location = New System.Drawing.Point(390, 102)
        Me.LayoutControlItem59.Name = "LayoutControlItem5"
        Me.LayoutControlItem59.Size = New System.Drawing.Size(58, 34)
        Me.LayoutControlItem59.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem59.TextVisible = False
        '
        'LayoutControlItem60
        '
        Me.LayoutControlItem60.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem60.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem60.Control = Me.CheckEdit12
        Me.LayoutControlItem60.Location = New System.Drawing.Point(0, 170)
        Me.LayoutControlItem60.Name = "LayoutControlItem33"
        Me.LayoutControlItem60.Size = New System.Drawing.Size(805, 23)
        Me.LayoutControlItem60.Text = "Exoneración IVA"
        Me.LayoutControlItem60.TextSize = New System.Drawing.Size(93, 16)
        '
        'LayoutControlGroup28
        '
        Me.LayoutControlGroup28.CaptionImage = CType(resources.GetObject("LayoutControlGroup28.CaptionImage"), System.Drawing.Image)
        Me.LayoutControlGroup28.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem77, Me.LayoutControlItem85, Me.LayoutControlItem86, Me.EmptySpaceItem53, Me.LayoutControlItem87})
        Me.LayoutControlGroup28.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup28.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup28.Size = New System.Drawing.Size(805, 323)
        Me.LayoutControlGroup28.Text = "Soportes"
        '
        'LayoutControlItem77
        '
        Me.LayoutControlItem77.Control = Me.GridControl1
        Me.LayoutControlItem77.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem77.Name = "LayoutControlItem12"
        Me.LayoutControlItem77.Size = New System.Drawing.Size(805, 297)
        Me.LayoutControlItem77.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem77.TextVisible = False
        '
        'LayoutControlItem85
        '
        Me.LayoutControlItem85.Control = Me.SimpleButton2
        Me.LayoutControlItem85.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem85.MaxSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem85.MinSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem85.Name = "LayoutControlItem19"
        Me.LayoutControlItem85.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem85.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem85.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem85.TextVisible = False
        '
        'LayoutControlItem86
        '
        Me.LayoutControlItem86.Control = Me.SimpleButton1
        Me.LayoutControlItem86.Location = New System.Drawing.Point(30, 0)
        Me.LayoutControlItem86.MaxSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem86.MinSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem86.Name = "LayoutControlItem26"
        Me.LayoutControlItem86.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem86.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem86.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem86.TextVisible = False
        '
        'EmptySpaceItem53
        '
        Me.EmptySpaceItem53.AllowHotTrack = False
        Me.EmptySpaceItem53.Location = New System.Drawing.Point(90, 0)
        Me.EmptySpaceItem53.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem53.Size = New System.Drawing.Size(715, 26)
        Me.EmptySpaceItem53.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem87
        '
        Me.LayoutControlItem87.Control = Me.SimpleButton3
        Me.LayoutControlItem87.CustomizationFormText = "LayoutControlItem19"
        Me.LayoutControlItem87.Location = New System.Drawing.Point(60, 0)
        Me.LayoutControlItem87.MaxSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem87.MinSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem87.Name = "LayoutControlItem6"
        Me.LayoutControlItem87.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem87.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem87.Text = "LayoutControlItem19"
        Me.LayoutControlItem87.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem87.TextVisible = False
        '
        'BarDockControl9
        '
        Me.BarDockControl9.CausesValidation = False
        Me.BarDockControl9.Dock = System.Windows.Forms.DockStyle.Left
        Me.BarDockControl9.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl9.Manager = Nothing
        Me.BarDockControl9.Size = New System.Drawing.Size(0, 432)
        '
        'BarDockControl10
        '
        Me.BarDockControl10.CausesValidation = False
        Me.BarDockControl10.Dock = System.Windows.Forms.DockStyle.Right
        Me.BarDockControl10.Location = New System.Drawing.Point(849, 0)
        Me.BarDockControl10.Manager = Nothing
        Me.BarDockControl10.Size = New System.Drawing.Size(0, 432)
        '
        'BarDockControl11
        '
        Me.BarDockControl11.CausesValidation = False
        Me.BarDockControl11.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BarDockControl11.Location = New System.Drawing.Point(0, 432)
        Me.BarDockControl11.Manager = Nothing
        Me.BarDockControl11.Size = New System.Drawing.Size(849, 0)
        '
        'BarDockControl12
        '
        Me.BarDockControl12.CausesValidation = False
        Me.BarDockControl12.Dock = System.Windows.Forms.DockStyle.Top
        Me.BarDockControl12.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl12.Manager = Nothing
        Me.BarDockControl12.Size = New System.Drawing.Size(849, 0)
        '
        'LayoutControl9
        '
        Me.LayoutControl9.Controls.Add(Me.Label3)
        Me.LayoutControl9.Controls.Add(Me.SimpleButton4)
        Me.LayoutControl9.Controls.Add(Me.SimpleButton5)
        Me.LayoutControl9.Controls.Add(Me.GridControl2)
        Me.LayoutControl9.Controls.Add(Me.Label4)
        Me.LayoutControl9.Controls.Add(Me.TextEdit61)
        Me.LayoutControl9.Controls.Add(Me.CheckEdit10)
        Me.LayoutControl9.Controls.Add(Me.TextEdit62)
        Me.LayoutControl9.Controls.Add(Me.TextEdit63)
        Me.LayoutControl9.Controls.Add(Me.TextEdit64)
        Me.LayoutControl9.Controls.Add(Me.LabelControl9)
        Me.LayoutControl9.Controls.Add(Me.TextEdit65)
        Me.LayoutControl9.Controls.Add(Me.SimpleButton6)
        Me.LayoutControl9.Controls.Add(Me.CheckEdit11)
        Me.LayoutControl9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl9.Location = New System.Drawing.Point(0, 80)
        Me.LayoutControl9.Name = "LayoutControl9"
        Me.LayoutControl9.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(70, 113, 250, 501)
        Me.LayoutControl9.Root = Me.LayoutControlGroup29
        Me.LayoutControl9.Size = New System.Drawing.Size(849, 352)
        Me.LayoutControl9.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(414, 151)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 30)
        Me.Label3.TabIndex = 55
        Me.Label3.Text = "0"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SimpleButton4
        '
        Me.SimpleButton4.ImageOptions.Image = CType(resources.GetObject("SimpleButton4.ImageOptions.Image"), System.Drawing.Image)
        Me.SimpleButton4.Location = New System.Drawing.Point(54, 49)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(26, 22)
        Me.SimpleButton4.StyleController = Me.LayoutControl9
        Me.SimpleButton4.TabIndex = 59
        Me.SimpleButton4.Visible = False
        '
        'SimpleButton5
        '
        Me.SimpleButton5.ImageOptions.Image = CType(resources.GetObject("SimpleButton5.ImageOptions.Image"), System.Drawing.Image)
        Me.SimpleButton5.Location = New System.Drawing.Point(24, 49)
        Me.SimpleButton5.Name = "SimpleButton5"
        Me.SimpleButton5.Size = New System.Drawing.Size(26, 22)
        Me.SimpleButton5.StyleController = Me.LayoutControl9
        Me.SimpleButton5.TabIndex = 58
        Me.SimpleButton5.Visible = False
        '
        'GridControl2
        '
        Me.GridControl2.Location = New System.Drawing.Point(24, 75)
        Me.GridControl2.MainView = Me.WinExplorerView2
        Me.GridControl2.MenuManager = Me.BarManager3
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(801, 253)
        Me.GridControl2.TabIndex = 57
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.WinExplorerView2})
        Me.GridControl2.Visible = False
        '
        'WinExplorerView2
        '
        Me.WinExplorerView2.Appearance.ItemDescriptionHovered.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WinExplorerView2.Appearance.ItemDescriptionHovered.ForeColor = System.Drawing.Color.Red
        Me.WinExplorerView2.Appearance.ItemDescriptionHovered.Options.UseFont = True
        Me.WinExplorerView2.Appearance.ItemDescriptionHovered.Options.UseForeColor = True
        Me.WinExplorerView2.Appearance.ItemDescriptionNormal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WinExplorerView2.Appearance.ItemDescriptionNormal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.WinExplorerView2.Appearance.ItemDescriptionNormal.Options.UseFont = True
        Me.WinExplorerView2.Appearance.ItemDescriptionNormal.Options.UseForeColor = True
        Me.WinExplorerView2.Appearance.ItemHovered.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WinExplorerView2.Appearance.ItemHovered.ForeColor = System.Drawing.Color.Blue
        Me.WinExplorerView2.Appearance.ItemHovered.Options.UseFont = True
        Me.WinExplorerView2.Appearance.ItemHovered.Options.UseForeColor = True
        Me.WinExplorerView2.Appearance.ItemNormal.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WinExplorerView2.Appearance.ItemNormal.ForeColor = System.Drawing.Color.Blue
        Me.WinExplorerView2.Appearance.ItemNormal.Options.UseFont = True
        Me.WinExplorerView2.Appearance.ItemNormal.Options.UseForeColor = True
        Me.WinExplorerView2.Appearance.ItemPressed.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WinExplorerView2.Appearance.ItemPressed.ForeColor = System.Drawing.Color.Blue
        Me.WinExplorerView2.Appearance.ItemPressed.Options.UseFont = True
        Me.WinExplorerView2.Appearance.ItemPressed.Options.UseForeColor = True
        Me.WinExplorerView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9})
        Me.WinExplorerView2.ColumnSet.DescriptionColumn = Me.GridColumn8
        Me.WinExplorerView2.ColumnSet.ExtraLargeImageColumn = Me.GridColumn6
        Me.WinExplorerView2.ColumnSet.ExtraLargeImageIndexColumn = Me.GridColumn6
        Me.WinExplorerView2.ColumnSet.LargeImageColumn = Me.GridColumn6
        Me.WinExplorerView2.ColumnSet.LargeImageIndexColumn = Me.GridColumn6
        Me.WinExplorerView2.ColumnSet.MediumImageColumn = Me.GridColumn6
        Me.WinExplorerView2.ColumnSet.MediumImageIndexColumn = Me.GridColumn6
        Me.WinExplorerView2.ColumnSet.SmallImageColumn = Me.GridColumn6
        Me.WinExplorerView2.ColumnSet.SmallImageIndexColumn = Me.GridColumn6
        Me.WinExplorerView2.ColumnSet.TextColumn = Me.GridColumn8
        Me.WinExplorerView2.GridControl = Me.GridControl2
        Me.WinExplorerView2.Name = "WinExplorerView2"
        Me.WinExplorerView2.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateAllContent
        Me.WinExplorerView2.OptionsView.Style = DevExpress.XtraGrid.Views.WinExplorer.WinExplorerViewStyle.ExtraLarge
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Imagen"
        Me.GridColumn6.FieldName = "Imagen"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 0
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "ProveedorID"
        Me.GridColumn7.FieldName = "ProveedorID"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 0
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Documento"
        Me.GridColumn8.FieldName = "Documento"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 0
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "TipoID"
        Me.GridColumn9.FieldName = "TipoID"
        Me.GridColumn9.Name = "GridColumn9"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Red
        Me.Label4.Location = New System.Drawing.Point(414, 117)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 30)
        Me.Label4.TabIndex = 54
        Me.Label4.Text = "0"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TextEdit61
        '
        Me.TextEdit61.EditValue = ""
        Me.TextEdit61.EnterMoveNextControl = True
        Me.TextEdit61.Location = New System.Drawing.Point(106, 185)
        Me.TextEdit61.Name = "TextEdit61"
        Me.TextEdit61.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit61.Properties.Appearance.Options.UseFont = True
        Me.TextEdit61.Properties.MaxLength = 10
        Me.TextEdit61.Size = New System.Drawing.Size(139, 30)
        Me.TextEdit61.StyleController = Me.LayoutControl9
        Me.TextEdit61.TabIndex = 6
        '
        'CheckEdit10
        '
        Me.CheckEdit10.EditValue = True
        Me.CheckEdit10.Location = New System.Drawing.Point(249, 49)
        Me.CheckEdit10.Name = "CheckEdit10"
        Me.CheckEdit10.Properties.Caption = "Activo"
        Me.CheckEdit10.Size = New System.Drawing.Size(52, 19)
        Me.CheckEdit10.StyleController = Me.LayoutControl9
        Me.CheckEdit10.TabIndex = 1
        '
        'TextEdit62
        '
        Me.TextEdit62.EditValue = ""
        Me.TextEdit62.EnterMoveNextControl = True
        Me.TextEdit62.Location = New System.Drawing.Point(106, 117)
        Me.TextEdit62.Name = "TextEdit62"
        Me.TextEdit62.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit62.Properties.Appearance.Options.UseFont = True
        Me.TextEdit62.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit62.Properties.MaxLength = 14
        Me.TextEdit62.Size = New System.Drawing.Size(304, 30)
        Me.TextEdit62.StyleController = Me.LayoutControl9
        Me.TextEdit62.TabIndex = 4
        '
        'TextEdit63
        '
        Me.TextEdit63.EditValue = ""
        Me.TextEdit63.EnterMoveNextControl = True
        Me.TextEdit63.Location = New System.Drawing.Point(106, 83)
        Me.TextEdit63.Name = "TextEdit63"
        Me.TextEdit63.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit63.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TextEdit63.Properties.Appearance.Options.UseFont = True
        Me.TextEdit63.Properties.Appearance.Options.UseForeColor = True
        Me.TextEdit63.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit63.Properties.MaxLength = 100
        Me.TextEdit63.Size = New System.Drawing.Size(694, 30)
        Me.TextEdit63.StyleController = Me.LayoutControl9
        Me.TextEdit63.TabIndex = 3
        '
        'TextEdit64
        '
        Me.TextEdit64.Enabled = False
        Me.TextEdit64.EnterMoveNextControl = True
        Me.TextEdit64.Location = New System.Drawing.Point(106, 49)
        Me.TextEdit64.Name = "TextEdit64"
        Me.TextEdit64.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit64.Properties.Appearance.Options.UseFont = True
        Me.TextEdit64.Size = New System.Drawing.Size(139, 30)
        Me.TextEdit64.StyleController = Me.LayoutControl9
        Me.TextEdit64.TabIndex = 2
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl9.StyleController = Me.LayoutControl9
        Me.LabelControl9.TabIndex = 1
        '
        'TextEdit65
        '
        Me.TextEdit65.EditValue = ""
        Me.TextEdit65.EnterMoveNextControl = True
        Me.TextEdit65.Location = New System.Drawing.Point(106, 151)
        Me.TextEdit65.Name = "TextEdit65"
        Me.TextEdit65.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit65.Properties.Appearance.Options.UseFont = True
        Me.TextEdit65.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit65.Properties.MaxLength = 14
        Me.TextEdit65.Size = New System.Drawing.Size(304, 30)
        Me.TextEdit65.StyleController = Me.LayoutControl9
        Me.TextEdit65.TabIndex = 5
        '
        'SimpleButton6
        '
        Me.SimpleButton6.ImageOptions.Image = CType(resources.GetObject("SimpleButton6.ImageOptions.Image"), System.Drawing.Image)
        Me.SimpleButton6.Location = New System.Drawing.Point(84, 49)
        Me.SimpleButton6.Name = "SimpleButton6"
        Me.SimpleButton6.Size = New System.Drawing.Size(26, 22)
        Me.SimpleButton6.StyleController = Me.LayoutControl9
        Me.SimpleButton6.TabIndex = 58
        Me.SimpleButton6.Visible = False
        '
        'CheckEdit11
        '
        Me.CheckEdit11.Location = New System.Drawing.Point(106, 219)
        Me.CheckEdit11.MenuManager = Me.BarManager4
        Me.CheckEdit11.Name = "CheckEdit11"
        Me.CheckEdit11.Properties.Caption = ""
        Me.CheckEdit11.Size = New System.Drawing.Size(719, 19)
        Me.CheckEdit11.StyleController = Me.LayoutControl9
        Me.CheckEdit11.TabIndex = 61
        '
        'BarManager4
        '
        Me.BarManager4.DockControls.Add(Me.BarDockControl17)
        Me.BarManager4.DockControls.Add(Me.BarDockControl18)
        Me.BarManager4.DockControls.Add(Me.BarDockControl19)
        Me.BarManager4.DockControls.Add(Me.BarDockControl20)
        Me.BarManager4.Form = Me
        Me.BarManager4.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItem6, Me.BarButtonItem7})
        Me.BarManager4.MaxItemId = 3
        '
        'BarDockControl17
        '
        Me.BarDockControl17.CausesValidation = False
        Me.BarDockControl17.Dock = System.Windows.Forms.DockStyle.Top
        Me.BarDockControl17.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl17.Manager = Me.BarManager4
        Me.BarDockControl17.Size = New System.Drawing.Size(1174, 0)
        '
        'BarDockControl18
        '
        Me.BarDockControl18.CausesValidation = False
        Me.BarDockControl18.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BarDockControl18.Location = New System.Drawing.Point(0, 583)
        Me.BarDockControl18.Manager = Me.BarManager4
        Me.BarDockControl18.Size = New System.Drawing.Size(1174, 0)
        '
        'BarDockControl19
        '
        Me.BarDockControl19.CausesValidation = False
        Me.BarDockControl19.Dock = System.Windows.Forms.DockStyle.Left
        Me.BarDockControl19.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl19.Manager = Me.BarManager4
        Me.BarDockControl19.Size = New System.Drawing.Size(0, 583)
        '
        'BarDockControl20
        '
        Me.BarDockControl20.CausesValidation = False
        Me.BarDockControl20.Dock = System.Windows.Forms.DockStyle.Right
        Me.BarDockControl20.Location = New System.Drawing.Point(1174, 0)
        Me.BarDockControl20.Manager = Me.BarManager4
        Me.BarDockControl20.Size = New System.Drawing.Size(0, 583)
        '
        'BarButtonItem6
        '
        Me.BarButtonItem6.Caption = "Guardar"
        Me.BarButtonItem6.Id = 0
        Me.BarButtonItem6.ImageOptions.Image = CType(resources.GetObject("BarButtonItem6.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem6.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem6.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem6.Name = "BarButtonItem6"
        '
        'BarButtonItem7
        '
        Me.BarButtonItem7.Caption = "Regresar"
        Me.BarButtonItem7.Id = 1
        Me.BarButtonItem7.ImageOptions.Image = CType(resources.GetObject("BarButtonItem7.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem7.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem7.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem7.Name = "BarButtonItem7"
        '
        'LayoutControlGroup29
        '
        Me.LayoutControlGroup29.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup29.GroupBordersVisible = False
        Me.LayoutControlGroup29.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.TabbedControlGroup10})
        Me.LayoutControlGroup29.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup29.Name = "Root"
        Me.LayoutControlGroup29.Size = New System.Drawing.Size(849, 352)
        Me.LayoutControlGroup29.Text = "Generales"
        Me.LayoutControlGroup29.TextVisible = False
        '
        'TabbedControlGroup10
        '
        Me.TabbedControlGroup10.Location = New System.Drawing.Point(0, 0)
        Me.TabbedControlGroup10.Name = "TabbedControlGroup1"
        Me.TabbedControlGroup10.SelectedTabPage = Me.LayoutControlGroup30
        Me.TabbedControlGroup10.SelectedTabPageIndex = 0
        Me.TabbedControlGroup10.Size = New System.Drawing.Size(829, 332)
        Me.TabbedControlGroup10.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup30, Me.LayoutControlGroup31})
        Me.TabbedControlGroup10.Text = "Generales"
        '
        'LayoutControlGroup30
        '
        Me.LayoutControlGroup30.CaptionImage = CType(resources.GetObject("LayoutControlGroup30.CaptionImage"), System.Drawing.Image)
        Me.LayoutControlGroup30.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem33, Me.LayoutControlItem61, Me.EmptySpaceItem54, Me.EmptySpaceItem55, Me.LayoutControlItem89, Me.LayoutControlItem90, Me.LayoutControlItem91, Me.EmptySpaceItem56, Me.EmptySpaceItem57, Me.EmptySpaceItem58, Me.EmptySpaceItem59, Me.LayoutControlItem92, Me.LayoutControlItem93, Me.LayoutControlItem94, Me.LayoutControlItem96})
        Me.LayoutControlGroup30.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup30.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup30.Size = New System.Drawing.Size(805, 283)
        Me.LayoutControlGroup30.Text = "Generales"
        '
        'LayoutControlItem33
        '
        Me.LayoutControlItem33.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem33.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem33.Control = Me.TextEdit63
        Me.LayoutControlItem33.Image = CType(resources.GetObject("LayoutControlItem33.Image"), System.Drawing.Image)
        Me.LayoutControlItem33.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem33.Location = New System.Drawing.Point(0, 34)
        Me.LayoutControlItem33.MaxSize = New System.Drawing.Size(780, 34)
        Me.LayoutControlItem33.MinSize = New System.Drawing.Size(780, 34)
        Me.LayoutControlItem33.Name = "TxtDesBancoitem"
        Me.LayoutControlItem33.Size = New System.Drawing.Size(780, 34)
        Me.LayoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem33.Text = "Retenido"
        Me.LayoutControlItem33.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem33.TextSize = New System.Drawing.Size(79, 16)
        '
        'LayoutControlItem61
        '
        Me.LayoutControlItem61.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem61.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem61.Control = Me.TextEdit64
        Me.LayoutControlItem61.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem61.MaxSize = New System.Drawing.Size(225, 34)
        Me.LayoutControlItem61.MinSize = New System.Drawing.Size(225, 34)
        Me.LayoutControlItem61.Name = "TxtBancoIDitem"
        Me.LayoutControlItem61.Size = New System.Drawing.Size(225, 34)
        Me.LayoutControlItem61.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem61.Text = "ID"
        Me.LayoutControlItem61.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem61.TextSize = New System.Drawing.Size(79, 16)
        '
        'EmptySpaceItem54
        '
        Me.EmptySpaceItem54.AllowHotTrack = False
        Me.EmptySpaceItem54.Location = New System.Drawing.Point(281, 0)
        Me.EmptySpaceItem54.MinSize = New System.Drawing.Size(104, 24)
        Me.EmptySpaceItem54.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem54.Size = New System.Drawing.Size(499, 34)
        Me.EmptySpaceItem54.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem54.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem55
        '
        Me.EmptySpaceItem55.AllowHotTrack = False
        Me.EmptySpaceItem55.Location = New System.Drawing.Point(0, 193)
        Me.EmptySpaceItem55.Name = "EmptySpaceItem37"
        Me.EmptySpaceItem55.Size = New System.Drawing.Size(805, 90)
        Me.EmptySpaceItem55.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem89
        '
        Me.LayoutControlItem89.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem89.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem89.Control = Me.TextEdit62
        Me.LayoutControlItem89.Image = CType(resources.GetObject("LayoutControlItem89.Image"), System.Drawing.Image)
        Me.LayoutControlItem89.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem89.Location = New System.Drawing.Point(0, 68)
        Me.LayoutControlItem89.MaxSize = New System.Drawing.Size(390, 34)
        Me.LayoutControlItem89.MinSize = New System.Drawing.Size(390, 34)
        Me.LayoutControlItem89.Name = "Txtsiglasitem"
        Me.LayoutControlItem89.Size = New System.Drawing.Size(390, 34)
        Me.LayoutControlItem89.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem89.Text = "RUC"
        Me.LayoutControlItem89.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem89.TextSize = New System.Drawing.Size(79, 16)
        '
        'LayoutControlItem90
        '
        Me.LayoutControlItem90.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem90.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem90.Control = Me.TextEdit61
        Me.LayoutControlItem90.Image = CType(resources.GetObject("LayoutControlItem90.Image"), System.Drawing.Image)
        Me.LayoutControlItem90.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem90.Location = New System.Drawing.Point(0, 136)
        Me.LayoutControlItem90.MaxSize = New System.Drawing.Size(225, 34)
        Me.LayoutControlItem90.MinSize = New System.Drawing.Size(225, 34)
        Me.LayoutControlItem90.Name = "LayoutControlItem40"
        Me.LayoutControlItem90.Size = New System.Drawing.Size(225, 34)
        Me.LayoutControlItem90.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem90.Text = "Telefono"
        Me.LayoutControlItem90.TextSize = New System.Drawing.Size(79, 16)
        '
        'LayoutControlItem91
        '
        Me.LayoutControlItem91.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem91.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem91.Control = Me.TextEdit65
        Me.LayoutControlItem91.CustomizationFormText = "Ruc"
        Me.LayoutControlItem91.Image = CType(resources.GetObject("LayoutControlItem91.Image"), System.Drawing.Image)
        Me.LayoutControlItem91.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem91.Location = New System.Drawing.Point(0, 102)
        Me.LayoutControlItem91.MaxSize = New System.Drawing.Size(390, 34)
        Me.LayoutControlItem91.MinSize = New System.Drawing.Size(390, 34)
        Me.LayoutControlItem91.Name = "Txtsiglasitem1"
        Me.LayoutControlItem91.Size = New System.Drawing.Size(390, 34)
        Me.LayoutControlItem91.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem91.Text = "No Cedula"
        Me.LayoutControlItem91.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem91.TextSize = New System.Drawing.Size(79, 16)
        '
        'EmptySpaceItem56
        '
        Me.EmptySpaceItem56.AllowHotTrack = False
        Me.EmptySpaceItem56.Location = New System.Drawing.Point(780, 0)
        Me.EmptySpaceItem56.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem56.Size = New System.Drawing.Size(25, 170)
        Me.EmptySpaceItem56.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem57
        '
        Me.EmptySpaceItem57.AllowHotTrack = False
        Me.EmptySpaceItem57.Location = New System.Drawing.Point(225, 136)
        Me.EmptySpaceItem57.MinSize = New System.Drawing.Size(104, 24)
        Me.EmptySpaceItem57.Name = "EmptySpaceItem34"
        Me.EmptySpaceItem57.Size = New System.Drawing.Size(555, 34)
        Me.EmptySpaceItem57.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem57.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem58
        '
        Me.EmptySpaceItem58.AllowHotTrack = False
        Me.EmptySpaceItem58.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmptySpaceItem58.AppearanceItemCaption.Options.UseFont = True
        Me.EmptySpaceItem58.Location = New System.Drawing.Point(448, 102)
        Me.EmptySpaceItem58.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem58.Size = New System.Drawing.Size(332, 34)
        Me.EmptySpaceItem58.Text = "Ejemplo : 0012701820003Q ( 14 Caracteres )"
        Me.EmptySpaceItem58.TextSize = New System.Drawing.Size(79, 0)
        Me.EmptySpaceItem58.TextVisible = True
        '
        'EmptySpaceItem59
        '
        Me.EmptySpaceItem59.AllowHotTrack = False
        Me.EmptySpaceItem59.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmptySpaceItem59.AppearanceItemCaption.Options.UseFont = True
        Me.EmptySpaceItem59.Location = New System.Drawing.Point(448, 68)
        Me.EmptySpaceItem59.Name = "EmptySpaceItem35"
        Me.EmptySpaceItem59.Size = New System.Drawing.Size(332, 34)
        Me.EmptySpaceItem59.Text = "Ejemplo : J0310000252904  ( 14 Caracteres )"
        Me.EmptySpaceItem59.TextSize = New System.Drawing.Size(79, 0)
        Me.EmptySpaceItem59.TextVisible = True
        '
        'LayoutControlItem92
        '
        Me.LayoutControlItem92.Control = Me.CheckEdit10
        Me.LayoutControlItem92.Location = New System.Drawing.Point(225, 0)
        Me.LayoutControlItem92.MinSize = New System.Drawing.Size(56, 23)
        Me.LayoutControlItem92.Name = "LayoutControlItem7"
        Me.LayoutControlItem92.Size = New System.Drawing.Size(56, 34)
        Me.LayoutControlItem92.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem92.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem92.TextVisible = False
        '
        'LayoutControlItem93
        '
        Me.LayoutControlItem93.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem93.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem93.Control = Me.Label4
        Me.LayoutControlItem93.Location = New System.Drawing.Point(390, 68)
        Me.LayoutControlItem93.Name = "LayoutControlItem4"
        Me.LayoutControlItem93.Size = New System.Drawing.Size(58, 34)
        Me.LayoutControlItem93.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem93.TextVisible = False
        '
        'LayoutControlItem94
        '
        Me.LayoutControlItem94.Control = Me.Label3
        Me.LayoutControlItem94.Location = New System.Drawing.Point(390, 102)
        Me.LayoutControlItem94.Name = "LayoutControlItem5"
        Me.LayoutControlItem94.Size = New System.Drawing.Size(58, 34)
        Me.LayoutControlItem94.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem94.TextVisible = False
        '
        'LayoutControlItem96
        '
        Me.LayoutControlItem96.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem96.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem96.Control = Me.CheckEdit11
        Me.LayoutControlItem96.CustomizationFormText = "Exoneración IR"
        Me.LayoutControlItem96.Location = New System.Drawing.Point(0, 170)
        Me.LayoutControlItem96.Name = "LayoutControlItem88"
        Me.LayoutControlItem96.Size = New System.Drawing.Size(805, 23)
        Me.LayoutControlItem96.Text = "Aplicar IMI"
        Me.LayoutControlItem96.TextSize = New System.Drawing.Size(79, 16)
        '
        'LayoutControlGroup31
        '
        Me.LayoutControlGroup31.CaptionImage = CType(resources.GetObject("LayoutControlGroup31.CaptionImage"), System.Drawing.Image)
        Me.LayoutControlGroup31.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem97, Me.LayoutControlItem98, Me.LayoutControlItem99, Me.EmptySpaceItem60, Me.LayoutControlItem100})
        Me.LayoutControlGroup31.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup31.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup31.Size = New System.Drawing.Size(805, 283)
        Me.LayoutControlGroup31.Text = "Soportes"
        '
        'LayoutControlItem97
        '
        Me.LayoutControlItem97.Control = Me.GridControl2
        Me.LayoutControlItem97.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem97.Name = "LayoutControlItem12"
        Me.LayoutControlItem97.Size = New System.Drawing.Size(805, 257)
        Me.LayoutControlItem97.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem97.TextVisible = False
        '
        'LayoutControlItem98
        '
        Me.LayoutControlItem98.Control = Me.SimpleButton5
        Me.LayoutControlItem98.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem98.MaxSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem98.MinSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem98.Name = "LayoutControlItem19"
        Me.LayoutControlItem98.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem98.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem98.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem98.TextVisible = False
        '
        'LayoutControlItem99
        '
        Me.LayoutControlItem99.Control = Me.SimpleButton4
        Me.LayoutControlItem99.Location = New System.Drawing.Point(30, 0)
        Me.LayoutControlItem99.MaxSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem99.MinSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem99.Name = "LayoutControlItem26"
        Me.LayoutControlItem99.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem99.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem99.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem99.TextVisible = False
        '
        'EmptySpaceItem60
        '
        Me.EmptySpaceItem60.AllowHotTrack = False
        Me.EmptySpaceItem60.Location = New System.Drawing.Point(90, 0)
        Me.EmptySpaceItem60.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem60.Size = New System.Drawing.Size(715, 26)
        Me.EmptySpaceItem60.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem100
        '
        Me.LayoutControlItem100.Control = Me.SimpleButton6
        Me.LayoutControlItem100.CustomizationFormText = "LayoutControlItem19"
        Me.LayoutControlItem100.Location = New System.Drawing.Point(60, 0)
        Me.LayoutControlItem100.MaxSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem100.MinSize = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem100.Name = "LayoutControlItem6"
        Me.LayoutControlItem100.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem100.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem100.Text = "LayoutControlItem19"
        Me.LayoutControlItem100.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem100.TextVisible = False
        '
        'BarDockControl21
        '
        Me.BarDockControl21.CausesValidation = False
        Me.BarDockControl21.Dock = System.Windows.Forms.DockStyle.Left
        Me.BarDockControl21.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl21.Manager = Nothing
        Me.BarDockControl21.Size = New System.Drawing.Size(0, 432)
        '
        'BarDockControl22
        '
        Me.BarDockControl22.CausesValidation = False
        Me.BarDockControl22.Dock = System.Windows.Forms.DockStyle.Right
        Me.BarDockControl22.Location = New System.Drawing.Point(849, 0)
        Me.BarDockControl22.Manager = Nothing
        Me.BarDockControl22.Size = New System.Drawing.Size(0, 432)
        '
        'BarDockControl23
        '
        Me.BarDockControl23.CausesValidation = False
        Me.BarDockControl23.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BarDockControl23.Location = New System.Drawing.Point(0, 432)
        Me.BarDockControl23.Manager = Nothing
        Me.BarDockControl23.Size = New System.Drawing.Size(849, 0)
        '
        'BarDockControl24
        '
        Me.BarDockControl24.CausesValidation = False
        Me.BarDockControl24.Dock = System.Windows.Forms.DockStyle.Top
        Me.BarDockControl24.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl24.Manager = Nothing
        Me.BarDockControl24.Size = New System.Drawing.Size(849, 0)
        '
        'Frm_IU_Proveedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1174, 583)
        Me.Controls.Add(Me.Frm_IU_AgenteConvertedLayout)
        Me.Controls.Add(Me.BarDockControl3)
        Me.Controls.Add(Me.BarDockControl4)
        Me.Controls.Add(Me.BarDockControl2)
        Me.Controls.Add(Me.BarDockControl1)
        Me.Controls.Add(Me.BarDockControl7)
        Me.Controls.Add(Me.BarDockControl8)
        Me.Controls.Add(Me.BarDockControl6)
        Me.Controls.Add(Me.BarDockControl5)
        Me.Controls.Add(Me.BarDockControl15)
        Me.Controls.Add(Me.BarDockControl16)
        Me.Controls.Add(Me.BarDockControl14)
        Me.Controls.Add(Me.BarDockControl13)
        Me.Controls.Add(Me.BarDockControl39)
        Me.Controls.Add(Me.BarDockControl40)
        Me.Controls.Add(Me.BarDockControl38)
        Me.Controls.Add(Me.BarDockControl37)
        Me.Controls.Add(Me.BarDockControl19)
        Me.Controls.Add(Me.BarDockControl20)
        Me.Controls.Add(Me.BarDockControl18)
        Me.Controls.Add(Me.BarDockControl17)
        Me.Name = "Frm_IU_Proveedor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.Frm_IU_AgenteConvertedLayout, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Frm_IU_AgenteConvertedLayout.ResumeLayout(False)
        Me.Frm_IU_AgenteConvertedLayout.PerformLayout()
        CType(Me.chk_ir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Grid_Imagenes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VGrid_Imagen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_celular.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk_Activo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_Ruc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_Retenido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_cedula.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk_imi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk_iva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabbedControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtDesBancoitem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtBancoIDitem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txtsiglasitem, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txtsiglasitem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem88, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem101, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        Me.LayoutControl1.PerformLayout()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabbedControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl2.ResumeLayout(False)
        Me.LayoutControl2.PerformLayout()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabbedControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl3.ResumeLayout(False)
        Me.LayoutControl3.PerformLayout()
        CType(Me.CheckEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit14.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit15.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit16.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit17.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit18.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabbedControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl4.ResumeLayout(False)
        Me.LayoutControl4.PerformLayout()
        CType(Me.CheckEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit20.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit21.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit22.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit23.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit24.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabbedControlGroup5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl5.ResumeLayout(False)
        Me.LayoutControl5.PerformLayout()
        CType(Me.CheckEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit26.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit27.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit28.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit29.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit30.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabbedControlGroup6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl6.ResumeLayout(False)
        Me.LayoutControl6.PerformLayout()
        CType(Me.CheckEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit32.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit33.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit34.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit35.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit36.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit37.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit38.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit39.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit40.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit41.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabbedControlGroup7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl7.ResumeLayout(False)
        Me.LayoutControl7.PerformLayout()
        CType(Me.CheckEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit19.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit25.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit31.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit42.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit43.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit44.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit45.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit46.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit47.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit48.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit49.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit50.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit51.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit52.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit53.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit54.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit55.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit56.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabbedControlGroup8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem66, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem67, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem68, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem69, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem70, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem71, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem74, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem75, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem76, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem78, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem79, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem80, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem81, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem82, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem84, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl8.ResumeLayout(False)
        Me.LayoutControl8.PerformLayout()
        CType(Me.CheckEdit12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WinExplorerView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit57.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit58.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit59.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit60.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabbedControlGroup9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem77, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem85, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem86, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem87, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl9.ResumeLayout(False)
        Me.LayoutControl9.PerformLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WinExplorerView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit61.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit62.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit63.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit64.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit65.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabbedControlGroup10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem89, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem90, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem91, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem92, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem93, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem94, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem96, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem97, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem98, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem99, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem100, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbl_rotulo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Txt_Retenido As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txt_Ruc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Frm_IU_AgenteConvertedLayout As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents chk_Activo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutConverter1 As DevExpress.XtraLayout.Converter.LayoutConverter
    Friend WithEvents TabbedControlGroup1 As DevExpress.XtraLayout.TabbedControlGroup
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents TxtDesBancoitem As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TxtBancoIDitem As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Txtsiglasitem As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlGroup5 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents TabbedControlGroup2 As DevExpress.XtraLayout.TabbedControlGroup
    Friend WithEvents LayoutControlGroup6 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem6 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem7 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem8 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem9 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem10 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup7 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit11 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit12 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlGroup8 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents TabbedControlGroup3 As DevExpress.XtraLayout.TabbedControlGroup
    Friend WithEvents LayoutControlGroup9 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem11 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem12 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem13 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem14 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem15 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup10 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControl3 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit14 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit15 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit16 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit17 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit18 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlGroup11 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents TabbedControlGroup4 As DevExpress.XtraLayout.TabbedControlGroup
    Friend WithEvents LayoutControlGroup12 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem16 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem17 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem18 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem19 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem20 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup13 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControl4 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit4 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit20 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit21 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit22 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit23 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit24 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlGroup14 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents TabbedControlGroup5 As DevExpress.XtraLayout.TabbedControlGroup
    Friend WithEvents LayoutControlGroup15 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem21 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem22 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem23 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem24 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem25 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup16 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControl5 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit5 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit26 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit27 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit28 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit29 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit30 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlGroup17 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents TabbedControlGroup6 As DevExpress.XtraLayout.TabbedControlGroup
    Friend WithEvents LayoutControlGroup18 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem34 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem35 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem36 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem37 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem26 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem27 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem28 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem29 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem30 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem39 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup19 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txt_celular As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem40 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl6 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit6 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit32 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit33 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit34 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit35 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit36 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit37 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit38 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit39 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit40 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents BarManager11 As DevExpress.XtraBars.BarManager
    Friend WithEvents btn_guardar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem20 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarDockControl37 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl38 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl39 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl40 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents TextEdit41 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup20 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents TabbedControlGroup7 As DevExpress.XtraLayout.TabbedControlGroup
    Friend WithEvents LayoutControlGroup21 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem38 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem41 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem42 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem43 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem44 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem31 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem32 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem46 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem47 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem48 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem49 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem50 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem51 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem33 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlGroup22 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem37 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents btn_guardar1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarDockControl1 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl2 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl3 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl4 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents LayoutControl7 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit7 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit13 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit19 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit25 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit31 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit42 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit43 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit44 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit45 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit46 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit47 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit48 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit49 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit50 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit51 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEdit8 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit52 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit53 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit54 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit55 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit56 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlGroup23 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents TabbedControlGroup8 As DevExpress.XtraLayout.TabbedControlGroup
    Friend WithEvents LayoutControlGroup24 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem62 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem63 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem64 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem65 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem40 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem66 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem67 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem68 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem69 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem70 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem71 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem41 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem42 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem72 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem43 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem73 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem74 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem44 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem75 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem45 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem46 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem76 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem47 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem78 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup25 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem79 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem80 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem81 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem48 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem82 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem49 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem83 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem84 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem34 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents txt_cedula As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txtsiglasitem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem35 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents lbl_cedula As System.Windows.Forms.Label
    Friend WithEvents lbl_ruc As System.Windows.Forms.Label
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents Grid_Imagenes As DevExpress.XtraGrid.GridControl
    Friend WithEvents VGrid_Imagen As DevExpress.XtraGrid.Views.WinExplorer.WinExplorerView
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents btn_quitar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_agregar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents btn_ver As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chk_ir As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem45 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents chk_imi As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents BarManager2 As DevExpress.XtraBars.BarManager
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarDockControl5 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl6 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl7 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl8 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents LayoutControlItem88 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl8 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents CheckEdit12 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents WinExplorerView1 As DevExpress.XtraGrid.Views.WinExplorer.WinExplorerView
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEdit13 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit57 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit58 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit59 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit60 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlGroup26 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents TabbedControlGroup9 As DevExpress.XtraLayout.TabbedControlGroup
    Friend WithEvents LayoutControlGroup27 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem52 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem53 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem36 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem38 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem54 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem55 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem56 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem39 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem50 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem51 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem52 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem57 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem58 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem59 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem60 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup28 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem77 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem85 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem86 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem53 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem87 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents BarDockControl9 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl10 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl11 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl12 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents chk_iva As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents BarManager3 As DevExpress.XtraBars.BarManager
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarDockControl13 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl14 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl15 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl16 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents LayoutControlItem101 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl9 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents WinExplorerView2 As DevExpress.XtraGrid.Views.WinExplorer.WinExplorerView
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextEdit61 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEdit10 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit62 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit63 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit64 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit65 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CheckEdit11 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents BarManager4 As DevExpress.XtraBars.BarManager
    Friend WithEvents BarButtonItem6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem7 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarDockControl17 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl18 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl19 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl20 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents LayoutControlGroup29 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents TabbedControlGroup10 As DevExpress.XtraLayout.TabbedControlGroup
    Friend WithEvents LayoutControlGroup30 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem33 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem61 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem54 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem55 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem89 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem90 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem91 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem56 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem57 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem58 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem59 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem92 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem93 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem94 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem96 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup31 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem97 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem98 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem99 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem60 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem100 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents BarDockControl21 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl22 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl23 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl24 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarButtonItem8 As DevExpress.XtraBars.BarButtonItem
End Class
