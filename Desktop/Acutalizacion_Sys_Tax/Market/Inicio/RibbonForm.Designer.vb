﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RibbonForm
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RibbonForm))
        Me.RibbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.btn_Agente = New DevExpress.XtraBars.BarButtonItem()
        Me.btn_Cliente = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.Plantilla = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem()
        Me.lbl_Data = New DevExpress.XtraBars.BarStaticItem()
        Me.lbl_server = New DevExpress.XtraBars.BarStaticItem()
        Me.BarButtonItem5 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem6 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem7 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem8 = New DevExpress.XtraBars.BarButtonItem()
        Me.rb_btn_usuario = New DevExpress.XtraBars.BarButtonItem()
        Me.rb_btn_datos = New DevExpress.XtraBars.BarButtonItem()
        Me.rb_btn_rol = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem9 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem10 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem11 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem12 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem13 = New DevExpress.XtraBars.BarButtonItem()
        Me.btn_reporte = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem14 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem15 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem16 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem17 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem18 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem19 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem20 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem21 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem()
        Me.BarStaticItem2 = New DevExpress.XtraBars.BarStaticItem()
        Me.rpt_dinamico = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem22 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem23 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem24 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem25 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem26 = New DevExpress.XtraBars.BarButtonItem()
        Me.btn_IR_full = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem28 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem27 = New DevExpress.XtraBars.BarButtonItem()
        Me.btnAuditoria = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPage3 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup4 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup11 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage1 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage2 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup3 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage4 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup5 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage5 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup6 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage6 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup7 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage7 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup8 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage8 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup10 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonStatusBar = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
        Me.XtraTabbedMdiManager1 = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        Me.RibbonPageGroup9 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RibbonControl
        '
        Me.RibbonControl.ApplicationIcon = CType(resources.GetObject("RibbonControl.ApplicationIcon"), System.Drawing.Bitmap)
        Me.RibbonControl.ExpandCollapseItem.Id = 0
        Me.RibbonControl.Images = Me.ImageList1
        Me.RibbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl.ExpandCollapseItem, Me.btn_Agente, Me.btn_Cliente, Me.BarButtonItem1, Me.BarButtonItem2, Me.BarButtonItem3, Me.Plantilla, Me.BarButtonItem4, Me.BarStaticItem1, Me.lbl_Data, Me.lbl_server, Me.BarButtonItem5, Me.BarButtonItem6, Me.BarButtonItem7, Me.BarButtonItem8, Me.rb_btn_usuario, Me.rb_btn_datos, Me.rb_btn_rol, Me.BarButtonItem9, Me.BarButtonItem10, Me.BarButtonItem11, Me.BarButtonItem12, Me.BarButtonItem13, Me.btn_reporte, Me.BarButtonItem14, Me.BarButtonItem15, Me.BarButtonItem16, Me.BarButtonItem17, Me.BarButtonItem18, Me.BarButtonItem19, Me.BarButtonItem20, Me.BarButtonItem21, Me.BarSubItem1, Me.BarStaticItem2, Me.rpt_dinamico, Me.BarButtonItem22, Me.BarButtonItem23, Me.BarButtonItem24, Me.BarButtonItem25, Me.BarButtonItem26, Me.btn_IR_full, Me.BarButtonItem28, Me.BarButtonItem27, Me.btnAuditoria})
        Me.RibbonControl.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl.MaxItemId = 51
        Me.RibbonControl.Name = "RibbonControl"
        Me.RibbonControl.PageHeaderItemLinks.Add(Me.BarButtonItem16)
        Me.RibbonControl.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.RibbonPage3, Me.RibbonPage1, Me.RibbonPage2, Me.RibbonPage4, Me.RibbonPage5, Me.RibbonPage6, Me.RibbonPage7, Me.RibbonPage8})
        Me.RibbonControl.Size = New System.Drawing.Size(1095, 146)
        Me.RibbonControl.StatusBar = Me.RibbonStatusBar
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "1 - Key.png")
        Me.ImageList1.Images.SetKeyName(1, "AboutInfoControl.png")
        '
        'btn_Agente
        '
        Me.btn_Agente.Caption = "Listado de Agentes"
        Me.btn_Agente.Id = 1
        Me.btn_Agente.ImageOptions.Image = CType(resources.GetObject("btn_Agente.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_Agente.ImageOptions.LargeImage = CType(resources.GetObject("btn_Agente.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.btn_Agente.LargeWidth = 60
        Me.btn_Agente.Name = "btn_Agente"
        '
        'btn_Cliente
        '
        Me.btn_Cliente.Caption = "Listado de Clientes"
        Me.btn_Cliente.Id = 2
        Me.btn_Cliente.ImageOptions.Image = CType(resources.GetObject("btn_Cliente.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_Cliente.ImageOptions.LargeImage = CType(resources.GetObject("btn_Cliente.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.btn_Cliente.LargeWidth = 60
        Me.btn_Cliente.Name = "btn_Cliente"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Periodo Facturación"
        Me.BarButtonItem1.Id = 4
        Me.BarButtonItem1.ImageOptions.Image = CType(resources.GetObject("BarButtonItem1.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem1.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem1.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem1.Name = "BarButtonItem1"
        Me.BarButtonItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "Conceptos Comerciales"
        Me.BarButtonItem2.Id = 5
        Me.BarButtonItem2.ImageOptions.Image = CType(resources.GetObject("BarButtonItem2.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem2.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem2.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "Listado de Facturas"
        Me.BarButtonItem3.Id = 6
        Me.BarButtonItem3.ImageOptions.Image = CType(resources.GetObject("BarButtonItem3.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem3.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem3.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem3.LargeWidth = 70
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'Plantilla
        '
        Me.Plantilla.Caption = "Proveedor"
        Me.Plantilla.Id = 7
        Me.Plantilla.ImageOptions.Image = CType(resources.GetObject("Plantilla.ImageOptions.Image"), System.Drawing.Image)
        Me.Plantilla.ImageOptions.LargeImage = CType(resources.GetObject("Plantilla.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.Plantilla.Name = "Plantilla"
        Me.Plantilla.Tag = "Formulario Proveedores"
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "Generar Facturas"
        Me.BarButtonItem4.Id = 8
        Me.BarButtonItem4.ImageOptions.Image = CType(resources.GetObject("BarButtonItem4.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem4.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem4.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem4.LargeWidth = 70
        Me.BarButtonItem4.Name = "BarButtonItem4"
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.Caption = "BarStaticItem1"
        Me.BarStaticItem1.Id = 10
        Me.BarStaticItem1.Name = "BarStaticItem1"
        '
        'lbl_Data
        '
        Me.lbl_Data.Id = 11
        Me.lbl_Data.ImageOptions.Image = CType(resources.GetObject("lbl_Data.ImageOptions.Image"), System.Drawing.Image)
        Me.lbl_Data.ImageOptions.LargeImage = CType(resources.GetObject("lbl_Data.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.lbl_Data.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Data.ItemAppearance.Normal.Options.UseFont = True
        Me.lbl_Data.Name = "lbl_Data"
        '
        'lbl_server
        '
        Me.lbl_server.Id = 12
        Me.lbl_server.ImageOptions.Image = CType(resources.GetObject("lbl_server.ImageOptions.Image"), System.Drawing.Image)
        Me.lbl_server.ImageOptions.LargeImage = CType(resources.GetObject("lbl_server.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.lbl_server.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_server.ItemAppearance.Normal.Options.UseFont = True
        Me.lbl_server.Name = "lbl_server"
        '
        'BarButtonItem5
        '
        Me.BarButtonItem5.Caption = "Usuarios System"
        Me.BarButtonItem5.Id = 13
        Me.BarButtonItem5.ImageOptions.Image = CType(resources.GetObject("BarButtonItem5.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem5.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem5.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem5.Name = "BarButtonItem5"
        Me.BarButtonItem5.Tag = "Boton Usuario System del Menu Principal"
        '
        'BarButtonItem6
        '
        Me.BarButtonItem6.Caption = "Listado de Cajas"
        Me.BarButtonItem6.Id = 14
        Me.BarButtonItem6.ImageOptions.Image = CType(resources.GetObject("BarButtonItem6.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem6.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem6.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem6.Name = "BarButtonItem6"
        '
        'BarButtonItem7
        '
        Me.BarButtonItem7.Caption = "Pagos de Facturas"
        Me.BarButtonItem7.Id = 15
        Me.BarButtonItem7.ImageOptions.Image = CType(resources.GetObject("BarButtonItem7.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem7.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem7.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem7.Name = "BarButtonItem7"
        '
        'BarButtonItem8
        '
        Me.BarButtonItem8.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.BarButtonItem8.Caption = "Usuario: "
        Me.BarButtonItem8.Id = 16
        Me.BarButtonItem8.ImageOptions.Image = CType(resources.GetObject("BarButtonItem8.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem8.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem8.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem8.Name = "BarButtonItem8"
        '
        'rb_btn_usuario
        '
        Me.rb_btn_usuario.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.rb_btn_usuario.Caption = "User"
        Me.rb_btn_usuario.Id = 17
        Me.rb_btn_usuario.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb_btn_usuario.ItemAppearance.Normal.Options.UseFont = True
        Me.rb_btn_usuario.Name = "rb_btn_usuario"
        '
        'rb_btn_datos
        '
        Me.rb_btn_datos.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.rb_btn_datos.Caption = "Name"
        Me.rb_btn_datos.Id = 18
        Me.rb_btn_datos.ImageOptions.Image = CType(resources.GetObject("rb_btn_datos.ImageOptions.Image"), System.Drawing.Image)
        Me.rb_btn_datos.ImageOptions.LargeImage = CType(resources.GetObject("rb_btn_datos.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.rb_btn_datos.Name = "rb_btn_datos"
        '
        'rb_btn_rol
        '
        Me.rb_btn_rol.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.rb_btn_rol.Caption = "Rol"
        Me.rb_btn_rol.Id = 19
        Me.rb_btn_rol.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rb_btn_rol.ItemAppearance.Normal.Options.UseFont = True
        Me.rb_btn_rol.Name = "rb_btn_rol"
        '
        'BarButtonItem9
        '
        Me.BarButtonItem9.Caption = "Zona de Pago"
        Me.BarButtonItem9.Id = 20
        Me.BarButtonItem9.ImageOptions.Image = CType(resources.GetObject("BarButtonItem9.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem9.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem9.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem9.Name = "BarButtonItem9"
        '
        'BarButtonItem10
        '
        Me.BarButtonItem10.Caption = "Tipo de Cambio"
        Me.BarButtonItem10.Id = 21
        Me.BarButtonItem10.ImageOptions.Image = CType(resources.GetObject("BarButtonItem10.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem10.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem10.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem10.Name = "BarButtonItem10"
        '
        'BarButtonItem11
        '
        Me.BarButtonItem11.Caption = "Sesión de Caja"
        Me.BarButtonItem11.Id = 22
        Me.BarButtonItem11.ImageOptions.Image = CType(resources.GetObject("BarButtonItem11.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem11.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem11.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem11.Name = "BarButtonItem11"
        '
        'BarButtonItem12
        '
        Me.BarButtonItem12.Caption = "Fecha de Apertura"
        Me.BarButtonItem12.Id = 23
        Me.BarButtonItem12.ImageOptions.Image = CType(resources.GetObject("BarButtonItem12.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem12.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem12.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem12.Name = "BarButtonItem12"
        '
        'BarButtonItem13
        '
        Me.BarButtonItem13.Caption = "Estado de Cuenta"
        Me.BarButtonItem13.Id = 24
        Me.BarButtonItem13.ImageOptions.Image = CType(resources.GetObject("BarButtonItem13.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem13.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem13.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem13.Name = "BarButtonItem13"
        '
        'btn_reporte
        '
        Me.btn_reporte.Caption = "Pagos de Facturas"
        Me.btn_reporte.Id = 28
        Me.btn_reporte.ImageOptions.Image = CType(resources.GetObject("btn_reporte.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_reporte.ImageOptions.LargeImage = CType(resources.GetObject("btn_reporte.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.btn_reporte.Name = "btn_reporte"
        '
        'BarButtonItem14
        '
        Me.BarButtonItem14.Caption = "Estado de Cuenta"
        Me.BarButtonItem14.Id = 30
        Me.BarButtonItem14.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem14.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem14.Name = "BarButtonItem14"
        '
        'BarButtonItem15
        '
        Me.BarButtonItem15.Caption = "Solicitud de Compras"
        Me.BarButtonItem15.Id = 31
        Me.BarButtonItem15.ImageOptions.Image = CType(resources.GetObject("BarButtonItem15.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem15.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem15.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem15.Name = "BarButtonItem15"
        '
        'BarButtonItem16
        '
        Me.BarButtonItem16.Caption = "BarButtonItem16"
        Me.BarButtonItem16.Id = 32
        Me.BarButtonItem16.Name = "BarButtonItem16"
        Me.BarButtonItem16.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItem17
        '
        Me.BarButtonItem17.Caption = "Formato Retención IMI"
        Me.BarButtonItem17.Id = 34
        Me.BarButtonItem17.ImageOptions.Image = CType(resources.GetObject("BarButtonItem17.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem17.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem17.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem17.Name = "BarButtonItem17"
        Me.BarButtonItem17.Tag = "Configurar Formato IMI"
        '
        'BarButtonItem18
        '
        Me.BarButtonItem18.Caption = "Impuestos & Retenciones"
        Me.BarButtonItem18.Id = 35
        Me.BarButtonItem18.ImageOptions.Image = CType(resources.GetObject("BarButtonItem18.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem18.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem18.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem18.Name = "BarButtonItem18"
        Me.BarButtonItem18.Tag = ""
        '
        'BarButtonItem19
        '
        Me.BarButtonItem19.Caption = "Impuestos"
        Me.BarButtonItem19.Id = 36
        Me.BarButtonItem19.ImageOptions.Image = CType(resources.GetObject("BarButtonItem19.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem19.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem19.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem19.Name = "BarButtonItem19"
        '
        'BarButtonItem20
        '
        Me.BarButtonItem20.Caption = "Oficinas"
        Me.BarButtonItem20.Id = 37
        Me.BarButtonItem20.ImageOptions.Image = CType(resources.GetObject("BarButtonItem20.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem20.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem20.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem20.Name = "BarButtonItem20"
        '
        'BarButtonItem21
        '
        Me.BarButtonItem21.Caption = "Talonarios"
        Me.BarButtonItem21.Id = 38
        Me.BarButtonItem21.ImageOptions.Image = CType(resources.GetObject("BarButtonItem21.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem21.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem21.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem21.Name = "BarButtonItem21"
        '
        'BarSubItem1
        '
        Me.BarSubItem1.Caption = "BarSubItem1"
        Me.BarSubItem1.Id = 39
        Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem2)})
        Me.BarSubItem1.Name = "BarSubItem1"
        '
        'BarStaticItem2
        '
        Me.BarStaticItem2.Caption = "BarStaticItem2"
        Me.BarStaticItem2.Id = 40
        Me.BarStaticItem2.ImageOptions.ImageIndex = 0
        Me.BarStaticItem2.Name = "BarStaticItem2"
        '
        'rpt_dinamico
        '
        Me.rpt_dinamico.Caption = "Dinamico"
        Me.rpt_dinamico.Id = 41
        Me.rpt_dinamico.ImageOptions.Image = CType(resources.GetObject("rpt_dinamico.ImageOptions.Image"), System.Drawing.Image)
        Me.rpt_dinamico.ImageOptions.LargeImage = CType(resources.GetObject("rpt_dinamico.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.rpt_dinamico.Name = "rpt_dinamico"
        '
        'BarButtonItem22
        '
        Me.BarButtonItem22.Caption = "Totalizado"
        Me.BarButtonItem22.Id = 42
        Me.BarButtonItem22.Name = "BarButtonItem22"
        '
        'BarButtonItem23
        '
        Me.BarButtonItem23.Id = 43
        Me.BarButtonItem23.Name = "BarButtonItem23"
        '
        'BarButtonItem24
        '
        Me.BarButtonItem24.Caption = "Formulario Consolidado"
        Me.BarButtonItem24.Id = 44
        Me.BarButtonItem24.ImageOptions.Image = CType(resources.GetObject("BarButtonItem24.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem24.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem24.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem24.LargeWidth = 70
        Me.BarButtonItem24.Name = "BarButtonItem24"
        Me.BarButtonItem24.Tag = ""
        '
        'BarButtonItem25
        '
        Me.BarButtonItem25.Caption = "Listado General"
        Me.BarButtonItem25.Id = 45
        Me.BarButtonItem25.ImageOptions.Image = CType(resources.GetObject("BarButtonItem25.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem25.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem25.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem25.LargeWidth = 70
        Me.BarButtonItem25.Name = "BarButtonItem25"
        '
        'BarButtonItem26
        '
        Me.BarButtonItem26.Caption = "IVA"
        Me.BarButtonItem26.Id = 46
        Me.BarButtonItem26.ImageOptions.Image = CType(resources.GetObject("BarButtonItem26.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem26.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem26.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem26.LargeWidth = 70
        Me.BarButtonItem26.Name = "BarButtonItem26"
        '
        'btn_IR_full
        '
        Me.btn_IR_full.Caption = "IR"
        Me.btn_IR_full.Id = 47
        Me.btn_IR_full.ImageOptions.Image = CType(resources.GetObject("btn_IR_full.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_IR_full.ImageOptions.LargeImage = CType(resources.GetObject("btn_IR_full.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.btn_IR_full.LargeWidth = 70
        Me.btn_IR_full.Name = "btn_IR_full"
        '
        'BarButtonItem28
        '
        Me.BarButtonItem28.Caption = "IMI"
        Me.BarButtonItem28.Id = 48
        Me.BarButtonItem28.ImageOptions.Image = CType(resources.GetObject("BarButtonItem28.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem28.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem28.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem28.LargeWidth = 70
        Me.BarButtonItem28.Name = "BarButtonItem28"
        '
        'BarButtonItem27
        '
        Me.BarButtonItem27.Caption = "Salvar"
        Me.BarButtonItem27.Id = 49
        Me.BarButtonItem27.ImageOptions.Image = CType(resources.GetObject("BarButtonItem27.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem27.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem27.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem27.Name = "BarButtonItem27"
        '
        'btnAuditoria
        '
        Me.btnAuditoria.Caption = "Auditoria"
        Me.btnAuditoria.Id = 50
        Me.btnAuditoria.ImageOptions.Image = CType(resources.GetObject("btnAuditoria.ImageOptions.Image"), System.Drawing.Image)
        Me.btnAuditoria.ImageOptions.LargeImage = CType(resources.GetObject("btnAuditoria.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.btnAuditoria.Name = "btnAuditoria"
        '
        'RibbonPage3
        '
        Me.RibbonPage3.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup4, Me.RibbonPageGroup11})
        Me.RibbonPage3.Image = CType(resources.GetObject("RibbonPage3.Image"), System.Drawing.Image)
        Me.RibbonPage3.Name = "RibbonPage3"
        Me.RibbonPage3.Text = "Configurar"
        '
        'RibbonPageGroup4
        '
        Me.RibbonPageGroup4.AllowTextClipping = False
        Me.RibbonPageGroup4.ItemLinks.Add(Me.BarButtonItem5)
        Me.RibbonPageGroup4.ItemLinks.Add(Me.BarButtonItem27)
        Me.RibbonPageGroup4.Name = "RibbonPageGroup4"
        Me.RibbonPageGroup4.Text = "Configuración"
        '
        'RibbonPageGroup11
        '
        Me.RibbonPageGroup11.ImageUri.Uri = "Show"
        Me.RibbonPageGroup11.ItemLinks.Add(Me.btnAuditoria)
        Me.RibbonPageGroup11.Name = "RibbonPageGroup11"
        Me.RibbonPageGroup11.Text = "RibbonPageGroup11"
        '
        'RibbonPage1
        '
        Me.RibbonPage1.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup1})
        Me.RibbonPage1.Image = CType(resources.GetObject("RibbonPage1.Image"), System.Drawing.Image)
        Me.RibbonPage1.Name = "RibbonPage1"
        Me.RibbonPage1.Text = "Catalogo"
        Me.RibbonPage1.Visible = False
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btn_Agente)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btn_Cliente)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.BarButtonItem2)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.BarButtonItem1)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.BarButtonItem6)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.BarButtonItem9)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.BarButtonItem10)
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.Text = "Catalogo"
        '
        'RibbonPage2
        '
        Me.RibbonPage2.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup2, Me.RibbonPageGroup3})
        Me.RibbonPage2.Image = CType(resources.GetObject("RibbonPage2.Image"), System.Drawing.Image)
        Me.RibbonPage2.Name = "RibbonPage2"
        Me.RibbonPage2.Text = "Facturación"
        Me.RibbonPage2.Visible = False
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.BarButtonItem3)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.Text = "Facturas"
        '
        'RibbonPageGroup3
        '
        Me.RibbonPageGroup3.AllowTextClipping = False
        Me.RibbonPageGroup3.ItemLinks.Add(Me.BarButtonItem4)
        Me.RibbonPageGroup3.Name = "RibbonPageGroup3"
        Me.RibbonPageGroup3.Text = "Mercado de Ocasión"
        '
        'RibbonPage4
        '
        Me.RibbonPage4.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup5})
        Me.RibbonPage4.Image = CType(resources.GetObject("RibbonPage4.Image"), System.Drawing.Image)
        Me.RibbonPage4.Name = "RibbonPage4"
        Me.RibbonPage4.Text = "Cajas"
        Me.RibbonPage4.Visible = False
        '
        'RibbonPageGroup5
        '
        Me.RibbonPageGroup5.ItemLinks.Add(Me.BarButtonItem12)
        Me.RibbonPageGroup5.ItemLinks.Add(Me.BarButtonItem11)
        Me.RibbonPageGroup5.ItemLinks.Add(Me.BarButtonItem7)
        Me.RibbonPageGroup5.Name = "RibbonPageGroup5"
        Me.RibbonPageGroup5.Text = "RibbonPageGroup5"
        '
        'RibbonPage5
        '
        Me.RibbonPage5.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup6})
        Me.RibbonPage5.Name = "RibbonPage5"
        Me.RibbonPage5.Text = "Reporte"
        Me.RibbonPage5.Visible = False
        '
        'RibbonPageGroup6
        '
        Me.RibbonPageGroup6.AllowTextClipping = False
        Me.RibbonPageGroup6.ItemLinks.Add(Me.btn_reporte)
        Me.RibbonPageGroup6.ItemLinks.Add(Me.BarButtonItem14)
        Me.RibbonPageGroup6.Name = "RibbonPageGroup6"
        Me.RibbonPageGroup6.Text = "Reportes"
        '
        'RibbonPage6
        '
        Me.RibbonPage6.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup7})
        Me.RibbonPage6.Image = CType(resources.GetObject("RibbonPage6.Image"), System.Drawing.Image)
        Me.RibbonPage6.Name = "RibbonPage6"
        Me.RibbonPage6.Text = "Compra"
        Me.RibbonPage6.Visible = False
        '
        'RibbonPageGroup7
        '
        Me.RibbonPageGroup7.AllowMinimize = False
        Me.RibbonPageGroup7.AllowTextClipping = False
        Me.RibbonPageGroup7.ItemLinks.Add(Me.BarButtonItem15)
        Me.RibbonPageGroup7.Name = "RibbonPageGroup7"
        Me.RibbonPageGroup7.Text = "Compras"
        '
        'RibbonPage7
        '
        Me.RibbonPage7.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup8})
        Me.RibbonPage7.Image = CType(resources.GetObject("RibbonPage7.Image"), System.Drawing.Image)
        Me.RibbonPage7.Name = "RibbonPage7"
        Me.RibbonPage7.Text = "Impuestos"
        '
        'RibbonPageGroup8
        '
        Me.RibbonPageGroup8.ItemLinks.Add(Me.BarButtonItem20)
        Me.RibbonPageGroup8.ItemLinks.Add(Me.BarButtonItem21)
        Me.RibbonPageGroup8.ItemLinks.Add(Me.Plantilla)
        Me.RibbonPageGroup8.ItemLinks.Add(Me.BarButtonItem17)
        Me.RibbonPageGroup8.ItemLinks.Add(Me.BarButtonItem18)
        Me.RibbonPageGroup8.Name = "RibbonPageGroup8"
        Me.RibbonPageGroup8.Text = "Opciones"
        '
        'RibbonPage8
        '
        Me.RibbonPage8.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup10})
        Me.RibbonPage8.Image = CType(resources.GetObject("RibbonPage8.Image"), System.Drawing.Image)
        Me.RibbonPage8.Name = "RibbonPage8"
        Me.RibbonPage8.Text = "Reportes"
        '
        'RibbonPageGroup10
        '
        Me.RibbonPageGroup10.ItemLinks.Add(Me.BarButtonItem24)
        Me.RibbonPageGroup10.ItemLinks.Add(Me.BarButtonItem25)
        Me.RibbonPageGroup10.ItemLinks.Add(Me.btn_IR_full)
        Me.RibbonPageGroup10.ItemLinks.Add(Me.BarButtonItem28)
        Me.RibbonPageGroup10.ItemLinks.Add(Me.BarButtonItem26)
        Me.RibbonPageGroup10.Name = "RibbonPageGroup10"
        Me.RibbonPageGroup10.Text = "Consolidados"
        '
        'RibbonStatusBar
        '
        Me.RibbonStatusBar.ItemLinks.Add(Me.lbl_Data)
        Me.RibbonStatusBar.ItemLinks.Add(Me.lbl_server)
        Me.RibbonStatusBar.ItemLinks.Add(Me.BarButtonItem8)
        Me.RibbonStatusBar.ItemLinks.Add(Me.rb_btn_usuario)
        Me.RibbonStatusBar.ItemLinks.Add(Me.rb_btn_datos)
        Me.RibbonStatusBar.ItemLinks.Add(Me.rb_btn_rol)
        Me.RibbonStatusBar.Location = New System.Drawing.Point(0, 418)
        Me.RibbonStatusBar.Name = "RibbonStatusBar"
        Me.RibbonStatusBar.Ribbon = Me.RibbonControl
        Me.RibbonStatusBar.Size = New System.Drawing.Size(1095, 31)
        '
        'XtraTabbedMdiManager1
        '
        Me.XtraTabbedMdiManager1.MdiParent = Me
        '
        'RibbonPageGroup9
        '
        Me.RibbonPageGroup9.ItemLinks.Add(Me.rpt_dinamico)
        Me.RibbonPageGroup9.ItemLinks.Add(Me.BarButtonItem22)
        Me.RibbonPageGroup9.ItemLinks.Add(Me.BarButtonItem23)
        Me.RibbonPageGroup9.Name = "RibbonPageGroup9"
        Me.RibbonPageGroup9.Text = "Consolidados"
        '
        'RibbonForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1095, 449)
        Me.Controls.Add(Me.RibbonStatusBar)
        Me.Controls.Add(Me.RibbonControl)
        Me.IsMdiContainer = True
        Me.Name = "RibbonForm"
        Me.Ribbon = Me.RibbonControl
        Me.StatusBar = Me.RibbonStatusBar
        Me.Text = "Tax -Impuestos  & Retenciones"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RibbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents RibbonPage1 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonStatusBar As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Friend WithEvents btn_Agente As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btn_Cliente As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents XtraTabbedMdiManager1 As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPage2 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents Plantilla As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup3 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents lbl_Data As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents lbl_server As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents BarButtonItem5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPage3 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup4 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPage4 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents BarButtonItem7 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup5 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem8 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rb_btn_usuario As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rb_btn_datos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents rb_btn_rol As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem9 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem10 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem11 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem12 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem13 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPage5 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents btn_reporte As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup6 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem14 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem15 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPage6 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup7 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem16 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem17 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem18 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPage7 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup8 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem19 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem20 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem21 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarStaticItem2 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents RibbonPage8 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents rpt_dinamico As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem22 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem23 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup9 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem24 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup10 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem25 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem26 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btn_IR_full As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem28 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem27 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnAuditoria As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup11 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
End Class
