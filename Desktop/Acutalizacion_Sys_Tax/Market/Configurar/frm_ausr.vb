Imports System.Data
Imports System.Data.SqlClient
Imports System.DBNull
Public Class frm_ausr
    Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion)
    Dim da As SqlDataAdapter
    Dim dt As DataTable
    Dim A As New Auditoria

    Public accion As String = String.Empty
    Public id As String = String.Empty

    Public Oficina_Add As New DataTable("Oficina_Add")
    Public Oficina_All As New DataTable("Oficina_All")

    Private Sub BtnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnRegresar.Click
        Me.Close()
    End Sub
    Function Revisar_Usr(ByVal usr As String) As Boolean
        Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
        Dim DT_usr As DataTable = Rutinas_SGC.obtener_datos("Select * from usuarios Where usuarios.usuario='" & usr & "'")

        Try
            If DT_usr.Rows.Count = 0 Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            MsgBox(ex.Message & " " & "Error al consultar si Alias Existe", MsgBoxStyle.Information, "Aviso Importante...")
        End Try

    End Function
    Sub GUARDAR_USUARIO()
        Dim sql_usr As String = "INSERT INTO USUARIOS (USUARIO,CLAVE,NOMBRES,APELLIDOS,cod_rol,FOTO,cedula,celular,telefono,fecha_ingreso,fecha_baja,estado,AP_Fecha,AP_Sesion,AP_Arqueo,AP_Cliente,AP_Anular,AP_Rectificar,AP_Tarifa,AP_Duplicado,AP_Servicios) VALUES (@USUARIO,@CLAVE,@NOMBRES,@APELLIDOS,@cod_rol,@FOTO,@cedula,@celular,@telefono,@fecha_ingreso,@fecha_baja,@estado,@AP_Fecha,@AP_Sesion,@AP_Arqueo,@AP_Cliente,@AP_Anular,@AP_Rectificar,@AP_Tarifa,@AP_Duplicado,@AP_Servicios)"
        Dim cmd As New SqlCommand(sql_usr, conexion)
        Dim transac As SqlTransaction

        Try
            conexion.Open()
            transac = conexion.BeginTransaction

            cmd.Parameters.Add("usuario", SqlDbType.NVarChar)
            cmd.Parameters("usuario").Value = Me.txtusr.Text.Trim

            cmd.Parameters.Add("clave", SqlDbType.NVarChar)
            cmd.Parameters("clave").Value = Me.txtcontraseña.Text.Trim

            cmd.Parameters.Add("nombres", SqlDbType.NVarChar)
            cmd.Parameters("nombres").Value = Me.txtnombre.Text.Trim.ToUpper

            cmd.Parameters.Add("apellidos", SqlDbType.NVarChar)
            cmd.Parameters("apellidos").Value = Me.txtapellidos.Text.Trim.ToUpper

            cmd.Parameters.Add("cedula", SqlDbType.NVarChar)
            cmd.Parameters("cedula").Value = Me.txt_cedula.Text.Trim

            cmd.Parameters.Add("celular", SqlDbType.NVarChar)
            cmd.Parameters("celular").Value = Me.txt_celular.Text.Trim

            cmd.Parameters.Add("telefono", SqlDbType.NVarChar)
            cmd.Parameters("telefono").Value = Me.txt_telefono.Text.Trim

            cmd.Parameters.Add("cod_rol", SqlDbType.NVarChar)
            cmd.Parameters("cod_rol").Value = Me.cmb_rol.EditValue

            cmd.Parameters.Add("fecha_ingreso", SqlDbType.SmallDateTime)
            cmd.Parameters("fecha_ingreso").Value = Me.dpt_fecha_ingreso.EditValue

            cmd.Parameters.Add("fecha_baja", SqlDbType.SmallDateTime)
            cmd.Parameters("fecha_baja").Value = Me.dpt_baja.EditValue

            cmd.Parameters.Add("estado", SqlDbType.Bit)
            cmd.Parameters("estado").Value = Me.opt_estado.EditValue

            cmd.Parameters.Add("AP_Fecha", SqlDbType.Bit)
            cmd.Parameters("AP_Fecha").Value = Me.opt_ac_fecha.EditValue

            cmd.Parameters.Add("AP_Sesion", SqlDbType.Bit)
            cmd.Parameters("AP_Sesion").Value = Me.opt_ac_sesion.EditValue

            cmd.Parameters.Add("AP_Arqueo", SqlDbType.Bit)
            cmd.Parameters("AP_Arqueo").Value = Me.opt_arqueo.EditValue

            cmd.Parameters.Add("AP_Cliente", SqlDbType.Bit)
            cmd.Parameters("AP_Cliente").Value = Me.opt_cliente.EditValue

            cmd.Parameters.Add("AP_Anular", SqlDbType.Bit)
            cmd.Parameters("AP_Anular").Value = Me.opt_arqueo.EditValue

            cmd.Parameters.Add("AP_rectificar", SqlDbType.Bit)
            cmd.Parameters("AP_rectificar").Value = Me.opt_cliente.EditValue

            cmd.Parameters.Add("AP_Tarifa", SqlDbType.Bit)
            cmd.Parameters("AP_Tarifa").Value = Me.opt_tarifas.EditValue

            cmd.Parameters.Add("AP_Duplicado", SqlDbType.Bit)
            cmd.Parameters("AP_Duplicado").Value = Me.opt_duplicado.EditValue

            cmd.Parameters.Add("AP_Servicios", SqlDbType.Bit)
            cmd.Parameters("AP_Servicios").Value = Me.opt_servicios.EditValue

            If Me.pic_foto.EditValue Is Nothing Then
                cmd.Parameters.Add("FOTO", SqlDbType.Image)
                cmd.Parameters("FOTO").Value = DBNull.Value
            Else
                cmd.Parameters.Add("FOTO", SqlDbType.Image)
                cmd.Parameters("FOTO").Value = Me.pic_foto.EditValue
            End If

            cmd.Transaction = transac
            cmd.ExecuteNonQuery()
            A.InsertarAuditoria(Usuario_, "Click en Guardar", "Formulario Ingresar/Modificar Usuario", " Agrego el Usuario: " + Me.txtusr.Text.Trim)

            ''' Cajas Usuarios 

            '   Agregar Cajas a Usuario
            For i As Integer = 0 To Me.v_fabrica.DataRowCount - 1
                If Me.v_fabrica.GetRowCellValue(i, "Asignar") Then
                    Dim _cmd As New SqlCommand("IUCajaUsuario", conexion)
                    _cmd.CommandType = CommandType.StoredProcedure

                    _cmd.Parameters.Add("CajaID", SqlDbType.Int)
                    _cmd.Parameters("CajaID").Value = Me.v_fabrica.GetRowCellValue(i, "CajaID")

                    _cmd.Parameters.Add("Usuario", SqlDbType.NVarChar)
                    _cmd.Parameters("Usuario").Value = Me.txtusr.Text

                    _cmd.Transaction = transac
                    _cmd.ExecuteNonQuery()
                    A.InsertarAuditoria(Usuario_, "Configuraciones de Usuario", "Formulario Ingresar/Modificar Usuario", " Agrego la caja: " + Me.v_fabrica.GetRowCellValue(i, "CajaID") + " | al usuario: " + txtusr.Text)

                End If
            Next

            '   Agregar Oficinas a los Usuario
            For i As Integer = 0 To Me.v_oficinasasignadas.DataRowCount - 1
                Dim _cmd As New SqlCommand("OficinaUsuarioInsert", conexion)
                _cmd.CommandType = CommandType.StoredProcedure

                _cmd.Parameters.Add("OficinaID", SqlDbType.Int)
                _cmd.Parameters("OficinaID").Value = Me.v_oficinasasignadas.GetRowCellValue(i, "OficinaID")

                _cmd.Parameters.Add("UsuarioID", SqlDbType.NVarChar)
                _cmd.Parameters("UsuarioID").Value = Me.txtusr.Text

                _cmd.Transaction = transac
                _cmd.ExecuteNonQuery()
                A.InsertarAuditoria(Usuario_, "Oficinas de Usuario", "Formulario Ingresar/Modificar Usuario", " Agrego la oficina: " + Me.v_oficinasasignadas.GetRowCellValue(i, "OficinaID") + " | al usuario: " + txtusr.Text)

            Next

            transac.Commit()
            conexion.Close()

        Catch ex As Exception

            transac.Rollback()
            conexion.Close()
            MsgBox(ex.Message & " " & "Error al Guardar", MsgBoxStyle.Information, "Aviso Importante...")
        End Try
    End Sub
    
    Private Sub btnguardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnguardar.Click
        If Me.txtusr.Text.Trim.Length = 0 Then
            MessageBox.Show("Usuarios, Dato Requerido...", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Question)
            Exit Sub
        End If
        If Me.txtnombre.Text.Trim.Length = 0 Then
            MessageBox.Show("Nombres, Dato Requerido...", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Question)
            Exit Sub
        End If
        If Me.txtapellidos.Text.Trim.Length = 0 Then
            MessageBox.Show("Apellidos, Dato Requerido...", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Question)
            Exit Sub
        End If
        If Me.cmb_rol.EditValue = Nothing Then
            MessageBox.Show("Rol, Dato Requerido...", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Question)
            Exit Sub
        End If
        If Me.dpt_fecha_ingreso.EditValue = Nothing Then
            MessageBox.Show("Fecha de Ingreso, Dato Requerido...", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Question)
            Exit Sub
        End If

        If String.Compare(txtcontraseña.Text.Trim, txtconfirmar.Text.Trim, False) Then
            MessageBox.Show("Contraseñas no son Iguales..., Dato Requerido...", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Question)
            Exit Sub
        End If



        If accion = "New" Then

            If Revisar_Usr(Me.txtusr.Text.Trim) Then
                MessageBox.Show("Error ya existe un Alias con este Usuario..., Dato Requerido...", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Question)
                Exit Sub
            End If

            Dim RSP As Integer = MessageBox.Show("Esta Seguro que desea Ingresar un Usuario", "Aviso Importante - Ingreso de Usuario", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
            If RSP = 6 Then
                GUARDAR_USUARIO()
                A.InsertarAuditoria(Usuario_, "Click en Guardar Nuevo", "Formulario Ingresar/Modificar Usuario", sender.text + " Guardo el Usuario: " + txtusr.Text + " | " + sender.tag)

                My.Forms.p_usr.cargar_datos()

                Me.Close()
            End If
        Else


            Dim RSP As Integer = MessageBox.Show("Esta Seguro que desea Modificar el Usuario", "Aviso Importante - Ingreso de Usuario", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
            If RSP = 6 Then
                GUARDAR_USUARIO_MODIFICA()
                A.InsertarAuditoria(Usuario_, "Click en Guardar Modificar", "Formulario Ingresar/Modificar Usuario", sender.text + " Modifico el Usuario: " + txtusr.Text + " | " + sender.tag)
                cambiar_datos_inicio_sesion()
                My.Forms.p_usr.cargar_datos()
                Me.Close()
            End If

        End If

       
    End Sub
    Private Sub cambiar_datos_inicio_sesion()

        Dim sql As String = "update usuarios set clave=@clave where usuario=@usuario"
        Dim da As New SqlDataAdapter(sql, conexion)

        da.SelectCommand.Parameters.Add("clave", SqlDbType.NVarChar)
        da.SelectCommand.Parameters("clave").Value = Me.txtcontraseña.Text.Trim

        da.SelectCommand.Parameters.Add("usuario", SqlDbType.NVarChar)
        da.SelectCommand.Parameters("usuario").Value = Me.txtusr.Text.Trim

        conexion.Open()
        da.SelectCommand.ExecuteNonQuery()
        conexion.Close()

    End Sub
 
    Private Sub frm_ausr_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Me.txtapellidos.Text = String.Empty
        Me.txtnombre.Text = String.Empty
        Me.txtusr.Text = String.Empty
        Me.txtconfirmar.Text = String.Empty
        Me.txtcontraseña.Text = String.Empty
    End Sub

    Private Sub frm_ausr_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Cargar_Usuario()
        Crear_Datatable()
       

        If accion = "New" Then

            CargarRutasItineariosDisponbiles()
            cargar_datos()

        Else
            CargarRutasItineariosDisponbiles()
            cargar_datos()
        End If

    End Sub
    Private Sub cargar_datos()
        Me.gridData.DataSource = Oficina_All
        Dim dt As New DataTable
        Dim dt_add As New DataTable

        dt = Rutinas_SGC.obtener_datos("OficinasAsignadasGetList '" & Me.txtusr.Text & "'")

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim d As DataRow
            d = Oficina_Add.NewRow()
            d("OficinaID") = dt.Rows(i).Item("OficinaID")
            d("Oficina") = dt.Rows(i).Item("Oficina")

            Oficina_Add.Rows.Add(d)

        Next
        Me.g_oficinasAsignadas.DataSource = Oficina_Add

    End Sub
    Public Shared Function RutasItineariosDisponbilesGetList(UsuarioID As String) As DataTable

        Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
        Dim cmd As New SqlDataAdapter("[OficinasDisponbilesGetList]", conexion)
        cmd.SelectCommand.CommandType = CommandType.StoredProcedure

        Dim dt As New DataTable

        cmd.SelectCommand.Parameters.Add("UsuarioID", SqlDbType.NVarChar)
        cmd.SelectCommand.Parameters("UsuarioID").Value = UsuarioID

        cmd.Fill(dt)
        Return dt

    End Function
    Private Sub CargarRutasItineariosDisponbiles()
        Dim dt As New DataTable
        dt = RutasItineariosDisponbilesGetList(Me.txtusr.Text)

        For i As Integer = 0 To dt.Rows.Count - 1
            Oficina_All.Rows.Add(dt.Rows(i).ItemArray)
        Next
        Me.g_oficinadisponibles.DataSource = Oficina_All

    End Sub
    Public Sub Crear_Datatable()

        ' Add Two columns to the table.
        ' Table Empresa_Add Contiene las Empresas Asignadas
        Oficina_Add.Columns.Add("OficinaID", GetType([String]))
        Oficina_Add.Columns.Add("Oficina", GetType([String]))

        Dim keys(0) As DataColumn
        keys(0) = Oficina_Add.Columns("OficinaID")
        Oficina_Add.PrimaryKey = keys

        ' Add Two columns to the table.
        ' Table Empresa_All Contiene Todas las Empresas

        Oficina_All.Columns.Add("OficinaID", GetType([String]))
        Oficina_All.Columns.Add("Oficina", GetType([String]))

        Dim keys_(0) As DataColumn
        keys_(0) = Oficina_All.Columns("OficinaID")
        Oficina_All.PrimaryKey = keys_

    End Sub

    Private Sub Cargar_Usuario()
        Me.cmb_rol.Properties.DataSource = Rutinas_SGC.obtener_datos("SELECT ru.cod_rol,ru.rol FROM rol_user AS ru")
        Me.cmb_rol.Properties.DisplayMember = "rol"
        Me.cmb_rol.Properties.ValueMember = "cod_rol"

        If accion = "New" Then
            Me.Text = "NUEVO USUARIO"
            'Me.gridData.DataSource = Caja.CajaGetList("%")


            Me.txtusr.BackColor = Color.White
            Me.txtusr.ReadOnly = False

        Else

            Me.Text = "MODIFICAR USUARIO"

            Me.txtusr.BackColor = Color.Gainsboro
            Me.txtusr.ReadOnly = True

            'If Caja.CajasUsuariosGetList(id).Rows.Count = 0 Then
            '    Me.gridData.DataSource = Caja.CajaGetList("%")
            'Else
            '    Me.gridData.DataSource = Caja.CajasUsuariosGetListSelect(id)
            'End If


            Dim dt_sql As DataTable = Rutinas_SGC.obtener_datos("pro_list_user '" & id & "','%'")

            Try
                If dt_sql.Rows.Count > 0 Then

                    txtusr.Text = dt_sql.Rows(0).Item("USUARIO").ToString

                    txtnombre.Text = dt_sql.Rows(0).Item("Nombres").ToString
                    txtapellidos.Text = dt_sql.Rows(0).Item("Apellidos").ToString

                    txtcontraseña.Text = dt_sql.Rows(0).Item("clave").ToString
                    txtconfirmar.Text = dt_sql.Rows(0).Item("clave").ToString
                    Me.cmb_rol.EditValue = dt_sql.Rows(0).Item("cod_rol")
                    Me.txt_cedula.Text = dt_sql.Rows(0).Item("cedula").ToString
                    Me.txt_celular.Text = dt_sql.Rows(0).Item("celular").ToString
                    Me.txt_telefono.Text = dt_sql.Rows(0).Item("telefono").ToString
                    Me.dpt_fecha_ingreso.EditValue = dt_sql.Rows(0).Item("fecha_ingreso")
                    Me.dpt_baja.EditValue = dt_sql.Rows(0).Item("fecha_baja")

                    Me.opt_estado.EditValue = dt_sql.Rows(0).Item("estado")

                    If dt_sql.Rows(0).Item("foto") Is DBNull.Value Then
                        Me.pic_foto.EditValue = Nothing
                    Else
                        Me.pic_foto.EditValue = dt_sql.Rows(0).Item("foto")
                    End If

                    Me.opt_ac_fecha.EditValue = dt_sql.Rows(0).Item("AP_Fecha")
                    Me.opt_ac_sesion.EditValue = dt_sql.Rows(0).Item("AP_Sesion")
                    Me.opt_arqueo.EditValue = dt_sql.Rows(0).Item("AP_Arqueo")
                    Me.opt_cliente.EditValue = dt_sql.Rows(0).Item("AP_Cliente")

                    Me.opt_anular.EditValue = dt_sql.Rows(0).Item("AP_Anular")
                    Me.opt_rectificar.EditValue = dt_sql.Rows(0).Item("AP_rectificar")

                    Me.opt_tarifas.EditValue = dt_sql.Rows(0).Item("AP_Tarifa")
                    Me.opt_duplicado.EditValue = dt_sql.Rows(0).Item("AP_Duplicado")
                    Me.opt_servicios.EditValue = dt_sql.Rows(0).Item("AP_Servicios")

                End If
            Catch ex As Exception
                conexion.Close()
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub dpt_fecha_ingreso_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dpt_fecha_ingreso.EditValueChanged
        Me.dpt_baja.EditValue = Me.dpt_fecha_ingreso.EditValue
    End Sub
    Sub GUARDAR_USUARIO_MODIFICA()

        Dim cmd As New SqlCommand("IU_Usuario", conexion)
        cmd.CommandType = CommandType.StoredProcedure
        Dim transac As SqlTransaction

        Try
            conexion.Open()
            transac = conexion.BeginTransaction

            cmd.Parameters.Add("usuario", SqlDbType.NVarChar)
            cmd.Parameters("usuario").Value = Me.txtusr.Text.Trim

            cmd.Parameters.Add("clave", SqlDbType.NVarChar)
            cmd.Parameters("clave").Value = Me.txtcontraseña.Text.Trim

            cmd.Parameters.Add("nombres", SqlDbType.NVarChar)
            cmd.Parameters("nombres").Value = Me.txtnombre.Text.Trim.ToUpper

            cmd.Parameters.Add("apellidos", SqlDbType.NVarChar)
            cmd.Parameters("apellidos").Value = Me.txtapellidos.Text.Trim.ToUpper

            cmd.Parameters.Add("cedula", SqlDbType.NVarChar)
            cmd.Parameters("cedula").Value = Me.txt_cedula.Text.Trim

            cmd.Parameters.Add("celular", SqlDbType.NVarChar)
            cmd.Parameters("celular").Value = Me.txt_celular.Text.Trim

            cmd.Parameters.Add("telefono", SqlDbType.NVarChar)
            cmd.Parameters("telefono").Value = Me.txt_telefono.Text.Trim

            cmd.Parameters.Add("cod_rol", SqlDbType.NVarChar)
            cmd.Parameters("cod_rol").Value = Me.cmb_rol.EditValue

            cmd.Parameters.Add("fecha_ingreso", SqlDbType.SmallDateTime)
            cmd.Parameters("fecha_ingreso").Value = Me.dpt_fecha_ingreso.EditValue

            cmd.Parameters.Add("fecha_baja", SqlDbType.SmallDateTime)
            cmd.Parameters("fecha_baja").Value = Me.dpt_baja.EditValue

            cmd.Parameters.Add("estado", SqlDbType.Bit)
            cmd.Parameters("estado").Value = Me.opt_estado.EditValue

            cmd.Parameters.Add("AP_Fecha", SqlDbType.Bit)
            cmd.Parameters("AP_Fecha").Value = Me.opt_ac_fecha.EditValue

            cmd.Parameters.Add("AP_Sesion", SqlDbType.Bit)
            cmd.Parameters("AP_Sesion").Value = Me.opt_ac_sesion.EditValue

            cmd.Parameters.Add("AP_Arqueo", SqlDbType.Bit)
            cmd.Parameters("AP_Arqueo").Value = Me.opt_arqueo.EditValue

            cmd.Parameters.Add("AP_Anular", SqlDbType.Bit)
            cmd.Parameters("AP_Anular").Value = Me.opt_arqueo.EditValue

            cmd.Parameters.Add("AP_Cliente", SqlDbType.Bit)
            cmd.Parameters("AP_Cliente").Value = Me.opt_cliente.EditValue

            cmd.Parameters.Add("AP_rectificar", SqlDbType.Bit)
            cmd.Parameters("AP_rectificar").Value = Me.opt_rectificar.EditValue

            cmd.Parameters.Add("AP_Tarifa", SqlDbType.Bit)
            cmd.Parameters("AP_Tarifa").Value = Me.opt_tarifas.EditValue

            cmd.Parameters.Add("AP_Duplicado", SqlDbType.Bit)
            cmd.Parameters("AP_Duplicado").Value = Me.opt_duplicado.EditValue

            cmd.Parameters.Add("AP_Servicios", SqlDbType.Bit)
            cmd.Parameters("AP_Servicios").Value = Me.opt_servicios.EditValue


            If Me.pic_foto.EditValue Is Nothing Then
                cmd.Parameters.Add("FOTO", SqlDbType.Image)
                cmd.Parameters("FOTO").Value = DBNull.Value
            Else
                cmd.Parameters.Add("FOTO", SqlDbType.Image)
                cmd.Parameters("FOTO").Value = Me.pic_foto.EditValue
            End If

            'If String.Compare(Me.txtusr.Text.Trim, My.Forms.mdi_dx_principal.rb_btn_usuario.Caption, True) = 0 Then

            '    nombres_ = Me.txtnombre.Text
            '    apellidos_ = Me.txtapellidos.Text
            '    Rol_ = Me.cmb_rol.Text
            '    My.Forms.mdi_dx_principal.rb_btn_datos.Caption = nombres_ + " " + apellidos_
            '    My.Forms.mdi_dx_principal.rb_pic_foto.EditValue = Me.pic_foto.EditValue

            'End If

            cmd.Transaction = transac
            cmd.ExecuteNonQuery()
            A.InsertarAuditoria(Usuario_, "Click en Guardar", "Formulario Ingresar/Modificar Usuario", " Modifico el Usuario: " + Me.txtusr.Text.Trim)


            ''' Cajas Usuarios 

            '   Agregar Cajas a Usuario
            For i As Integer = 0 To Me.v_fabrica.DataRowCount - 1
                If Me.v_fabrica.GetRowCellValue(i, "Asignar") Then
                    Dim _cmd As New SqlCommand("IUCajaUsuario", conexion)
                    _cmd.CommandType = CommandType.StoredProcedure

                    _cmd.Parameters.Add("CajaID", SqlDbType.Int)
                    _cmd.Parameters("CajaID").Value = Me.v_fabrica.GetRowCellValue(i, "CajaID")

                    _cmd.Parameters.Add("Usuario", SqlDbType.NVarChar)
                    _cmd.Parameters("Usuario").Value = Me.txtusr.Text

                    _cmd.Transaction = transac
                    _cmd.ExecuteNonQuery()
                    A.InsertarAuditoria(Usuario_, "Configuraciones de Usuario", "Formulario Ingresar/Modificar Usuario", " Agrego la caja: " + Me.v_fabrica.GetRowCellValue(i, "CajaID") + " | al usuario: " + txtusr.Text)

                End If
            Next


            '   Agregar Oficinas a los Usuario
            For i As Integer = 0 To Me.v_oficinasasignadas.DataRowCount - 1
                Dim _cmd As New SqlCommand("OficinaUsuarioInsert", conexion)
                _cmd.CommandType = CommandType.StoredProcedure

                _cmd.Parameters.Add("OficinaID", SqlDbType.Int)
                _cmd.Parameters("OficinaID").Value = Me.v_oficinasasignadas.GetRowCellValue(i, "OficinaID")

                _cmd.Parameters.Add("UsuarioID", SqlDbType.NVarChar)
                _cmd.Parameters("UsuarioID").Value = Me.txtusr.Text

                _cmd.Transaction = transac
                _cmd.ExecuteNonQuery()
                A.InsertarAuditoria(Usuario_, "Oficinas de Usuario", "Formulario Ingresar/Modificar Usuario", " Agrego la oficina: " + Me.v_oficinasasignadas.GetRowCellValue(i, "OficinaID") + " | al usuario: " + txtusr.Text)


            Next




            transac.Commit()
            conexion.Close()

        Catch ex As Exception
            transac.Rollback()
            conexion.Close()

            MsgBox(ex.Message & " " & "Error al Guardar", MsgBoxStyle.Information, "Aviso Importante...")
        End Try
    End Sub
    

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles btn_add.Click
        Add_Item_()
    End Sub
    Private Sub Add_Item_()

        If Me.v_oficinasdisponibles.RowCount = 0 Then
            Exit Sub
        End If

        If Look_Item(Me.v_oficinasdisponibles.GetRowCellValue(Me.v_oficinasdisponibles.FocusedRowHandle, "OficinaID")) Then

            Dim row_ As DataRow
            row_ = Oficina_Add.NewRow

            row_.Item("OficinaID") = Me.v_oficinasdisponibles.GetRowCellValue(Me.v_fabrica.FocusedRowHandle, "OficinaID")
            row_.Item("Oficina") = Me.v_oficinasdisponibles.GetRowCellValue(Me.v_fabrica.FocusedRowHandle, "Oficina")


            Oficina_Add.Rows.Add(row_)
            g_oficinasAsignadas.DataSource = Oficina_Add

            RemoverItem_(Me.v_oficinasdisponibles.GetRowCellValue(Me.v_oficinasdisponibles.FocusedRowHandle, "OficinaID"))
        Else
            MessageBox.Show("esta asignada", "Aviso Importante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If


    End Sub
    Private Sub RemoverItem_(codigo As String)

        Dim foundRow As DataRow

        foundRow = Oficina_All.Rows.Find(codigo)
        Oficina_All.Rows.Remove(foundRow)

        Me.g_oficinadisponibles.DataSource = Oficina_All
        Me.g_oficinasAsignadas.DataSource = Oficina_Add

    End Sub



    Function Look_Item(ByVal codigo As String) As Boolean
        If v_oficinasdisponibles.RowCount = 0 Then
            Return True
        Else
            Dim rows As DataRow
            rows = Oficina_Add.Rows.Find(codigo)
            If rows IsNot Nothing Then
                Return False
            End If
        End If
        Return True
    End Function

    Private Sub g_oficinadisponibles_DoubleClick(sender As Object, e As EventArgs) Handles g_oficinadisponibles.DoubleClick
        Add_Item_()
    End Sub
    Sub removerItem()
        If Me.v_oficinasasignadas.RowCount = 0 Then
            Exit Sub
        End If

        If revisar_grid(Me.v_oficinasasignadas.GetRowCellValue(Me.v_oficinasasignadas.FocusedRowHandle, "OficinaID")) Then

            Dim row_ As DataRow
            row_ = Oficina_All.NewRow

            row_.Item("OficinaID") = Me.v_oficinasasignadas.GetRowCellValue(Me.v_oficinasasignadas.FocusedRowHandle, "OficinaID")
            row_.Item("Oficina") = Me.v_oficinasasignadas.GetRowCellValue(Me.v_oficinasasignadas.FocusedRowHandle, "Oficina")

            Oficina_All.Rows.Add(row_)

            Me.g_oficinadisponibles.DataSource = Oficina_All

            Quitar_(Me.v_oficinasasignadas.GetRowCellValue(Me.v_oficinasasignadas.FocusedRowHandle, "OficinaID"))
        Else
            MessageBox.Show("La Oficina Selecionada ya esta asignada", "Aviso Importante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

    End Sub

    Private Sub Quitar_(codigo As String)

        Dim foundRow As DataRow

        foundRow = Oficina_Add.Rows.Find(codigo)
        Oficina_Add.Rows.Remove(foundRow)
        Me.g_oficinasAsignadas.DataSource = Oficina_Add

    End Sub

    Function revisar_grid(ByVal codigo As String) As Boolean
        If Me.v_oficinasasignadas.RowCount = 0 Then
            Return True
        Else
            Dim rows As DataRow
            rows = Oficina_All.Rows.Find(codigo)
            If rows IsNot Nothing Then
                Return False
            End If
        End If
        Return True
    End Function

    Private Sub g_oficinasAsignadas_DoubleClick(sender As Object, e As EventArgs) Handles g_oficinasAsignadas.DoubleClick
        removerItem()
    End Sub

    Private Sub btn_remove_Click(sender As Object, e As EventArgs) Handles btn_remove.Click
        removerItem()
    End Sub
End Class