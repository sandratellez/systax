﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class Oficinas

    Public Shared Function GetlistOficinas(OficinaID As String) As DataTable

        ' Obtenemos las oficinas que estan asociadas  con el usuario 

        Dim dt As New DataTable

        Try
            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("GetlistOficinas", conexion)
            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("OficinaID", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("OficinaID").Value = OficinaID

            cmd.Fill(dt)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return dt

    End Function


    Public Shared Function GetListOficinasUser(UsuarioID As String) As DataTable

        ' Obtenemos las oficinas que estan asociadas  con el usuario 

        Dim dt As New DataTable

        Try
            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("GetOficinas", conexion)
            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("UsuarioID", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("UsuarioID").Value = UsuarioID

            cmd.Fill(dt)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return dt

    End Function

    Public Shared Function GetComboxOficinaUsuario(UsuarioID As String, ByRef cmb As DevExpress.XtraEditors.GridLookUpEdit)

        ' Carga en un GridLookUpEdit las oficinas que estan asociadas con el usuario 
        Dim dt As New DataTable
        dt = Oficinas.GetListOficinasUser(Usuario_)

        cmb.Properties.DataSource = dt
        cmb.Properties.ValueMember = "OficinaID"
        cmb.Properties.DisplayMember = "Oficina"
        cmb.Properties.PopulateViewColumns()

        If dt.Rows.Count = 1 Then
            cmb.EditValue = dt.Rows(0).Item("OficinaID")
        End If

    End Function

    Public Shared Function IU(OficinaID As Integer, Codigo As String, Oficina As String, Activo As Boolean, Accion As String)

        Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
        Dim cmd As New SqlCommand("IU_Oficinas", conexion)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            cmd.Parameters.Add("OficinaID", SqlDbType.Int)
            cmd.Parameters("OficinaID").Value = OficinaID

            cmd.Parameters.Add("Codigo", SqlDbType.NVarChar)
            cmd.Parameters("Codigo").Value = Codigo

            cmd.Parameters.Add("Oficina", SqlDbType.NVarChar)
            cmd.Parameters("Oficina").Value = Oficina

            cmd.Parameters.Add("Activo", SqlDbType.Bit)
            cmd.Parameters("Activo").Value = Activo

            cmd.Parameters.Add("Accion", SqlDbType.NVarChar)
            cmd.Parameters("Accion").Value = Accion

            conexion.Open()
            cmd.ExecuteNonQuery()
            conexion.Close()

        Catch ex As Exception

            conexion.Close()
            MsgBox(ex.Message)
        End Try



    End Function


End Class
