﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_IU_Oficina
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_IU_Oficina))
        Me.lbl_rotulo = New DevExpress.XtraEditors.LabelControl()
        Me.Frm_IU_AgenteConvertedLayout = New DevExpress.XtraLayout.LayoutControl()
        Me.chk_Activo = New DevExpress.XtraEditors.CheckEdit()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar2 = New DevExpress.XtraBars.Bar()
        Me.btn_guardar = New DevExpress.XtraBars.BarButtonItem()
        Me.btn_regresar = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.Txt_codigo = New DevExpress.XtraEditors.TextEdit()
        Me.Txt_Oficina = New DevExpress.XtraEditors.TextEdit()
        Me.TxtID = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutConverter1 = New DevExpress.XtraLayout.Converter.LayoutConverter(Me.components)
        CType(Me.Frm_IU_AgenteConvertedLayout, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Frm_IU_AgenteConvertedLayout.SuspendLayout()
        CType(Me.chk_Activo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_codigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_Oficina.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbl_rotulo
        '
        Me.lbl_rotulo.Location = New System.Drawing.Point(12, 12)
        Me.lbl_rotulo.Name = "lbl_rotulo"
        Me.lbl_rotulo.Size = New System.Drawing.Size(66, 13)
        Me.lbl_rotulo.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.lbl_rotulo.TabIndex = 47
        '
        'Frm_IU_AgenteConvertedLayout
        '
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.chk_Activo)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.Txt_codigo)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.Txt_Oficina)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.TxtID)
        Me.Frm_IU_AgenteConvertedLayout.Controls.Add(Me.lbl_rotulo)
        Me.Frm_IU_AgenteConvertedLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Frm_IU_AgenteConvertedLayout.Location = New System.Drawing.Point(0, 40)
        Me.Frm_IU_AgenteConvertedLayout.Name = "Frm_IU_AgenteConvertedLayout"
        Me.Frm_IU_AgenteConvertedLayout.Root = Me.LayoutControlGroup1
        Me.Frm_IU_AgenteConvertedLayout.Size = New System.Drawing.Size(843, 357)
        Me.Frm_IU_AgenteConvertedLayout.TabIndex = 52
        '
        'chk_Activo
        '
        Me.chk_Activo.EditValue = True
        Me.chk_Activo.Location = New System.Drawing.Point(181, 12)
        Me.chk_Activo.MenuManager = Me.BarManager1
        Me.chk_Activo.Name = "chk_Activo"
        Me.chk_Activo.Properties.Caption = "Activo"
        Me.chk_Activo.Size = New System.Drawing.Size(90, 19)
        Me.chk_Activo.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.chk_Activo.TabIndex = 1
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btn_guardar, Me.btn_regresar})
        Me.BarManager1.MainMenu = Me.Bar2
        Me.BarManager1.MaxItemId = 3
        '
        'Bar2
        '
        Me.Bar2.BarName = "Menú principal"
        Me.Bar2.DockCol = 0
        Me.Bar2.DockRow = 0
        Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btn_guardar), New DevExpress.XtraBars.LinkPersistInfo(Me.btn_regresar)})
        Me.Bar2.OptionsBar.MultiLine = True
        Me.Bar2.OptionsBar.UseWholeRow = True
        Me.Bar2.Text = "Menú principal"
        '
        'btn_guardar
        '
        Me.btn_guardar.Caption = "Guardar"
        Me.btn_guardar.Glyph = CType(resources.GetObject("btn_guardar.Glyph"), System.Drawing.Image)
        Me.btn_guardar.Id = 0
        Me.btn_guardar.LargeGlyph = CType(resources.GetObject("btn_guardar.LargeGlyph"), System.Drawing.Image)
        Me.btn_guardar.Name = "btn_guardar"
        '
        'btn_regresar
        '
        Me.btn_regresar.Caption = "Regresar"
        Me.btn_regresar.Glyph = CType(resources.GetObject("btn_regresar.Glyph"), System.Drawing.Image)
        Me.btn_regresar.Id = 1
        Me.btn_regresar.LargeGlyph = CType(resources.GetObject("btn_regresar.LargeGlyph"), System.Drawing.Image)
        Me.btn_regresar.Name = "btn_regresar"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(843, 40)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 397)
        Me.barDockControlBottom.Size = New System.Drawing.Size(843, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 40)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 357)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(843, 40)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 357)
        '
        'Txt_codigo
        '
        Me.Txt_codigo.Location = New System.Drawing.Point(54, 42)
        Me.Txt_codigo.Name = "Txt_codigo"
        Me.Txt_codigo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_codigo.Properties.Appearance.Options.UseFont = True
        Me.Txt_codigo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_codigo.Properties.MaxLength = 50
        Me.Txt_codigo.Size = New System.Drawing.Size(123, 26)
        Me.Txt_codigo.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.Txt_codigo.TabIndex = 3
        '
        'Txt_Oficina
        '
        Me.Txt_Oficina.Location = New System.Drawing.Point(54, 72)
        Me.Txt_Oficina.Name = "Txt_Oficina"
        Me.Txt_Oficina.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Oficina.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Txt_Oficina.Properties.Appearance.Options.UseFont = True
        Me.Txt_Oficina.Properties.Appearance.Options.UseForeColor = True
        Me.Txt_Oficina.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Txt_Oficina.Properties.MaxLength = 100
        Me.Txt_Oficina.Size = New System.Drawing.Size(311, 26)
        Me.Txt_Oficina.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.Txt_Oficina.TabIndex = 2
        '
        'TxtID
        '
        Me.TxtID.Enabled = False
        Me.TxtID.Location = New System.Drawing.Point(54, 12)
        Me.TxtID.Name = "TxtID"
        Me.TxtID.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtID.Properties.Appearance.Options.UseFont = True
        Me.TxtID.Size = New System.Drawing.Size(123, 26)
        Me.TxtID.StyleController = Me.Frm_IU_AgenteConvertedLayout
        Me.TxtID.TabIndex = 0
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem3, Me.EmptySpaceItem1, Me.EmptySpaceItem5, Me.LayoutControlItem7, Me.LayoutControlItem1, Me.EmptySpaceItem3, Me.EmptySpaceItem4})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(843, 357)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.Txt_Oficina
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 60)
        Me.LayoutControlItem2.MaxSize = New System.Drawing.Size(357, 30)
        Me.LayoutControlItem2.MinSize = New System.Drawing.Size(357, 30)
        Me.LayoutControlItem2.Name = "TxtDesBancoitem"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(357, 30)
        Me.LayoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem2.Text = "Oficina"
        Me.LayoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(39, 16)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem3.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem3.Control = Me.TxtID
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem3.MaxSize = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem3.MinSize = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem3.Name = "TxtBancoIDitem"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem3.Text = "ID"
        Me.LayoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(39, 16)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(263, 0)
        Me.EmptySpaceItem1.MaxSize = New System.Drawing.Size(560, 30)
        Me.EmptySpaceItem1.MinSize = New System.Drawing.Size(560, 30)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(560, 30)
        Me.EmptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = False
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(357, 60)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(466, 30)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = False
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(169, 30)
        Me.EmptySpaceItem4.MaxSize = New System.Drawing.Size(654, 30)
        Me.EmptySpaceItem4.MinSize = New System.Drawing.Size(654, 30)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(654, 30)
        Me.EmptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem5
        '
        Me.EmptySpaceItem5.AllowHotTrack = False
        Me.EmptySpaceItem5.Location = New System.Drawing.Point(0, 90)
        Me.EmptySpaceItem5.MaxSize = New System.Drawing.Size(823, 247)
        Me.EmptySpaceItem5.MinSize = New System.Drawing.Size(823, 247)
        Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Size = New System.Drawing.Size(823, 247)
        Me.EmptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.chk_Activo
        Me.LayoutControlItem7.Location = New System.Drawing.Point(169, 0)
        Me.LayoutControlItem7.MaxSize = New System.Drawing.Size(94, 30)
        Me.LayoutControlItem7.MinSize = New System.Drawing.Size(94, 30)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(94, 30)
        Me.LayoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem1.Control = Me.Txt_codigo
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 30)
        Me.LayoutControlItem1.MaxSize = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem1.MinSize = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem1.Name = "Txtsiglasitem"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(169, 30)
        Me.LayoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem1.Text = "Código"
        Me.LayoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(39, 16)
        '
        'Frm_IU_Oficina
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(843, 397)
        Me.Controls.Add(Me.Frm_IU_AgenteConvertedLayout)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "Frm_IU_Oficina"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.Frm_IU_AgenteConvertedLayout, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Frm_IU_AgenteConvertedLayout.ResumeLayout(False)
        Me.Frm_IU_AgenteConvertedLayout.PerformLayout()
        CType(Me.chk_Activo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_codigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_Oficina.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbl_rotulo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Txt_Oficina As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txt_codigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Frm_IU_AgenteConvertedLayout As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents chk_Activo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
    Friend WithEvents btn_guardar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btn_regresar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutConverter1 As DevExpress.XtraLayout.Converter.LayoutConverter
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
End Class
