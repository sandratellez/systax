﻿Public Class Frm_IU_Oficina
    Public Accion As String
    Public ID As String


    Private Sub Frm_IUBanco_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Accion = "NEW" Then
            Me.Text = "Registrar Oficina"
        End If
        If Accion = "EDIT" Then
            Me.Text = "Editar Oficina"
            cargar()
        End If

    End Sub

    Sub cargar()
        Dim Agent As DataTable

        Agent = Oficinas.GetlistOficinas(ID)
        If Agent.Rows.Count > 0 Then

            Me.TxtID.EditValue = Agent.Rows(0).Item("OficinaID").ToString
            Me.Txt_codigo.EditValue = Agent.Rows(0).Item("Codigo").ToString
            Me.Txt_Oficina.EditValue = Agent.Rows(0).Item("Oficina").ToString
            Me.chk_Activo.EditValue = Agent.Rows(0).Item("Activo")
      

        End If




    End Sub

    Function validar()

        If String.IsNullOrEmpty(Me.Txt_Oficina.Text) Then
            MessageBox.Show("Registrar Oficina", "Aviso Importante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.Txt_Oficina.Focus()
            Return False
        End If

    
        If String.IsNullOrEmpty(Me.Txt_codigo.Text) Then
            MessageBox.Show("Registrar Código", "Aviso Importante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.Txt_codigo.Focus()
            Return False
        End If

        Return True

    End Function

    Sub limpiar()

        Me.Txt_Oficina.Text = String.Empty
        Me.Txt_codigo.Text = String.Empty
        Me.Txt_Oficina.Focus()

    End Sub


    Private Sub btn_guardar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_guardar.ItemClick
        If validar() = True Then
            If Accion = "NEW" Then
                If MessageBox.Show("¿Registrar Oficina?", "Aviso Importante", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If

                Oficinas.IU(0, Me.Txt_codigo.Text, Me.Txt_Oficina.Text, Me.chk_Activo.Checked, Accion)
                limpiar()
                My.Forms.Frm_P_Agente.Cargar()

            Else
                If MessageBox.Show("¿Actualizar Agente?", "Aviso Importante", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If

                Oficinas.IU(ID, Me.Txt_codigo.Text, Me.Txt_Oficina.Text, Me.chk_Activo.Checked, Accion)
                My.Forms.Frm_P_Agente.Cargar()
                Me.Close()

            End If
        End If
    End Sub

    Private Sub btn_regresar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_regresar.ItemClick
        Me.Close()
    End Sub
End Class