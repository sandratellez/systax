﻿Imports System.Data.SqlClient
Imports System.Data.Sql

Public Class Frm_IU_Retencion

    Dim _ID As Integer
    Public Property ID() As Integer
        Get
            Return _ID
        End Get
        Set(value As Integer)
            _ID = value
        End Set
    End Property

    Dim _ProveedorID As Integer
    Public Property ProveedorID() As Integer
        Get
            Return _ProveedorID
        End Get
        Set(value As Integer)
            _ProveedorID = value
        End Set
    End Property


    Dim _ReciboIMI As String
    Public Property ReciboIMI() As String
        Get
            Return _ReciboIMI
        End Get
        Set(value As String)
            _ReciboIMI = value
        End Set
    End Property

    Dim _ReciboIR As String
    Public Property ReciboIR() As String
        Get
            Return _ReciboIR
        End Get
        Set(value As String)
            _ReciboIR = value
        End Set
    End Property

    Dim _Accion As String
    Public Property Accion() As String
        Get
            Return _Accion
        End Get
        Set(value As String)
            _Accion = value
        End Set
    End Property

    Dim RetenidoDB As New DataTable
    Private Sub Frm_IU_Retencion_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cargarProveedor()

        cmb_banco.Properties.DataSource = Rutinas_SGC.obtener_datos("SELECT b.BancoID,b.Banco,b.Siglas FROM Bancos AS b")
        cmb_banco.Properties.ValueMember = "BancoID"
        cmb_banco.Properties.DisplayMember = "Siglas"
        cmb_banco.Properties.PopulateViewColumns()


        If Accion = "NEW" Then
            Me.Text = "Registrar Retención"
            cargartalonario()
            Me.cmb_retenido.Focus()
        Else
            Me.Text = "Editar Retención"

            Dim DT_Facturas As DataTable = Retensiones.Unique(ID)

            If DT_Facturas.Rows.Count > 0 Then

                Me.cmb_retenido.EditValue = DT_Facturas.Rows(0).Item("ProveedorID")
                Me.dpt_fecha.EditValue = DT_Facturas.Rows(0).Item("Fecha")
                Me.txt_No_Factura.EditValue = DT_Facturas.Rows(0).Item("FacturaNo_")
                Me.txt_concepto.EditValue = DT_Facturas.Rows(0).Item("Concepto")
                Me.txt_Importe.EditValue = DT_Facturas.Rows(0).Item("ImporteFactura")
                Me.txt_por_imi.EditValue = DT_Facturas.Rows(0).Item("IMI_por")
                Me.txt_IMI.EditValue = DT_Facturas.Rows(0).Item("ImporteIMI")
                Me.txt_IMI_Recibo.EditValue = DT_Facturas.Rows(0).Item("ReciboIMI")
                Me.chk_automatico.EditValue = DT_Facturas.Rows(0).Item("Automatico")
                Me.txt_ck.EditValue = DT_Facturas.Rows(0).Item("Cheque")
                Me.cmb_banco.EditValue = DT_Facturas.Rows(0).Item("BancoID")
                Me.txt_Observaciones.EditValue = DT_Facturas.Rows(0).Item("Observaciones")
                Me.dpt_fecha.Focus()

            End If



        End If




        'Combox.View.BestFitColumns()
        'Combox.PopupFormMinSize = New Point(Combox.Size.Width, 0)
        'Combox.ShowFooter = True
        'Combox.Text = String.Empty
        Me.dpt_fecha.Focus()

    End Sub
    Sub cargartalonario()
        Dim imi_talonario As New DataTable
        Dim ir_talonario As New DataTable


        ''imi_talonario = Talonario.GetLisTalonario("IMI")
        ''ir_talonario = Talonario.GetLisTalonario("IR")

        ''If Talonario.GetLisTalonario("IMI").Rows.Count > 0 Then
        ''    Me.txt_IMI_Recibo.EditValue = imi_talonario.Rows(0).Item("ReciboID")
        ''    Me.txt_IMI_Serie.EditValue = imi_talonario.Rows(0).Item("Serie").ToString
        ''Else
        ''    MessageBox.Show("Revisar Talonarios de IMI", "Aviso Importante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        ''End If

        ''If Talonario.GetLisTalonario("IR").Rows.Count > 0 Then
        ''    Me.txt_IR_Recibo.EditValue = ir_talonario.Rows(0).Item("ReciboID")
        ''    Me.txt_IR_serie.EditValue = ir_talonario.Rows(0).Item("Serie").ToString
        ''Else
        ''    MessageBox.Show("Revisar Talonarios de IR", "Aviso Importante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        ''End If
    End Sub
    Sub cargarProveedor()

        RetenidoDB = Rutinas_SGC.obtener_datos("GetListProveedor '%'")
        cmb_retenido.Properties.DataSource = RetenidoDB
        cmb_retenido.Properties.ValueMember = "ProveedorID"
        cmb_retenido.Properties.DisplayMember = "Proveedor"
        cmb_retenido.Properties.PopulateViewColumns()

    End Sub

    Private Sub cmb_retenido_ButtonClick(sender As Object, e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles cmb_retenido.ButtonClick
        'DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis
        If e.Button.Index = 1 Then

            My.Forms.Frm_IU_Proveedor.Accion = "NEW"
            My.Forms.Frm_IU_Proveedor.ShowDialog()

            cargarProveedor()
            Me.cmb_retenido.Text = Rutinas_SGC.obtener_datos("SELECT max(r.ProveedorID) ID FROM Proveedor AS r").Rows(0).Item("ID")
            cmb_retenido_Validated(Nothing, Nothing)
            Me.dpt_fecha.Focus()


        End If

        If e.Button.Index = 2 Then

            If String.IsNullOrEmpty(Me.cmb_retenido.Text) Then
                Exit Sub
            End If

            My.Forms.Frm_IU_Proveedor.Accion = "EDIT"
            My.Forms.Frm_IU_Proveedor.ID = Me.cmb_retenido.EditValue
            My.Forms.Frm_IU_Proveedor.ShowDialog()
            cargarProveedor()
            Me.cmb_retenido.EditValue = ProveedorID
            cmb_retenido_Validated(Nothing, Nothing)
            Me.dpt_fecha.Focus()



        End If


    End Sub

    Private Sub cmb_retenido_Click(sender As Object, e As EventArgs) Handles cmb_retenido.Click

    End Sub

    Private Sub cmb_retenido_EditValueChanged(sender As Object, e As EventArgs) Handles cmb_retenido.EditValueChanged
        If Me.cmb_retenido.EditValue IsNot Nothing Then
            Dim row As DataRow = Me.cmb_retenido.Properties.View.GetDataRow(Me.cmb_retenido.Properties.View.FocusedRowHandle)
            If row IsNot Nothing Then
                retenido(row.Item("ProveedorID"))
                '       Me.dpt_fecha.Focus()
            End If
        End If
    End Sub
    Sub retenido(ID As String)
        Try


            Dim retenido As DataTable

            retenido = Proveedor.GetList(ID)
            If retenido.Rows.Count > 0 Then

                Me.ProveedorID = retenido.Rows(0).Item("ProveedorID").ToString
                Me.Txt_Ruc.EditValue = retenido.Rows(0).Item("Ruc").ToString
                Me.txt_telefono.EditValue = retenido.Rows(0).Item("Telefono").ToString
                Me.Txt_Cedula.EditValue = retenido.Rows(0).Item("Cedula").ToString

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Aviso Importante")
        End Try

    End Sub

    Private Sub cmb_retenido_Validated(sender As Object, e As EventArgs) Handles cmb_retenido.Validated

        If String.IsNullOrEmpty(Me.cmb_retenido.Text) Then
            Exit Sub
        End If


        Dim nic As String
        If Me.cmb_retenido.EditValue IsNot Nothing Then
            Dim precio = From premio In RetenidoDB.AsEnumerable() Where premio.Field(Of Integer)("ProveedorID") = Me.cmb_retenido.EditValue
            For Each p As Object In precio
                nic = p.item("ProveedorID")
            Next
        End If

        If nic = String.Empty Then
            Exit Sub
        End If

        retenido(nic)
        'Me.dpt_fecha.Focus()

    End Sub

    Private Sub TextEdit6_Validated(sender As Object, e As EventArgs) Handles txt_Importe.Validated

        If Me.chk_automatico.CheckState = CheckState.Checked Then
            calcular()
        End If

    End Sub
   


    Private Sub btn_guardar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_guardar.ItemClick
        If validar() Then



            If Accion = "NEW" Then

                If MessageBox.Show("¿ Registrar Retención ?", "Aviso Importante...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If


                Try
                    Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
                    Dim cmd As New SqlCommand("IU_Retenciones", conexion)
                    cmd.CommandType = CommandType.StoredProcedure

                    cmd.Parameters.Add("RetencionID", SqlDbType.Int)
                    cmd.Parameters("RetencionID").Value = 0

                    cmd.Parameters.Add("ProveedorID", SqlDbType.Int)
                    cmd.Parameters("ProveedorID").Value = Me.cmb_retenido.EditValue

                    cmd.Parameters.Add("Fecha", SqlDbType.Date)
                    cmd.Parameters("Fecha").Value = Me.dpt_fecha.EditValue

                    cmd.Parameters.Add("FacturaNo", SqlDbType.NVarChar)
                    cmd.Parameters("FacturaNo").Value = Me.txt_No_Factura.EditValue

                    cmd.Parameters.Add("Concepto", SqlDbType.NVarChar)
                    cmd.Parameters("Concepto").Value = Me.txt_concepto.EditValue

                    cmd.Parameters.Add("ImporteFactura", SqlDbType.Decimal)
                    cmd.Parameters("ImporteFactura").Value = CDec(Me.txt_Importe.EditValue)

                    cmd.Parameters.Add("IMI_por", SqlDbType.Decimal)
                    cmd.Parameters("IMI_por").Value = CDec(Me.txt_por_imi.EditValue)

                    cmd.Parameters.Add("IR_por", SqlDbType.Decimal)
                    cmd.Parameters("IR_por").Value = CDec(Me.txt_por_ir.EditValue)

                    cmd.Parameters.Add("ImporteIMI", SqlDbType.Decimal)
                    cmd.Parameters("ImporteIMI").Value = CDec(Me.txt_IMI.EditValue)

                    cmd.Parameters.Add("ImporteIR", SqlDbType.Decimal)
                    cmd.Parameters("ImporteIR").Value = CDec(Me.txt_IR.EditValue)

                    cmd.Parameters.Add("ReciboIMI", SqlDbType.NVarChar)
                    cmd.Parameters("ReciboIMI").Value = Me.txt_IMI_Recibo.EditValue

                    cmd.Parameters.Add("ReciboIR", SqlDbType.NVarChar)
                    cmd.Parameters("ReciboIR").Value = Me.txt_IR_Recibo.EditValue

                    cmd.Parameters.Add("IMISerie", SqlDbType.NVarChar)
                    cmd.Parameters("IMISerie").Value = Me.txt_IMI_Serie.EditValue

                    cmd.Parameters.Add("IRSerie", SqlDbType.NVarChar)
                    cmd.Parameters("IRSerie").Value = Me.txt_IR_serie.EditValue

                    cmd.Parameters.Add("ckIMI", SqlDbType.Bit)
                    cmd.Parameters("ckIMI").Value = Me.ck_imi.EditValue

                    cmd.Parameters.Add("ckIR", SqlDbType.Bit)
                    cmd.Parameters("ckIR").Value = Me.chk_ir.EditValue

                    cmd.Parameters.Add("Automatico", SqlDbType.Bit)
                    cmd.Parameters("Automatico").Value = Me.chk_automatico.EditValue

                    cmd.Parameters.Add("Cheque", SqlDbType.NVarChar)
                    cmd.Parameters("Cheque").Value = Me.txt_ck.Text.ToString

                    cmd.Parameters.Add("BancoID", SqlDbType.NVarChar)
                    cmd.Parameters("BancoID").Value = IIf(String.IsNullOrEmpty(Me.cmb_banco.Text), "", Me.cmb_banco.EditValue)

                    cmd.Parameters.Add("OrigenID", SqlDbType.Int)
                    cmd.Parameters("OrigenID").Value = 0

                    cmd.Parameters.Add("Observacion", SqlDbType.NVarChar)
                    cmd.Parameters("Observacion").Value = Me.txt_Observaciones.Text.ToUpper

                    cmd.Parameters.Add("Accion", SqlDbType.NVarChar)
                    cmd.Parameters("Accion").Value = "New"

                    conexion.Open()
                    cmd.ExecuteNonQuery()
                    conexion.Close()

                    Me.cmb_retenido.Focus()
                    Me.cmb_retenido.Text = String.Empty
                    Me.Txt_Cedula.Text = String.Empty
                    Me.Txt_Ruc.Text = String.Empty
                    Me.txt_telefono.Text = String.Empty

                    Me.dpt_fecha.EditValue = Now

                    Me.txt_No_Factura.Text = String.Empty
                    Me.txt_concepto.Text = String.Empty

                    Me.txt_Importe.EditValue = 0.0

                    Me.txt_por_imi.Text = "1.00"
                    Me.txt_por_ir.Text = "2.00"

                    Me.txt_IMI_Recibo.Text = String.Empty
                    Me.txt_IR_Recibo.Text = String.Empty

                    Me.chk_ir.Checked = True
                    Me.ck_imi.Checked = True
                    Me.chk_automatico.Checked = True


                    Me.txt_IMI.EditValue = 0.0
                    Me.txt_IR.EditValue = 0.0

                    Me.txt_ck.Text = String.Empty
                    Me.cmb_banco.EditValue = String.Empty

                    cargartalonario()
                    My.Forms.Frm_P_Retencion.loadata()

                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Aviso Importe", MessageBoxButtons.OK, MessageBoxIcon.Question)
                End Try
            Else
                Try
                    Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
                    Dim cmd As New SqlCommand("IU_Retenciones", conexion)
                    cmd.CommandType = CommandType.StoredProcedure

                    cmd.Parameters.Add("RetencionID", SqlDbType.Int)
                    cmd.Parameters("RetencionID").Value = ID

                    cmd.Parameters.Add("ProveedorID", SqlDbType.Int)
                    cmd.Parameters("ProveedorID").Value = Me.cmb_retenido.EditValue

                    cmd.Parameters.Add("Fecha", SqlDbType.Date)
                    cmd.Parameters("Fecha").Value = Me.dpt_fecha.EditValue

                    cmd.Parameters.Add("FacturaNo", SqlDbType.NVarChar)
                    cmd.Parameters("FacturaNo").Value = Me.txt_No_Factura.EditValue

                    cmd.Parameters.Add("Concepto", SqlDbType.NVarChar)
                    cmd.Parameters("Concepto").Value = Me.txt_concepto.EditValue

                    cmd.Parameters.Add("ImporteFactura", SqlDbType.Decimal)
                    cmd.Parameters("ImporteFactura").Value = CDec(Me.txt_Importe.EditValue)

                    cmd.Parameters.Add("IMI_por", SqlDbType.Decimal)
                    cmd.Parameters("IMI_por").Value = CDec(Me.txt_por_imi.EditValue)

                    cmd.Parameters.Add("IR_por", SqlDbType.Decimal)
                    cmd.Parameters("IR_por").Value = CDec(Me.txt_por_ir.EditValue)

                    cmd.Parameters.Add("ImporteIMI", SqlDbType.Decimal)
                    cmd.Parameters("ImporteIMI").Value = CDec(Me.txt_IMI.EditValue)

                    cmd.Parameters.Add("ImporteIR", SqlDbType.Decimal)
                    cmd.Parameters("ImporteIR").Value = CDec(Me.txt_IR.EditValue)

                    cmd.Parameters.Add("ReciboIMI", SqlDbType.NVarChar)
                    cmd.Parameters("ReciboIMI").Value = Me.txt_IMI_Recibo.EditValue

                    cmd.Parameters.Add("ReciboIR", SqlDbType.NVarChar)
                    cmd.Parameters("ReciboIR").Value = Me.txt_IR_Recibo.EditValue

                    cmd.Parameters.Add("IMISerie", SqlDbType.NVarChar)
                    cmd.Parameters("IMISerie").Value = "NE"

                    cmd.Parameters.Add("IRSerie", SqlDbType.NVarChar)
                    cmd.Parameters("IRSerie").Value = ""

                    cmd.Parameters.Add("ckIMI", SqlDbType.Bit)
                    cmd.Parameters("ckIMI").Value = Me.ck_imi.EditValue

                    cmd.Parameters.Add("ckIR", SqlDbType.Bit)
                    cmd.Parameters("ckIR").Value = Me.chk_ir.EditValue

                    cmd.Parameters.Add("Automatico", SqlDbType.Bit)
                    cmd.Parameters("Automatico").Value = Me.chk_automatico.EditValue

                    cmd.Parameters.Add("Cheque", SqlDbType.NVarChar)
                    cmd.Parameters("Cheque").Value = Me.txt_ck.Text.ToString

                    cmd.Parameters.Add("BancoID", SqlDbType.NVarChar)
                    cmd.Parameters("BancoID").Value = IIf(String.IsNullOrEmpty(Me.cmb_banco.Text), "", Me.cmb_banco.EditValue)

                    cmd.Parameters.Add("OrigenID", SqlDbType.Int)
                    cmd.Parameters("OrigenID").Value = 0

                    cmd.Parameters.Add("Observacion", SqlDbType.NVarChar)
                    cmd.Parameters("Observacion").Value = Me.txt_Observaciones.Text.ToUpper

                    cmd.Parameters.Add("Accion", SqlDbType.NVarChar)
                    cmd.Parameters("Accion").Value = "EDIT"

                    conexion.Open()
                    cmd.ExecuteNonQuery()
                    conexion.Close()

                    'Me.cmb_retenido.Focus()
                    'Me.cmb_retenido.Text = String.Empty
                    'Me.Txt_Cedula.Text = String.Empty
                    'Me.Txt_Ruc.Text = String.Empty
                    'Me.txt_telefono.Text = String.Empty

                    'Me.dpt_fecha.EditValue = Now

                    'Me.txt_No_Factura.Text = String.Empty
                    'Me.txt_concepto.Text = String.Empty

                    'Me.txt_Importe.EditValue = 0.0

                    'Me.txt_por_imi.Text = "1.00"
                    'Me.txt_por_ir.Text = "2.00"

                    'Me.txt_IMI_Recibo.Text = String.Empty
                    'Me.txt_IR_Recibo.Text = String.Empty

                    'Me.chk_ir.Checked = True
                    'Me.ck_imi.Checked = True
                    'Me.chk_automatico.Checked = True


                    'Me.txt_IMI.EditValue = 0.0
                    'Me.txt_IR.EditValue = 0.0

                    'Me.txt_ck.Text = String.Empty
                    'Me.cmb_banco.EditValue = String.Empty

                    'cargartalonario()
                    My.Forms.Frm_P_Retencion.loadata()
                    Me.Close()

                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Aviso Importe", MessageBoxButtons.OK, MessageBoxIcon.Question)
                End Try




            End If
        End If

    End Sub
    Function validar()

        If String.IsNullOrEmpty(Me.cmb_retenido.Text) Then
            MessageBox.Show("Falta Datos del Retenido", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.cmb_retenido.Focus()
            Return False
        End If

        If String.IsNullOrEmpty(Me.dpt_fecha.Text) Then
            MessageBox.Show("Falta Datos de la Fecha", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.dpt_fecha.Focus()
            Return False
        End If

        If String.IsNullOrEmpty(Me.txt_No_Factura.Text) Then
            MessageBox.Show("Falta Datos del # de Factura", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.txt_No_Factura.Focus()
            Return False
        End If

        If String.IsNullOrEmpty(Me.txt_concepto.Text) Then
            MessageBox.Show("Falta Datos del Concepto", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.txt_concepto.Focus()
            Return False
        End If

        'If CDec(Me.txt_Importe.Text) = 0.0 Then
        '    MessageBox.Show("Falta Datos del Importe", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    Me.txt_Importe.Focus()
        '    Return False
        'End If

        'If CDec(Me.txt_IMI.Text) = 0.0 Then
        '    MessageBox.Show("Falta Datos del Importe del IMI", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    Me.txt_IMI.Focus()
        '    Return False
        'End If

        'If CDec(Me.txt_IR.Text) = 0.0 Then
        '    MessageBox.Show("Falta Datos del Importe del IR", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    Me.txt_IR.Focus()
        '    Return False
        'End If

        Return True
    End Function

    Private Sub chk_ir_EditValueChanged(sender As Object, e As EventArgs) Handles chk_ir.EditValueChanged
        If Not Me.chk_ir.CheckState = CheckState.Checked Then
            Me.txt_IR.Text = "0.00"
            Me.txt_por_ir.Text = "0.00"
        Else
            Me.txt_por_ir.Text = "2.00"
            calcular()
        End If
    End Sub

    Private Sub ck_imi_EditValueChanged(sender As Object, e As EventArgs) Handles ck_imi.EditValueChanged
        If Not Me.ck_imi.CheckState = CheckState.Checked Then
            Me.txt_IMI.Text = "0.00"
            Me.txt_por_imi.Text = "0.00"
        Else
            Me.txt_por_imi.Text = "1.00"
            calcular()
        End If
    End Sub

    Sub calcular()
        Me.txt_IMI.EditValue = Decimal.Round((CDec(Me.txt_Importe.EditValue) * (CDec(Me.txt_por_imi.EditValue) / 100)), 2)
        Me.txt_IR.EditValue = Decimal.Round((CDec(Me.txt_Importe.EditValue) * (CDec(Me.txt_por_ir.EditValue) / 100)), 2)
    End Sub
    
    Private Sub btn_regresar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_regresar.ItemClick
        Me.Close()
    End Sub
End Class