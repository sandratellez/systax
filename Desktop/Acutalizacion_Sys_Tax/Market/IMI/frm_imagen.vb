﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class frm_imagen
    Dim _ID As Integer
    Public Property ID() As String
        Get
            Return _ID
        End Get
        Set(value As String)
            _ID = value
        End Set
    End Property

    Dim _Por As Integer
    Public Property Por() As String
        Get
            Return _Por
        End Get
        Set(value As String)
            _Por = value
        End Set
    End Property


    Private Sub frm_imagen_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Por = 50

        'Me.cmb_Tipo.Properties.DataSource = Rutinas_SGC.obtener_datos("SELECT ds.TipoID,ds.Documento FROM DocumentoScan AS ds")
        'Me.cmb_Tipo.Properties.ValueMember = "TipoID"
        'Me.cmb_Tipo.Properties.DisplayMember = "Documento"

        Me.cmb_Tipo.Properties.DataSource = Rutinas_SGC.obtener_datos("SELECT tdp.TipoID,tdp.Documento FROM TipoDocProveedor AS tdp")
        Me.cmb_Tipo.Properties.ValueMember = "TipoID"
        Me.cmb_Tipo.Properties.DisplayMember = "Documento"



    End Sub

    Private Sub btn_guardar_ItemClick(sender As Object, e As EventArgs) Handles btn_guardar1.Click

        'If validar() = True Then

        '    Try

        '        Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
        '        Dim cmd As New SqlCommand("IU_RetencionIMagen", conexion)
        '        cmd.CommandType = CommandType.StoredProcedure

        '        cmd.Parameters.Add("RetencionID", SqlDbType.Int)
        '        cmd.Parameters("RetencionID").Value = ID

        '        cmd.Parameters.Add("Nombre", SqlDbType.NVarChar)
        '        cmd.Parameters("Nombre").Value = Me.txt_nombre.EditValue

        '        cmd.Parameters.Add("TipoID", SqlDbType.Int)
        '        cmd.Parameters("TipoID").Value = Me.cmb_Tipo.EditValue

        '        cmd.Parameters.Add("Imagen", SqlDbType.Image)

        '        If Me.pic_foto.EditValue Is DBNull.Value Then
        '            cmd.Parameters("Imagen").Value = DBNull.Value
        '        Else
        '            cmd.Parameters("Imagen").Value = Me.pic_foto.EditValue
        '        End If

        '        conexion.Open()
        '        cmd.ExecuteNonQuery()
        '        conexion.Close()

        '        Me.cmb_Tipo.EditValue = Nothing
        '        Me.txt_nombre.EditValue = String.Empty
        '        Me.pic_foto.EditValue = Nothing

        '    Catch ex As Exception
        '        MessageBox.Show(ex.Message, "Aviso Importe", MessageBoxButtons.OK, MessageBoxIcon.Question)
        '    End Try
        '        End If


        If validar() = True Then
            Try

                Dim row_ As DataRow
                row_ = My.Forms.Frm_IU_Proveedor.ProveedorImage.NewRow

                row_.Item("ProveedorID") = My.Forms.Frm_IU_Proveedor.ProveedorImage.Rows.Count + 1
                row_.Item("TipoID") = Me.cmb_Tipo.EditValue
                row_.Item("Documento") = Me.cmb_Tipo.Text

                If Me.pic_foto.EditValue Is DBNull.Value Then
                    row_.Item("Imagen") = DBNull.Value
                Else
                    row_.Item("Imagen") = Me.pic_foto.EditValue
                End If

                My.Forms.Frm_IU_Proveedor.ProveedorImage.Rows.Add(row_)
                My.Forms.Frm_IU_Proveedor.Grid_Imagenes.DataSource = My.Forms.Frm_IU_Proveedor.ProveedorImage

                Me.cmb_Tipo.EditValue = Nothing
                Me.pic_foto.EditValue = Nothing

            Catch ex As Exception
                MessageBox.Show(ex.Message, "Aviso Importante")
            End Try

        End If

    End Sub
    Function validar()

        If String.IsNullOrEmpty(Me.cmb_Tipo.Text) Then
            MessageBox.Show("Falta Tipo Soporte", "Aviso Importe", MessageBoxButtons.OK)
            Me.cmb_Tipo.Focus()
            Return False
        End If

        If Me.pic_foto.EditValue Is Nothing Then
            MessageBox.Show("Falta Documentot Scaneado", "Aviso Importe", MessageBoxButtons.OK)
            Me.pic_foto.Focus()
            Return False
        End If

        Return True
    End Function


    Private Sub btn_regresar_ItemClick(sender As Object, e As EventArgs) Handles btn_regresar1.Click
        Me.Close()
    End Sub

    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs)
        Me.pic_foto.Properties.ZoomPercent = 200
        Me.pic_foto.Refresh()
    End Sub

    Private Sub SimpleButton4_Click(sender As Object, e As EventArgs)
        Me.pic_foto.Properties.ZoomPercent = 50
        Me.pic_foto.Refresh()
    End Sub

 
    Private Sub ZoomTrackBarControl1_EditValueChanged(sender As Object, e As EventArgs) Handles ZoomTrackBarControl1.EditValueChanged
        'MessageBox.Show(Me.ZoomTrackBarControl1.EditValue.ToString)
        'Me.txt_nombre.Text = Me.ZoomTrackBarControl1.EditValue
        Me.pic_foto.Properties.ZoomPercent = Me.ZoomTrackBarControl1.EditValue

    End Sub

    
End Class