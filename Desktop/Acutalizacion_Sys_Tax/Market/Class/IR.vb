﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class IR
    Public Shared Function GetListIR(ID As String) As DataTable
        Dim dt As New DataTable

        Try
            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("GetListIR", conexion)
            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("ID", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("ID").Value = ID


            cmd.Fill(dt)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return dt
    End Function

   
End Class

