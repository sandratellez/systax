Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.DBNull
Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Configuration
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraEditors
Imports System.Net
Imports DevExpress.XtraSplashScreen
Imports TC_Consumo.Servicio
Imports System.Xml
Imports System.Xml.Linq


Public Class Rutinas_SGC
    Public Shared Function Conexion() As String
        Dim StrCnn As String = vbNullString
        Dim s As String() = LeeData()

        StrCnn = "Data Source=" & s(0) & ";Initial Catalog=" & s(1) & ";Persist Security Info=True;User ID=" & s(2) & ";Password=" & s(3)

        'StrCnn = String.Format("Data Source={0};Initial Catalog={1};User Id={2};Password={3};", s(0), s(1), s(2))

        Return StrCnn
    End Function

    Public Shared Function LeeData() As String()

        Dim Cadena(3) As String
        '
        Try
            'Cadena(1) = GetSetting(My.Application.Info.ProductName.ToString, "Cadena", "NBase")

            ''SQL

            'MessageBox.Show(My.Application.Info.ProductName.ToString)

            'MessageBox.Show(GetSetting(My.Application.Info.ProductName.ToString, "Cadena", "NBase_IMP"))
            'MessageBox.Show(GetSetting(My.Application.Info.ProductName.ToString, "Cadena", "NUsuario"))
            'MessageBox.Show(GetSetting(My.Application.Info.ProductName.ToString, "Cadena", "NPassword"))


            'MessageBox.Show(Rutinas_SGC.descodificar(GetSetting(My.Application.Info.ProductName.ToString, "Cadena", "NServer")))
            'MessageBox.Show(Rutinas_SGC.descodificar(GetSetting(My.Application.Info.ProductName.ToString, "Cadena", "NBase_IMP")))
            'MessageBox.Show(Rutinas_SGC.descodificar(GetSetting(My.Application.Info.ProductName.ToString, "Cadena", "NUsuario")))
            'MessageBox.Show(Rutinas_SGC.descodificar(GetSetting(My.Application.Info.ProductName.ToString, "Cadena", "NPassword")))


            Cadena(0) = GetSetting(My.Application.Info.ProductName.ToString, "Cadena", "NServer")
            Cadena(1) = GetSetting(My.Application.Info.ProductName.ToString, "Cadena", "NBase_IMP")
            Cadena(2) = GetSetting(My.Application.Info.ProductName.ToString, "Cadena", "NUsuario")
            Cadena(3) = GetSetting(My.Application.Info.ProductName.ToString, "Cadena", "NPassword")

            '' No se Encuentra la Confiuraci�n

            If Cadena(0).Trim.Length = 0 Then
                Cadena(0) = ""
                Cadena(1) = ""
                Cadena(2) = ""
                Cadena(3) = ""
            End If
        Catch ex As Exception

            MessageBox.Show(ex.Message)

            Cadena(0) = ""
            Cadena(1) = ""
            Cadena(2) = ""
            Cadena(3) = ""
        End Try

        Return Cadena
    End Function


    Public Shared Sub EscribeData(ByVal Servidor As String, ByVal Usuario As String, ByVal Password As String, Base As String)

        Try

            'HKEY_CURRENT_USER\Software\VB and VBA Program Settings\SGC\Cadena

            'SaveSetting(My.Application.Info.ProductName.ToString, "Cadena", "NBase", Trim(Base))

            SaveSetting(My.Application.Info.ProductName.ToString, "Cadena", "NServer", Trim(Servidor))
            SaveSetting(My.Application.Info.ProductName.ToString, "Cadena", "NBase_IMP", Trim(Base))
            SaveSetting(My.Application.Info.ProductName.ToString, "Cadena", "NUsuario", Trim(Usuario))
            SaveSetting(My.Application.Info.ProductName.ToString, "Cadena", "NPassword", Trim(Password))

        Catch ex As Exception

            MessageBox.Show("Error al Guardar, favor consultar con el area de Soporte Tecnico")

        End Try

    End Sub


    Public Shared Sub ShowSplash(Optional ByVal _Descripcion As String = "")
        Try
            Caption = "Por favor espere"

            If _Descripcion Is Nothing Then
                Descripcion = "Cargando Informaci�n"
            Else
                Descripcion = _Descripcion
            End If

            SplashScreenManager.ShowForm(GetType(WaitForm1))

        Catch ex As Exception

        End Try

        'Dim WaitWindows As New DevExpress.Utils.WaitDialogForm("Cargando Sistema", "Espere un momento por favor")
        'oAlerta.Show(FrmMain, "Bienvenido al Sistema", "Elaborado por Ing. Daniel Bojorge", "", My.Resources.debs32)
        ''Mensaje("Sistema Elaborado por Ing. Daniel Bojorge", , , "Bienvenido al Sistema")
        'oAlerta.AutoFormDelay = 5000
        'WaitWindows.Close()

    End Sub

    Public Shared Sub HideSplash()
        Try
            Descripcion = String.Empty
            SplashScreenManager.CloseForm()
        Catch ex As Exception

        End Try

    End Sub

    Shared Function CadenaConexion() As String

        Return Conexion()

        'Dim conexion As String
        'Dim ConnectionFile As StreamReader

        'If Not File.Exists(Application.StartupPath & "\Configurar.ini") Then
        '    MessageBox.Show("Error, No se encuentra el Archivo de Configuraci�n del S.G.C : Configurar.ini", "Aviso Importante, Configurar.ini Is Null", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    MessageBox.Show("Pongase en Contacto con el Administrador del S.G.C", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    Return "Nothing"
        '    Exit Function
        'End If

        'ConnectionFile = New StreamReader(Application.StartupPath & "\Configurar.ini")

        ''Leer Lineas del Archivo Configurar

        'SERVERDB = ConnectionFile.ReadLine.ToString()
        'CATALOG = ConnectionFile.ReadLine.ToString()

        'conexion = "Data Source=" & SERVERDB & ";Initial Catalog=" & CATALOG & ";Persist Security Info=True;User ID=UserDosa;Password=D05A#SGC@2016$"

        '  conexion = "Data Source=" & SERVERDB & ";Initial Catalog=" & CATALOG & ";Persist Security Info=True;User ID=sa;Password=Suministro"

        'conexion = "Data Source=" & SERVERDB & ";Initial Catalog=" & CATALOG & ";Persist Security Info=True;User ID=sa;Password=D0sa.informatica"

        'conexion = "Data Source=" & SERVERDB & ";Initial Catalog=" & CATALOG & ";Persist Security Info=True;User ID=" & usuario_ & ";Password=" & Pass_

        Return Conexion()

        End


        Return 0
    End Function


    Shared Function obtener_datos(ByVal consulta_sql As String) As DataTable

        Dim DA As New SqlDataAdapter(consulta_sql, Rutinas_SGC.CadenaConexion())
        Dim DS As New DataTable
        'DA.SelectCommand.CommandTimeout = 5000
        DA.Fill(DS)
        Return DS

    End Function
    Public Shared Function Letras(ByVal tyCantidad As Double, MonedaID As Integer) As String

        ' Autor: Fexche    
        ' Fecha: 05/08/2009 10.45.pm
        ' Descripcion: convierte una cantidad con decimal en cantidad en letras
        ' Modificado: 15/10/2009 3.45.pm

        Dim i As Integer
        Dim lyCantidad As Double, lyCentavos As Double, lnDigito As Byte, lnPrimerDigito As Byte, lnSegundoDigito As Byte, lnTercerDigito As Byte, lcBloque As String, lnNumeroBloques As Byte, lnBloqueCero
        'Redondea el valor a dos coma flotante
        tyCantidad = Math.Round(tyCantidad, 2)
        'Redondea el valor sin coma flotante
        lyCantidad = Int(tyCantidad)
        'Nos determina la cantidad de centavos
        lyCentavos = (tyCantidad - lyCantidad) * 100
        Dim laUnidades() As String = {"Un", "Dos", "Tres", "Cuatro", "Cinco", "Seis", "Siete", "Ocho", "Nueve", "Diez", "Once", "Doce", "Trece", "Catorce", "Quince", "Dieciseis", "Diecisiete", "Dieciocho", "Diecinueve", "Veinte", "Veintiun", "Veintidos", "Veintitres", "Veinticuatro", "Veinticinco", "Veintiseis", "Veintisiete", "Veintiocho", "Veintinueve"}
        Dim laDecenas() As String = {"Diez", "Veinte", "Treinta", "Cuarenta", "Cincuenta", "Sesenta", "Setenta", "Ochenta", "Noventa"}
        'Arreglo que nos determina las centenas de los numeros
        Dim laCentenas() As String = {"Ciento", "Doscientos", "Trescientos", "Cuatrocientos", "Quinientos", "Seiscientos", "Setecientos", "Ochocientos", "Novecientos"}
        'Nos indica los bloques de tres que se peocesan. Inicialmente es uno. Unidades, decenas y centenas.
        lnNumeroBloques = 1

        Do
            lnPrimerDigito = 0
            lnSegundoDigito = 0
            lnTercerDigito = 0
            lcBloque = ""
            lnBloqueCero = 0

            'Ciclo for que nos determina con cada ciclo las unidades (1) decenas (2) centenas (3)
            For i = 1 To 3
                'lnDigito toma el residuo de lyCantidad dividido entre diez
                lnDigito = lyCantidad Mod 10
                If lnDigito <> 0 Then
                    Select Case i
                        Case 1
                            'Asigna nombre a las unidades. i vale una indicando que son unidades
                            lcBloque = " " & laUnidades(lnDigito - 1)
                            lnPrimerDigito = lnDigito
                        Case 2
                            If lnDigito <= 2 Then
                                lcBloque = " " & laUnidades((lnDigito * 10) + lnPrimerDigito - 1)
                            Else
                                lcBloque = " " & laDecenas(lnDigito - 1) & IIf(lnPrimerDigito <> 0, " Y", System.DBNull.Value) & lcBloque
                            End If
                            lnSegundoDigito = lnDigito
                        Case 3
                            lcBloque = " " & IIf(lnDigito = 1 And lnPrimerDigito = 0 And lnSegundoDigito = 0, "Cien", laCentenas(lnDigito - 1)) & lcBloque
                            lnTercerDigito = lnDigito
                    End Select
                Else
                    lnBloqueCero = lnBloqueCero + 1
                End If
                lyCantidad = Int(lyCantidad / 10)
                If lyCantidad = 0 Then
                    Exit For
                End If
            Next i
            Select Case lnNumeroBloques
                Case 1
                    Letras = lcBloque
                Case 2
                    Letras = lcBloque & IIf(lnBloqueCero = 3, System.DBNull.Value, " Mil") & Letras
                Case 3
                    Letras = lcBloque & IIf(lnPrimerDigito = 1 And lnSegundoDigito = 0 And lnTercerDigito = 0, " Millon", " Millones") & Letras
            End Select
            lnNumeroBloques = lnNumeroBloques + 1
        Loop Until lyCantidad = 0

        Dim Comparar As String
        Dim MonedaLetras As String

        Comparar = Letras.Trim

        If MonedaID = 1 Then
            MonedaLetras = " CORDOBAS CON "
        Else
            If Comparar = "Un" Then
                MonedaLetras = " DOLAR CON "
            Else
                MonedaLetras = " DOLARES CON "
            End If
        End If

        Letras = (Letras & IIf(tyCantidad > 1, " ", " CERO ") & MonedaLetras & CInt(lyCentavos) & "/100").ToString.ToUpper


    End Function
    Public Shared Function codificar(ByVal in_string) As String

        Dim FRUS As Integer = 31
        Dim COUNTER As Integer = 0
        Dim IN_LEN As Integer = 0
        Dim NEXT_CHAR As String = String.Empty
        Dim out_string As String = String.Empty

        in_string = LTrim(RTrim(in_string.ToString.ToUpper))
        IN_LEN = in_string.ToString.Length

        For COUNTER = 0 To IN_LEN - 1
            NEXT_CHAR = in_string.ToString.Substring(COUNTER, 1)
            out_string = out_string + Chr((Asc(NEXT_CHAR) + FRUS) * 2)
        Next
        Return out_string

    End Function
    Public Shared Function descodificar(ByVal in_string) As String


        MessageBox.Show(in_string, "Entrada a Descodificar")

        Dim FRUS As Integer = 31
        Dim COUNTER As Integer = 0
        Dim IN_LEN As Integer = 0
        Dim NEXT_CHAR As String = String.Empty
        Dim out_string As String = String.Empty
        MessageBox.Show("303")
        in_string = LTrim(RTrim(in_string.ToString))
        ' MessageBox.Show("305")
        IN_LEN = in_string.ToString.Length
        MessageBox.Show("307")

        For COUNTER = 0 To IN_LEN - 1
            MessageBox.Show("Entra al Ciclo For")
            NEXT_CHAR = in_string.ToString.Substring(COUNTER, 1)
            MessageBox.Show(NEXT_CHAR, "Valor Next Char")
            out_string = out_string + Chr((Asc(NEXT_CHAR) / 2) - FRUS)
            MessageBox.Show(out_string, "out_string")
        Next

        MessageBox.Show(out_string, "Salida Descodificada")
        Return out_string

    End Function
    Public Shared Function dia(ByVal no_dia As Integer) As String

        Dim mes_ As String = String.Empty

        Select Case no_dia
            Case 0
                mes_ = "Domingo"
            Case 1
                mes_ = "Lunes"
            Case 2
                mes_ = "Martes"
            Case 3
                mes_ = "Miercoles"
            Case 4
                mes_ = "Jueves"
            Case 5
                mes_ = "Viernes"
            Case 6
                mes_ = "Sabado"
        End Select

        Return mes_

    End Function

    Public Shared Function mes(ByVal No_Mes As Integer) As String

        Dim mes_ As String = String.Empty

        Select Case No_Mes
            Case 1
                mes_ = "Enero"
            Case 2
                mes_ = "Febrero"
            Case 3
                mes_ = "Marzo"
            Case 4
                mes_ = "Abril"
            Case 5
                mes_ = "Mayo"
            Case 6
                mes_ = "Junio"
            Case 7
                mes_ = "Julio"
            Case 8
                mes_ = "Agosto"
            Case 9
                mes_ = "Septiembre"
            Case 10
                mes_ = "Octubre"
            Case 11
                mes_ = "Noviembre"
            Case 12
                mes_ = "Diciembre"
        End Select

        Return mes_
    End Function
    Public Shared Function LeeEstilo() As String
        'Try
        '    Dim sk As String = GetSetting(My.Application.Info.ProductName.ToString, "Skin", "NSkin")

        '    ''No se Encuentra la Confiuraci�n
        '    If sk.Trim.Length = 0 Then
        '        EscribeEstilo("DevExpress Style")
        '        Return "DevExpress Style"
        '    Else
        '        Return sk
        '    End If
        'Catch ex As Exception
        '    Return "DevExpress Style"
        'End Try

        Try
            Dim dt As DataTable = Rutinas_SGC.obtener_datos("select skin from usuarios where usuario='" & usuario_ & "'")
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item("skin")
            Else
                Return "DevExpress Style"
            End If
        Catch ex As Exception
            Return "DevExpress Style"
        End Try
    End Function
    Public Shared Function EscribeEstilo(ByVal Valor As String)
        'Try
        '    SaveSetting(My.Application.Info.ProductName.ToString, "Skin", "NSkin", Valor)
        'Catch ex As Exception
        '    MessageBox.Show("Error al Guardar en el registro de windows, favor consultar con el area de Soporte Tecnico.")
        'End Try

        Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
        Dim cmd As New SqlCommand("UPDATE usuarios SET skin = @skin WHERE usuario=@usuario", conexion)
        Try
            cmd.Parameters.Add("skin", SqlDbType.NVarChar)
            cmd.Parameters("skin").Value = Valor

            cmd.Parameters.Add("usuario", SqlDbType.NVarChar)
            cmd.Parameters("usuario").Value = usuario_

            conexion.Open()
            cmd.ExecuteNonQuery()
            conexion.Close()

        Catch ex As Exception
            conexion.Close()
            MessageBox.Show(ex.Message)
        End Try
    End Function
    Public Shared Sub SetStyleController(ByVal parent As Control, ByVal g As StyleController)


        g.AppearanceFocused.BackColor = Color.FromArgb(255, 217, 109)
        For Each control As Control In parent.Controls
            Dim baseControl = TryCast(control, BaseControl)
            If baseControl IsNot Nothing Then
                baseControl.StyleController = g
            End If
            SetStyleController(control, g)
        Next control
    End Sub

    Public Shared Function EventsAction(ControType As String, ControlName As String, EventsAction_ As String, Click As String, RibbonPage As String, OptionPage As String, Form As String) As Boolean

        Dim mIpHostEntry As IPHostEntry
        mIpHostEntry = Dns.GetHostEntry(My.Computer.Name)

        Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
        Dim cmd As New SqlCommand("sp_insert_eventsaction", conexion)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            cmd.Parameters.Add("@UsrID", SqlDbType.NVarChar)
            cmd.Parameters("@UsrID").Value = usuario_

            cmd.Parameters.Add("@IP", SqlDbType.NVarChar)
            cmd.Parameters("@IP").Value = mIpHostEntry.AddressList(1).ToString()

            cmd.Parameters.Add("@HostName", SqlDbType.NVarChar)
            cmd.Parameters("@HostName").Value = My.Computer.Name

            cmd.Parameters.Add("@ControlType", SqlDbType.NVarChar)
            cmd.Parameters("@ControlType").Value = ControType

            cmd.Parameters.Add("@ControlName", SqlDbType.NVarChar)
            cmd.Parameters("@ControlName").Value = ControlName

            cmd.Parameters.Add("@EventsMouse", SqlDbType.NVarChar)
            cmd.Parameters("@EventsMouse").Value = EventsAction_

            cmd.Parameters.Add("@Click", SqlDbType.NVarChar)
            cmd.Parameters("@Click").Value = Click

            cmd.Parameters.Add("@RibbonPage", SqlDbType.NVarChar)
            cmd.Parameters("@RibbonPage").Value = RibbonPage

            cmd.Parameters.Add("@OptionPage", SqlDbType.NVarChar)
            cmd.Parameters("@OptionPage").Value = OptionPage

            cmd.Parameters.Add("@Form", SqlDbType.NVarChar)
            cmd.Parameters("@Form").Value = Form

            conexion.Open()
            cmd.ExecuteNonQuery()
            conexion.Close()

        Catch ex As Exception

            MessageBox.Show(ex.Message)

        End Try

    End Function
    
    Public Shared Function GetMes() As DataTable

        Dim DA As New SqlDataAdapter("select * from meses", Rutinas_SGC.CadenaConexion)
        Dim DS As New DataTable

        DA.Fill(DS)
        Return DS

    End Function
    Public Shared Function GetColectionData(Objet As DevExpress.XtraEditors.GridLookUpEdit)

        Objet.Properties.DataSource = GetMes()
        Objet.Properties.ValueMember = "id_mes"
        Objet.Properties.DisplayMember = "mes"

    End Function

    Public Shared Function Transaccion_Commit()
        Transaccion_Global.Commit()
        Conexion_Global.Close()
        Conexion_Global = Nothing
        Transaccion_Global = Nothing
    End Function

    Public Shared Function Transaccion_Rollback()
        Transaccion_Global.Rollback()
        Conexion_Global.Close()
        Conexion_Global = Nothing
        Transaccion_Global = Nothing
    End Function

    Public Shared Function IsNull(ByVal Var, Optional ByVal Valor = vbNullString)
        If IsDBNull(Var) OrElse IsNothing(Var) OrElse Var = vbNullString OrElse Var = Nothing Then
            Return IIf(IsNothing(Valor), "", Valor)
        Else
            Return Var
        End If
    End Function

End Class

