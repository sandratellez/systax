﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_P_Retencion
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_P_Retencion))
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip3 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem3 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem3 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip4 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem4 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem4 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip5 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem5 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem5 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip6 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem6 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem6 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip7 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem7 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem7 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip8 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem8 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem8 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip9 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem9 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem9 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip10 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem10 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem10 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip11 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem11 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim SuperToolTip12 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem12 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem11 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip13 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem13 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem12 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim GridFormatRule1 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue1 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule2 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue2 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Me.GridColumn27 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn17 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Link_adjunto = New DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar2 = New DevExpress.XtraBars.Bar()
        Me.btn_filtro = New DevExpress.XtraBars.BarButtonItem()
        Me.lbl_periodo = New DevExpress.XtraBars.BarStaticItem()
        Me.btn_nuevo = New DevExpress.XtraBars.BarButtonItem()
        Me.btn_editar = New DevExpress.XtraBars.BarButtonItem()
        Me.btn_anulado = New DevExpress.XtraBars.BarButtonItem()
        Me.btn_print = New DevExpress.XtraBars.BarButtonItem()
        Me.btn_openadjunto = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem7 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem8 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.btn_exportar = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem6 = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.btn_eliminar = New DevExpress.XtraBars.BarButtonItem()
        Me.btn_imprimir = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarListItem1 = New DevExpress.XtraBars.BarListItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.GridRetencion = New DevExpress.XtraGrid.GridControl()
        Me.GridViewRetension = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn24 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn28 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn29 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn30 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn32 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn33 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn18 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn19 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn20 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.RepositoryItemCheckEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        CType(Me.Link_adjunto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridRetencion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewRetension, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridColumn27
        '
        Me.GridColumn27.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn27.AppearanceCell.Options.UseFont = True
        Me.GridColumn27.Caption = " "
        Me.GridColumn27.FieldName = "Activo"
        Me.GridColumn27.Image = CType(resources.GetObject("GridColumn27.Image"), System.Drawing.Image)
        Me.GridColumn27.ImageAlignment = System.Drawing.StringAlignment.Center
        Me.GridColumn27.Name = "GridColumn27"
        Me.GridColumn27.OptionsColumn.AllowEdit = False
        Me.GridColumn27.OptionsColumn.ReadOnly = True
        Me.GridColumn27.Visible = True
        Me.GridColumn27.VisibleIndex = 23
        Me.GridColumn27.Width = 68
        '
        'GridColumn17
        '
        Me.GridColumn17.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn17.AppearanceCell.Options.UseFont = True
        Me.GridColumn17.AppearanceHeader.Options.UseTextOptions = True
        Me.GridColumn17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridColumn17.Caption = " "
        Me.GridColumn17.ColumnEdit = Me.Link_adjunto
        Me.GridColumn17.FieldName = "Adjunto"
        Me.GridColumn17.Image = CType(resources.GetObject("GridColumn17.Image"), System.Drawing.Image)
        Me.GridColumn17.ImageAlignment = System.Drawing.StringAlignment.Center
        Me.GridColumn17.Name = "GridColumn17"
        Me.GridColumn17.Visible = True
        Me.GridColumn17.VisibleIndex = 4
        Me.GridColumn17.Width = 35
        '
        'Link_adjunto
        '
        Me.Link_adjunto.AutoHeight = False
        Me.Link_adjunto.Name = "Link_adjunto"
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btn_filtro, Me.btn_nuevo, Me.btn_editar, Me.btn_eliminar, Me.btn_imprimir, Me.BarButtonItem6, Me.btn_exportar, Me.BarButtonItem2, Me.BarListItem1, Me.lbl_periodo, Me.btn_anulado, Me.BarButtonItem1, Me.BarButtonItem3, Me.BarButtonItem4, Me.btn_openadjunto, Me.BarButtonItem7, Me.BarButtonItem8, Me.btn_print})
        Me.BarManager1.MainMenu = Me.Bar2
        Me.BarManager1.MaxItemId = 18
        '
        'Bar2
        '
        Me.Bar2.BarName = "Menú principal"
        Me.Bar2.DockCol = 0
        Me.Bar2.DockRow = 0
        Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btn_filtro), New DevExpress.XtraBars.LinkPersistInfo(Me.lbl_periodo, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btn_nuevo, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btn_editar), New DevExpress.XtraBars.LinkPersistInfo(Me.btn_anulado), New DevExpress.XtraBars.LinkPersistInfo(Me.btn_print), New DevExpress.XtraBars.LinkPersistInfo(Me.btn_openadjunto, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem7), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem8, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem4), New DevExpress.XtraBars.LinkPersistInfo(Me.btn_exportar), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem6, True)})
        Me.Bar2.OptionsBar.MultiLine = True
        Me.Bar2.OptionsBar.UseWholeRow = True
        Me.Bar2.Text = "Menú principal"
        '
        'btn_filtro
        '
        Me.btn_filtro.Caption = "Busqueda"
        Me.btn_filtro.Id = 0
        Me.btn_filtro.ImageOptions.Image = CType(resources.GetObject("btn_filtro.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_filtro.ImageOptions.LargeImage = CType(resources.GetObject("btn_filtro.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.btn_filtro.Name = "btn_filtro"
        Me.btn_filtro.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Busqueda"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Bucar retenciones por periodo de fecha"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.btn_filtro.SuperTip = SuperToolTip1
        Me.btn_filtro.Tag = "Busqueda Impuesto Rentenciones"
        '
        'lbl_periodo
        '
        Me.lbl_periodo.Id = 9
        Me.lbl_periodo.ItemAppearance.Normal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbl_periodo.ItemAppearance.Normal.Options.UseForeColor = True
        Me.lbl_periodo.Name = "lbl_periodo"
        '
        'btn_nuevo
        '
        Me.btn_nuevo.Caption = "Agregar"
        Me.btn_nuevo.Id = 1
        Me.btn_nuevo.ImageOptions.Image = CType(resources.GetObject("btn_nuevo.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_nuevo.Name = "btn_nuevo"
        ToolTipTitleItem2.Appearance.Image = CType(resources.GetObject("resource.Image1"), System.Drawing.Image)
        ToolTipTitleItem2.Appearance.Options.UseImage = True
        ToolTipTitleItem2.Image = CType(resources.GetObject("ToolTipTitleItem2.Image"), System.Drawing.Image)
        ToolTipTitleItem2.Text = "Agregar"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Crea un retencion IMI e IR"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.btn_nuevo.SuperTip = SuperToolTip2
        Me.btn_nuevo.Tag = "crear una retencion IMI e IR"
        '
        'btn_editar
        '
        Me.btn_editar.Caption = "Impuesto & Retencion"
        Me.btn_editar.Id = 2
        Me.btn_editar.ImageOptions.Image = CType(resources.GetObject("btn_editar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_editar.Name = "btn_editar"
        ToolTipTitleItem3.Appearance.Image = CType(resources.GetObject("resource.Image2"), System.Drawing.Image)
        ToolTipTitleItem3.Appearance.Options.UseImage = True
        ToolTipTitleItem3.Image = CType(resources.GetObject("ToolTipTitleItem3.Image"), System.Drawing.Image)
        ToolTipTitleItem3.Text = "Editar"
        ToolTipItem3.LeftIndent = 6
        ToolTipItem3.Text = "Modificar los datos de una retención"
        SuperToolTip3.Items.Add(ToolTipTitleItem3)
        SuperToolTip3.Items.Add(ToolTipItem3)
        Me.btn_editar.SuperTip = SuperToolTip3
        Me.btn_editar.Tag = "Editando Retencion"
        '
        'btn_anulado
        '
        Me.btn_anulado.Caption = "Impuestos  & Retenciones"
        Me.btn_anulado.Id = 10
        Me.btn_anulado.ImageOptions.Image = CType(resources.GetObject("btn_anulado.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_anulado.Name = "btn_anulado"
        ToolTipTitleItem4.Appearance.Image = CType(resources.GetObject("resource.Image3"), System.Drawing.Image)
        ToolTipTitleItem4.Appearance.Options.UseImage = True
        ToolTipTitleItem4.Image = CType(resources.GetObject("ToolTipTitleItem4.Image"), System.Drawing.Image)
        ToolTipTitleItem4.Text = "Anulación"
        ToolTipItem4.LeftIndent = 6
        ToolTipItem4.Text = "Anular Trasacción Impuestos & Retenciones"
        SuperToolTip4.Items.Add(ToolTipTitleItem4)
        SuperToolTip4.Items.Add(ToolTipItem4)
        Me.btn_anulado.SuperTip = SuperToolTip4
        Me.btn_anulado.Tag = "Anulando Retencion"
        '
        'btn_print
        '
        Me.btn_print.Caption = "Impuestos & Retenciones"
        Me.btn_print.Id = 17
        Me.btn_print.ImageOptions.Image = CType(resources.GetObject("btn_print.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_print.Name = "btn_print"
        ToolTipTitleItem5.Text = "Imprimir"
        ToolTipItem5.LeftIndent = 6
        ToolTipItem5.Text = "Formato Constancia IMI"
        SuperToolTip5.Items.Add(ToolTipTitleItem5)
        SuperToolTip5.Items.Add(ToolTipItem5)
        Me.btn_print.SuperTip = SuperToolTip5
        Me.btn_print.Tag = "Imprimiendo Retencion"
        '
        'btn_openadjunto
        '
        Me.btn_openadjunto.Caption = "Impuestos & Retenciones"
        Me.btn_openadjunto.Id = 14
        Me.btn_openadjunto.ImageOptions.Image = CType(resources.GetObject("btn_openadjunto.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_openadjunto.Name = "btn_openadjunto"
        ToolTipTitleItem6.Text = "Vista Previa"
        ToolTipItem6.LeftIndent = 6
        ToolTipItem6.Text = "Ver Documento Adjunto"
        SuperToolTip6.Items.Add(ToolTipTitleItem6)
        SuperToolTip6.Items.Add(ToolTipItem6)
        Me.btn_openadjunto.SuperTip = SuperToolTip6
        Me.btn_openadjunto.Tag = "Ver Archivo Adjunto"
        '
        'BarButtonItem7
        '
        Me.BarButtonItem7.Caption = "Impuestos & Retenciones"
        Me.BarButtonItem7.Id = 15
        Me.BarButtonItem7.ImageOptions.Image = CType(resources.GetObject("BarButtonItem7.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem7.Name = "BarButtonItem7"
        ToolTipTitleItem7.Appearance.Image = CType(resources.GetObject("resource.Image4"), System.Drawing.Image)
        ToolTipTitleItem7.Appearance.Options.UseImage = True
        ToolTipTitleItem7.Image = CType(resources.GetObject("ToolTipTitleItem7.Image"), System.Drawing.Image)
        ToolTipTitleItem7.Text = "Anular"
        ToolTipItem7.LeftIndent = 6
        ToolTipItem7.Text = "Elimina el documento adjunto"
        SuperToolTip7.Items.Add(ToolTipTitleItem7)
        SuperToolTip7.Items.Add(ToolTipItem7)
        Me.BarButtonItem7.SuperTip = SuperToolTip7
        Me.BarButtonItem7.Tag = "Anulando Archivo Adjunto"
        '
        'BarButtonItem8
        '
        Me.BarButtonItem8.Id = 16
        Me.BarButtonItem8.ImageOptions.Image = CType(resources.GetObject("BarButtonItem8.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem8.Name = "BarButtonItem8"
        Me.BarButtonItem8.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        ToolTipTitleItem8.Appearance.Image = CType(resources.GetObject("resource.Image5"), System.Drawing.Image)
        ToolTipTitleItem8.Appearance.Options.UseImage = True
        ToolTipTitleItem8.Image = CType(resources.GetObject("ToolTipTitleItem8.Image"), System.Drawing.Image)
        ToolTipTitleItem8.Text = "Exportar"
        ToolTipItem8.LeftIndent = 6
        ToolTipItem8.Text = "Exportar Listado General"
        SuperToolTip8.Items.Add(ToolTipTitleItem8)
        SuperToolTip8.Items.Add(ToolTipItem8)
        Me.BarButtonItem8.SuperTip = SuperToolTip8
        Me.BarButtonItem8.Tag = "Exportar Listado General"
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "IVA"
        Me.BarButtonItem4.Id = 13
        Me.BarButtonItem4.ImageOptions.Image = CType(resources.GetObject("BarButtonItem4.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem4.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.BarButtonItem4.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Maroon
        Me.BarButtonItem4.ItemAppearance.Normal.Options.UseFont = True
        Me.BarButtonItem4.ItemAppearance.Normal.Options.UseForeColor = True
        Me.BarButtonItem4.Name = "BarButtonItem4"
        Me.BarButtonItem4.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        ToolTipTitleItem9.Text = "Exportar"
        ToolTipItem9.LeftIndent = 6
        ToolTipItem9.Text = "Impuesto al Valor Agregado IVA"
        SuperToolTip9.Items.Add(ToolTipTitleItem9)
        SuperToolTip9.Items.Add(ToolTipItem9)
        Me.BarButtonItem4.SuperTip = SuperToolTip9
        Me.BarButtonItem4.Tag = "Exportar Impuesto al valor agregado IVA"
        '
        'btn_exportar
        '
        Me.btn_exportar.Caption = "IMI "
        Me.btn_exportar.Id = 6
        Me.btn_exportar.ImageOptions.Image = CType(resources.GetObject("btn_exportar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_exportar.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_exportar.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Blue
        Me.btn_exportar.ItemAppearance.Normal.Options.UseFont = True
        Me.btn_exportar.ItemAppearance.Normal.Options.UseForeColor = True
        Me.btn_exportar.Name = "btn_exportar"
        Me.btn_exportar.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        ToolTipTitleItem10.Appearance.Image = CType(resources.GetObject("resource.Image6"), System.Drawing.Image)
        ToolTipTitleItem10.Appearance.Options.UseImage = True
        ToolTipTitleItem10.Image = CType(resources.GetObject("ToolTipTitleItem10.Image"), System.Drawing.Image)
        ToolTipTitleItem10.Text = "Exportar "
        ToolTipItem10.LeftIndent = 6
        ToolTipItem10.Text = "Impuesto Municipal Sobre Ingresos -  IMI"
        SuperToolTip10.Items.Add(ToolTipTitleItem10)
        SuperToolTip10.Items.Add(ToolTipItem10)
        Me.btn_exportar.SuperTip = SuperToolTip10
        Me.btn_exportar.Tag = "Exportar Impuesto Municipal Sobre Ingreso IMI"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "IR"
        Me.BarButtonItem1.Id = 11
        Me.BarButtonItem1.ImageOptions.Image = CType(resources.GetObject("BarButtonItem1.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem1.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem1.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem1.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarButtonItem1.ItemAppearance.Normal.ForeColor = System.Drawing.Color.Red
        Me.BarButtonItem1.ItemAppearance.Normal.Options.UseFont = True
        Me.BarButtonItem1.ItemAppearance.Normal.Options.UseForeColor = True
        Me.BarButtonItem1.Name = "BarButtonItem1"
        Me.BarButtonItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        Me.BarButtonItem1.Tag = "Exportar Impuesto Sobre la Renta IR"
        '
        'BarButtonItem6
        '
        Me.BarButtonItem6.Caption = "Regresar"
        Me.BarButtonItem6.Id = 5
        Me.BarButtonItem6.ImageOptions.Image = CType(resources.GetObject("BarButtonItem6.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem6.Name = "BarButtonItem6"
        ToolTipTitleItem11.Text = "Regresar"
        SuperToolTip11.Items.Add(ToolTipTitleItem11)
        Me.BarButtonItem6.SuperTip = SuperToolTip11
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Manager = Me.BarManager1
        Me.barDockControlTop.Size = New System.Drawing.Size(1370, 40)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 488)
        Me.barDockControlBottom.Manager = Me.BarManager1
        Me.barDockControlBottom.Size = New System.Drawing.Size(1370, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 40)
        Me.barDockControlLeft.Manager = Me.BarManager1
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 448)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(1370, 40)
        Me.barDockControlRight.Manager = Me.BarManager1
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 448)
        '
        'btn_eliminar
        '
        Me.btn_eliminar.Caption = "BarButtonItem4"
        Me.btn_eliminar.Id = 3
        Me.btn_eliminar.ImageOptions.Image = CType(resources.GetObject("btn_eliminar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_eliminar.Name = "btn_eliminar"
        ToolTipTitleItem12.Appearance.Image = CType(resources.GetObject("resource.Image7"), System.Drawing.Image)
        ToolTipTitleItem12.Appearance.Options.UseImage = True
        ToolTipTitleItem12.Image = CType(resources.GetObject("ToolTipTitleItem12.Image"), System.Drawing.Image)
        ToolTipTitleItem12.Text = "Anular"
        ToolTipItem11.LeftIndent = 6
        ToolTipItem11.Text = "Anular la retención selecionada"
        SuperToolTip12.Items.Add(ToolTipTitleItem12)
        SuperToolTip12.Items.Add(ToolTipItem11)
        Me.btn_eliminar.SuperTip = SuperToolTip12
        '
        'btn_imprimir
        '
        Me.btn_imprimir.Caption = "BarButtonItem5"
        Me.btn_imprimir.Id = 4
        Me.btn_imprimir.ImageOptions.Image = CType(resources.GetObject("btn_imprimir.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_imprimir.Name = "btn_imprimir"
        ToolTipTitleItem13.Appearance.Image = CType(resources.GetObject("resource.Image8"), System.Drawing.Image)
        ToolTipTitleItem13.Appearance.Options.UseImage = True
        ToolTipTitleItem13.Image = CType(resources.GetObject("ToolTipTitleItem13.Image"), System.Drawing.Image)
        ToolTipTitleItem13.Text = "Imprimir "
        ToolTipItem12.LeftIndent = 6
        ToolTipItem12.Text = "Imprime la retención IMI selecionada "
        SuperToolTip13.Items.Add(ToolTipTitleItem13)
        SuperToolTip13.Items.Add(ToolTipItem12)
        Me.btn_imprimir.SuperTip = SuperToolTip13
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "BarButtonItem2"
        Me.BarButtonItem2.Id = 7
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarListItem1
        '
        Me.BarListItem1.Caption = "BarListItem1"
        Me.BarListItem1.Id = 8
        Me.BarListItem1.Name = "BarListItem1"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "IR "
        Me.BarButtonItem3.Id = 12
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'GridRetencion
        '
        Me.GridRetencion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridRetencion.Location = New System.Drawing.Point(0, 40)
        Me.GridRetencion.MainView = Me.GridViewRetension
        Me.GridRetencion.Name = "GridRetencion"
        Me.GridRetencion.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1, Me.RepositoryItemCheckEdit2, Me.Link_adjunto})
        Me.GridRetencion.Size = New System.Drawing.Size(1370, 448)
        Me.GridRetencion.TabIndex = 11
        Me.GridRetencion.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewRetension})
        '
        'GridViewRetension
        '
        Me.GridViewRetension.Appearance.FocusedCell.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GridViewRetension.Appearance.FocusedCell.Options.UseBackColor = True
        Me.GridViewRetension.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GridViewRetension.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridViewRetension.Appearance.FooterPanel.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridViewRetension.Appearance.FooterPanel.Options.UseFont = True
        Me.GridViewRetension.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
        Me.GridViewRetension.Appearance.GroupRow.Options.UseForeColor = True
        Me.GridViewRetension.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GridViewRetension.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridViewRetension.Appearance.Row.BackColor = System.Drawing.Color.White
        Me.GridViewRetension.Appearance.Row.Options.UseBackColor = True
        Me.GridViewRetension.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.GridViewRetension.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridViewRetension.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn15, Me.GridColumn2, Me.GridColumn6, Me.GridColumn7, Me.GridColumn3, Me.GridColumn24, Me.GridColumn27, Me.GridColumn28, Me.GridColumn29, Me.GridColumn30, Me.GridColumn32, Me.GridColumn33, Me.GridColumn1, Me.GridColumn4, Me.GridColumn5, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10, Me.GridColumn11, Me.GridColumn12, Me.GridColumn13, Me.GridColumn14, Me.GridColumn16, Me.GridColumn17, Me.GridColumn18, Me.GridColumn19, Me.GridColumn20})
        Me.GridViewRetension.CustomizationFormBounds = New System.Drawing.Rectangle(167, 372, 322, 354)
        GridFormatRule1.ApplyToRow = True
        GridFormatRule1.Column = Me.GridColumn27
        GridFormatRule1.Name = "Format0"
        FormatConditionRuleValue1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        FormatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red
        FormatConditionRuleValue1.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue1.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue1.Value1 = False
        GridFormatRule1.Rule = FormatConditionRuleValue1
        GridFormatRule2.Column = Me.GridColumn17
        GridFormatRule2.ColumnApplyTo = Me.GridColumn17
        GridFormatRule2.Name = "Adjunto"
        FormatConditionRuleValue2.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        FormatConditionRuleValue2.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue2.Value1 = "Sí"
        GridFormatRule2.Rule = FormatConditionRuleValue2
        Me.GridViewRetension.FormatRules.Add(GridFormatRule1)
        Me.GridViewRetension.FormatRules.Add(GridFormatRule2)
        Me.GridViewRetension.GridControl = Me.GridRetencion
        Me.GridViewRetension.HorzScrollStep = 25
        Me.GridViewRetension.Name = "GridViewRetension"
        Me.GridViewRetension.OptionsFind.FindNullPrompt = "Enter para buscar el texto"
        Me.GridViewRetension.OptionsView.ColumnAutoWidth = False
        Me.GridViewRetension.OptionsView.ShowAutoFilterRow = True
        Me.GridViewRetension.OptionsView.ShowFooter = True
        Me.GridViewRetension.OptionsView.ShowGroupPanel = False
        Me.GridViewRetension.OptionsView.ShowIndicator = False
        Me.GridViewRetension.RowHeight = 25
        '
        'GridColumn15
        '
        Me.GridColumn15.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn15.AppearanceCell.Options.UseFont = True
        Me.GridColumn15.Caption = "FileName"
        Me.GridColumn15.FieldName = "NameFile"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.OptionsColumn.AllowEdit = False
        Me.GridColumn15.OptionsColumn.ReadOnly = True
        '
        'GridColumn2
        '
        Me.GridColumn2.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn2.AppearanceCell.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GridColumn2.AppearanceCell.Options.UseFont = True
        Me.GridColumn2.AppearanceCell.Options.UseForeColor = True
        Me.GridColumn2.Caption = "Retenido"
        Me.GridColumn2.FieldName = "Proveedor"
        Me.GridColumn2.Image = CType(resources.GetObject("GridColumn2.Image"), System.Drawing.Image)
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.OptionsColumn.ReadOnly = True
        Me.GridColumn2.OptionsFilter.AllowFilter = False
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 7
        Me.GridColumn2.Width = 221
        '
        'GridColumn6
        '
        Me.GridColumn6.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn6.AppearanceCell.ForeColor = System.Drawing.Color.Black
        Me.GridColumn6.AppearanceCell.Options.UseFont = True
        Me.GridColumn6.AppearanceCell.Options.UseForeColor = True
        Me.GridColumn6.Caption = "Ruc"
        Me.GridColumn6.FieldName = "DocumentoID"
        Me.GridColumn6.Image = CType(resources.GetObject("GridColumn6.Image"), System.Drawing.Image)
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.OptionsColumn.AllowEdit = False
        Me.GridColumn6.OptionsColumn.ReadOnly = True
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 5
        Me.GridColumn6.Width = 154
        '
        'GridColumn7
        '
        Me.GridColumn7.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn7.AppearanceCell.Options.UseFont = True
        Me.GridColumn7.Caption = "Fecha"
        Me.GridColumn7.FieldName = "Fecha"
        Me.GridColumn7.Image = CType(resources.GetObject("GridColumn7.Image"), System.Drawing.Image)
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.OptionsColumn.AllowEdit = False
        Me.GridColumn7.OptionsColumn.ReadOnly = True
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 3
        Me.GridColumn7.Width = 80
        '
        'GridColumn3
        '
        Me.GridColumn3.AppearanceCell.BackColor = System.Drawing.Color.LightYellow
        Me.GridColumn3.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn3.AppearanceCell.ForeColor = System.Drawing.Color.Blue
        Me.GridColumn3.AppearanceCell.Options.UseBackColor = True
        Me.GridColumn3.AppearanceCell.Options.UseFont = True
        Me.GridColumn3.AppearanceCell.Options.UseForeColor = True
        Me.GridColumn3.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.GridColumn3.Caption = "IMI C$"
        Me.GridColumn3.DisplayFormat.FormatString = "{0:n2}"
        Me.GridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn3.FieldName = "ImporteIMI"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.OptionsColumn.AllowEdit = False
        Me.GridColumn3.OptionsColumn.ReadOnly = True
        Me.GridColumn3.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ImporteIMI", "{0:n2}")})
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 11
        Me.GridColumn3.Width = 100
        '
        'GridColumn24
        '
        Me.GridColumn24.AppearanceCell.BackColor = System.Drawing.Color.AliceBlue
        Me.GridColumn24.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn24.AppearanceCell.ForeColor = System.Drawing.Color.Red
        Me.GridColumn24.AppearanceCell.Options.UseBackColor = True
        Me.GridColumn24.AppearanceCell.Options.UseFont = True
        Me.GridColumn24.AppearanceCell.Options.UseForeColor = True
        Me.GridColumn24.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn24.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridColumn24.AppearanceHeader.Options.UseTextOptions = True
        Me.GridColumn24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridColumn24.Caption = "Constancia IMI"
        Me.GridColumn24.FieldName = "ReciboIMI"
        Me.GridColumn24.Name = "GridColumn24"
        Me.GridColumn24.OptionsColumn.AllowEdit = False
        Me.GridColumn24.OptionsColumn.ReadOnly = True
        Me.GridColumn24.Visible = True
        Me.GridColumn24.VisibleIndex = 16
        Me.GridColumn24.Width = 80
        '
        'GridColumn28
        '
        Me.GridColumn28.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn28.AppearanceCell.Options.UseFont = True
        Me.GridColumn28.Caption = "Concepto"
        Me.GridColumn28.FieldName = "Concepto"
        Me.GridColumn28.Image = CType(resources.GetObject("GridColumn28.Image"), System.Drawing.Image)
        Me.GridColumn28.Name = "GridColumn28"
        Me.GridColumn28.OptionsColumn.AllowEdit = False
        Me.GridColumn28.OptionsColumn.ReadOnly = True
        Me.GridColumn28.Visible = True
        Me.GridColumn28.VisibleIndex = 8
        Me.GridColumn28.Width = 317
        '
        'GridColumn29
        '
        Me.GridColumn29.AppearanceCell.BackColor = System.Drawing.Color.LightYellow
        Me.GridColumn29.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn29.AppearanceCell.ForeColor = System.Drawing.Color.Blue
        Me.GridColumn29.AppearanceCell.Options.UseBackColor = True
        Me.GridColumn29.AppearanceCell.Options.UseFont = True
        Me.GridColumn29.AppearanceCell.Options.UseForeColor = True
        Me.GridColumn29.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn29.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.GridColumn29.Caption = "Sub Total C$"
        Me.GridColumn29.DisplayFormat.FormatString = "{0:n2}"
        Me.GridColumn29.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn29.FieldName = "ImporteFactura"
        Me.GridColumn29.Name = "GridColumn29"
        Me.GridColumn29.OptionsColumn.AllowEdit = False
        Me.GridColumn29.OptionsColumn.ReadOnly = True
        Me.GridColumn29.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ImporteFactura", "{0:n2}")})
        Me.GridColumn29.Visible = True
        Me.GridColumn29.VisibleIndex = 9
        Me.GridColumn29.Width = 100
        '
        'GridColumn30
        '
        Me.GridColumn30.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn30.AppearanceCell.Options.UseFont = True
        Me.GridColumn30.Caption = "Factura #"
        Me.GridColumn30.FieldName = "FacturaNo_"
        Me.GridColumn30.Image = CType(resources.GetObject("GridColumn30.Image"), System.Drawing.Image)
        Me.GridColumn30.Name = "GridColumn30"
        Me.GridColumn30.OptionsColumn.AllowEdit = False
        Me.GridColumn30.OptionsColumn.ReadOnly = True
        Me.GridColumn30.Visible = True
        Me.GridColumn30.VisibleIndex = 6
        Me.GridColumn30.Width = 100
        '
        'GridColumn32
        '
        Me.GridColumn32.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn32.AppearanceCell.Options.UseFont = True
        Me.GridColumn32.Caption = "Ck"
        Me.GridColumn32.FieldName = "Cheque"
        Me.GridColumn32.Name = "GridColumn32"
        Me.GridColumn32.OptionsColumn.AllowEdit = False
        Me.GridColumn32.OptionsColumn.ReadOnly = True
        Me.GridColumn32.Visible = True
        Me.GridColumn32.VisibleIndex = 20
        Me.GridColumn32.Width = 79
        '
        'GridColumn33
        '
        Me.GridColumn33.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn33.AppearanceCell.Options.UseFont = True
        Me.GridColumn33.Caption = "Banco"
        Me.GridColumn33.FieldName = "Siglas"
        Me.GridColumn33.Name = "GridColumn33"
        Me.GridColumn33.OptionsColumn.AllowEdit = False
        Me.GridColumn33.OptionsColumn.ReadOnly = True
        Me.GridColumn33.Visible = True
        Me.GridColumn33.VisibleIndex = 21
        Me.GridColumn33.Width = 136
        '
        'GridColumn1
        '
        Me.GridColumn1.AppearanceCell.BackColor = System.Drawing.Color.AliceBlue
        Me.GridColumn1.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn1.AppearanceCell.ForeColor = System.Drawing.Color.Red
        Me.GridColumn1.AppearanceCell.Options.UseBackColor = True
        Me.GridColumn1.AppearanceCell.Options.UseFont = True
        Me.GridColumn1.AppearanceCell.Options.UseForeColor = True
        Me.GridColumn1.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridColumn1.AppearanceHeader.Options.UseTextOptions = True
        Me.GridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridColumn1.Caption = "Constancia IR"
        Me.GridColumn1.FieldName = "ReciboIR"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.OptionsColumn.ReadOnly = True
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 17
        Me.GridColumn1.Width = 80
        '
        'GridColumn4
        '
        Me.GridColumn4.AppearanceCell.BackColor = System.Drawing.Color.LightYellow
        Me.GridColumn4.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn4.AppearanceCell.ForeColor = System.Drawing.Color.Blue
        Me.GridColumn4.AppearanceCell.Options.UseBackColor = True
        Me.GridColumn4.AppearanceCell.Options.UseFont = True
        Me.GridColumn4.AppearanceCell.Options.UseForeColor = True
        Me.GridColumn4.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.GridColumn4.AppearanceHeader.Options.UseTextOptions = True
        Me.GridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridColumn4.Caption = "IR C$"
        Me.GridColumn4.DisplayFormat.FormatString = "{0:n2}"
        Me.GridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn4.FieldName = "ImporteIR"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.OptionsColumn.AllowEdit = False
        Me.GridColumn4.OptionsColumn.ReadOnly = True
        Me.GridColumn4.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ImporteIR", "{0:n2}")})
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 12
        Me.GridColumn4.Width = 100
        '
        'GridColumn5
        '
        Me.GridColumn5.AppearanceCell.BackColor = System.Drawing.Color.LightYellow
        Me.GridColumn5.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn5.AppearanceCell.ForeColor = System.Drawing.Color.Blue
        Me.GridColumn5.AppearanceCell.Options.UseBackColor = True
        Me.GridColumn5.AppearanceCell.Options.UseFont = True
        Me.GridColumn5.AppearanceCell.Options.UseForeColor = True
        Me.GridColumn5.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.GridColumn5.AppearanceHeader.Options.UseTextOptions = True
        Me.GridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridColumn5.Caption = "IVA C$"
        Me.GridColumn5.DisplayFormat.FormatString = "{0:n2}"
        Me.GridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn5.FieldName = "ImporteIVA"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.OptionsColumn.AllowEdit = False
        Me.GridColumn5.OptionsColumn.ReadOnly = True
        Me.GridColumn5.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ImporteIVA", "{0:n2}")})
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 10
        Me.GridColumn5.Width = 100
        '
        'GridColumn8
        '
        Me.GridColumn8.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn8.AppearanceCell.Options.UseFont = True
        Me.GridColumn8.Caption = "Oficina"
        Me.GridColumn8.FieldName = "Oficina"
        Me.GridColumn8.Image = CType(resources.GetObject("GridColumn8.Image"), System.Drawing.Image)
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.OptionsColumn.AllowEdit = False
        Me.GridColumn8.OptionsColumn.ReadOnly = True
        Me.GridColumn8.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "Oficina", "{0}")})
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 1
        Me.GridColumn8.Width = 171
        '
        'GridColumn9
        '
        Me.GridColumn9.AppearanceCell.BackColor = System.Drawing.Color.WhiteSmoke
        Me.GridColumn9.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn9.AppearanceCell.Options.UseBackColor = True
        Me.GridColumn9.AppearanceCell.Options.UseFont = True
        Me.GridColumn9.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn9.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridColumn9.AppearanceHeader.Options.UseTextOptions = True
        Me.GridColumn9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridColumn9.Caption = "Alicuota IMI"
        Me.GridColumn9.DisplayFormat.FormatString = "{0:n2}"
        Me.GridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn9.FieldName = "IMI_por"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.OptionsColumn.AllowEdit = False
        Me.GridColumn9.OptionsColumn.ReadOnly = True
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 14
        '
        'GridColumn10
        '
        Me.GridColumn10.AppearanceCell.BackColor = System.Drawing.Color.WhiteSmoke
        Me.GridColumn10.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn10.AppearanceCell.Options.UseBackColor = True
        Me.GridColumn10.AppearanceCell.Options.UseFont = True
        Me.GridColumn10.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn10.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridColumn10.AppearanceHeader.Options.UseTextOptions = True
        Me.GridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridColumn10.Caption = "Alicuota IR"
        Me.GridColumn10.DisplayFormat.FormatString = "{0:n2}"
        Me.GridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn10.FieldName = "IR_por"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.OptionsColumn.AllowEdit = False
        Me.GridColumn10.OptionsColumn.ReadOnly = True
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 15
        '
        'GridColumn11
        '
        Me.GridColumn11.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn11.AppearanceCell.Options.UseFont = True
        Me.GridColumn11.Caption = "Concepto del Gasto"
        Me.GridColumn11.FieldName = "Descripcion"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.OptionsColumn.AllowEdit = False
        Me.GridColumn11.OptionsColumn.ReadOnly = True
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 19
        Me.GridColumn11.Width = 291
        '
        'GridColumn12
        '
        Me.GridColumn12.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn12.AppearanceCell.Options.UseFont = True
        Me.GridColumn12.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn12.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridColumn12.Caption = "Código IR"
        Me.GridColumn12.FieldName = "IR_ActID"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.OptionsColumn.AllowEdit = False
        Me.GridColumn12.OptionsColumn.ReadOnly = True
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 18
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "Observaciones"
        Me.GridColumn13.FieldName = "Observaciones"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.OptionsColumn.AllowEdit = False
        Me.GridColumn13.OptionsColumn.ReadOnly = True
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 22
        Me.GridColumn13.Width = 153
        '
        'GridColumn14
        '
        Me.GridColumn14.AppearanceCell.BackColor = System.Drawing.Color.WhiteSmoke
        Me.GridColumn14.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn14.AppearanceCell.Options.UseBackColor = True
        Me.GridColumn14.AppearanceCell.Options.UseFont = True
        Me.GridColumn14.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn14.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridColumn14.Caption = "Alicuota IVA"
        Me.GridColumn14.DisplayFormat.FormatString = "{0:n2}"
        Me.GridColumn14.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn14.FieldName = "IVA_por"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.OptionsColumn.AllowEdit = False
        Me.GridColumn14.OptionsColumn.ReadOnly = True
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 13
        '
        'GridColumn16
        '
        Me.GridColumn16.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn16.AppearanceCell.Options.UseFont = True
        Me.GridColumn16.Caption = "Path"
        Me.GridColumn16.FieldName = "FilePath"
        Me.GridColumn16.Name = "GridColumn16"
        Me.GridColumn16.OptionsColumn.AllowEdit = False
        Me.GridColumn16.OptionsColumn.ReadOnly = True
        '
        'GridColumn18
        '
        Me.GridColumn18.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn18.AppearanceCell.Options.UseFont = True
        Me.GridColumn18.Caption = "Usuario"
        Me.GridColumn18.FieldName = "UsuarioID"
        Me.GridColumn18.Name = "GridColumn18"
        Me.GridColumn18.OptionsColumn.AllowEdit = False
        Me.GridColumn18.OptionsColumn.ReadOnly = True
        Me.GridColumn18.Visible = True
        Me.GridColumn18.VisibleIndex = 2
        '
        'GridColumn19
        '
        Me.GridColumn19.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn19.AppearanceCell.Image = CType(resources.GetObject("GridColumn19.AppearanceCell.Image"), System.Drawing.Image)
        Me.GridColumn19.AppearanceCell.Options.UseFont = True
        Me.GridColumn19.AppearanceCell.Options.UseImage = True
        Me.GridColumn19.Caption = " "
        Me.GridColumn19.FieldName = "OficinaUNI"
        Me.GridColumn19.Image = CType(resources.GetObject("GridColumn19.Image"), System.Drawing.Image)
        Me.GridColumn19.ImageAlignment = System.Drawing.StringAlignment.Center
        Me.GridColumn19.Name = "GridColumn19"
        Me.GridColumn19.OptionsColumn.AllowEdit = False
        Me.GridColumn19.OptionsColumn.ReadOnly = True
        Me.GridColumn19.Visible = True
        Me.GridColumn19.VisibleIndex = 0
        Me.GridColumn19.Width = 46
        '
        'GridColumn20
        '
        Me.GridColumn20.AppearanceCell.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.GridColumn20.AppearanceCell.Options.UseFont = True
        Me.GridColumn20.AppearanceHeader.Options.UseTextOptions = True
        Me.GridColumn20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridColumn20.Caption = "ID"
        Me.GridColumn20.FieldName = "RetencionID"
        Me.GridColumn20.Name = "GridColumn20"
        Me.GridColumn20.OptionsColumn.AllowEdit = False
        Me.GridColumn20.OptionsColumn.ReadOnly = True
        Me.GridColumn20.Width = 49
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'RepositoryItemCheckEdit2
        '
        Me.RepositoryItemCheckEdit2.AutoHeight = False
        Me.RepositoryItemCheckEdit2.Name = "RepositoryItemCheckEdit2"
        '
        'Frm_P_Retencion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1370, 488)
        Me.Controls.Add(Me.GridRetencion)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "Frm_P_Retencion"
        Me.Tag = "Formulario Impuestos Retenciones "
        Me.Text = "Tax - Impuestos & Retenciones"
        CType(Me.Link_adjunto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridRetencion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewRetension, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents btn_filtro As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btn_nuevo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btn_editar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btn_eliminar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btn_imprimir As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btn_exportar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents lbl_periodo As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarListItem1 As DevExpress.XtraBars.BarListItem
    Friend WithEvents btn_anulado As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents GridRetencion As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewRetension As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn24 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn27 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn28 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn29 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn30 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn32 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn33 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents RepositoryItemCheckEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn17 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btn_openadjunto As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem7 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents GridColumn18 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BarButtonItem8 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents GridColumn19 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btn_print As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents GridColumn20 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Link_adjunto As DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit
End Class
