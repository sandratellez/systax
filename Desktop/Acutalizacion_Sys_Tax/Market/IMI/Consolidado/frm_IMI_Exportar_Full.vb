﻿Public Class frm_IMI_Exportar_Full
    Public fecha1 As New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
    Public fecha2 As New DateTime
    Public OficinaID As String
    Dim A As New Auditoria
    Private Sub frm_IMI_Exportar_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        fecha1 = New DateTime(Date.Now.Year, Date.Now.Month, 1)
        fecha2 = fecha1.AddMonths(1).AddDays(-1)
        OficinaID = "%"

    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_exportar.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Exportar", "Exportar", e.Item.Caption + "|" + e.Item.Tag)
        Dim f As New frmExportarImprimir
        f.Mostrar(Me.GridRetencion)
    End Sub

    Private Sub BarButtonItem2_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem2.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Regresar", "Regresar", e.Item.Caption + "|" + e.Item.Tag)
        Me.Close()
    End Sub

    Private Sub btn_filter_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_filter.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Buscar", "Filtro de Busquedas", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.frm_filtros.dtp_fechadesde.EditValue = CDate(fecha1)
        My.Forms.frm_filtros.dtp_fechahasta.EditValue = CDate(fecha2)
        My.Forms.frm_filtros.StartPosition = FormStartPosition.CenterScreen
        My.Forms.frm_filtros.WindowState = FormWindowState.Normal

        If My.Forms.frm_filtros.ShowDialog() = Windows.Forms.DialogResult.Yes Then

            fecha1 = CDate(My.Forms.frm_filtros.dtp_fechadesde.EditValue).ToShortDateString
            fecha2 = CDate(My.Forms.frm_filtros.dtp_fechahasta.EditValue).ToShortDateString
            OficinaID = My.Forms.frm_filtros.cmb_oficina.EditValue

            Me.lbl_periodo.Caption = CDate(My.Forms.frm_filtros.dtp_fechadesde.EditValue).ToShortDateString + " - " + CDate(My.Forms.frm_filtros.dtp_fechahasta.EditValue).ToShortDateString
            Me.lbl_oficinas.Caption = IIf(My.Forms.frm_filtros.OficinaID = "%", "Todos", My.Forms.frm_filtros.cmb_oficina.Text)
            Me.GridRetencion.DataSource = Retensiones.GetListRetencionImpuestosFULL(fecha1.ToShortDateString, fecha2.ToShortDateString, "IMI", My.Forms.frm_filtros.OficinaID)
        End If
    End Sub

    Private Sub BarButtonItem3_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem3.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Imprimir", "Imprimir", e.Item.Caption + "|" + e.Item.Tag)
    End Sub
End Class