﻿Public Class frm_ViewPicture 
    Dim Top_ As Integer
    Dim Step_ As Integer
    Private Sub frm_ViewPicture_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Top_ = My.Forms.Frm_IU_Proveedor.ProveedorImage.Rows.Count - 1
        Step_ = 0
        Me.txt_item.Caption = (Step_ + 1).ToString + " / " + (Top_ + 1).ToString
        Me.pic_foto.EditValue = My.Forms.Frm_IU_Proveedor.ProveedorImage.Rows(Step_).Item("Imagen")
    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_anterior.ItemClick

        If Step_ <= 0 Then
            Step_ = 0
        Else
            Step_ = Step_ - 1
        End If

        cargar(Step_)

    End Sub
    Sub cargar(Step_ As Integer)

        Me.txt_item.Caption = (Step_ + 1).ToString + " / " + (Top_ + 1).ToString
        Me.pic_foto.EditValue = My.Forms.Frm_IU_Proveedor.ProveedorImage.Rows(Step_).Item("Imagen")

    End Sub

    Private Sub BarButtonItem2_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_siguiente.ItemClick
        If Step_ >= Top_ Then
            Step_ = Top_
        Else
            Step_ = Step_ + 1
        End If

        cargar(Step_)

    End Sub

    'Private Sub BarEditItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bar_zoom.ItemClick
    '    Me.pic_foto.Properties.ZoomPercent = Me.bar_zoom.EditValue
    'End Sub

    Private Sub btn_regresar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_regresar.ItemClick
        Me.Close()
    End Sub
End Class