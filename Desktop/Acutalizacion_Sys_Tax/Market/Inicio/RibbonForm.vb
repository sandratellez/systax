﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.DBNull
Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Configuration
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraEditors
Imports System.Net
Imports DevExpress.XtraSplashScreen

Imports TC_Consumo.Servicio
Imports System.Xml
Imports System.Xml.Linq


Public Class RibbonForm
    Dim A As New Auditoria
    Private Sub Plantilla_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles Plantilla.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Proveedor", "Formulario Proveedor", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.Frm_P_Proveedor.MdiParent = Me
        My.Forms.Frm_P_Proveedor.Show()
        My.Forms.Frm_P_Proveedor.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub RibbonForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try

            Dim s As String() = Rutinas_SGC.LeeData()
            Me.lbl_server.Caption = s(0).ToUpper
            Me.lbl_Data.Caption = s(1).ToUpper

        Catch ex As Exception
        End Try

        
    End Sub

    Private Sub BarButtonItem5_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem5.ItemClick
        My.Forms.p_usr.MdiParent = Me
        My.Forms.p_usr.Show()
        My.Forms.p_usr.WindowState = FormWindowState.Maximized
        A.InsertarAuditoria(Usuario_, "Click", "Formulario Principal", e.Item.Caption + " | " + e.Item.Tag)
    End Sub


    Private Sub BarButtonItem18_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem18.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Impuesto", "Formulario Impuesto", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.Frm_P_Retencion.MdiParent = Me
        My.Forms.Frm_P_Retencion.Show()
        My.Forms.Frm_P_Retencion.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub BarButtonItem17_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem17.ItemClick
        A.InsertarAuditoria(Usuario_, "Click formulario IMI", "Boton de formulario IMI", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.FrmCargarDiseno.MdiParent = Me
        My.Forms.FrmCargarDiseno.Show()
        My.Forms.FrmCargarDiseno.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub BarButtonItem20_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem20.ItemClick
        A.InsertarAuditoria(Usuario_, " Click formulario oficina", "Boton de formulario oficinas", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.Frm_P_Agente.MdiParent = Me
        My.Forms.Frm_P_Agente.Show()
        My.Forms.Frm_P_Agente.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub BarButtonItem21_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem21.ItemClick
        A.InsertarAuditoria(Usuario_, " Click Talonarios", "Boton de formulario Talonarios", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.Frm_P_Talonario.MdiParent = Me
        My.Forms.Frm_P_Talonario.Show()
        My.Forms.Frm_P_Talonario.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub BarButtonItem24_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem24.ItemClick
        A.InsertarAuditoria(Usuario_, " Click Consolidado", "Consolidado", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.frm_consolidado.MdiParent = Me
        My.Forms.frm_consolidado.Show()
        My.Forms.frm_consolidado.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub btn_IR_full_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_IR_full.ItemClick
        A.InsertarAuditoria(Usuario_, "Click IR", "Formulario IR", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.frm_IR_Exportar_Full.MdiParent = Me
        My.Forms.frm_IR_Exportar_Full.Show()
        My.Forms.frm_IR_Exportar_Full.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub BarButtonItem28_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem28.ItemClick
        A.InsertarAuditoria(Usuario_, "Click IMI ", "Formulario IMI", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.frm_IMI_Exportar_Full.MdiParent = Me
        My.Forms.frm_IMI_Exportar_Full.Show()
        My.Forms.frm_IMI_Exportar_Full.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub BarButtonItem26_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem26.ItemClick
        A.InsertarAuditoria(Usuario_, "Click IVA", "Formulario IVA", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.frm_IVA_Exportar_Full.MdiParent = Me
        My.Forms.frm_IVA_Exportar_Full.Show()
        My.Forms.frm_IVA_Exportar_Full.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub BarButtonItem25_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem25.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Listado", "Formulario Listado", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.frm_General_Exportar_Full.MdiParent = Me
        My.Forms.frm_General_Exportar_Full.Show()
        My.Forms.frm_General_Exportar_Full.WindowState = FormWindowState.Maximized
    End Sub


    Private Sub btnAuditoria_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnAuditoria.ItemClick
        My.Forms.FrmAuditoriavb.MdiParent = Me
        My.Forms.FrmAuditoriavb.Show()
        My.Forms.p_usr.WindowState = FormWindowState.Maximized
    End Sub


End Class