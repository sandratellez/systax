﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_IU_Retencion
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_IU_Retencion))
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject2 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SerializableAppearanceObject3 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.txt_concepto = New DevExpress.XtraEditors.TextEdit()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar2 = New DevExpress.XtraBars.Bar()
        Me.btn_guardar = New DevExpress.XtraBars.BarButtonItem()
        Me.btn_regresar = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.btn_anulado = New DevExpress.XtraBars.BarButtonItem()
        Me.txt_Observaciones = New DevExpress.XtraEditors.TextEdit()
        Me.chk_ir = New DevExpress.XtraEditors.CheckEdit()
        Me.ck_imi = New DevExpress.XtraEditors.CheckEdit()
        Me.chk_automatico = New DevExpress.XtraEditors.CheckEdit()
        Me.cmb_banco = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridLookUpEdit2View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txt_IMI_Serie = New DevExpress.XtraEditors.TextEdit()
        Me.txt_ck = New DevExpress.XtraEditors.TextEdit()
        Me.txt_IR_Recibo = New DevExpress.XtraEditors.TextEdit()
        Me.txt_IMI_Recibo = New DevExpress.XtraEditors.TextEdit()
        Me.txt_IR = New DevExpress.XtraEditors.TextEdit()
        Me.txt_por_ir = New DevExpress.XtraEditors.TextEdit()
        Me.txt_IMI = New DevExpress.XtraEditors.TextEdit()
        Me.dpt_fecha = New DevExpress.XtraEditors.DateEdit()
        Me.txt_por_imi = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Importe = New DevExpress.XtraEditors.TextEdit()
        Me.txt_No_Factura = New DevExpress.XtraEditors.TextEdit()
        Me.txt_telefono = New DevExpress.XtraEditors.TextEdit()
        Me.Txt_Cedula = New DevExpress.XtraEditors.TextEdit()
        Me.Txt_Ruc = New DevExpress.XtraEditors.TextEdit()
        Me.cmb_retenido = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txt_IR_serie = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem7 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem9 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem8 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem6 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.txt_concepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Observaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk_ir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ck_imi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk_automatico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_banco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit2View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IMI_Serie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_ck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IR_Recibo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IMI_Recibo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_por_ir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IMI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpt_fecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpt_fecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_por_imi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Importe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_No_Factura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_telefono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_Cedula.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_Ruc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_retenido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IR_serie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.txt_concepto)
        Me.LayoutControl1.Controls.Add(Me.txt_Observaciones)
        Me.LayoutControl1.Controls.Add(Me.chk_ir)
        Me.LayoutControl1.Controls.Add(Me.ck_imi)
        Me.LayoutControl1.Controls.Add(Me.chk_automatico)
        Me.LayoutControl1.Controls.Add(Me.cmb_banco)
        Me.LayoutControl1.Controls.Add(Me.txt_IMI_Serie)
        Me.LayoutControl1.Controls.Add(Me.txt_ck)
        Me.LayoutControl1.Controls.Add(Me.txt_IR_Recibo)
        Me.LayoutControl1.Controls.Add(Me.txt_IMI_Recibo)
        Me.LayoutControl1.Controls.Add(Me.txt_IR)
        Me.LayoutControl1.Controls.Add(Me.txt_por_ir)
        Me.LayoutControl1.Controls.Add(Me.txt_IMI)
        Me.LayoutControl1.Controls.Add(Me.dpt_fecha)
        Me.LayoutControl1.Controls.Add(Me.txt_por_imi)
        Me.LayoutControl1.Controls.Add(Me.txt_Importe)
        Me.LayoutControl1.Controls.Add(Me.txt_No_Factura)
        Me.LayoutControl1.Controls.Add(Me.txt_telefono)
        Me.LayoutControl1.Controls.Add(Me.Txt_Cedula)
        Me.LayoutControl1.Controls.Add(Me.Txt_Ruc)
        Me.LayoutControl1.Controls.Add(Me.cmb_retenido)
        Me.LayoutControl1.Controls.Add(Me.txt_IR_serie)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 40)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1134, 550)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'txt_concepto
        '
        Me.txt_concepto.EnterMoveNextControl = True
        Me.txt_concepto.Location = New System.Drawing.Point(127, 167)
        Me.txt_concepto.MenuManager = Me.BarManager1
        Me.txt_concepto.Name = "txt_concepto"
        Me.txt_concepto.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_concepto.Properties.Appearance.Options.UseFont = True
        Me.txt_concepto.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_concepto.Size = New System.Drawing.Size(691, 26)
        Me.txt_concepto.StyleController = Me.LayoutControl1
        Me.txt_concepto.TabIndex = 4
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btn_regresar, Me.btn_guardar, Me.btn_anulado})
        Me.BarManager1.MainMenu = Me.Bar2
        Me.BarManager1.MaxItemId = 3
        '
        'Bar2
        '
        Me.Bar2.BarName = "Menú principal"
        Me.Bar2.DockCol = 0
        Me.Bar2.DockRow = 0
        Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btn_guardar), New DevExpress.XtraBars.LinkPersistInfo(Me.btn_regresar)})
        Me.Bar2.OptionsBar.MultiLine = True
        Me.Bar2.OptionsBar.UseWholeRow = True
        Me.Bar2.Text = "Menú principal"
        '
        'btn_guardar
        '
        Me.btn_guardar.Caption = "BarButtonItem2"
        Me.btn_guardar.Glyph = CType(resources.GetObject("btn_guardar.Glyph"), System.Drawing.Image)
        Me.btn_guardar.Id = 1
        Me.btn_guardar.Name = "btn_guardar"
        ToolTipTitleItem1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Appearance.Options.UseImage = True
        ToolTipTitleItem1.Image = CType(resources.GetObject("ToolTipTitleItem1.Image"), System.Drawing.Image)
        ToolTipTitleItem1.Text = "Guardar"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Guardar Retención" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.btn_guardar.SuperTip = SuperToolTip1
        '
        'btn_regresar
        '
        Me.btn_regresar.Caption = "BarButtonItem1"
        Me.btn_regresar.Glyph = CType(resources.GetObject("btn_regresar.Glyph"), System.Drawing.Image)
        Me.btn_regresar.Id = 0
        Me.btn_regresar.Name = "btn_regresar"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(1134, 40)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 590)
        Me.barDockControlBottom.Size = New System.Drawing.Size(1134, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 40)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 550)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(1134, 40)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 550)
        '
        'btn_anulado
        '
        Me.btn_anulado.Caption = "BarButtonItem1"
        Me.btn_anulado.Glyph = CType(resources.GetObject("btn_anulado.Glyph"), System.Drawing.Image)
        Me.btn_anulado.Id = 2
        Me.btn_anulado.Name = "btn_anulado"
        '
        'txt_Observaciones
        '
        Me.txt_Observaciones.Location = New System.Drawing.Point(127, 347)
        Me.txt_Observaciones.MenuManager = Me.BarManager1
        Me.txt_Observaciones.Name = "txt_Observaciones"
        Me.txt_Observaciones.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Observaciones.Properties.Appearance.Options.UseFont = True
        Me.txt_Observaciones.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Observaciones.Size = New System.Drawing.Size(691, 26)
        Me.txt_Observaciones.StyleController = Me.LayoutControl1
        Me.txt_Observaciones.TabIndex = 14
        '
        'chk_ir
        '
        Me.chk_ir.AutoSizeInLayoutControl = True
        Me.chk_ir.EnterMoveNextControl = True
        Me.chk_ir.Location = New System.Drawing.Point(349, 257)
        Me.chk_ir.MenuManager = Me.BarManager1
        Me.chk_ir.Name = "chk_ir"
        Me.chk_ir.Properties.Caption = ""
        Me.chk_ir.Size = New System.Drawing.Size(19, 19)
        Me.chk_ir.StyleController = Me.LayoutControl1
        Me.chk_ir.TabIndex = 16
        '
        'ck_imi
        '
        Me.ck_imi.AutoSizeInLayoutControl = True
        Me.ck_imi.EnterMoveNextControl = True
        Me.ck_imi.Location = New System.Drawing.Point(347, 227)
        Me.ck_imi.MenuManager = Me.BarManager1
        Me.ck_imi.Name = "ck_imi"
        Me.ck_imi.Properties.Caption = ""
        Me.ck_imi.Size = New System.Drawing.Size(19, 19)
        Me.ck_imi.StyleController = Me.LayoutControl1
        Me.ck_imi.TabIndex = 9
        Me.ck_imi.Visible = False
        '
        'chk_automatico
        '
        Me.chk_automatico.EnterMoveNextControl = True
        Me.chk_automatico.Location = New System.Drawing.Point(348, 197)
        Me.chk_automatico.Name = "chk_automatico"
        Me.chk_automatico.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chk_automatico.Properties.Appearance.Options.UseFont = True
        Me.chk_automatico.Properties.Caption = ""
        Me.chk_automatico.Size = New System.Drawing.Size(326, 19)
        Me.chk_automatico.StyleController = Me.LayoutControl1
        Me.chk_automatico.TabIndex = 6
        '
        'cmb_banco
        '
        Me.cmb_banco.EnterMoveNextControl = True
        Me.cmb_banco.Location = New System.Drawing.Point(127, 317)
        Me.cmb_banco.Name = "cmb_banco"
        Me.cmb_banco.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_banco.Properties.Appearance.Options.UseFont = True
        Me.cmb_banco.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_banco.Properties.NullText = ""
        Me.cmb_banco.Properties.PopupFormMinSize = New System.Drawing.Size(470, 0)
        Me.cmb_banco.Properties.PopupFormSize = New System.Drawing.Size(470, 0)
        Me.cmb_banco.Properties.View = Me.GridLookUpEdit2View
        Me.cmb_banco.Size = New System.Drawing.Size(217, 26)
        Me.cmb_banco.StyleController = Me.LayoutControl1
        Me.cmb_banco.TabIndex = 13
        '
        'GridLookUpEdit2View
        '
        Me.GridLookUpEdit2View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn6, Me.GridColumn7, Me.GridColumn8})
        Me.GridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit2View.Name = "GridLookUpEdit2View"
        Me.GridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit2View.OptionsView.ShowGroupPanel = False
        Me.GridLookUpEdit2View.RowHeight = 25
        '
        'GridColumn6
        '
        Me.GridColumn6.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn6.AppearanceCell.Options.UseFont = True
        Me.GridColumn6.Caption = "GridColumn6"
        Me.GridColumn6.FieldName = "BancoID"
        Me.GridColumn6.Name = "GridColumn6"
        '
        'GridColumn7
        '
        Me.GridColumn7.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn7.AppearanceCell.Options.UseFont = True
        Me.GridColumn7.Caption = "Entidad Bancaria"
        Me.GridColumn7.FieldName = "Banco"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 0
        '
        'GridColumn8
        '
        Me.GridColumn8.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn8.AppearanceCell.Options.UseFont = True
        Me.GridColumn8.Caption = "Siglas"
        Me.GridColumn8.FieldName = "Siglas"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 1
        '
        'txt_IMI_Serie
        '
        Me.txt_IMI_Serie.EditValue = "NE"
        Me.txt_IMI_Serie.Enabled = False
        Me.txt_IMI_Serie.EnterMoveNextControl = True
        Me.txt_IMI_Serie.Location = New System.Drawing.Point(492, 227)
        Me.txt_IMI_Serie.Name = "txt_IMI_Serie"
        Me.txt_IMI_Serie.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IMI_Serie.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.txt_IMI_Serie.Properties.Appearance.Options.UseFont = True
        Me.txt_IMI_Serie.Properties.Appearance.Options.UseForeColor = True
        Me.txt_IMI_Serie.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.txt_IMI_Serie.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IMI_Serie.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Red
        Me.txt_IMI_Serie.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_IMI_Serie.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_IMI_Serie.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_IMI_Serie.Size = New System.Drawing.Size(72, 26)
        Me.txt_IMI_Serie.StyleController = Me.LayoutControl1
        Me.txt_IMI_Serie.TabIndex = 11
        '
        'txt_ck
        '
        Me.txt_ck.EditValue = ""
        Me.txt_ck.EnterMoveNextControl = True
        Me.txt_ck.Location = New System.Drawing.Point(127, 287)
        Me.txt_ck.Name = "txt_ck"
        Me.txt_ck.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ck.Properties.Appearance.Options.UseFont = True
        Me.txt_ck.Properties.MaxLength = 10
        Me.txt_ck.Size = New System.Drawing.Size(217, 26)
        Me.txt_ck.StyleController = Me.LayoutControl1
        Me.txt_ck.TabIndex = 12
        '
        'txt_IR_Recibo
        '
        Me.txt_IR_Recibo.EditValue = ""
        Me.txt_IR_Recibo.Enabled = False
        Me.txt_IR_Recibo.EnterMoveNextControl = True
        Me.txt_IR_Recibo.Location = New System.Drawing.Point(372, 257)
        Me.txt_IR_Recibo.Name = "txt_IR_Recibo"
        Me.txt_IR_Recibo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IR_Recibo.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.txt_IR_Recibo.Properties.Appearance.Options.UseFont = True
        Me.txt_IR_Recibo.Properties.Appearance.Options.UseForeColor = True
        Me.txt_IR_Recibo.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_IR_Recibo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_IR_Recibo.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.txt_IR_Recibo.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IR_Recibo.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Red
        Me.txt_IR_Recibo.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_IR_Recibo.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_IR_Recibo.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_IR_Recibo.Size = New System.Drawing.Size(118, 26)
        Me.txt_IR_Recibo.StyleController = Me.LayoutControl1
        Me.txt_IR_Recibo.TabIndex = 17
        '
        'txt_IMI_Recibo
        '
        Me.txt_IMI_Recibo.EditValue = ""
        Me.txt_IMI_Recibo.EnterMoveNextControl = True
        Me.txt_IMI_Recibo.Location = New System.Drawing.Point(370, 227)
        Me.txt_IMI_Recibo.Name = "txt_IMI_Recibo"
        Me.txt_IMI_Recibo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IMI_Recibo.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.txt_IMI_Recibo.Properties.Appearance.Options.UseFont = True
        Me.txt_IMI_Recibo.Properties.Appearance.Options.UseForeColor = True
        Me.txt_IMI_Recibo.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_IMI_Recibo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_IMI_Recibo.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.txt_IMI_Recibo.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IMI_Recibo.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Red
        Me.txt_IMI_Recibo.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_IMI_Recibo.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_IMI_Recibo.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_IMI_Recibo.Size = New System.Drawing.Size(118, 26)
        Me.txt_IMI_Recibo.StyleController = Me.LayoutControl1
        Me.txt_IMI_Recibo.TabIndex = 10
        '
        'txt_IR
        '
        Me.txt_IR.EditValue = "0.00"
        Me.txt_IR.EnterMoveNextControl = True
        Me.txt_IR.Location = New System.Drawing.Point(186, 257)
        Me.txt_IR.Name = "txt_IR"
        Me.txt_IR.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IR.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.txt_IR.Properties.Appearance.Options.UseFont = True
        Me.txt_IR.Properties.Appearance.Options.UseForeColor = True
        Me.txt_IR.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_IR.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_IR.Properties.Mask.EditMask = "n2"
        Me.txt_IR.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_IR.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_IR.Size = New System.Drawing.Size(159, 26)
        Me.txt_IR.StyleController = Me.LayoutControl1
        Me.txt_IR.TabIndex = 15
        '
        'txt_por_ir
        '
        Me.txt_por_ir.EditValue = "2.00"
        Me.txt_por_ir.EnterMoveNextControl = True
        Me.txt_por_ir.Location = New System.Drawing.Point(127, 257)
        Me.txt_por_ir.Name = "txt_por_ir"
        Me.txt_por_ir.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_por_ir.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.txt_por_ir.Properties.Appearance.Options.UseFont = True
        Me.txt_por_ir.Properties.Appearance.Options.UseForeColor = True
        Me.txt_por_ir.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.txt_por_ir.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_por_ir.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.txt_por_ir.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_por_ir.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_por_ir.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_por_ir.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_por_ir.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_por_ir.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_por_ir.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_por_ir.Properties.Mask.EditMask = "n2"
        Me.txt_por_ir.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_por_ir.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_por_ir.Properties.ReadOnly = True
        Me.txt_por_ir.Size = New System.Drawing.Size(55, 26)
        Me.txt_por_ir.StyleController = Me.LayoutControl1
        Me.txt_por_ir.TabIndex = 14
        '
        'txt_IMI
        '
        Me.txt_IMI.EditValue = "0.00"
        Me.txt_IMI.EnterMoveNextControl = True
        Me.txt_IMI.Location = New System.Drawing.Point(185, 227)
        Me.txt_IMI.Name = "txt_IMI"
        Me.txt_IMI.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IMI.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.txt_IMI.Properties.Appearance.Options.UseFont = True
        Me.txt_IMI.Properties.Appearance.Options.UseForeColor = True
        Me.txt_IMI.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_IMI.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_IMI.Properties.Mask.EditMask = "n2"
        Me.txt_IMI.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_IMI.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_IMI.Size = New System.Drawing.Size(158, 26)
        Me.txt_IMI.StyleController = Me.LayoutControl1
        Me.txt_IMI.TabIndex = 8
        '
        'dpt_fecha
        '
        Me.dpt_fecha.EditValue = Nothing
        Me.dpt_fecha.EnterMoveNextControl = True
        Me.dpt_fecha.Location = New System.Drawing.Point(127, 107)
        Me.dpt_fecha.Name = "dpt_fecha"
        Me.dpt_fecha.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dpt_fecha.Properties.Appearance.Options.UseFont = True
        Me.dpt_fecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpt_fecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpt_fecha.Size = New System.Drawing.Size(217, 26)
        Me.dpt_fecha.StyleController = Me.LayoutControl1
        Me.dpt_fecha.TabIndex = 2
        '
        'txt_por_imi
        '
        Me.txt_por_imi.EditValue = "1.00"
        Me.txt_por_imi.EnterMoveNextControl = True
        Me.txt_por_imi.Location = New System.Drawing.Point(127, 227)
        Me.txt_por_imi.Name = "txt_por_imi"
        Me.txt_por_imi.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_por_imi.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.txt_por_imi.Properties.Appearance.Options.UseFont = True
        Me.txt_por_imi.Properties.Appearance.Options.UseForeColor = True
        Me.txt_por_imi.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.txt_por_imi.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_por_imi.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.txt_por_imi.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_por_imi.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_por_imi.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_por_imi.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_por_imi.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_por_imi.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_por_imi.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_por_imi.Properties.Mask.EditMask = "n2"
        Me.txt_por_imi.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_por_imi.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_por_imi.Properties.ReadOnly = True
        Me.txt_por_imi.Size = New System.Drawing.Size(54, 26)
        Me.txt_por_imi.StyleController = Me.LayoutControl1
        Me.txt_por_imi.TabIndex = 7
        '
        'txt_Importe
        '
        Me.txt_Importe.EditValue = "0.00"
        Me.txt_Importe.EnterMoveNextControl = True
        Me.txt_Importe.Location = New System.Drawing.Point(127, 197)
        Me.txt_Importe.Name = "txt_Importe"
        Me.txt_Importe.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Importe.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.txt_Importe.Properties.Appearance.Options.UseFont = True
        Me.txt_Importe.Properties.Appearance.Options.UseForeColor = True
        Me.txt_Importe.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Importe.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Importe.Properties.Mask.EditMask = "n2"
        Me.txt_Importe.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_Importe.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_Importe.Size = New System.Drawing.Size(217, 26)
        Me.txt_Importe.StyleController = Me.LayoutControl1
        Me.txt_Importe.TabIndex = 5
        '
        'txt_No_Factura
        '
        Me.txt_No_Factura.EditValue = ""
        Me.txt_No_Factura.EnterMoveNextControl = True
        Me.txt_No_Factura.Location = New System.Drawing.Point(127, 137)
        Me.txt_No_Factura.Name = "txt_No_Factura"
        Me.txt_No_Factura.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_No_Factura.Properties.Appearance.Options.UseFont = True
        Me.txt_No_Factura.Properties.MaxLength = 50
        Me.txt_No_Factura.Size = New System.Drawing.Size(437, 26)
        Me.txt_No_Factura.StyleController = Me.LayoutControl1
        Me.txt_No_Factura.TabIndex = 3
        '
        'txt_telefono
        '
        Me.txt_telefono.Enabled = False
        Me.txt_telefono.EnterMoveNextControl = True
        Me.txt_telefono.Location = New System.Drawing.Point(792, 53)
        Me.txt_telefono.Name = "txt_telefono"
        Me.txt_telefono.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_telefono.Properties.Appearance.Options.UseFont = True
        Me.txt_telefono.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.txt_telefono.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.txt_telefono.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_telefono.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_telefono.Size = New System.Drawing.Size(337, 26)
        Me.txt_telefono.StyleController = Me.LayoutControl1
        Me.txt_telefono.TabIndex = 3
        '
        'Txt_Cedula
        '
        Me.Txt_Cedula.EditValue = ""
        Me.Txt_Cedula.Enabled = False
        Me.Txt_Cedula.EnterMoveNextControl = True
        Me.Txt_Cedula.Location = New System.Drawing.Point(458, 53)
        Me.Txt_Cedula.Name = "Txt_Cedula"
        Me.Txt_Cedula.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Cedula.Properties.Appearance.Options.UseFont = True
        Me.Txt_Cedula.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.Txt_Cedula.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.Txt_Cedula.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.Txt_Cedula.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.Txt_Cedula.Size = New System.Drawing.Size(208, 26)
        Me.Txt_Cedula.StyleController = Me.LayoutControl1
        Me.Txt_Cedula.TabIndex = 2
        '
        'Txt_Ruc
        '
        Me.Txt_Ruc.EditValue = ""
        Me.Txt_Ruc.Enabled = False
        Me.Txt_Ruc.EnterMoveNextControl = True
        Me.Txt_Ruc.Location = New System.Drawing.Point(127, 53)
        Me.Txt_Ruc.Name = "Txt_Ruc"
        Me.Txt_Ruc.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Ruc.Properties.Appearance.Options.UseFont = True
        Me.Txt_Ruc.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.Txt_Ruc.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.Txt_Ruc.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.Txt_Ruc.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.Txt_Ruc.Size = New System.Drawing.Size(205, 26)
        Me.Txt_Ruc.StyleController = Me.LayoutControl1
        Me.Txt_Ruc.TabIndex = 1
        '
        'cmb_retenido
        '
        Me.cmb_retenido.EditValue = ""
        Me.cmb_retenido.EnterMoveNextControl = True
        Me.cmb_retenido.Location = New System.Drawing.Point(127, 23)
        Me.cmb_retenido.Name = "cmb_retenido"
        Me.cmb_retenido.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_retenido.Properties.Appearance.Options.UseFont = True
        Me.cmb_retenido.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("cmb_retenido.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "", Nothing, Nothing, True), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("cmb_retenido.Properties.Buttons1"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject2, "", Nothing, Nothing, True), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("cmb_retenido.Properties.Buttons2"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject3, "", Nothing, Nothing, True)})
        Me.cmb_retenido.Properties.NullText = ""
        Me.cmb_retenido.Properties.PopupFormMinSize = New System.Drawing.Size(1200, 0)
        Me.cmb_retenido.Properties.PopupFormSize = New System.Drawing.Size(1200, 0)
        Me.cmb_retenido.Properties.View = Me.GridLookUpEdit1View
        Me.cmb_retenido.Size = New System.Drawing.Size(1002, 26)
        Me.cmb_retenido.StyleController = Me.LayoutControl1
        Me.cmb_retenido.TabIndex = 0
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5})
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn1.AppearanceCell.Options.UseFont = True
        Me.GridColumn1.Caption = "Retenido"
        Me.GridColumn1.FieldName = "Proveedor"
        Me.GridColumn1.Image = CType(resources.GetObject("GridColumn1.Image"), System.Drawing.Image)
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 200
        '
        'GridColumn2
        '
        Me.GridColumn2.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn2.AppearanceCell.Options.UseFont = True
        Me.GridColumn2.Caption = "RUC"
        Me.GridColumn2.FieldName = "RUC"
        Me.GridColumn2.Image = CType(resources.GetObject("GridColumn2.Image"), System.Drawing.Image)
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 45
        '
        'GridColumn3
        '
        Me.GridColumn3.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn3.AppearanceCell.Options.UseFont = True
        Me.GridColumn3.Caption = "Cedula"
        Me.GridColumn3.FieldName = "Cedula"
        Me.GridColumn3.Image = CType(resources.GetObject("GridColumn3.Image"), System.Drawing.Image)
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 45
        '
        'GridColumn4
        '
        Me.GridColumn4.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn4.AppearanceCell.Options.UseFont = True
        Me.GridColumn4.Caption = "Telefono"
        Me.GridColumn4.FieldName = "Telefono"
        Me.GridColumn4.Image = CType(resources.GetObject("GridColumn4.Image"), System.Drawing.Image)
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 3
        Me.GridColumn4.Width = 45
        '
        'GridColumn5
        '
        Me.GridColumn5.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn5.AppearanceCell.Options.UseFont = True
        Me.GridColumn5.Caption = " "
        Me.GridColumn5.FieldName = "ProveedorID"
        Me.GridColumn5.Image = CType(resources.GetObject("GridColumn5.Image"), System.Drawing.Image)
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 4
        Me.GridColumn5.Width = 49
        '
        'txt_IR_serie
        '
        Me.txt_IR_serie.Enabled = False
        Me.txt_IR_serie.EnterMoveNextControl = True
        Me.txt_IR_serie.Location = New System.Drawing.Point(494, 257)
        Me.txt_IR_serie.Name = "txt_IR_serie"
        Me.txt_IR_serie.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IR_serie.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.txt_IR_serie.Properties.Appearance.Options.UseFont = True
        Me.txt_IR_serie.Properties.Appearance.Options.UseForeColor = True
        Me.txt_IR_serie.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.txt_IR_serie.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IR_serie.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Red
        Me.txt_IR_serie.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_IR_serie.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_IR_serie.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_IR_serie.Size = New System.Drawing.Size(72, 26)
        Me.txt_IR_serie.StyleController = Me.LayoutControl1
        Me.txt_IR_serie.TabIndex = 18
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup2, Me.LayoutControlGroup3})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1134, 550)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem3})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(1134, 84)
        Me.LayoutControlGroup2.Text = " Datos Generales del Retenido"
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem1.BestFitWeight = 50
        Me.LayoutControlItem1.Control = Me.cmb_retenido
        Me.LayoutControlItem1.Image = CType(resources.GetObject("LayoutControlItem1.Image"), System.Drawing.Image)
        Me.LayoutControlItem1.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(1128, 30)
        Me.LayoutControlItem1.Text = "Retenido"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(119, 18)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem4.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem4.BestFitWeight = 50
        Me.LayoutControlItem4.Control = Me.Txt_Cedula
        Me.LayoutControlItem4.Image = CType(resources.GetObject("LayoutControlItem4.Image"), System.Drawing.Image)
        Me.LayoutControlItem4.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem4.Location = New System.Drawing.Point(331, 30)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(334, 30)
        Me.LayoutControlItem4.Text = "Cedula"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(119, 18)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem5.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem5.BestFitWeight = 50
        Me.LayoutControlItem5.Control = Me.txt_telefono
        Me.LayoutControlItem5.Image = CType(resources.GetObject("LayoutControlItem5.Image"), System.Drawing.Image)
        Me.LayoutControlItem5.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem5.Location = New System.Drawing.Point(665, 30)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(463, 30)
        Me.LayoutControlItem5.Text = "Telefono"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(119, 18)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem3.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem3.BestFitWeight = 50
        Me.LayoutControlItem3.Control = Me.Txt_Ruc
        Me.LayoutControlItem3.Image = CType(resources.GetObject("LayoutControlItem3.Image"), System.Drawing.Image)
        Me.LayoutControlItem3.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 30)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(331, 30)
        Me.LayoutControlItem3.Text = "RUC"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(119, 18)
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem9, Me.LayoutControlItem7, Me.LayoutControlItem6, Me.LayoutControlItem10, Me.EmptySpaceItem1, Me.EmptySpaceItem3, Me.EmptySpaceItem4, Me.LayoutControlItem11, Me.LayoutControlItem14, Me.LayoutControlItem16, Me.LayoutControlItem17, Me.LayoutControlItem19, Me.EmptySpaceItem5, Me.LayoutControlItem2, Me.EmptySpaceItem2, Me.EmptySpaceItem7, Me.EmptySpaceItem9, Me.LayoutControlItem18, Me.LayoutControlItem22, Me.LayoutControlItem20, Me.EmptySpaceItem8, Me.LayoutControlItem13, Me.LayoutControlItem12, Me.LayoutControlItem21, Me.LayoutControlItem15, Me.EmptySpaceItem6, Me.LayoutControlItem8})
        Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 84)
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(1134, 466)
        Me.LayoutControlGroup3.Text = "Datos de Retención"
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem9.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem9.Control = Me.txt_por_imi
        Me.LayoutControlItem9.Image = CType(resources.GetObject("LayoutControlItem9.Image"), System.Drawing.Image)
        Me.LayoutControlItem9.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem9.MaxSize = New System.Drawing.Size(180, 30)
        Me.LayoutControlItem9.MinSize = New System.Drawing.Size(180, 30)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(180, 30)
        Me.LayoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem9.Text = "Retención IMI"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(119, 19)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem7.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem7.Control = Me.txt_Importe
        Me.LayoutControlItem7.Image = CType(resources.GetObject("LayoutControlItem7.Image"), System.Drawing.Image)
        Me.LayoutControlItem7.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 90)
        Me.LayoutControlItem7.MaxSize = New System.Drawing.Size(343, 30)
        Me.LayoutControlItem7.MinSize = New System.Drawing.Size(343, 30)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(343, 30)
        Me.LayoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem7.Text = "Sub Total"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(119, 19)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem6.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem6.Control = Me.txt_No_Factura
        Me.LayoutControlItem6.Image = CType(resources.GetObject("LayoutControlItem6.Image"), System.Drawing.Image)
        Me.LayoutControlItem6.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 30)
        Me.LayoutControlItem6.MaxSize = New System.Drawing.Size(563, 30)
        Me.LayoutControlItem6.MinSize = New System.Drawing.Size(563, 30)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(563, 30)
        Me.LayoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem6.Text = "Factura No"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(119, 19)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem10.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem10.Control = Me.dpt_fecha
        Me.LayoutControlItem10.Image = CType(resources.GetObject("LayoutControlItem10.Image"), System.Drawing.Image)
        Me.LayoutControlItem10.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem10.MaxSize = New System.Drawing.Size(343, 30)
        Me.LayoutControlItem10.MinSize = New System.Drawing.Size(343, 30)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(343, 30)
        Me.LayoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem10.Text = "Fecha"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(119, 19)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(343, 0)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(785, 30)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = False
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(563, 30)
        Me.EmptySpaceItem3.MaxSize = New System.Drawing.Size(561, 30)
        Me.EmptySpaceItem3.MinSize = New System.Drawing.Size(561, 30)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(565, 30)
        Me.EmptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = False
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(673, 90)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(455, 30)
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem11.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem11.Control = Me.txt_IMI
        Me.LayoutControlItem11.Location = New System.Drawing.Point(180, 120)
        Me.LayoutControlItem11.MaxSize = New System.Drawing.Size(162, 30)
        Me.LayoutControlItem11.MinSize = New System.Drawing.Size(162, 30)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(162, 30)
        Me.LayoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem14.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem14.Control = Me.txt_IMI_Recibo
        Me.LayoutControlItem14.Location = New System.Drawing.Point(365, 120)
        Me.LayoutControlItem14.MaxSize = New System.Drawing.Size(122, 30)
        Me.LayoutControlItem14.MinSize = New System.Drawing.Size(122, 30)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(122, 30)
        Me.LayoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem14.Text = "No"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem14.TextVisible = False
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem16.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem16.Control = Me.txt_ck
        Me.LayoutControlItem16.Image = CType(resources.GetObject("LayoutControlItem16.Image"), System.Drawing.Image)
        Me.LayoutControlItem16.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 180)
        Me.LayoutControlItem16.MaxSize = New System.Drawing.Size(343, 30)
        Me.LayoutControlItem16.MinSize = New System.Drawing.Size(343, 30)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(343, 30)
        Me.LayoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem16.Text = "Cheques"
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(119, 19)
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.txt_IMI_Serie
        Me.LayoutControlItem17.Location = New System.Drawing.Point(487, 120)
        Me.LayoutControlItem17.MaxSize = New System.Drawing.Size(76, 30)
        Me.LayoutControlItem17.MinSize = New System.Drawing.Size(76, 30)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(76, 30)
        Me.LayoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem17.TextVisible = False
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.chk_automatico
        Me.LayoutControlItem19.Location = New System.Drawing.Point(343, 90)
        Me.LayoutControlItem19.MinSize = New System.Drawing.Size(79, 23)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(330, 30)
        Me.LayoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem19.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem19.TextVisible = False
        Me.LayoutControlItem19.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'EmptySpaceItem5
        '
        Me.EmptySpaceItem5.AllowHotTrack = False
        Me.EmptySpaceItem5.Location = New System.Drawing.Point(817, 60)
        Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Size = New System.Drawing.Size(311, 30)
        Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.cmb_banco
        Me.LayoutControlItem2.Image = CType(resources.GetObject("LayoutControlItem2.Image"), System.Drawing.Image)
        Me.LayoutControlItem2.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 210)
        Me.LayoutControlItem2.MaxSize = New System.Drawing.Size(343, 30)
        Me.LayoutControlItem2.MinSize = New System.Drawing.Size(343, 30)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(343, 30)
        Me.LayoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem2.Text = "Banco"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(119, 19)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(343, 180)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(785, 30)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem7
        '
        Me.EmptySpaceItem7.AllowHotTrack = False
        Me.EmptySpaceItem7.Location = New System.Drawing.Point(563, 120)
        Me.EmptySpaceItem7.Name = "EmptySpaceItem7"
        Me.EmptySpaceItem7.Size = New System.Drawing.Size(565, 30)
        Me.EmptySpaceItem7.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem9
        '
        Me.EmptySpaceItem9.AllowHotTrack = False
        Me.EmptySpaceItem9.Location = New System.Drawing.Point(343, 210)
        Me.EmptySpaceItem9.Name = "EmptySpaceItem9"
        Me.EmptySpaceItem9.Size = New System.Drawing.Size(785, 30)
        Me.EmptySpaceItem9.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.ck_imi
        Me.LayoutControlItem18.Location = New System.Drawing.Point(342, 120)
        Me.LayoutControlItem18.MaxSize = New System.Drawing.Size(23, 30)
        Me.LayoutControlItem18.MinSize = New System.Drawing.Size(23, 30)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(23, 30)
        Me.LayoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem18.TextVisible = False
        Me.LayoutControlItem18.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem22.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem22.Control = Me.txt_Observaciones
        Me.LayoutControlItem22.Location = New System.Drawing.Point(0, 240)
        Me.LayoutControlItem22.MaxSize = New System.Drawing.Size(817, 30)
        Me.LayoutControlItem22.MinSize = New System.Drawing.Size(817, 30)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(817, 202)
        Me.LayoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem22.Text = "Observaciones"
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(119, 19)
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem20.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem20.Control = Me.txt_concepto
        Me.LayoutControlItem20.Image = CType(resources.GetObject("LayoutControlItem20.Image"), System.Drawing.Image)
        Me.LayoutControlItem20.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 60)
        Me.LayoutControlItem20.MaxSize = New System.Drawing.Size(817, 30)
        Me.LayoutControlItem20.MinSize = New System.Drawing.Size(817, 30)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(817, 30)
        Me.LayoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem20.Text = "Conceptos"
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(119, 19)
        '
        'EmptySpaceItem8
        '
        Me.EmptySpaceItem8.AllowHotTrack = False
        Me.EmptySpaceItem8.Location = New System.Drawing.Point(817, 240)
        Me.EmptySpaceItem8.Name = "EmptySpaceItem8"
        Me.EmptySpaceItem8.Size = New System.Drawing.Size(311, 202)
        Me.EmptySpaceItem8.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem13.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem13.Control = Me.txt_IR
        Me.LayoutControlItem13.Location = New System.Drawing.Point(181, 150)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(163, 30)
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem13.TextVisible = False
        Me.LayoutControlItem13.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem12.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem12.Control = Me.txt_por_ir
        Me.LayoutControlItem12.Image = CType(resources.GetObject("LayoutControlItem12.Image"), System.Drawing.Image)
        Me.LayoutControlItem12.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 150)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(181, 30)
        Me.LayoutControlItem12.Text = "Retención IR"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(119, 19)
        Me.LayoutControlItem12.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.chk_ir
        Me.LayoutControlItem21.Location = New System.Drawing.Point(344, 150)
        Me.LayoutControlItem21.MinSize = New System.Drawing.Size(23, 23)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(23, 30)
        Me.LayoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem21.TextVisible = False
        Me.LayoutControlItem21.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem15.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem15.Control = Me.txt_IR_Recibo
        Me.LayoutControlItem15.Location = New System.Drawing.Point(367, 150)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(122, 30)
        Me.LayoutControlItem15.Text = "No"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem15.TextVisible = False
        Me.LayoutControlItem15.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'EmptySpaceItem6
        '
        Me.EmptySpaceItem6.AllowHotTrack = False
        Me.EmptySpaceItem6.Location = New System.Drawing.Point(565, 150)
        Me.EmptySpaceItem6.Name = "EmptySpaceItem6"
        Me.EmptySpaceItem6.Size = New System.Drawing.Size(563, 30)
        Me.EmptySpaceItem6.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.txt_IR_serie
        Me.LayoutControlItem8.CustomizationFormText = "LayoutControlItem17"
        Me.LayoutControlItem8.Location = New System.Drawing.Point(489, 150)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(76, 30)
        Me.LayoutControlItem8.Text = "LayoutControlItem17"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem8.TextVisible = False
        Me.LayoutControlItem8.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'Frm_IU_Retencion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1134, 590)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "Frm_IU_Retencion"
        Me.Text = "Retenciones IMI"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.txt_concepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Observaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk_ir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ck_imi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk_automatico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_banco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit2View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IMI_Serie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_ck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IR_Recibo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IMI_Recibo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_por_ir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IMI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpt_fecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpt_fecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_por_imi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Importe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_No_Factura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_telefono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_Cedula.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_Ruc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_retenido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IR_serie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents cmb_retenido As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_telefono As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txt_Cedula As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txt_Ruc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_IR_Recibo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_IMI_Recibo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_IR As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_por_ir As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_IMI As DevExpress.XtraEditors.TextEdit
    Friend WithEvents dpt_fecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txt_por_imi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Importe As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_No_Factura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_ck As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_IMI_Serie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents cmb_banco As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridLookUpEdit2View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chk_automatico As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem6 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem7 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem9 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents txt_IR_serie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
    Friend WithEvents btn_guardar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btn_regresar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents chk_ir As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ck_imi As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_Observaciones As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_concepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem8 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents btn_anulado As DevExpress.XtraBars.BarButtonItem
End Class
