﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class Origen
    Public Shared Function GetlistCombox(ByRef Combox As DevExpress.XtraEditors.GridLookUpEdit)


        Combox.Properties.DataSource = Rutinas_SGC.obtener_datos("GetlistOrigenCombox")
        Combox.Properties.ValueMember = "OrigenID"
        Combox.Properties.DisplayMember = "Origen"
        Combox.Properties.PopulateViewColumns()

        Combox.Properties.View.Columns(0).Visible = False
        Combox.Properties.View.Columns(1).Caption = "Origen"

        Combox.Properties.View.BestFitColumns()

        Combox.Properties.PopupFormMinSize = New Point(Combox.Size.Width + 50, 0)
        Combox.Properties.ShowFooter = True
        Combox.Text = String.Empty


    End Function


End Class
