<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acceso
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acceso))
        Me.btnllaves = New System.Windows.Forms.Button()
        Me.btnsalir = New System.Windows.Forms.Button()
        Me.rotulos = New System.Windows.Forms.ToolTip(Me.components)
        Me.lbl_empresa = New System.Windows.Forms.Label()
        Me.lbl_lema = New System.Windows.Forms.Label()
        Me.lbl_ruc = New System.Windows.Forms.Label()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.LayoutView1 = New DevExpress.XtraGrid.Views.Layout.LayoutView()
        Me.BandedGridColumn5 = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.RepositoryItemPictureEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit()
        Me.Item1 = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.BandedGridColumn6 = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.Item15 = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.BandedGridColumn7 = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.Item3 = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.BandedGridColumn8 = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.Item4 = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.LayoutViewColumn1 = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.Item16 = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.LayoutViewColumn2 = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.Item9 = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.LayoutViewColumn3 = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.Item14 = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.LayoutViewCard1 = New DevExpress.XtraGrid.Views.Layout.LayoutViewCard()
        Me.Item2 = New DevExpress.XtraLayout.SimpleSeparator()
        Me.Group1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.Group4 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.CardView1 = New DevExpress.XtraGrid.Views.Card.CardView()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.AdvBandedGridView1 = New DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView()
        Me.GridBand1 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.BandedGridColumn1 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.BandedGridColumn2 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.BandedGridColumn3 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.BandedGridColumn4 = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.pnc_inicio = New System.Windows.Forms.Panel()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.lbl_welcome = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txtusuario = New DevExpress.XtraEditors.TextEdit()
        Me.txtclave = New DevExpress.XtraEditors.TextEdit()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btn_configurar = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemPictureEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Item1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Item15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Item3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Item4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Item16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Item9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Item14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutViewCard1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Item2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Group1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Group4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CardView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.pnc_inicio.SuspendLayout()
        CType(Me.txtusuario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtclave.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnllaves
        '
        Me.btnllaves.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnllaves.BackColor = System.Drawing.Color.Transparent
        Me.btnllaves.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnllaves.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnllaves.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnllaves.FlatAppearance.BorderSize = 0
        Me.btnllaves.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btnllaves.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnllaves.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnllaves.Image = CType(resources.GetObject("btnllaves.Image"), System.Drawing.Image)
        Me.btnllaves.Location = New System.Drawing.Point(444, 5)
        Me.btnllaves.Name = "btnllaves"
        Me.btnllaves.Size = New System.Drawing.Size(77, 67)
        Me.btnllaves.TabIndex = 11
        Me.btnllaves.UseVisualStyleBackColor = True
        '
        'btnsalir
        '
        Me.btnsalir.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnsalir.BackColor = System.Drawing.Color.Transparent
        Me.btnsalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnsalir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnsalir.FlatAppearance.BorderSize = 0
        Me.btnsalir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btnsalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnsalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnsalir.Image = CType(resources.GetObject("btnsalir.Image"), System.Drawing.Image)
        Me.btnsalir.Location = New System.Drawing.Point(545, 4)
        Me.btnsalir.Name = "btnsalir"
        Me.btnsalir.Size = New System.Drawing.Size(77, 67)
        Me.btnsalir.TabIndex = 12
        Me.btnsalir.UseVisualStyleBackColor = True
        '
        'lbl_empresa
        '
        Me.lbl_empresa.BackColor = System.Drawing.Color.Transparent
        Me.lbl_empresa.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_empresa.ForeColor = System.Drawing.Color.White
        Me.lbl_empresa.Location = New System.Drawing.Point(4, 6)
        Me.lbl_empresa.Name = "lbl_empresa"
        Me.lbl_empresa.Size = New System.Drawing.Size(718, 16)
        Me.lbl_empresa.TabIndex = 14
        Me.lbl_empresa.Text = " "
        Me.lbl_empresa.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl_lema
        '
        Me.lbl_lema.BackColor = System.Drawing.Color.Transparent
        Me.lbl_lema.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_lema.ForeColor = System.Drawing.Color.White
        Me.lbl_lema.Location = New System.Drawing.Point(4, 29)
        Me.lbl_lema.Name = "lbl_lema"
        Me.lbl_lema.Size = New System.Drawing.Size(718, 15)
        Me.lbl_lema.TabIndex = 15
        Me.lbl_lema.Text = " "
        Me.lbl_lema.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl_ruc
        '
        Me.lbl_ruc.BackColor = System.Drawing.Color.Transparent
        Me.lbl_ruc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ruc.ForeColor = System.Drawing.Color.White
        Me.lbl_ruc.Location = New System.Drawing.Point(4, 51)
        Me.lbl_ruc.Name = "lbl_ruc"
        Me.lbl_ruc.Size = New System.Drawing.Size(718, 15)
        Me.lbl_ruc.TabIndex = 18
        Me.lbl_ruc.Text = " "
        Me.lbl_ruc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GridControl2
        '
        Me.GridControl2.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.GridControl2.AllowRestoreSelectionAndFocusedRow = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.GridControl2.Location = New System.Drawing.Point(980, 489)
        Me.GridControl2.LookAndFeel.SkinName = "Black"
        Me.GridControl2.LookAndFeel.UseDefaultLookAndFeel = False
        Me.GridControl2.MainView = Me.LayoutView1
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemPictureEdit1, Me.RepositoryItemTextEdit1})
        Me.GridControl2.Size = New System.Drawing.Size(196, 83)
        Me.GridControl2.TabIndex = 21
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.LayoutView1, Me.GridView1, Me.CardView1, Me.AdvBandedGridView1})
        Me.GridControl2.Visible = False
        '
        'LayoutView1
        '
        Me.LayoutView1.Appearance.Card.Options.UseImage = True
        Me.LayoutView1.Appearance.FieldValue.BackColor = System.Drawing.Color.Transparent
        Me.LayoutView1.Appearance.FieldValue.Options.UseBackColor = True
        Me.LayoutView1.Appearance.ViewBackground.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.LayoutView1.Appearance.ViewBackground.Image = CType(resources.GetObject("LayoutView1.Appearance.ViewBackground.Image"), System.Drawing.Image)
        Me.LayoutView1.Appearance.ViewBackground.Options.UseImage = True
        Me.LayoutView1.AppearancePrint.FilterPanel.Options.UseImage = True
        Me.LayoutView1.CardCaptionFormat = "Usuario del S.G.C"
        Me.LayoutView1.CardHorzInterval = 14
        Me.LayoutView1.CardMinSize = New System.Drawing.Size(329, 516)
        Me.LayoutView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.LayoutViewColumn() {Me.BandedGridColumn5, Me.BandedGridColumn6, Me.BandedGridColumn7, Me.BandedGridColumn8, Me.LayoutViewColumn1, Me.LayoutViewColumn2, Me.LayoutViewColumn3})
        Me.LayoutView1.DetailHeight = 400
        Me.LayoutView1.GridControl = Me.GridControl2
        Me.LayoutView1.Images = Me.ImageList1
        Me.LayoutView1.Name = "LayoutView1"
        Me.LayoutView1.OptionsBehavior.ScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.[Auto]
        Me.LayoutView1.OptionsCarouselMode.BottomCardScale = 0.6!
        Me.LayoutView1.OptionsCarouselMode.CardCount = 8
        Me.LayoutView1.OptionsCarouselMode.FrameDelay = 9500
        Me.LayoutView1.OptionsCarouselMode.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic
        Me.LayoutView1.OptionsCarouselMode.PitchAngle = 1.5!
        Me.LayoutView1.OptionsCarouselMode.RollAngle = 3.1416!
        Me.LayoutView1.OptionsItemText.AlignMode = DevExpress.XtraGrid.Views.Layout.FieldTextAlignMode.AutoSize
        Me.LayoutView1.OptionsItemText.TextToControlDistance = 4
        Me.LayoutView1.OptionsView.AllowHotTrackFields = False
        Me.LayoutView1.OptionsView.CardArrangeRule = DevExpress.XtraGrid.Views.Layout.LayoutCardArrangeRule.AllowPartialCards
        Me.LayoutView1.OptionsView.ShowCardBorderIfCaptionHidden = False
        Me.LayoutView1.OptionsView.ShowCardExpandButton = False
        Me.LayoutView1.OptionsView.ShowCardFieldBorders = True
        Me.LayoutView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.LayoutView1.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.Carousel
        Me.LayoutView1.TemplateCard = Me.LayoutViewCard1
        '
        'BandedGridColumn5
        '
        Me.BandedGridColumn5.Caption = "Foto"
        Me.BandedGridColumn5.ColumnEdit = Me.RepositoryItemPictureEdit1
        Me.BandedGridColumn5.CustomizationCaption = "Foto"
        Me.BandedGridColumn5.FieldName = "foto"
        Me.BandedGridColumn5.LayoutViewField = Me.Item1
        Me.BandedGridColumn5.Name = "BandedGridColumn5"
        Me.BandedGridColumn5.OptionsColumn.ReadOnly = True
        Me.BandedGridColumn5.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
        '
        'RepositoryItemPictureEdit1
        '
        Me.RepositoryItemPictureEdit1.Name = "RepositoryItemPictureEdit1"
        Me.RepositoryItemPictureEdit1.PictureStoreMode = DevExpress.XtraEditors.Controls.PictureStoreMode.Image
        Me.RepositoryItemPictureEdit1.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze
        Me.RepositoryItemPictureEdit1.ZoomAccelerationFactor = 1.0R
        '
        'Item1
        '
        Me.Item1.EditorPreferredWidth = 318
        Me.Item1.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
        Me.Item1.Location = New System.Drawing.Point(0, 0)
        Me.Item1.Name = "Item1"
        Me.Item1.Padding = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.Item1.Size = New System.Drawing.Size(325, 29)
        Me.Item1.Spacing = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
        Me.Item1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.Item1.TextSize = New System.Drawing.Size(0, 0)
        Me.Item1.TextToControlDistance = 0
        Me.Item1.TextVisible = False
        '
        'BandedGridColumn6
        '
        Me.BandedGridColumn6.Caption = "Usuario"
        Me.BandedGridColumn6.FieldName = "usuario"
        Me.BandedGridColumn6.ImageIndex = 1
        Me.BandedGridColumn6.LayoutViewField = Me.Item15
        Me.BandedGridColumn6.Name = "BandedGridColumn6"
        Me.BandedGridColumn6.OptionsColumn.ReadOnly = True
        '
        'Item15
        '
        Me.Item15.EditorPreferredWidth = 237
        Me.Item15.ImageIndex = 1
        Me.Item15.Location = New System.Drawing.Point(0, 0)
        Me.Item15.Name = "Item15"
        Me.Item15.Padding = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.Item15.Size = New System.Drawing.Size(309, 27)
        Me.Item15.Spacing = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
        Me.Item15.TextSize = New System.Drawing.Size(61, 20)
        '
        'BandedGridColumn7
        '
        Me.BandedGridColumn7.Caption = "Nombres"
        Me.BandedGridColumn7.FieldName = "razon"
        Me.BandedGridColumn7.LayoutViewField = Me.Item3
        Me.BandedGridColumn7.Name = "BandedGridColumn7"
        Me.BandedGridColumn7.OptionsColumn.ReadOnly = True
        '
        'Item3
        '
        Me.Item3.EditorPreferredWidth = 245
        Me.Item3.Location = New System.Drawing.Point(0, 0)
        Me.Item3.MaxSize = New System.Drawing.Size(0, 23)
        Me.Item3.MinSize = New System.Drawing.Size(92, 23)
        Me.Item3.Name = "Item3"
        Me.Item3.Padding = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.Item3.Size = New System.Drawing.Size(317, 23)
        Me.Item3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.Item3.Spacing = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
        Me.Item3.TextSize = New System.Drawing.Size(61, 20)
        '
        'BandedGridColumn8
        '
        Me.BandedGridColumn8.Caption = "Rol"
        Me.BandedGridColumn8.FieldName = "rol"
        Me.BandedGridColumn8.LayoutViewField = Me.Item4
        Me.BandedGridColumn8.Name = "BandedGridColumn8"
        Me.BandedGridColumn8.OptionsColumn.ReadOnly = True
        '
        'Item4
        '
        Me.Item4.EditorPreferredWidth = 245
        Me.Item4.Location = New System.Drawing.Point(0, 23)
        Me.Item4.Name = "Item4"
        Me.Item4.Padding = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.Item4.Size = New System.Drawing.Size(317, 27)
        Me.Item4.Spacing = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
        Me.Item4.TextSize = New System.Drawing.Size(61, 20)
        '
        'LayoutViewColumn1
        '
        Me.LayoutViewColumn1.Caption = "Clave"
        Me.LayoutViewColumn1.ColumnEdit = Me.RepositoryItemTextEdit1
        Me.LayoutViewColumn1.FieldName = "c"
        Me.LayoutViewColumn1.ImageIndex = 0
        Me.LayoutViewColumn1.LayoutViewField = Me.Item16
        Me.LayoutViewColumn1.Name = "LayoutViewColumn1"
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        Me.RepositoryItemTextEdit1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        '
        'Item16
        '
        Me.Item16.EditorPreferredWidth = 240
        Me.Item16.ImageIndex = 0
        Me.Item16.Location = New System.Drawing.Point(0, 27)
        Me.Item16.Name = "Item16"
        Me.Item16.Padding = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.Item16.Size = New System.Drawing.Size(309, 43)
        Me.Item16.Spacing = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
        Me.Item16.TextSize = New System.Drawing.Size(58, 20)
        '
        'LayoutViewColumn2
        '
        Me.LayoutViewColumn2.Caption = "Cedula"
        Me.LayoutViewColumn2.FieldName = "cedula"
        Me.LayoutViewColumn2.LayoutViewField = Me.Item9
        Me.LayoutViewColumn2.Name = "LayoutViewColumn2"
        '
        'Item9
        '
        Me.Item9.EditorPreferredWidth = 248
        Me.Item9.Location = New System.Drawing.Point(0, 50)
        Me.Item9.Name = "Item9"
        Me.Item9.Padding = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.Item9.Size = New System.Drawing.Size(317, 27)
        Me.Item9.Spacing = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
        Me.Item9.TextSize = New System.Drawing.Size(58, 20)
        '
        'LayoutViewColumn3
        '
        Me.LayoutViewColumn3.Caption = "Ingreso"
        Me.LayoutViewColumn3.FieldName = "fecha_ingreso"
        Me.LayoutViewColumn3.LayoutViewField = Me.Item14
        Me.LayoutViewColumn3.Name = "LayoutViewColumn3"
        '
        'Item14
        '
        Me.Item14.EditorPreferredWidth = 245
        Me.Item14.Location = New System.Drawing.Point(0, 77)
        Me.Item14.Name = "Item14"
        Me.Item14.Padding = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.Item14.Size = New System.Drawing.Size(317, 27)
        Me.Item14.Spacing = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
        Me.Item14.TextSize = New System.Drawing.Size(61, 20)
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "keys-16x16.png")
        Me.ImageList1.Images.SetKeyName(1, "BO_Users.png")
        '
        'LayoutViewCard1
        '
        Me.LayoutViewCard1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutViewCard1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutViewCard1.BackgroundImage = CType(resources.GetObject("LayoutViewCard1.BackgroundImage"), System.Drawing.Image)
        Me.LayoutViewCard1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.LayoutViewCard1.CaptionImageIndex = 0
        Me.LayoutViewCard1.CustomizationFormText = "TemplateCard"
        Me.LayoutViewCard1.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText
        Me.LayoutViewCard1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.Item1, Me.Item2, Me.Group1})
        Me.LayoutViewCard1.Name = "LayoutViewCard1"
        Me.LayoutViewCard1.OptionsCustomization.AllowDrag = DevExpress.XtraLayout.ItemDragDropMode.Allow
        Me.LayoutViewCard1.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.CustomSize
        Me.LayoutViewCard1.OptionsItemText.TextToControlDistance = 4
        Me.LayoutViewCard1.Padding = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.LayoutViewCard1.Text = "TemplateCard"
        Me.LayoutViewCard1.TextLocation = DevExpress.Utils.Locations.[Default]
        '
        'Item2
        '
        Me.Item2.AllowHotTrack = False
        Me.Item2.CustomizationFormText = "item1"
        Me.Item2.Location = New System.Drawing.Point(0, 239)
        Me.Item2.Name = "Item2"
        Me.Item2.Padding = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.Item2.ShowInCustomizationForm = False
        Me.Item2.Size = New System.Drawing.Size(325, 2)
        Me.Item2.Spacing = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
        Me.Item2.Text = "SimpleSeparator1"
        '
        'Group1
        '
        Me.Group1.CustomizationFormText = "Datos Generales"
        Me.Group1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.Item3, Me.Item4, Me.Item9, Me.Item14, Me.Group4})
        Me.Group1.Location = New System.Drawing.Point(0, 29)
        Me.Group1.Name = "Group1"
        Me.Group1.Padding = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.Group1.Size = New System.Drawing.Size(325, 210)
        Me.Group1.Text = "Datos Generales"
        Me.Group1.TextLocation = DevExpress.Utils.Locations.[Default]
        '
        'Group4
        '
        Me.Group4.AppearanceGroup.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Group4.AppearanceGroup.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Group4.AppearanceGroup.BorderColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Group4.AppearanceGroup.Options.UseBackColor = True
        Me.Group4.AppearanceGroup.Options.UseBorderColor = True
        Me.Group4.CustomizationFormText = "Datos de Configuración"
        Me.Group4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.Item15, Me.Item16})
        Me.Group4.Location = New System.Drawing.Point(0, 104)
        Me.Group4.Name = "Group4"
        Me.Group4.Padding = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.Group4.Size = New System.Drawing.Size(317, 92)
        Me.Group4.Text = "Datos de Configuración"
        Me.Group4.TextLocation = DevExpress.Utils.Locations.[Default]
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn4, Me.GridColumn5, Me.GridColumn6})
        Me.GridView1.GridControl = Me.GridControl2
        Me.GridView1.Name = "GridView1"
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Codigo"
        Me.GridColumn4.FieldName = "usuario"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 0
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Nombres"
        Me.GridColumn5.FieldName = "razon"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 1
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Nivel"
        Me.GridColumn6.FieldName = "nivel"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 2
        '
        'CardView1
        '
        Me.CardView1.CardCaptionFormat = "Usuariosdel SGC"
        Me.CardView1.CardInterval = 50
        Me.CardView1.CardWidth = 500
        Me.CardView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn7, Me.GridColumn1, Me.GridColumn2, Me.GridColumn3})
        Me.CardView1.DetailHeight = 400
        Me.CardView1.FocusedCardTopFieldIndex = 0
        Me.CardView1.GridControl = Me.GridControl2
        Me.CardView1.MaximumCardColumns = 5
        Me.CardView1.MaximumCardRows = 6
        Me.CardView1.Name = "CardView1"
        Me.CardView1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.[Auto]
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Foto"
        Me.GridColumn7.ColumnEdit = Me.RepositoryItemPictureEdit1
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 3
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Codigo"
        Me.GridColumn1.FieldName = "usuario"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Nombres"
        Me.GridColumn2.FieldName = "razon"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Nivel"
        Me.GridColumn3.FieldName = "nivel"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        '
        'AdvBandedGridView1
        '
        Me.AdvBandedGridView1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand1})
        Me.AdvBandedGridView1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.BandedGridColumn1, Me.BandedGridColumn2, Me.BandedGridColumn3, Me.BandedGridColumn4})
        Me.AdvBandedGridView1.DetailHeight = 400
        Me.AdvBandedGridView1.GridControl = Me.GridControl2
        Me.AdvBandedGridView1.Name = "AdvBandedGridView1"
        '
        'GridBand1
        '
        Me.GridBand1.Caption = "asdfasdf"
        Me.GridBand1.Columns.Add(Me.BandedGridColumn1)
        Me.GridBand1.Columns.Add(Me.BandedGridColumn2)
        Me.GridBand1.Columns.Add(Me.BandedGridColumn3)
        Me.GridBand1.Columns.Add(Me.BandedGridColumn4)
        Me.GridBand1.Name = "GridBand1"
        Me.GridBand1.VisibleIndex = 0
        Me.GridBand1.Width = 300
        '
        'BandedGridColumn1
        '
        Me.BandedGridColumn1.Caption = "Foto"
        Me.BandedGridColumn1.ColumnEdit = Me.RepositoryItemPictureEdit1
        Me.BandedGridColumn1.Name = "BandedGridColumn1"
        Me.BandedGridColumn1.Visible = True
        '
        'BandedGridColumn2
        '
        Me.BandedGridColumn2.Caption = "Codigo"
        Me.BandedGridColumn2.FieldName = "usuario"
        Me.BandedGridColumn2.Name = "BandedGridColumn2"
        Me.BandedGridColumn2.Visible = True
        '
        'BandedGridColumn3
        '
        Me.BandedGridColumn3.Caption = "Nombres"
        Me.BandedGridColumn3.FieldName = "razon"
        Me.BandedGridColumn3.Name = "BandedGridColumn3"
        Me.BandedGridColumn3.Visible = True
        '
        'BandedGridColumn4
        '
        Me.BandedGridColumn4.Caption = "Nivel"
        Me.BandedGridColumn4.FieldName = "nivel"
        Me.BandedGridColumn4.Name = "BandedGridColumn4"
        Me.BandedGridColumn4.Visible = True
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Black
        Me.Panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel4.Controls.Add(Me.lbl_empresa)
        Me.Panel4.Controls.Add(Me.lbl_lema)
        Me.Panel4.Controls.Add(Me.lbl_ruc)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(632, 76)
        Me.Panel4.TabIndex = 378
        '
        'pnc_inicio
        '
        Me.pnc_inicio.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pnc_inicio.BackColor = System.Drawing.Color.Transparent
        Me.pnc_inicio.Controls.Add(Me.LabelControl3)
        Me.pnc_inicio.Controls.Add(Me.lbl_welcome)
        Me.pnc_inicio.Controls.Add(Me.LabelControl5)
        Me.pnc_inicio.Controls.Add(Me.Panel3)
        Me.pnc_inicio.Controls.Add(Me.LabelControl2)
        Me.pnc_inicio.Controls.Add(Me.LabelControl1)
        Me.pnc_inicio.Controls.Add(Me.txtusuario)
        Me.pnc_inicio.Controls.Add(Me.txtclave)
        Me.pnc_inicio.Location = New System.Drawing.Point(103, 122)
        Me.pnc_inicio.Name = "pnc_inicio"
        Me.pnc_inicio.Size = New System.Drawing.Size(426, 207)
        Me.pnc_inicio.TabIndex = 379
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Appearance.ForeColor = System.Drawing.Color.White
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Appearance.Options.UseForeColor = True
        Me.LabelControl3.Location = New System.Drawing.Point(85, 55)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(176, 14)
        Me.LabelControl3.TabIndex = 11
        Me.LabelControl3.Text = "Tax - Impuestos && Retenciones "
        '
        'lbl_welcome
        '
        Me.lbl_welcome.Appearance.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_welcome.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbl_welcome.Appearance.Options.UseFont = True
        Me.lbl_welcome.Appearance.Options.UseForeColor = True
        Me.lbl_welcome.Location = New System.Drawing.Point(85, 53)
        Me.lbl_welcome.Name = "lbl_welcome"
        Me.lbl_welcome.Size = New System.Drawing.Size(0, 18)
        Me.lbl_welcome.TabIndex = 10
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(3, 3)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(0, 16)
        Me.LabelControl5.TabIndex = 8
        '
        'Panel3
        '
        Me.Panel3.BackgroundImage = CType(resources.GetObject("Panel3.BackgroundImage"), System.Drawing.Image)
        Me.Panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel3.Location = New System.Drawing.Point(283, 12)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(130, 184)
        Me.Panel3.TabIndex = 5
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Appearance.ForeColor = System.Drawing.Color.White
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Appearance.Options.UseForeColor = True
        Me.LabelControl2.Location = New System.Drawing.Point(10, 110)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(61, 14)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "Contraseña"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Appearance.ForeColor = System.Drawing.Color.White
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Appearance.Options.UseForeColor = True
        Me.LabelControl1.Location = New System.Drawing.Point(42, 79)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(29, 14)
        Me.LabelControl1.TabIndex = 3
        Me.LabelControl1.Text = "Login"
        '
        'txtusuario
        '
        Me.txtusuario.EditValue = "sandra"
        Me.txtusuario.Location = New System.Drawing.Point(82, 76)
        Me.txtusuario.Name = "txtusuario"
        Me.txtusuario.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtusuario.Properties.Appearance.Options.UseFont = True
        Me.txtusuario.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtusuario.Size = New System.Drawing.Size(188, 24)
        Me.txtusuario.TabIndex = 2
        '
        'txtclave
        '
        Me.txtclave.EditValue = "123"
        Me.txtclave.Location = New System.Drawing.Point(82, 107)
        Me.txtclave.Name = "txtclave"
        Me.txtclave.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtclave.Properties.Appearance.Options.UseFont = True
        Me.txtclave.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtclave.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(8226)
        Me.txtclave.Size = New System.Drawing.Size(188, 24)
        Me.txtclave.TabIndex = 3
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Black
        Me.Panel2.Controls.Add(Me.btn_configurar)
        Me.Panel2.Controls.Add(Me.btnllaves)
        Me.Panel2.Controls.Add(Me.btnsalir)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 375)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(632, 75)
        Me.Panel2.TabIndex = 380
        '
        'btn_configurar
        '
        Me.btn_configurar.BackColor = System.Drawing.Color.Transparent
        Me.btn_configurar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_configurar.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btn_configurar.FlatAppearance.BorderSize = 0
        Me.btn_configurar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn_configurar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn_configurar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_configurar.Image = CType(resources.GetObject("btn_configurar.Image"), System.Drawing.Image)
        Me.btn_configurar.Location = New System.Drawing.Point(4, 3)
        Me.btn_configurar.Name = "btn_configurar"
        Me.btn_configurar.Size = New System.Drawing.Size(78, 68)
        Me.btn_configurar.TabIndex = 13
        Me.btn_configurar.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        '
        'frm_acceso
        '
        Me.AcceptButton = Me.btnllaves
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlText
        Me.ClientSize = New System.Drawing.Size(632, 450)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.pnc_inicio)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.GridControl2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frm_acceso"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SISTEMA DE GESTION COMERCIAL - ACCESO AL SISTEMA"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemPictureEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Item1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Item15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Item3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Item4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Item16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Item9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Item14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutViewCard1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Item2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Group1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Group4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CardView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.pnc_inicio.ResumeLayout(False)
        Me.pnc_inicio.PerformLayout()
        CType(Me.txtusuario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtclave.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnllaves As System.Windows.Forms.Button
    Friend WithEvents btnsalir As System.Windows.Forms.Button
    Friend WithEvents rotulos As System.Windows.Forms.ToolTip
    Friend WithEvents lbl_empresa As System.Windows.Forms.Label
    Friend WithEvents lbl_lema As System.Windows.Forms.Label
    Friend WithEvents lbl_ruc As System.Windows.Forms.Label
    Private WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents CardView1 As DevExpress.XtraGrid.Views.Card.CardView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemPictureEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit
    Friend WithEvents LayoutView1 As DevExpress.XtraGrid.Views.Layout.LayoutView
    Friend WithEvents BandedGridColumn5 As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Friend WithEvents BandedGridColumn6 As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Friend WithEvents BandedGridColumn7 As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Friend WithEvents BandedGridColumn8 As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Friend WithEvents AdvBandedGridView1 As DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
    Friend WithEvents GridBand1 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents BandedGridColumn1 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents BandedGridColumn2 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents BandedGridColumn3 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents BandedGridColumn4 As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents LayoutViewColumn1 As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents LayoutViewColumn2 As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Friend WithEvents LayoutViewColumn3 As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Friend WithEvents Item1 As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents Item15 As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents Item3 As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents Item4 As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents Item16 As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents Item9 As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents Item14 As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents LayoutViewCard1 As DevExpress.XtraGrid.Views.Layout.LayoutViewCard
    Friend WithEvents Item2 As DevExpress.XtraLayout.SimpleSeparator
    Friend WithEvents Group1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents Group4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents pnc_inicio As System.Windows.Forms.Panel
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtusuario As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtclave As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents btn_configurar As System.Windows.Forms.Button
    Friend WithEvents lbl_welcome As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
End Class
