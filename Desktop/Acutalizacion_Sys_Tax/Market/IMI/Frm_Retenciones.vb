﻿Imports System.Data.SqlClient
Imports System.Data.Sql

Public Class Frm_Retenciones
    Dim A As New Auditoria
    Dim _ID As Integer
    Public Property ID() As Integer
        Get
            Return _ID
        End Get
        Set(value As Integer)
            _ID = value
        End Set
    End Property

    Dim _ProveedorID As Integer
    Public Property ProveedorID() As Integer
        Get
            Return _ProveedorID
        End Get
        Set(value As Integer)
            _ProveedorID = value
        End Set
    End Property


    Dim _ReciboIMI As String
    Public Property ReciboIMI() As String
        Get
            Return _ReciboIMI
        End Get
        Set(value As String)
            _ReciboIMI = value
        End Set
    End Property

    Dim _ReciboIR As String
    Public Property ReciboIR() As String
        Get
            Return _ReciboIR
        End Get
        Set(value As String)
            _ReciboIR = value
        End Set
    End Property

    Dim _Accion As String
    Public Property Accion() As String
        Get
            Return _Accion
        End Get
        Set(value As String)
            _Accion = value
        End Set
    End Property

    Dim _IRPOR As Decimal
    Public Property IRPOR() As String
        Get
            Return _IRPOR
        End Get
        Set(value As String)
            _IRPOR = value
        End Set
    End Property

    Dim _PathFile As String
    Public Property varFilePath() As String
        Get
            Return _PathFile
        End Get
        Set(value As String)
            _PathFile = value
        End Set
    End Property

    Dim _FileName As String
    Public Property varNameFile() As String
        Get
            Return _FileName
        End Get
        Set(value As String)
            _FileName = value
        End Set
    End Property



    Dim RetenidoDB As New DataTable
    Dim IR As New DataTable
    Private Sub Frm_IU_Retencion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarOficina()
        cargarProveedor()
        cargarIR()

        Me.Grid_Imagenes.DataSource = Rutinas_SGC.obtener_datos("SELECT ri.RetencionID, ri.Imagen, ds.Documento +' '+ ri.Nombre Nombre FROM RetencionImagen AS ri INNER JOIN DocumentoScan AS ds ON ds.TipoID = ri.TipoID")

        cmb_banco.Properties.DataSource = Rutinas_SGC.obtener_datos("SELECT b.BancoID,b.Banco,b.Siglas FROM Bancos AS b")
        cmb_banco.Properties.ValueMember = "BancoID"
        cmb_banco.Properties.DisplayMember = "Siglas"
        cmb_banco.Properties.PopulateViewColumns()


        If Accion = "NEW" Then
            Me.Text = "Registrar Retención"

            Me.cmb_retenido.Focus()
        Else
            Me.Text = "Editar Retención"

            Dim DT_Facturas As DataTable = Retensiones.Unique(ID)

            If DT_Facturas.Rows.Count > 0 Then

                Me.cmb_retenido.EditValue = DT_Facturas.Rows(0).Item("ProveedorID")

                Me.Txt_Cedula.EditValue = DT_Facturas.Rows(0).Item("Cedula")
                Me.Txt_Ruc.EditValue = DT_Facturas.Rows(0).Item("RUC")
                Me.txt_telefono.EditValue = DT_Facturas.Rows(0).Item("Telefono")

                Me.dpt_fecha.EditValue = DT_Facturas.Rows(0).Item("Fecha")
                Me.txt_No_Factura.EditValue = DT_Facturas.Rows(0).Item("FacturaNo_")
                Me.txt_concepto.EditValue = DT_Facturas.Rows(0).Item("Concepto")
                Me.txt_Importe.EditValue = DT_Facturas.Rows(0).Item("ImporteFactura")

                Me.txt_por_imi.EditValue = DT_Facturas.Rows(0).Item("IMI_por")
                Me.txt_por_ir.EditValue = DT_Facturas.Rows(0).Item("IR_por")
                Me.txt_por_iva.EditValue = DT_Facturas.Rows(0).Item("IVA_por")

                Me.chk_imi.Checked = DT_Facturas.Rows(0).Item("ckIMI")
                Me.chk_ir.Checked = DT_Facturas.Rows(0).Item("ckIR")
                Me.chk_iva.Checked = DT_Facturas.Rows(0).Item("ckIVA")

                Me.txt_IMI.EditValue = DT_Facturas.Rows(0).Item("ImporteIMI")
                Me.txt_IMI_Recibo.EditValue = DT_Facturas.Rows(0).Item("ReciboIMI")

                Me.txt_IR.EditValue = DT_Facturas.Rows(0).Item("ImporteIR")
                Me.txt_IR_Recibo.EditValue = DT_Facturas.Rows(0).Item("ReciboIR")

                Me.txt_iva.EditValue = DT_Facturas.Rows(0).Item("ImporteIVA")

                Me.chk_automatico.EditValue = DT_Facturas.Rows(0).Item("Automatico")
                Me.txt_ck.EditValue = DT_Facturas.Rows(0).Item("Cheque")
                Me.cmb_banco.EditValue = DT_Facturas.Rows(0).Item("BancoID")
                Me.txt_Observaciones.EditValue = DT_Facturas.Rows(0).Item("Observaciones")

                Me.cmb_oficina.EditValue = DT_Facturas.Rows(0).Item("OficinaID")
                Me.cmb_ir.EditValue = DT_Facturas.Rows(0).Item("TipoRetencionID")
                Me.txt_filename.EditValue = DT_Facturas.Rows(0).Item("NameFile")

                varNameFile = DT_Facturas.Rows(0).Item("NameFile")
                varFilePath = DT_Facturas.Rows(0).Item("FilePath")

                Me.dpt_fecha.Focus()

            End If



        End If




        'Combox.View.BestFitColumns()
        'Combox.PopupFormMinSize = New Point(Combox.Size.Width, 0)
        'Combox.ShowFooter = True
        'Combox.Text = String.Empty
        Me.dpt_fecha.Focus()

    End Sub
    Sub cargartalonario()
        'Dim imi_talonario As New DataTable
        'Dim ir_talonario As New DataTable

        'If Me.cmb_oficina.EditValue Is Nothing Then
        '    Exit Sub
        'End If

        'If String.IsNullOrEmpty(Me.cmb_oficina.Text) Then
        '    Exit Sub
        'End If

        'imi_talonario = Talonario.GetLisTalonario("IMI", Me.cmb_oficina.EditValue)
        'ir_talonario = Talonario.GetLisTalonario("IR", Me.cmb_oficina.EditValue)

        'If Talonario.GetLisTalonario("IMI", Me.cmb_oficina.EditValue).Rows.Count > 0 Then
        '    Me.txt_IMI_Recibo.EditValue = imi_talonario.Rows(0).Item("ReciboID")
        '    Me.txt_IMI_Serie.EditValue = imi_talonario.Rows(0).Item("Serie").ToString
        'Else
        '    MessageBox.Show("Revisar Talonarios de IMI", "Aviso Importante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        'End If

        'If Talonario.GetLisTalonario("IR", Me.cmb_oficina.EditValue).Rows.Count > 0 Then
        '    Me.txt_IR_Recibo.EditValue = ir_talonario.Rows(0).Item("ReciboID")
        '    Me.txt_IR_serie.EditValue = ir_talonario.Rows(0).Item("Serie").ToString
        'Else
        '    MessageBox.Show("Revisar Talonarios de IR", "Aviso Importante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        'End If

      



    End Sub
    Sub cargarProveedor()

        RetenidoDB = Rutinas_SGC.obtener_datos("GetListProveedor '%'")
        cmb_retenido.Properties.DataSource = RetenidoDB
        cmb_retenido.Properties.ValueMember = "ProveedorID"
        cmb_retenido.Properties.DisplayMember = "Proveedor"
        cmb_retenido.Properties.PopulateViewColumns()

    End Sub
    Sub cargarOficina()

        Dim dt As New DataTable
        dt = Rutinas_SGC.obtener_datos("GetOficinas '" & Usuario_ & "'")
        
        cmb_oficina.Properties.DataSource = dt
        cmb_oficina.Properties.ValueMember = "OficinaID"
        cmb_oficina.Properties.DisplayMember = "Oficina"
        cmb_oficina.Properties.PopulateViewColumns()

        If dt.Rows.Count = 1 Then
            cmb_oficina.EditValue = dt.Rows(0).Item("OficinaID")
        End If

    End Sub

    Sub cargarIR()

        IR = Rutinas_SGC.obtener_datos("GetListIR '%'")
        cmb_ir.Properties.DataSource = IR
        cmb_ir.Properties.ValueMember = "ID"
        cmb_ir.Properties.DisplayMember = "Descripcion"
        cmb_ir.Properties.PopulateViewColumns()

    End Sub

    Private Sub cmb_retenido_ButtonClick(sender As Object, e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles cmb_retenido.ButtonClick
        'DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis
        If e.Button.Index = 1 Then

            My.Forms.Frm_IU_Proveedor.Accion = "NEW"
            My.Forms.Frm_IU_Proveedor.ShowDialog()

            cargarProveedor()
            Me.cmb_retenido.Text = Rutinas_SGC.obtener_datos("SELECT max(r.ProveedorID) ID FROM Proveedor AS r").Rows(0).Item("ID")
            cmb_retenido_Validated(Nothing, Nothing)
            Me.dpt_fecha.Focus()


        End If

        If e.Button.Index = 2 Then

            If String.IsNullOrEmpty(Me.cmb_retenido.Text) Then
                Exit Sub
            End If

            My.Forms.Frm_IU_Proveedor.Accion = "EDIT"
            My.Forms.Frm_IU_Proveedor.ID = Me.cmb_retenido.EditValue
            My.Forms.Frm_IU_Proveedor.ShowDialog()
            cargarProveedor()
            Me.cmb_retenido.EditValue = ProveedorID
            cmb_retenido_Validated(Nothing, Nothing)
            Me.dpt_fecha.Focus()



        End If


    End Sub

    Private Sub cmb_retenido_EditValueChanged(sender As Object, e As EventArgs) Handles cmb_retenido.EditValueChanged
        If Me.cmb_retenido.EditValue IsNot Nothing Then
            Dim row As DataRow = Me.cmb_retenido.Properties.View.GetDataRow(Me.cmb_retenido.Properties.View.FocusedRowHandle)
            If row IsNot Nothing Then
                retenido(row.Item("ProveedorID"))
                '       Me.dpt_fecha.Focus()
            End If
        End If
    End Sub
    Sub retenido(ID As String)
        Try


            Dim retenido As DataTable

            retenido = Proveedor.GetList(ID)
            If retenido.Rows.Count > 0 Then

                Me.ProveedorID = retenido.Rows(0).Item("ProveedorID").ToString
                Me.Txt_Ruc.EditValue = retenido.Rows(0).Item("Ruc").ToString
                Me.txt_telefono.EditValue = retenido.Rows(0).Item("Telefono").ToString
                Me.Txt_Cedula.EditValue = retenido.Rows(0).Item("Cedula").ToString

                Me.chk_pro_iva.Checked = CBool(retenido.Rows(0).Item("iva"))
                Me.chk_pro_ir.Checked = CBool(retenido.Rows(0).Item("ir"))
                Me.chk_pro_imi.Checked = CBool(retenido.Rows(0).Item("imi"))

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Aviso Importante")
        End Try

    End Sub

    Private Sub cmb_retenido_Validated(sender As Object, e As EventArgs) Handles cmb_retenido.Validated

        If String.IsNullOrEmpty(Me.cmb_retenido.Text) Then
            Exit Sub
        End If


        Dim nic As String
        If Me.cmb_retenido.EditValue IsNot Nothing Then
            Dim precio = From premio In RetenidoDB.AsEnumerable() Where premio.Field(Of Integer)("ProveedorID") = Me.cmb_retenido.EditValue
            For Each p As Object In precio
                nic = p.item("ProveedorID")
            Next
        End If

        If nic = String.Empty Then
            Exit Sub
        End If

        retenido(nic)
        'Me.dpt_fecha.Focus()

    End Sub

    Private Sub TextEdit6_Validated(sender As Object, e As EventArgs) Handles txt_Importe.Validated

        If Me.chk_automatico.CheckState = CheckState.Checked Then
            calcular()
        End If

    End Sub



    Private Sub btn_guardar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_guardar.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Guardar", "Guardar Retencion", e.Item.Caption + "|" + e.Item.Tag)
        If validar() Then

            If Accion = "NEW" Then

                If MessageBox.Show("¿ Registrar Retención ?", "Aviso Importante...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If


                Try
                    Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
                    Dim cmd As New SqlCommand("IU_Retenciones", conexion)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim RetencionID As Integer

                    Dim FileName As String = System.IO.Path.GetFileName(OpenFile.FileName)
                    Dim NameAsign As String = String.Empty

                    cmd.Parameters.Add("NameFile", SqlDbType.NVarChar)
                    cmd.Parameters("NameFile").Value = IIf(Not String.IsNullOrWhiteSpace(FileName.ToString), FileName.ToString, String.Empty)

                    cmd.Parameters.Add("OficinaID", SqlDbType.Int)
                    cmd.Parameters("OficinaID").Value = Me.cmb_oficina.EditValue

                    cmd.Parameters.Add("UsuarioID", SqlDbType.NVarChar)
                    cmd.Parameters("UsuarioID").Value = Usuario_

                    cmd.Parameters.Add("TipoRetencionID", SqlDbType.Int)
                    cmd.Parameters("TipoRetencionID").Value = Me.cmb_ir.EditValue

                    cmd.Parameters.Add("ProveedorID", SqlDbType.Int)
                    cmd.Parameters("ProveedorID").Value = Me.cmb_retenido.EditValue

                    cmd.Parameters.Add("Fecha", SqlDbType.Date)
                    cmd.Parameters("Fecha").Value = Me.dpt_fecha.EditValue

                    cmd.Parameters.Add("FacturaNo", SqlDbType.NVarChar)
                    cmd.Parameters("FacturaNo").Value = Me.txt_No_Factura.EditValue

                    cmd.Parameters.Add("Concepto", SqlDbType.NVarChar)
                    cmd.Parameters("Concepto").Value = Me.txt_concepto.EditValue

                    cmd.Parameters.Add("ImporteFactura", SqlDbType.Decimal)
                    cmd.Parameters("ImporteFactura").Value = CDec(Me.txt_Importe.EditValue)

                    ' Valores del IMI

                    cmd.Parameters.Add("IMI_por", SqlDbType.Decimal)
                    cmd.Parameters("IMI_por").Value = IIf(Me.chk_imi.Checked = True, CDec(Me.txt_por_imi.EditValue), 0.0)

                    cmd.Parameters.Add("ImporteIMI", SqlDbType.Decimal)
                    cmd.Parameters("ImporteIMI").Value = IIf(Me.chk_imi.Checked = True, CDec(Me.txt_IMI.EditValue), 0.0)

                    cmd.Parameters.Add("ReciboIMI", SqlDbType.NVarChar)
                    cmd.Parameters("ReciboIMI").Value = IIf(Me.chk_imi.Checked = True, Me.txt_IMI_Recibo.Text, String.Empty)

                    cmd.Parameters.Add("IMISerie", SqlDbType.NVarChar)
                    cmd.Parameters("IMISerie").Value = IIf(Me.chk_imi.Checked = True, Me.txt_IMI_Serie.Text, String.Empty)

                    cmd.Parameters.Add("ckIMI", SqlDbType.Bit)
                    cmd.Parameters("ckIMI").Value = Me.chk_imi.Checked

                    ' Valores del IR

                    cmd.Parameters.Add("IR_por", SqlDbType.Decimal)
                    cmd.Parameters("IR_por").Value = IIf(Me.chk_ir.Checked = True, CDec(Me.txt_por_ir.EditValue), 0.0)

                    cmd.Parameters.Add("ImporteIR", SqlDbType.Decimal)
                    cmd.Parameters("ImporteIR").Value = IIf(Me.chk_ir.Checked = True, CDec(Me.txt_IR.EditValue), 0.0)

                    cmd.Parameters.Add("ReciboIR", SqlDbType.NVarChar)
                    cmd.Parameters("ReciboIR").Value = IIf(Me.chk_ir.Checked = True, Me.txt_IR_Recibo.Text.ToString, String.Empty)

                    cmd.Parameters.Add("IRSerie", SqlDbType.NVarChar)
                    cmd.Parameters("IRSerie").Value = IIf(Me.chk_ir.Checked = True, Me.txt_IR_serie.Text.ToString, String.Empty)

                    cmd.Parameters.Add("ckIR", SqlDbType.Bit)
                    cmd.Parameters("ckIR").Value = Me.chk_ir.Checked

                    ' Valores del IVA

                    cmd.Parameters.Add("IVA_por", SqlDbType.Decimal)
                    cmd.Parameters("IVA_por").Value = IIf(Me.chk_iva.Checked = True, CDec(Me.txt_por_iva.EditValue), 0.0)

                    cmd.Parameters.Add("ImporteIVA", SqlDbType.Decimal)
                    cmd.Parameters("ImporteIVA").Value = IIf(Me.chk_iva.Checked = True, CDec(Me.txt_iva.EditValue), 0.0)

                    cmd.Parameters.Add("ckIVA", SqlDbType.Bit)
                    cmd.Parameters("ckIVA").Value = Me.chk_iva.Checked

                    ''' ------ -'''' 

                    cmd.Parameters.Add("Automatico", SqlDbType.Bit)
                    cmd.Parameters("Automatico").Value = Me.chk_automatico.EditValue

                    cmd.Parameters.Add("Cheque", SqlDbType.NVarChar)
                    cmd.Parameters("Cheque").Value = Me.txt_ck.Text.ToString

                    cmd.Parameters.Add("BancoID", SqlDbType.NVarChar)
                    cmd.Parameters("BancoID").Value = IIf(String.IsNullOrEmpty(Me.cmb_banco.Text), "", Me.cmb_banco.EditValue)

                    cmd.Parameters.Add("OrigenID", SqlDbType.Int)
                    cmd.Parameters("OrigenID").Value = 0

                    cmd.Parameters.Add("Observacion", SqlDbType.NVarChar)
                    cmd.Parameters("Observacion").Value = Me.txt_Observaciones.Text.ToUpper

                    cmd.Parameters.Add("Accion", SqlDbType.NVarChar)
                    cmd.Parameters("Accion").Value = "New"


                    'cmd.Parameters.Add("RetencionID", SqlDbType.Int)
                    'cmd.Parameters("RetencionID").Value = 0

                    Dim _RetencionID As SqlParameter = New SqlParameter("RetencionID", SqlDbType.Int)
                    _RetencionID.Direction = ParameterDirection.Output
                    cmd.Parameters.Add(_RetencionID)

                    conexion.Open()
                    cmd.ExecuteNonQuery()
                    RetencionID = _RetencionID.Value
                    conexion.Close()

                    'Dim FileName As String = System.IO.Path.GetFileName(OpenFile.FileName)
                    'Dim NameAsign As String = String.Empty
                    'Dim Path As String = "\\192.168.15.232\BaseDatosSQL\Anexos\"
                    'Dim Fecha As String = Date.Today
                    'Fecha = Fecha.Replace("/", "_")
                    'NameAsign = "\\192.168.15.232\BaseDatosSQL\Anexos\" & RetencionID.ToString & "_" & Me.cmb_oficina.EditValue.ToString & "_" & Me.cmb_retenido.EditValue.ToString & "_" & FileName

                    If Not String.IsNullOrWhiteSpace(FileName) Then

                        NameAsign = FilePath & RetencionID.ToString & "_" & FileName
                        System.IO.File.Copy(OpenFile.FileName, NameAsign)

                    End If

                    ' Oficina_Proveedor_RetencionID_Fecha_
                    '15_11_09_01_2020_'

                    Me.cmb_retenido.Focus()
                    Me.cmb_retenido.Text = String.Empty
                    Me.Txt_Cedula.Text = String.Empty
                    Me.Txt_Ruc.Text = String.Empty
                    Me.txt_telefono.Text = String.Empty

                    Me.dpt_fecha.EditValue = Now

                    Me.txt_No_Factura.Text = String.Empty
                    Me.txt_concepto.Text = String.Empty

                    Me.txt_iva.EditValue = 0.0
                    Me.txt_Importe.EditValue = 0.0

                    Me.txt_IMI_Recibo.Text = String.Empty
                    Me.txt_IR_Recibo.Text = String.Empty

                    Me.chk_ir.Checked = True
                    Me.chk_imi.Checked = True
                    Me.chk_automatico.Checked = True


                    Me.txt_IMI.EditValue = 0.0
                    Me.txt_IR.EditValue = 0.0

                    Me.txt_ck.Text = String.Empty
                    Me.cmb_banco.EditValue = String.Empty

                    Me.txt_filename.Text = String.Empty
                    OpenFile.FileName = Nothing
                    OpenFile.Reset()

                    cargartalonario()
                    My.Forms.Frm_P_Retencion.loadata()

                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Aviso Importe", MessageBoxButtons.OK, MessageBoxIcon.Question)
                End Try
            Else
                Try
                    Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
                    Dim cmd As New SqlCommand("EditRetenciones", conexion)
                    cmd.CommandType = CommandType.StoredProcedure


                    Dim FileName As String = System.IO.Path.GetFileName(OpenFile.FileName)
                    Dim NameAsign As String = String.Empty

                    NameAsign = FilePath & ID.ToString & "_" & FileName


                    If Not String.IsNullOrWhiteSpace(FileName.ToString) Then

                        cmd.Parameters.Add("FilePath", SqlDbType.NVarChar)
                        cmd.Parameters("FilePath").Value = NameAsign.ToString

                        cmd.Parameters.Add("NameFile", SqlDbType.NVarChar)
                        cmd.Parameters("NameFile").Value = FileName.ToString

                    Else

                        cmd.Parameters.Add("FilePath", SqlDbType.NVarChar)
                        cmd.Parameters("FilePath").Value = varFilePath

                        cmd.Parameters.Add("NameFile", SqlDbType.NVarChar)
                        cmd.Parameters("NameFile").Value = varNameFile

                    End If

                    cmd.Parameters.Add("OficinaID", SqlDbType.Int)
                    cmd.Parameters("OficinaID").Value = Me.cmb_oficina.EditValue

                    cmd.Parameters.Add("UsuarioID", SqlDbType.NVarChar)
                    cmd.Parameters("UsuarioID").Value = Usuario_

                    cmd.Parameters.Add("TipoRetencionID", SqlDbType.Int)
                    cmd.Parameters("TipoRetencionID").Value = Me.cmb_ir.EditValue

                    cmd.Parameters.Add("RetencionID", SqlDbType.Int)
                    cmd.Parameters("RetencionID").Value = ID

                    cmd.Parameters.Add("ProveedorID", SqlDbType.Int)
                    cmd.Parameters("ProveedorID").Value = Me.cmb_retenido.EditValue

                    cmd.Parameters.Add("Fecha", SqlDbType.Date)
                    cmd.Parameters("Fecha").Value = Me.dpt_fecha.EditValue

                    cmd.Parameters.Add("FacturaNo", SqlDbType.NVarChar)
                    cmd.Parameters("FacturaNo").Value = Me.txt_No_Factura.EditValue

                    cmd.Parameters.Add("Concepto", SqlDbType.NVarChar)
                    cmd.Parameters("Concepto").Value = Me.txt_concepto.EditValue

                    cmd.Parameters.Add("ImporteFactura", SqlDbType.Decimal)
                    cmd.Parameters("ImporteFactura").Value = CDec(Me.txt_Importe.EditValue)

                    cmd.Parameters.Add("IMI_por", SqlDbType.Decimal)
                    cmd.Parameters("IMI_por").Value = IIf(Me.chk_imi.Checked = True, CDec(Me.txt_por_imi.EditValue), 0.0)

                    cmd.Parameters.Add("ImporteIMI", SqlDbType.Decimal)
                    cmd.Parameters("ImporteIMI").Value = IIf(Me.chk_imi.Checked = True, CDec(Me.txt_IMI.EditValue), 0.0)

                    cmd.Parameters.Add("ReciboIMI", SqlDbType.NVarChar)
                    cmd.Parameters("ReciboIMI").Value = IIf(Me.chk_imi.Checked = True, Me.txt_IMI_Recibo.Text, String.Empty)

                    cmd.Parameters.Add("IMISerie", SqlDbType.NVarChar)
                    cmd.Parameters("IMISerie").Value = IIf(Me.chk_imi.Checked = True, Me.txt_IMI_Serie.Text, String.Empty)

                    cmd.Parameters.Add("ckIMI", SqlDbType.Bit)
                    cmd.Parameters("ckIMI").Value = Me.chk_imi.Checked

                    ' Valores del IR

                    cmd.Parameters.Add("IR_por", SqlDbType.Decimal)
                    cmd.Parameters("IR_por").Value = IIf(Me.chk_ir.Checked = True, CDec(Me.txt_por_ir.EditValue), 0.0)

                    cmd.Parameters.Add("ImporteIR", SqlDbType.Decimal)
                    cmd.Parameters("ImporteIR").Value = IIf(Me.chk_ir.Checked = True, CDec(Me.txt_IR.EditValue), 0.0)

                    cmd.Parameters.Add("ReciboIR", SqlDbType.NVarChar)
                    cmd.Parameters("ReciboIR").Value = IIf(Me.chk_ir.Checked = True, Me.txt_IR_Recibo.Text.ToString, String.Empty)

                    cmd.Parameters.Add("IRSerie", SqlDbType.NVarChar)
                    cmd.Parameters("IRSerie").Value = IIf(Me.chk_ir.Checked = True, Me.txt_IR_serie.Text.ToString, String.Empty)

                    cmd.Parameters.Add("ckIR", SqlDbType.Bit)
                    cmd.Parameters("ckIR").Value = Me.chk_ir.Checked

                    ' Valores del IVA

                    cmd.Parameters.Add("IVA_por", SqlDbType.Decimal)
                    cmd.Parameters("IVA_por").Value = IIf(Me.chk_iva.Checked = True, CDec(Me.txt_por_iva.EditValue), 0.0)

                    cmd.Parameters.Add("ImporteIVA", SqlDbType.Decimal)
                    cmd.Parameters("ImporteIVA").Value = IIf(Me.chk_iva.Checked = True, CDec(Me.txt_iva.EditValue), 0.0)

                    cmd.Parameters.Add("ckIVA", SqlDbType.Bit)
                    cmd.Parameters("ckIVA").Value = Me.chk_iva.Checked


                    cmd.Parameters.Add("Automatico", SqlDbType.Bit)
                    cmd.Parameters("Automatico").Value = Me.chk_automatico.EditValue

                    cmd.Parameters.Add("Cheque", SqlDbType.NVarChar)
                    cmd.Parameters("Cheque").Value = Me.txt_ck.Text.ToString

                    cmd.Parameters.Add("BancoID", SqlDbType.NVarChar)
                    cmd.Parameters("BancoID").Value = IIf(String.IsNullOrEmpty(Me.cmb_banco.Text), "", Me.cmb_banco.EditValue)

                    cmd.Parameters.Add("OrigenID", SqlDbType.Int)
                    cmd.Parameters("OrigenID").Value = 0

                    cmd.Parameters.Add("Observacion", SqlDbType.NVarChar)
                    cmd.Parameters("Observacion").Value = Me.txt_Observaciones.Text.ToUpper

                    cmd.Parameters.Add("Accion", SqlDbType.NVarChar)
                    cmd.Parameters("Accion").Value = "EDIT"

                    conexion.Open()
                    cmd.ExecuteNonQuery()
                    conexion.Close()

                    If Not String.IsNullOrWhiteSpace(FileName) Then

                        If Not String.IsNullOrWhiteSpace(varFilePath) Then
                            My.Computer.FileSystem.DeleteFile(varFilePath)
                        End If

                        System.IO.File.Copy(OpenFile.FileName, NameAsign)

                    End If

                    'Me.cmb_retenido.Focus()
                    'Me.cmb_retenido.Text = String.Empty
                    'Me.Txt_Cedula.Text = String.Empty
                    'Me.Txt_Ruc.Text = String.Empty
                    'Me.txt_telefono.Text = String.Empty

                    'Me.dpt_fecha.EditValue = Now

                    'Me.txt_No_Factura.Text = String.Empty
                    'Me.txt_concepto.Text = String.Empty

                    'Me.txt_Importe.EditValue = 0.0

                    'Me.txt_por_imi.Text = "1.00"
                    'Me.txt_por_ir.Text = "2.00"

                    'Me.txt_IMI_Recibo.Text = String.Empty
                    'Me.txt_IR_Recibo.Text = String.Empty

                    'Me.chk_ir.Checked = True
                    'Me.ck_imi.Checked = True
                    'Me.chk_automatico.Checked = True


                    'Me.txt_IMI.EditValue = 0.0
                    'Me.txt_IR.EditValue = 0.0

                    'Me.txt_ck.Text = String.Empty
                    'Me.cmb_banco.EditValue = String.Empty

                    'cargartalonario()
                    My.Forms.Frm_P_Retencion.loadata()
                    Me.Close()

                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Aviso Importe", MessageBoxButtons.OK, MessageBoxIcon.Question)
                End Try




            End If
        End If

    End Sub
    Function validar()


        If String.IsNullOrEmpty(Me.cmb_retenido.Text) Then
            MessageBox.Show("Falta Datos del Retenido", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.cmb_retenido.Focus()
            Return False
        End If

        If String.IsNullOrEmpty(Me.dpt_fecha.Text) Then
            MessageBox.Show("Falta Datos de la Fecha", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.dpt_fecha.Focus()
            Return False
        End If


        Dim fecha1 As New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
        Dim fecha2 As New DateTime
        fecha1 = New DateTime(Date.Now.Year, Date.Now.Month, 1)
        fecha2 = fecha1.AddMonths(1).AddDays(-1)


        If CDate(Me.dpt_fecha.EditValue) > fecha1 And fecha2 < CDate(Me.dpt_fecha.EditValue) Then

            MessageBox.Show("Fecha : " & Me.dpt_fecha.Text.ToString & " => ( " & fecha1.ToShortDateString & " - " & fecha2.ToShortDateString & " )", "Revisar fecha fuera del periodo", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.dpt_fecha.Focus()

        End If

        If String.IsNullOrEmpty(Me.txt_No_Factura.Text) Then
            Me.txt_No_Factura.Text = "0"
        End If

        If String.IsNullOrEmpty(Me.txt_concepto.Text) Then
            MessageBox.Show("Falta Datos del Concepto", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.txt_concepto.Focus()
            Return False
        End If

        If String.IsNullOrEmpty(Me.cmb_ir.Text) Then
            MessageBox.Show("Falta Datos del IR", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.cmb_ir.Focus()
            Return False
        End If

        If CDec(Me.txt_Importe.Text) = 0.0 Then
            MessageBox.Show("Falta Datos del Importe", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.txt_Importe.Focus()
            Return False
        End If

        ' Validar los Chk para los numeros de Constancias

        If Me.chk_imi.Checked = True Then

            If String.IsNullOrWhiteSpace(Me.txt_IMI_Recibo.Text) Then
                MessageBox.Show("Falta Datos de la Constancia del IMI", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Me.txt_IMI_Recibo.Focus()
                Return False
            End If

            If Accion="New"
            Dim IMI As DataTable
            IMI = Retensiones.GetConstanciaUnique("IMI", Me.txt_IMI_Serie.Text.ToString, Me.txt_IMI_Recibo.Text.ToString)

            If IMI.Rows.Count > 0 Then
                MessageBox.Show("La Constancia IMI # " & Me.txt_IMI_Recibo.Text.ToString & " esta asignado", "Constancia IMI", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return False
                End If
            End If
        End If

        If Me.chk_ir.Checked = True Then
            If String.IsNullOrWhiteSpace(Me.txt_IR_Recibo.Text) Then
                MessageBox.Show("Falta Datos de la Constancia del IR", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Me.txt_IR_Recibo.Focus()
                Return False
            End If

            If Accion = "New" Then
                Dim IR As DataTable
                IR = Retensiones.GetConstanciaUnique("IR", Me.txt_IR_serie.Text.ToString, Me.txt_IR_Recibo.Text.ToString)

                If IR.Rows.Count > 0 Then
                    MessageBox.Show("La Constancia IR # " & Me.txt_IR_Recibo.Text.ToString & " esta asignado", "Constancia IR", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return False
                End If
            End If
        End If

        Return True
    End Function

    Private Sub chk_ir_EditValueChanged(sender As Object, e As EventArgs) Handles chk_ir.EditValueChanged

        If Not Me.chk_ir.CheckState = CheckState.Checked Then

            Me.txt_IR.Text = "0.00"
            Me.txt_IR_Recibo.Text = String.Empty
            Me.txt_IR_serie.Text = String.Empty
            Me.cmb_ir.EditValue = 11
            Me.txt_por_ir.EditValue = 0.0

        Else
            calcular()
            cargartalonario()
        End If



        If Not String.IsNullOrWhiteSpace(cmb_ir.Text) Then
            If cmb_ir.EditValue = 11 Then
                Me.chk_ir.Checked = False
            End If
        End If

    End Sub

    Private Sub ck_imi_EditValueChanged(sender As Object, e As EventArgs) Handles chk_imi.EditValueChanged
        If Not Me.chk_imi.CheckState = CheckState.Checked Then

            Me.txt_IMI.Text = "0.00"
            Me.txt_IMI_Recibo.Text = String.Empty
            Me.txt_IMI_Serie.Text = String.Empty

        Else
            Me.txt_IMI_Serie.Text = "NE"
            calcular()
            cargartalonario()
        End If
    End Sub

    Sub calcular()

        If chk_automatico.Checked = True Then

            If Me.chk_iva.CheckState = CheckState.Checked Then
                Me.txt_iva.EditValue = Decimal.Round((CDec(Me.txt_Importe.EditValue) * (CDec(Me.txt_por_iva.EditValue) / 100)), 2)
            Else
                Me.txt_iva.EditValue = 0.0
            End If

            If Me.chk_imi.CheckState = CheckState.Checked Then
                Me.txt_IMI.EditValue = Decimal.Round((CDec(Me.txt_Importe.EditValue) * (CDec(Me.txt_por_imi.EditValue) / 100)), 2)
            Else
                Me.txt_IMI.EditValue = 0.0
            End If

            If Me.chk_ir.CheckState = CheckState.Checked Then
                Me.txt_IR.EditValue = Decimal.Round((CDec(Me.txt_Importe.EditValue) * (CDec(Me.txt_por_ir.EditValue) / 100)), 2)
            Else
                Me.txt_IR.EditValue = 0.0
            End If

        Else

            If Me.chk_iva.CheckState = CheckState.Unchecked Then
                Me.txt_iva.EditValue = 0.0
            End If

            If Me.chk_imi.CheckState = CheckState.Unchecked Then
                Me.txt_IMI.EditValue = 0.0
            End If

            If Me.chk_ir.CheckState = CheckState.Unchecked Then
                Me.txt_IR.EditValue = 0.0
            End If

        End If

    End Sub

    Private Sub btn_regresar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_regresar.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Regresar", "Regresar", e.Item.Caption + "|" + e.Item.Tag)
        Me.Close()
    End Sub

    Private Sub chk_iva_EditValueChanged(sender As Object, e As EventArgs) Handles chk_iva.EditValueChanged
        If Not Me.chk_iva.CheckState = CheckState.Checked Then
            Me.txt_iva.Text = "0.00"
        Else
            calcular()
        End If
    End Sub
    Private Sub cmb_ir_EditValueChanged(sender As Object, e As EventArgs) Handles cmb_ir.EditValueChanged
        If Me.cmb_ir.EditValue IsNot Nothing Then
            Dim row As DataRow = Me.cmb_ir.Properties.View.GetDataRow(Me.cmb_ir.Properties.View.FocusedRowHandle)
            If row IsNot Nothing Then

                IRPOR = row.Item("Alicuota")
                Me.txt_por_ir.Text = IRPOR
                If IRPOR = 0 Then
                    Me.chk_ir.Checked = False
                Else
                    Me.chk_ir.Checked = True
                End If

            End If
        End If
        calcular()
    End Sub

    Private Sub cmb_ir_Validated(sender As Object, e As EventArgs) Handles cmb_ir.Validated
        If String.IsNullOrEmpty(Me.cmb_ir.Text) Then
            Exit Sub
        End If

        If Me.cmb_ir.EditValue IsNot Nothing Then
            Dim precio = From premio In IR.AsEnumerable() Where premio.Field(Of Integer)("ID") = Me.cmb_ir.EditValue
            For Each p As Object In precio
                IRPOR = p.item("Alicuota")
            Next
        End If

        Me.txt_por_ir.Text = IRPOR
        calcular()
    End Sub
    Private Sub txt_iva_Validated(sender As Object, e As EventArgs) Handles txt_IMI.Validated, txt_IR.Validated, txt_iva.Validated, chk_imi.Validated, chk_ir.Validated, chk_iva.Validated, txt_Importe.Validated
        calcular()
    End Sub

    Private Sub btn_agregarimagen_Click(sender As Object, e As EventArgs) Handles btn_agregarimagen.Click
        My.Forms.frm_imagen.ID = ID
        My.Forms.frm_imagen.WindowState = FormWindowState.Maximized
        My.Forms.frm_imagen.ShowDialog()
    End Sub

    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles btn_grande.Click
        Me.VGrid_Imagen.OptionsView.Style = DevExpress.XtraGrid.Views.WinExplorer.WinExplorerViewStyle.ExtraLarge
    End Sub

    Private Sub btn_mediano_Click(sender As Object, e As EventArgs) Handles btn_mediano.Click
        Me.VGrid_Imagen.OptionsView.Style = DevExpress.XtraGrid.Views.WinExplorer.WinExplorerViewStyle.Large
    End Sub

    Private Sub btn_lista_Click(sender As Object, e As EventArgs) Handles btn_lista.Click
        Me.VGrid_Imagen.OptionsView.Style = DevExpress.XtraGrid.Views.WinExplorer.WinExplorerViewStyle.List
    End Sub

    Private Sub chk_automatico_CheckedChanged(sender As Object, e As EventArgs) Handles chk_automatico.CheckedChanged
        calcular()
    End Sub

    Private Sub txt_No_Factura_EditValueChanged(sender As Object, e As EventArgs) Handles txt_No_Factura.EditValueChanged

    End Sub

    Private Sub txt_No_Factura_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txt_No_Factura.KeyPress
        If Char.IsControl(e.KeyChar) Then
            e.Handled = False
            If Asc(e.KeyChar) = 13 Then

                If String.IsNullOrEmpty(Me.txt_No_Factura.Text) Then
                    Me.txt_No_Factura.Text = "0"
                End If

            End If
        End If
    End Sub
    Private Sub cmb_oficina_EditValueChanged(sender As Object, e As EventArgs) Handles cmb_oficina.EditValueChanged

        If cmb_oficina.EditValue = "1" Then
            Me.panel_chk.Enabled = True
            Me.panel_banco.Enabled = True
            Me.panel_obs.Enabled = True

        Else
            Me.panel_chk.Enabled = False
            Me.panel_banco.Enabled = False
            Me.panel_obs.Enabled = False
        End If

        cargartalonario()

    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        OpenFile.Title = "Selecionar el anexo de la factura"
        OpenFile.Filter = "PDF|*.pdf"
        If OpenFile.ShowDialog() = Windows.Forms.DialogResult.OK Then

            Me.txt_filename.Text = System.IO.Path.GetFileName(OpenFile.FileName)
          
        End If

    End Sub

    Private Sub txt_IMI_EditValueChanged(sender As Object, e As EventArgs) Handles txt_IMI.EditValueChanged

    End Sub

    Private Sub chk_imi_CheckedChanged(sender As Object, e As EventArgs) Handles chk_imi.CheckedChanged

    End Sub
End Class