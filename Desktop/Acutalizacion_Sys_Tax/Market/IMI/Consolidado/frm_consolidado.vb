﻿Public Class frm_consolidado 
    Public fecha1 As New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
    Public fecha2 As New DateTime
    Dim A As New Auditoria
    Private Sub frm_consolidado_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadata()
    End Sub
    Public Sub loadata()

        fecha1 = New DateTime(Date.Now.Year, Date.Now.Month, 1)
        fecha2 = fecha1.AddMonths(1).AddDays(-1)

        Me.lbl_periodo.Caption = Me.fecha1.ToShortDateString + " - " + fecha2.ToShortDateString
        Me.Pivot.DataSource = Retensiones.GetListRetencionImpuestosTOTAL(fecha1.ToShortDateString, fecha2.ToShortDateString)

    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Buscar", "Filtros Busqueda", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.frm_filtros.dtp_fechadesde.EditValue = CDate(fecha1)
        My.Forms.frm_filtros.dtp_fechahasta.EditValue = CDate(fecha2)

        My.Forms.frm_filtros.StartPosition = FormStartPosition.CenterScreen
        My.Forms.frm_filtros.WindowState = FormWindowState.Normal
        If My.Forms.frm_filtros.ShowDialog() = Windows.Forms.DialogResult.Yes Then
            Me.lbl_periodo.Caption = CDate(My.Forms.frm_filtros.dtp_fechadesde.EditValue).ToShortDateString + " - " + CDate(My.Forms.frm_filtros.dtp_fechahasta.EditValue).ToShortDateString
            Me.Pivot.DataSource = Retensiones.GetListRetencionImpuestosTOTAL(CDate(My.Forms.frm_filtros.dtp_fechadesde.EditValue).ToShortDateString, CDate(My.Forms.frm_filtros.dtp_fechahasta.EditValue).ToShortDateString)
        End If


    End Sub

    Private Sub BarButtonItem3_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem3.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Regresar", "Regresando", e.Item.Caption + "|" + e.Item.Tag)
        Me.Close()
    End Sub

    Private Sub BarButtonItem2_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem2.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Exportar", "Exportar", e.Item.Caption + "|" + e.Item.Tag)
        If MessageBox.Show("Exportar Listado General", "Aviso Importante", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim f As New frmExportarImprimir
            f.Mostrar(Me.Pivot)
        End If
    End Sub
End Class