﻿Public Class Frm_P_Agente
    Dim A As New Auditoria

    Private Sub Frm_Banco_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Cargar()
    End Sub

    Public Function Cargar()
        Me.GridBanco.DataSource = Oficinas.GetlistOficinas("%")
        Me.GridViewBanco.BestFitColumns()

    End Function

    Private Sub btn_nuevo_Click(sender As Object, e As EventArgs) Handles btn_nuevo.Click
        A.InsertarAuditoria(Usuario_, "Click en nuevo", "Formulario Oficinas", sender.text + "|" + sender.tag)
        My.Forms.Frm_IU_Oficina.Accion = "NEW"
        My.Forms.Frm_IU_Oficina.MdiParent = Me.MdiParent
        My.Forms.Frm_IU_Oficina.Show()


    End Sub

    Private Sub btn_editar_Click(sender As Object, e As EventArgs) Handles btn_editar.Click
        A.InsertarAuditoria(Usuario_, "Click en editar", "formulario Oficinas", sender.text + "|" + sender.tag)
        If MessageBox.Show(Me.GridViewBanco.GetRowCellValue(Me.GridViewBanco.FocusedRowHandle, "Oficina"), "Aviso Importante", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        If Me.GridViewBanco.GetRowCellValue(Me.GridViewBanco.FocusedRowHandle, "OficinaID") <> Nothing Then

            If Me.GridViewBanco.RowCount > 0 Then

                My.Forms.Frm_IU_Oficina.Accion = "EDIT"
                My.Forms.Frm_IU_Oficina.ID = Me.GridViewBanco.GetRowCellValue(Me.GridViewBanco.FocusedRowHandle, "OficinaID")
                My.Forms.Frm_IU_Oficina.MdiParent = Me.MdiParent
                My.Forms.Frm_IU_Oficina.Show()

            End If
        End If

    End Sub

    Private Sub btn_exportar_Click(sender As Object, e As EventArgs) Handles btn_exportar.Click
        A.InsertarAuditoria(Usuario_, "Click en exportar", "Formulario Oficinas", sender.text + "|" + sender.tag)
        Dim f As New frmExportarImprimir
        f.Mostrar(Me.GridViewBanco)
    End Sub

    'Private Sub btn_eliminar_Click(sender As Object, e As EventArgs) Handles btn_eliminar.Click


    '    If Me.GridViewBanco.GetRowCellValue(Me.GridViewBanco.FocusedRowHandle, "BancoID") <> Nothing Then

    '        If Me.GridViewBanco.RowCount > 0 Then
    '            If MessageBox.Show("¿ Seguro Eliminar el Banco ?", "Aviso Importante", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
    '                Banco.DeleteBanco(Me.GridViewBanco.GetRowCellValue(Me.GridViewBanco.FocusedRowHandle, "BancoID"), "DELETE")
    '                Cargar()
    '            End If
    '        End If
    '    End If

    'End Sub

    Private Sub btn_regresar_Click(sender As Object, e As EventArgs) Handles btn_regresar.Click
        A.InsertarAuditoria(Usuario_, "Click en regresar", "Formulario Oficinas", sender.text + "|" + sender.tag)
        Me.Close()
    End Sub


    Private Sub GridBanco_Click_1(sender As Object, e As EventArgs) Handles GridBanco.Click

    End Sub

    Private Sub GridBanco_DoubleClick(sender As Object, e As EventArgs) Handles GridBanco.DoubleClick

    End Sub

    Private Sub btn_eliminar_Click(sender As Object, e As EventArgs) Handles btn_eliminar.Click

    End Sub
End Class