﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class Usuarios

    Public Shared Function UsuarioGetRol(UsuarioID As String, Contraseña As String, Origen As String) As DataTable
        Dim dt As New DataTable

        Try
            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("UsuarioGetRol", conexion)
            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("usuario", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("usuario").Value = UsuarioID.ToUpper

            cmd.SelectCommand.Parameters.Add("clave", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("clave").Value = Contraseña.ToUpper

            cmd.SelectCommand.Parameters.Add("Origen", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("Origen").Value = Origen

            cmd.Fill(dt)
            Return dt

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return dt

    End Function


    Public Shared Function UsuarioGetItem(UsuarioID As String, Contraseña As String) As DataTable
        Dim dt As New DataTable

        Try
            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("pro_list_user", conexion)
            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("usuario", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("usuario").Value = UsuarioID.ToUpper

            cmd.SelectCommand.Parameters.Add("clave", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("clave").Value = Contraseña.ToUpper


            cmd.Fill(dt)
            Return dt

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return dt

    End Function

    Public Shared Function UsuarioAdminGetItem(UsuarioID As String, Contraseña As String) As DataTable
        Dim dt As New DataTable

        Try
            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("UsuarioAdminGetItem", conexion)
            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("usuario", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("usuario").Value = UsuarioID.ToUpper

            cmd.SelectCommand.Parameters.Add("clave", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("clave").Value = Contraseña.ToUpper


            cmd.Fill(dt)
            Return dt

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return dt

    End Function
    Public Shared Function UsuarioCajaGroupGetList(UsuarioID As String) As DataTable
        Dim dt As New DataTable

        Try
            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("UsuarioCajaGroupGetList", conexion)
            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("usuario", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("usuario").Value = UsuarioID

            cmd.Fill(dt)
            Return dt

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return dt

    End Function
   
End Class
