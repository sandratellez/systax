Imports System.Data
Imports System.Data.SqlClient
Public Class p_usr
    Dim A As New Auditoria
    Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion)
    Public id_primario As String = String.Empty
    Dim sql_usr As String = "SELECT u.usuario,u.nombres +' ' + u.apellidos razon,u.fecha_ingreso,ru.rol,u.estado FROM usuarios u INNER JOIN rol_user ru ON ru.cod_rol = u.cod_rol"

    Private Sub p_usr_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cargar_datos()

    End Sub
    Public Sub cargar_datos()
        Dim DA_usr As New SqlDataAdapter(sql_usr, conexion)
        Dim DT_usr As New DataTable

        DA_usr.Fill(DT_usr)
        Me.gridData.DataSource = DT_usr


    End Sub
    Private Sub btnnuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnuevo.Click
        A.InsertarAuditoria(Usuario_, "Click en Nuevo", "Formulario de Usuario", sender.text + " | " + sender.tag)

        My.Forms.frm_ausr.MdiParent = Me.MdiParent
        My.Forms.frm_ausr.accion = "New"
        My.Forms.frm_ausr.Show()
        My.Forms.frm_ausr.WindowState = FormWindowState.Maximized
    End Sub
    Private Sub btnmodificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnmodificar.Click
        A.InsertarAuditoria(Usuario_, "Click en Modificar", "Formulario de Usuario", sender.text + " | " + sender.tag)

        If Me.v_fabrica.RowCount > 0 Then
            My.Forms.frm_ausr.MdiParent = Me.MdiParent
            My.Forms.frm_ausr.id = Me.v_fabrica.GetRowCellValue(Me.v_fabrica.FocusedRowHandle, "usuario")
            My.Forms.frm_ausr.accion = "Edit"
            My.Forms.frm_ausr.Show()
            My.Forms.frm_ausr.WindowState = FormWindowState.Maximized

        End If
    End Sub
    Private Sub btneliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btneliminar.Click

        If Me.v_fabrica.RowCount > 0 Then
            'If StrComp(Me.v_fabrica.GetRowCellValue(Me.v_fabrica.FocusedRowHandle, "usuario"), "FEXCHE") <> 0 Then
            Dim msn As String = "¿Esta Seguro que desea Eliminar a " + Me.v_fabrica.GetRowCellValue(Me.v_fabrica.FocusedRowHandle, "usuario") + " ?"
            Dim rsp As Integer = MessageBox.Show(msn, "Eliminar usuario...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
            If rsp = 6 Then
                conexion.Open()

                ' Eliminar accesos del Usuario
                'Dim Usuario As String = "Delete from usuario_permiso where Usuario_Permiso.usuario=@usr"
                'Dim Da_usuario As New SqlDataAdapter(Usuario, conexion)

                'Da_usuario.SelectCommand.Parameters.Add("usr", SqlDbType.NVarChar)
                'Da_usuario.SelectCommand.Parameters("usr").Value = Me.v_fabrica.GetRowCellValue(Me.v_fabrica.FocusedRowHandle, "usuario")
                'Da_usuario.SelectCommand.ExecuteNonQuery()

                ' Eliminar Usuario en la tabla usuario
                Dim delete As String = "Delete from usuarios where usuarios.usuario=@usuario"
                Dim Da_delete As New SqlDataAdapter(delete, conexion)

                Da_delete.SelectCommand.Parameters.Add("usuario", SqlDbType.NVarChar)
                Da_delete.SelectCommand.Parameters("usuario").Value = Me.v_fabrica.GetRowCellValue(Me.v_fabrica.FocusedRowHandle, "usuario")

                Da_delete.SelectCommand.ExecuteNonQuery()
                A.InsertarAuditoria(Usuario_, "Click en Eliminar Usuario", "Formulario Ingresar/Modificar Usuario", " Elimino el Usuario: " + Me.v_fabrica.GetRowCellValue(Me.v_fabrica.FocusedRowHandle, "usuario") + " | " + btneliminar.Tag)
                conexion.Close()
                cargar_datos()
            End If
        Else
            MessageBox.Show("No se Puede Eliminar el Usuario por Defecto...", "Atenci�n Importante...", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        '   End If
    End Sub
    Private Sub btnregresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnregresar.Click
        Me.Close()
    End Sub
End Class