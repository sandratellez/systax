<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class p_usr
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(p_usr))
        Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.btnnuevo = New System.Windows.Forms.ToolStripButton()
        Me.btnmodificar = New System.Windows.Forms.ToolStripButton()
        Me.btneliminar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnregresar = New System.Windows.Forms.ToolStripButton()
        Me.gridData = New DevExpress.XtraGrid.GridControl()
        Me.v_fabrica = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.vProductores = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.gridData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.v_fabrica, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.vProductores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridColumn11
        '
        Me.GridColumn11.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn11.AppearanceCell.Options.UseFont = True
        Me.GridColumn11.Caption = "Codigo"
        Me.GridColumn11.FieldName = "usuario"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 0
        Me.GridColumn11.Width = 104
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnnuevo, Me.btnmodificar, Me.btneliminar, Me.ToolStripSeparator3, Me.ToolStripSeparator2, Me.ToolStripSeparator1, Me.btnregresar})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1089, 40)
        Me.ToolStrip1.TabIndex = 158
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'btnnuevo
        '
        Me.btnnuevo.AutoSize = False
        Me.btnnuevo.Image = CType(resources.GetObject("btnnuevo.Image"), System.Drawing.Image)
        Me.btnnuevo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.btnnuevo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnnuevo.Name = "btnnuevo"
        Me.btnnuevo.Size = New System.Drawing.Size(120, 38)
        Me.btnnuevo.Tag = "Creacion de un Nuevo Usuario"
        Me.btnnuevo.Text = "&Nuevo"
        '
        'btnmodificar
        '
        Me.btnmodificar.AutoSize = False
        Me.btnmodificar.Image = CType(resources.GetObject("btnmodificar.Image"), System.Drawing.Image)
        Me.btnmodificar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.btnmodificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnmodificar.Name = "btnmodificar"
        Me.btnmodificar.Size = New System.Drawing.Size(120, 38)
        Me.btnmodificar.Tag = "Modificar el Usuario"
        Me.btnmodificar.Text = "&Modificar"
        '
        'btneliminar
        '
        Me.btneliminar.AutoSize = False
        Me.btneliminar.Image = CType(resources.GetObject("btneliminar.Image"), System.Drawing.Image)
        Me.btneliminar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.btneliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btneliminar.Name = "btneliminar"
        Me.btneliminar.Size = New System.Drawing.Size(120, 38)
        Me.btneliminar.Tag = "Boton Eliminar"
        Me.btneliminar.Text = "&Eliminar"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 40)
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 40)
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 40)
        '
        'btnregresar
        '
        Me.btnregresar.AutoSize = False
        Me.btnregresar.Image = CType(resources.GetObject("btnregresar.Image"), System.Drawing.Image)
        Me.btnregresar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.btnregresar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnregresar.Name = "btnregresar"
        Me.btnregresar.Size = New System.Drawing.Size(120, 38)
        Me.btnregresar.Text = "&Regresar"
        '
        'gridData
        '
        Me.gridData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gridData.Location = New System.Drawing.Point(1, 43)
        Me.gridData.LookAndFeel.SkinName = "Black"
        Me.gridData.MainView = Me.v_fabrica
        Me.gridData.Name = "gridData"
        Me.gridData.Size = New System.Drawing.Size(1088, 648)
        Me.gridData.TabIndex = 376
        Me.gridData.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.v_fabrica, Me.vProductores})
        '
        'v_fabrica
        '
        Me.v_fabrica.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_fabrica.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_fabrica.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.v_fabrica.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.v_fabrica.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.v_fabrica.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.v_fabrica.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(154, Byte), Integer), CType(CType(190, Byte), Integer), CType(CType(243, Byte), Integer))
        Me.v_fabrica.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.v_fabrica.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_fabrica.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.v_fabrica.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.v_fabrica.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.v_fabrica.Appearance.Empty.BackColor = System.Drawing.Color.White
        Me.v_fabrica.Appearance.Empty.Options.UseBackColor = True
        Me.v_fabrica.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.EvenRow.Options.UseBackColor = True
        Me.v_fabrica.Appearance.EvenRow.Options.UseForeColor = True
        Me.v_fabrica.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_fabrica.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_fabrica.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.v_fabrica.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.v_fabrica.Appearance.FilterCloseButton.Options.UseForeColor = True
        Me.v_fabrica.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(62, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.v_fabrica.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White
        Me.v_fabrica.Appearance.FilterPanel.Options.UseBackColor = True
        Me.v_fabrica.Appearance.FilterPanel.Options.UseForeColor = True
        Me.v_fabrica.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.v_fabrica.Appearance.FixedLine.Options.UseBackColor = True
        Me.v_fabrica.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
        Me.v_fabrica.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.FocusedCell.Options.UseBackColor = True
        Me.v_fabrica.Appearance.FocusedCell.Options.UseForeColor = True
        Me.v_fabrica.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(197, Byte), Integer))
        Me.v_fabrica.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.v_fabrica.Appearance.FocusedRow.Options.UseBackColor = True
        Me.v_fabrica.Appearance.FocusedRow.Options.UseForeColor = True
        Me.v_fabrica.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_fabrica.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_fabrica.Appearance.FooterPanel.Options.UseBackColor = True
        Me.v_fabrica.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.v_fabrica.Appearance.FooterPanel.Options.UseForeColor = True
        Me.v_fabrica.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_fabrica.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_fabrica.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.GroupButton.Options.UseBackColor = True
        Me.v_fabrica.Appearance.GroupButton.Options.UseBorderColor = True
        Me.v_fabrica.Appearance.GroupButton.Options.UseForeColor = True
        Me.v_fabrica.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_fabrica.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_fabrica.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.GroupFooter.Options.UseBackColor = True
        Me.v_fabrica.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.v_fabrica.Appearance.GroupFooter.Options.UseForeColor = True
        Me.v_fabrica.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(62, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.v_fabrica.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.GroupPanel.Options.UseBackColor = True
        Me.v_fabrica.Appearance.GroupPanel.Options.UseForeColor = True
        Me.v_fabrica.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_fabrica.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_fabrica.Appearance.GroupRow.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.v_fabrica.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.GroupRow.Options.UseBackColor = True
        Me.v_fabrica.Appearance.GroupRow.Options.UseBorderColor = True
        Me.v_fabrica.Appearance.GroupRow.Options.UseFont = True
        Me.v_fabrica.Appearance.GroupRow.Options.UseForeColor = True
        Me.v_fabrica.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_fabrica.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_fabrica.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.v_fabrica.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.v_fabrica.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.v_fabrica.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(106, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_fabrica.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.v_fabrica.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.v_fabrica.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.v_fabrica.Appearance.HorzLine.BackColor = System.Drawing.Color.Gainsboro
        Me.v_fabrica.Appearance.HorzLine.Options.UseBackColor = True
        Me.v_fabrica.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.v_fabrica.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.OddRow.Options.UseBackColor = True
        Me.v_fabrica.Appearance.OddRow.Options.UseForeColor = True
        Me.v_fabrica.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.v_fabrica.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.v_fabrica.Appearance.Preview.Options.UseBackColor = True
        Me.v_fabrica.Appearance.Preview.Options.UseForeColor = True
        Me.v_fabrica.Appearance.Row.BackColor = System.Drawing.Color.White
        Me.v_fabrica.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.Row.Options.UseBackColor = True
        Me.v_fabrica.Appearance.Row.Options.UseForeColor = True
        Me.v_fabrica.Appearance.RowSeparator.BackColor = System.Drawing.Color.White
        Me.v_fabrica.Appearance.RowSeparator.Options.UseBackColor = True
        Me.v_fabrica.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(217, Byte), Integer))
        Me.v_fabrica.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
        Me.v_fabrica.Appearance.SelectedRow.Options.UseBackColor = True
        Me.v_fabrica.Appearance.SelectedRow.Options.UseForeColor = True
        Me.v_fabrica.Appearance.VertLine.BackColor = System.Drawing.Color.Gainsboro
        Me.v_fabrica.Appearance.VertLine.Options.UseBackColor = True
        Me.v_fabrica.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn11, Me.GridColumn7, Me.GridColumn13, Me.GridColumn5, Me.GridColumn6})
        StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.Transparent
        StyleFormatCondition1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Strikeout)
        StyleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.DimGray
        StyleFormatCondition1.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        StyleFormatCondition1.Appearance.Options.UseBackColor = True
        StyleFormatCondition1.Appearance.Options.UseFont = True
        StyleFormatCondition1.Appearance.Options.UseForeColor = True
        StyleFormatCondition1.ApplyToRow = True
        StyleFormatCondition1.Column = Me.GridColumn11
        StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal
        StyleFormatCondition1.Value1 = "CIERRE DE CUENTA DEFINITIVO"
        Me.v_fabrica.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
        Me.v_fabrica.GridControl = Me.gridData
        Me.v_fabrica.Name = "v_fabrica"
        Me.v_fabrica.OptionsBehavior.Editable = False
        Me.v_fabrica.OptionsView.ShowAutoFilterRow = True
        Me.v_fabrica.OptionsView.ShowGroupPanel = False
        Me.v_fabrica.OptionsView.ShowIndicator = False
        Me.v_fabrica.RowHeight = 25
        '
        'GridColumn7
        '
        Me.GridColumn7.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn7.AppearanceCell.Options.UseFont = True
        Me.GridColumn7.Caption = "Nombre y Apellidos"
        Me.GridColumn7.FieldName = "razon"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.OptionsColumn.ReadOnly = True
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 1
        Me.GridColumn7.Width = 434
        '
        'GridColumn13
        '
        Me.GridColumn13.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn13.AppearanceCell.Options.UseFont = True
        Me.GridColumn13.Caption = "Rol"
        Me.GridColumn13.FieldName = "rol"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 3
        Me.GridColumn13.Width = 148
        '
        'GridColumn5
        '
        Me.GridColumn5.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn5.AppearanceCell.Options.UseFont = True
        Me.GridColumn5.Caption = "Estado"
        Me.GridColumn5.FieldName = "estado"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 4
        Me.GridColumn5.Width = 128
        '
        'GridColumn6
        '
        Me.GridColumn6.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn6.AppearanceCell.Options.UseFont = True
        Me.GridColumn6.Caption = "Creado"
        Me.GridColumn6.FieldName = "fecha_ingreso"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 2
        Me.GridColumn6.Width = 132
        '
        'vProductores
        '
        Me.vProductores.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray
        Me.vProductores.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray
        Me.vProductores.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray
        Me.vProductores.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.vProductores.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.vProductores.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.vProductores.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray
        Me.vProductores.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray
        Me.vProductores.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro
        Me.vProductores.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.vProductores.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.vProductores.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.vProductores.Appearance.Empty.BackColor = System.Drawing.Color.DimGray
        Me.vProductores.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal
        Me.vProductores.Appearance.Empty.Options.UseBackColor = True
        Me.vProductores.Appearance.EvenRow.BackColor = System.Drawing.Color.White
        Me.vProductores.Appearance.EvenRow.Options.UseBackColor = True
        Me.vProductores.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray
        Me.vProductores.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray
        Me.vProductores.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.vProductores.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.vProductores.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray
        Me.vProductores.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
        Me.vProductores.Appearance.FilterPanel.Options.UseBackColor = True
        Me.vProductores.Appearance.FilterPanel.Options.UseForeColor = True
        Me.vProductores.Appearance.FocusedRow.BackColor = System.Drawing.Color.Black
        Me.vProductores.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.vProductores.Appearance.FocusedRow.Options.UseBackColor = True
        Me.vProductores.Appearance.FocusedRow.Options.UseForeColor = True
        Me.vProductores.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray
        Me.vProductores.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray
        Me.vProductores.Appearance.FooterPanel.Options.UseBackColor = True
        Me.vProductores.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.vProductores.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver
        Me.vProductores.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver
        Me.vProductores.Appearance.GroupButton.Options.UseBackColor = True
        Me.vProductores.Appearance.GroupButton.Options.UseBorderColor = True
        Me.vProductores.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver
        Me.vProductores.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver
        Me.vProductores.Appearance.GroupFooter.Options.UseBackColor = True
        Me.vProductores.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.vProductores.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray
        Me.vProductores.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White
        Me.vProductores.Appearance.GroupPanel.Options.UseBackColor = True
        Me.vProductores.Appearance.GroupPanel.Options.UseForeColor = True
        Me.vProductores.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver
        Me.vProductores.Appearance.GroupRow.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.vProductores.Appearance.GroupRow.Options.UseBackColor = True
        Me.vProductores.Appearance.GroupRow.Options.UseFont = True
        Me.vProductores.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray
        Me.vProductores.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray
        Me.vProductores.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.vProductores.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.vProductores.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray
        Me.vProductores.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.vProductores.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray
        Me.vProductores.Appearance.HorzLine.Options.UseBackColor = True
        Me.vProductores.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke
        Me.vProductores.Appearance.OddRow.Options.UseBackColor = True
        Me.vProductores.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro
        Me.vProductores.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray
        Me.vProductores.Appearance.Preview.Options.UseBackColor = True
        Me.vProductores.Appearance.Preview.Options.UseForeColor = True
        Me.vProductores.Appearance.Row.BackColor = System.Drawing.Color.White
        Me.vProductores.Appearance.Row.Options.UseBackColor = True
        Me.vProductores.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray
        Me.vProductores.Appearance.RowSeparator.Options.UseBackColor = True
        Me.vProductores.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray
        Me.vProductores.Appearance.SelectedRow.Options.UseBackColor = True
        Me.vProductores.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray
        Me.vProductores.Appearance.VertLine.Options.UseBackColor = True
        Me.vProductores.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4})
        Me.vProductores.GridControl = Me.gridData
        Me.vProductores.Name = "vProductores"
        Me.vProductores.OptionsView.EnableAppearanceEvenRow = True
        Me.vProductores.OptionsView.EnableAppearanceOddRow = True
        Me.vProductores.OptionsView.ShowAutoFilterRow = True
        Me.vProductores.OptionsView.ShowGroupPanel = False
        Me.vProductores.PaintStyleName = "UltraFlat"
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "No"
        Me.GridColumn1.FieldName = "Nucleo_No"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Nombre"
        Me.GridColumn2.FieldName = "Nucleo_Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Dirección"
        Me.GridColumn3.FieldName = "Nucleo_Direccion"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "GridColumn4"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 3
        '
        'p_usr
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1089, 693)
        Me.ControlBox = False
        Me.Controls.Add(Me.gridData)
        Me.Controls.Add(Me.ToolStrip1)
        Me.DoubleBuffered = True
        Me.Name = "p_usr"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Listada de Usuarios"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.gridData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.v_fabrica, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.vProductores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents btnnuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnmodificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btneliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnregresar As System.Windows.Forms.ToolStripButton
    Friend WithEvents gridData As DevExpress.XtraGrid.GridControl
    Friend WithEvents v_fabrica As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents vProductores As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
End Class
