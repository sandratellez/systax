﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Media
Imports DevExpress.XtraReports
Imports DevExpress.XtraReports.UI
Imports System.IO

Public Class Frm_IU_Proveedor
    Public Accion As String
    Public ID As String
    Public ProveedorImage As New DataTable("Proveedor")
    Dim A As New Auditoria
    Private Sub Frm_IUBanco_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CreateTable()

        If Accion = "NEW" Then
            Me.Text = "Registrar Retenido"
            Me.Txt_Retenido.Focus()
        End If
        If Accion = "EDIT" Then
            Me.Text = "Editar Retenido"
            cargar()
            Me.Txt_Retenido.Focus()
        End If

    End Sub
    Private Sub CreateTable()
        ' Add Two columns to the table.
        ' Table Empresa_Add Contiene las Empresas Asignadas
        ProveedorImage.Columns.Add("ProveedorID", GetType([String]))
        ProveedorImage.Columns.Add("TipoID", GetType(Integer))
        ProveedorImage.Columns.Add("Documento", GetType([String]))
        ProveedorImage.Columns.Add("Imagen", GetType(Image))

        Dim keys(0) As DataColumn
        keys(0) = ProveedorImage.Columns("TipoID")
        ProveedorImage.PrimaryKey = keys

        'Dim keys(1) As DataColumn
        'keys(0) = ProveedorImage.Columns("ProveedorID")
        'keys(1) = ProveedorImage.Columns("TipoID")
        'ProveedorImage.PrimaryKey = keys



        Me.Grid_Imagenes.DataSource = ProveedorImage

    End Sub



    Sub cargar()
        Try


            Dim Agent As DataTable

            Agent = Proveedor.GetList(ID)
            If Agent.Rows.Count > 0 Then

                Me.TxtID.EditValue = Agent.Rows(0).Item("ProveedorID").ToString
                Me.Txt_Retenido.EditValue = Agent.Rows(0).Item("Proveedor").ToString
                Me.Txt_Ruc.EditValue = Agent.Rows(0).Item("Ruc").ToString
                Me.txt_celular.EditValue = Agent.Rows(0).Item("Telefono").ToString
                Me.txt_cedula.EditValue = Agent.Rows(0).Item("Cedula").ToString
                Me.chk_Activo.EditValue = Agent.Rows(0).Item("Activo")
                Me.chk_iva.EditValue = Agent.Rows(0).Item("iva")
                Me.chk_ir.EditValue = Agent.Rows(0).Item("ir")
                Me.chk_imi.EditValue = Agent.Rows(0).Item("imi")

                Dim dt As New DataTable
                dt = Proveedor.GetlistProveedorImagen(ID)

                For i = 0 To dt.Rows.Count - 1

                    Dim row_ As DataRow
                    row_ = ProveedorImage.NewRow

                    row_.Item("ProveedorID") = dt.Rows(i).Item("ProveedorID")
                    row_.Item("TipoID") = dt.Rows(i).Item("TipoID")
                    row_.Item("Documento") = dt.Rows(i).Item("Documento")
                    row_.Item("Imagen") = byteArrayToImage(dt.Rows(i).Item("Imagen"))

                    ProveedorImage.Rows.Add(row_)
                    Me.Grid_Imagenes.DataSource = ProveedorImage

                Next

                Me.Grid_Imagenes.DataSource = ProveedorImage

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Aviso Importante")
        End Try


    End Sub
    Public Function byteArrayToImage(ByVal byteArrayIn As Byte()) As Image
        Dim ms As MemoryStream = New MemoryStream(byteArrayIn)
        Dim returnImage As Image = Image.FromStream(ms)
        Return returnImage
    End Function

    Function validar()

        If String.IsNullOrEmpty(Me.Txt_Retenido.Text) Then
            MessageBox.Show("Registrar Razon Social", "Aviso Importante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.Txt_Retenido.Focus()
            Return False
        End If

        'If String.IsNullOrEmpty(Me.txt_cedula.Text) Or String.IsNullOrEmpty(Me.Txt_Ruc.Text) Then

        '    MessageBox.Show("Registrar Cedula o RUC", "Aviso Importante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    Me.txt_cedula.Focus()
        '    Return False

        'End If

        If (Me.txt_cedula.Text).Length < 14 Then
            MessageBox.Show("Cedula y Ruc deben de contener 14 digitos!", "Aviso Importante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.txt_cedula.Focus()
            Return False
        End If

        If (Me.Txt_Ruc.Text).Length < 14 Then
            MessageBox.Show("Cedula y Ruc deben de contener 14 digitos!", "Aviso Importante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.Txt_Ruc.Focus()
            Return False
        End If



        Return True

    End Function

    Sub limpiar()

        Me.TxtID.EditValue = String.Empty
        Me.Txt_Retenido.EditValue = String.Empty
        Me.Txt_Ruc.EditValue = String.Empty
        Me.txt_cedula.EditValue = String.Empty
        Me.txt_celular.EditValue = String.Empty
        Me.Txt_Retenido.Focus()

    End Sub

    Private Sub BarButtonItem20_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem20.ItemClick
        Me.Close()
    End Sub

    Private Sub btn_guardar_ItemClick_1(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_guardar1.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Guardar", "Listado Proveedor", e.Item.Caption + "|" + e.Item.Tag)
        If validar() = True Then

            If Accion = "NEW" Then
                If MessageBox.Show("¿ Registrar ?", "Aviso Importante", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

                    Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion)
                    Dim Transaccion As SqlTransaction
                    Try
                        conexion.Open()
                        Transaccion = conexion.BeginTransaction
                        Dim ProveedorID As Integer

                        Proveedor.IU(conexion, Transaccion, ProveedorID, Txt_Retenido.Text, Me.Txt_Ruc.Text, Me.txt_celular.Text, Me.txt_cedula.Text, "New", Me.chk_Activo.EditValue, Me.chk_iva.EditValue, Me.chk_ir.EditValue, Me.chk_imi.EditValue)
                        Proveedor.IUDetalles(conexion, Transaccion, ProveedorID, ProveedorImage)

                        Transaccion.Commit()
                        conexion.Close()

                        My.Forms.Frm_IU_Retencion.cmb_retenido.Text = Me.Txt_Retenido.Text
                        limpiar()
                        My.Forms.Frm_P_Proveedor.Cargar()
                        Me.Close()
                    Catch ex As Exception
                        Transaccion.Rollback()
                        conexion.Close()
                        MessageBox.Show(ex.Message, "Aviso Importante")
                    End Try
                   

                End If
            End If

            If Accion = "EDIT" Then
                If MessageBox.Show("¿ Actualizar ?", "Aviso Importante", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion)
                    Dim Transaccion As SqlTransaction

                    Try
                       
                        conexion.Open()
                        Transaccion = conexion.BeginTransaction

                        Proveedor.Edit_Proveedor(conexion, Transaccion, ID, Txt_Retenido.Text, Me.Txt_Ruc.Text, Me.txt_celular.Text, Me.txt_cedula.Text, "Edit", Me.chk_Activo.EditValue, Me.chk_iva.EditValue, Me.chk_ir.EditValue, Me.chk_imi.EditValue)
                        Proveedor.IUDetalles(conexion, Transaccion, ID, ProveedorImage)

                        Transaccion.Commit()
                        conexion.Close()

                        limpiar()
                        My.Forms.Frm_P_Proveedor.Cargar()
                        Me.Close()

                    Catch ex As Exception
                        Transaccion.Rollback()
                        conexion.Close()

                        MessageBox.Show(ex.Message, "Aviso Importante")
                    End Try


                    

                End If
            End If
        End If
    End Sub

    Private Sub BarButtonItem2_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem2.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Regresar", "Listado Proveedor", e.Item.Caption + "|" + e.Item.Tag)
        Me.Close()
    End Sub

    Private Sub Txt_Ruc_EditValueChanged(sender As Object, e As EventArgs) Handles Txt_Ruc.EditValueChanged

        Me.lbl_ruc.Text = Me.Txt_Ruc.Text.ToString.Length
        Me.txt_cedula.Text = Me.Txt_Ruc.Text

    End Sub

    Private Sub txt_cedula_EditValueChanged(sender As Object, e As EventArgs) Handles txt_cedula.EditValueChanged
        Me.lbl_cedula.Text = Me.txt_cedula.Text.ToString.Length
    End Sub

    Private Sub btn_agregar_Click(sender As Object, e As EventArgs) Handles btn_agregar.Click
        A.InsertarAuditoria(Usuario_, "Click Agregar", "Agregar Imagen", sender.text + "|" + sender.Tag)
        My.Forms.frm_imagen.ID = ID
        My.Forms.frm_imagen.WindowState = FormWindowState.Maximized
        My.Forms.frm_imagen.ShowDialog()
    End Sub

    Private Sub btn_ver_Click(sender As Object, e As EventArgs) Handles btn_ver.Click
        A.InsertarAuditoria(Usuario_, "Click Ver", "Vista Imagen", sender.text + "|" + sender.Tag)

        If MessageBox.Show("Vista previa de imagenes", "Aviso Importante", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        My.Forms.frm_ViewPicture.WindowState = FormWindowState.Maximized
        My.Forms.frm_ViewPicture.ShowDialog()

    End Sub
    Private Sub btn_quitar_Click(sender As Object, e As EventArgs) Handles btn_quitar.Click
        A.InsertarAuditoria(Usuario_, "Click Quitar", "Quitar Imagen ", sender.text + "|" + sender.Tag)
        If MessageBox.Show("Eliminar " & Me.VGrid_Imagen.GetRowCellValue(Me.VGrid_Imagen.FocusedRowHandle, "Documento"), "Eliminar Imagen", MessageBoxButtons.YesNo, MessageBoxIcon.Error) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        If Accion = "New" Then

        Else
            Proveedor.DeleteProveedorImage(Me.VGrid_Imagen.GetRowCellValue(Me.VGrid_Imagen.FocusedRowHandle, "ProveedorID"), Me.VGrid_Imagen.GetRowCellValue(Me.VGrid_Imagen.FocusedRowHandle, "TipoID"))
            Me.ProveedorImage.Rows(Me.VGrid_Imagen.GetDataSourceRowIndex(Me.VGrid_Imagen.FocusedRowHandle)).Delete()

            Dim dt As New DataTable
            dt = Proveedor.GetlistProveedorImagen(ID)

            For i = 0 To dt.Rows.Count - 1

                Dim row_ As DataRow
                row_ = ProveedorImage.NewRow

                row_.Item("ProveedorID") = dt.Rows(i).Item("ProveedorID")
                row_.Item("TipoID") = dt.Rows(i).Item("TipoID")
                row_.Item("Documento") = dt.Rows(i).Item("Documento")
                row_.Item("Imagen") = byteArrayToImage(dt.Rows(i).Item("Imagen"))

                ProveedorImage.Rows.Add(row_)
                Me.Grid_Imagenes.DataSource = ProveedorImage

            Next

        End If

    End Sub

    Private Sub BarButtonItem8_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem8.ItemClick

    End Sub
End Class