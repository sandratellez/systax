﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_filtros
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_filtros))
        Me.dtp_fechadesde = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.dtp_fechahasta = New DevExpress.XtraEditors.DateEdit()
        Me.btn_aceptar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_regresar = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmb_oficina = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.chk_all = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.dtp_fechadesde.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtp_fechadesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtp_fechahasta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtp_fechahasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_oficina.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk_all.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dtp_fechadesde
        '
        Me.dtp_fechadesde.EditValue = Nothing
        Me.dtp_fechadesde.Location = New System.Drawing.Point(81, 22)
        Me.dtp_fechadesde.Name = "dtp_fechadesde"
        Me.dtp_fechadesde.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_fechadesde.Properties.Appearance.Options.UseFont = True
        Me.dtp_fechadesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtp_fechadesde.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtp_fechadesde.Size = New System.Drawing.Size(279, 24)
        Me.dtp_fechadesde.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(4, 26)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(73, 16)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Fecha Desde"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(7, 70)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(70, 16)
        Me.LabelControl2.TabIndex = 3
        Me.LabelControl2.Text = "Fecha Hasta"
        '
        'dtp_fechahasta
        '
        Me.dtp_fechahasta.EditValue = Nothing
        Me.dtp_fechahasta.Location = New System.Drawing.Point(81, 65)
        Me.dtp_fechahasta.Name = "dtp_fechahasta"
        Me.dtp_fechahasta.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtp_fechahasta.Properties.Appearance.Options.UseFont = True
        Me.dtp_fechahasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtp_fechahasta.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtp_fechahasta.Size = New System.Drawing.Size(279, 24)
        Me.dtp_fechahasta.TabIndex = 2
        '
        'btn_aceptar
        '
        Me.btn_aceptar.ImageOptions.Image = CType(resources.GetObject("btn_aceptar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_aceptar.Location = New System.Drawing.Point(29, 158)
        Me.btn_aceptar.Name = "btn_aceptar"
        Me.btn_aceptar.Size = New System.Drawing.Size(152, 43)
        Me.btn_aceptar.TabIndex = 4
        Me.btn_aceptar.Tag = "Boton Aceptar"
        Me.btn_aceptar.Text = "Aceptar"
        '
        'btn_regresar
        '
        Me.btn_regresar.ImageOptions.Image = CType(resources.GetObject("btn_regresar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_regresar.Location = New System.Drawing.Point(222, 158)
        Me.btn_regresar.Name = "btn_regresar"
        Me.btn_regresar.Size = New System.Drawing.Size(152, 43)
        Me.btn_regresar.TabIndex = 5
        Me.btn_regresar.Tag = "Boton Regresar"
        Me.btn_regresar.Text = "Regresar"
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(-4, 140)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(418, 8)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        '
        'cmb_oficina
        '
        Me.cmb_oficina.EditValue = ""
        Me.cmb_oficina.Location = New System.Drawing.Point(81, 103)
        Me.cmb_oficina.Name = "cmb_oficina"
        Me.cmb_oficina.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_oficina.Properties.Appearance.Options.UseFont = True
        Me.cmb_oficina.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_oficina.Properties.NullText = ""
        Me.cmb_oficina.Properties.PopupFormSize = New System.Drawing.Size(450, 0)
        Me.cmb_oficina.Properties.View = Me.GridLookUpEdit1View
        Me.cmb_oficina.Size = New System.Drawing.Size(279, 24)
        Me.cmb_oficina.TabIndex = 7
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.Appearance.Row.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridLookUpEdit1View.Appearance.Row.Options.UseFont = True
        Me.GridLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3})
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "OficinaID"
        Me.GridColumn1.FieldName = "OficinaID"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Oficina Comercial"
        Me.GridColumn2.FieldName = "Oficina"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 0
        Me.GridColumn2.Width = 200
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Codigo"
        Me.GridColumn3.FieldName = "Codigo"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 1
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(38, 106)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl3.TabIndex = 8
        Me.LabelControl3.Text = "Oficina"
        '
        'chk_all
        '
        Me.chk_all.Location = New System.Drawing.Point(363, 106)
        Me.chk_all.Name = "chk_all"
        Me.chk_all.Properties.Caption = ""
        Me.chk_all.Size = New System.Drawing.Size(23, 19)
        Me.chk_all.TabIndex = 9
        '
        'frm_filtros
        '
        Me.AcceptButton = Me.btn_aceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(402, 212)
        Me.Controls.Add(Me.chk_all)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.cmb_oficina)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btn_regresar)
        Me.Controls.Add(Me.btn_aceptar)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.dtp_fechahasta)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.dtp_fechadesde)
        Me.Name = "frm_filtros"
        Me.Tag = "Filtros De Busquedas"
        Me.Text = "Filtros de Busquedas"
        CType(Me.dtp_fechadesde.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtp_fechadesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtp_fechahasta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtp_fechahasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_oficina.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk_all.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtp_fechadesde As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dtp_fechahasta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents btn_aceptar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_regresar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmb_oficina As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chk_all As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
End Class
