﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_ViewPicture
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_ViewPicture))
        Me.pic_foto = New DevExpress.XtraEditors.PictureEdit()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.btn_anterior = New DevExpress.XtraBars.BarButtonItem()
        Me.btn_siguiente = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.bar_zoom = New DevExpress.XtraBars.BarEditItem()
        Me.RepositoryItemZoomTrackBar1 = New DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar()
        Me.btn_regresar = New DevExpress.XtraBars.BarButtonItem()
        Me.txt_item = New DevExpress.XtraBars.BarStaticItem()
        CType(Me.pic_foto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemZoomTrackBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pic_foto
        '
        Me.pic_foto.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pic_foto.Location = New System.Drawing.Point(0, 31)
        Me.pic_foto.Name = "pic_foto"
        Me.pic_foto.Properties.AllowScrollViaMouseDrag = True
        Me.pic_foto.Properties.AllowZoomOnMouseWheel = DevExpress.Utils.DefaultBoolean.[True]
        Me.pic_foto.Properties.ShowZoomSubMenu = DevExpress.Utils.DefaultBoolean.[True]
        Me.pic_foto.Properties.ZoomingOperationMode = DevExpress.XtraEditors.Repository.ZoomingOperationMode.MouseWheel
        Me.pic_foto.Size = New System.Drawing.Size(805, 355)
        Me.pic_foto.TabIndex = 37
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btn_anterior, Me.btn_siguiente, Me.bar_zoom, Me.btn_regresar, Me.txt_item})
        Me.BarManager1.MaxItemId = 5
        Me.BarManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemZoomTrackBar1})
        '
        'Bar1
        '
        Me.Bar1.BarName = "Herramientas"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btn_anterior), New DevExpress.XtraBars.LinkPersistInfo(Me.txt_item, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btn_siguiente), New DevExpress.XtraBars.LinkPersistInfo(Me.btn_regresar)})
        Me.Bar1.Text = "Herramientas"
        '
        'btn_anterior
        '
        Me.btn_anterior.Caption = "Anterior"
        Me.btn_anterior.Id = 0
        Me.btn_anterior.Name = "btn_anterior"
        '
        'btn_siguiente
        '
        Me.btn_siguiente.Caption = "Siguiente"
        Me.btn_siguiente.Id = 1
        Me.btn_siguiente.Name = "btn_siguiente"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(805, 31)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 386)
        Me.barDockControlBottom.Size = New System.Drawing.Size(805, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 31)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 355)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(805, 31)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 355)
        '
        'bar_zoom
        '
        Me.bar_zoom.Caption = "BarEditItem1"
        Me.bar_zoom.Edit = Me.RepositoryItemZoomTrackBar1
        Me.bar_zoom.EditWidth = 500
        Me.bar_zoom.Id = 2
        Me.bar_zoom.Name = "bar_zoom"
        '
        'RepositoryItemZoomTrackBar1
        '
        Me.RepositoryItemZoomTrackBar1.Maximum = 500
        Me.RepositoryItemZoomTrackBar1.Middle = 5
        Me.RepositoryItemZoomTrackBar1.Name = "RepositoryItemZoomTrackBar1"
        Me.RepositoryItemZoomTrackBar1.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.ArrowDownRight
        Me.RepositoryItemZoomTrackBar1.SmallChange = 100
        '
        'btn_regresar
        '
        Me.btn_regresar.Caption = "BarButtonItem3"
        Me.btn_regresar.Glyph = CType(resources.GetObject("btn_regresar.Glyph"), System.Drawing.Image)
        Me.btn_regresar.Id = 3
        Me.btn_regresar.LargeGlyph = CType(resources.GetObject("btn_regresar.LargeGlyph"), System.Drawing.Image)
        Me.btn_regresar.Name = "btn_regresar"
        '
        'txt_item
        '
        Me.txt_item.Id = 4
        Me.txt_item.Name = "txt_item"
        Me.txt_item.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'frm_ViewPicture
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(805, 386)
        Me.Controls.Add(Me.pic_foto)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "frm_ViewPicture"
        Me.Text = "Vista Previa"
        CType(Me.pic_foto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemZoomTrackBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pic_foto As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents btn_anterior As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btn_siguiente As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bar_zoom As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemZoomTrackBar1 As DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents btn_regresar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents txt_item As DevExpress.XtraBars.BarStaticItem
End Class
