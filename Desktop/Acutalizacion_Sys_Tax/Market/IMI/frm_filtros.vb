﻿Public Class frm_filtros
    Public OficinaID As String
    Dim A As New Auditoria

    Private Sub btn_aceptar_Click(sender As Object, e As EventArgs) Handles btn_aceptar.Click
        A.InsertarAuditoria(Usuario_, "Click Aceptar", "Filtros De Busqueda", sender.text + "|" + sender.tag)
        Me.DialogResult = Windows.Forms.DialogResult.Yes
    End Sub

    Private Sub btn_regresar_Click(sender As Object, e As EventArgs) Handles btn_regresar.Click
        A.InsertarAuditoria(Usuario_, "Click Regresar", "Regresar ", sender.text + "|" + sender.tag)
        Me.DialogResult = Windows.Forms.DialogResult.No
    End Sub

    Private Sub chk_all_CheckedChanged(sender As Object, e As EventArgs) Handles chk_all.CheckedChanged
        Me.cmb_oficina.Enabled = Not Me.chk_all.Checked
    End Sub

    Private Sub frm_filtros_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If Me.chk_all.Checked = True Then
            OficinaID = "%"
        Else
            OficinaID = Me.cmb_oficina.EditValue
        End If
    End Sub

    Private Sub frm_filtros_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.cmb_oficina.Properties.DataSource = Oficinas.GetlistOficinas("%")
        Me.cmb_oficina.Properties.DisplayMember = "Oficina"
        Me.cmb_oficina.Properties.ValueMember = "OficinaID"

        Me.cmb_oficina.EditValue = 1



    End Sub
End Class