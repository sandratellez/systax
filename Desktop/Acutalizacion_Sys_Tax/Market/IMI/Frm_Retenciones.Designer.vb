﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_Retenciones
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_Retenciones))
        Dim SuperToolTip3 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem3 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem3 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim EditorButtonImageOptions1 As DevExpress.XtraEditors.Controls.EditorButtonImageOptions = New DevExpress.XtraEditors.Controls.EditorButtonImageOptions()
        Dim EditorButtonImageOptions2 As DevExpress.XtraEditors.Controls.EditorButtonImageOptions = New DevExpress.XtraEditors.Controls.EditorButtonImageOptions()
        Dim EditorButtonImageOptions3 As DevExpress.XtraEditors.Controls.EditorButtonImageOptions = New DevExpress.XtraEditors.Controls.EditorButtonImageOptions()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.txt_filename = New DevExpress.XtraEditors.TextEdit()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar2 = New DevExpress.XtraBars.Bar()
        Me.btn_guardar = New DevExpress.XtraBars.BarButtonItem()
        Me.btn_regresar = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.btn_anulado = New DevExpress.XtraBars.BarButtonItem()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.chk_pro_ir = New DevExpress.XtraEditors.CheckEdit()
        Me.chk_pro_imi = New DevExpress.XtraEditors.CheckEdit()
        Me.chk_pro_iva = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.btn_grande = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_mediano = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_lista = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_agregarimagen = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_eliminarimagen = New DevExpress.XtraEditors.SimpleButton()
        Me.Grid_Imagenes = New DevExpress.XtraGrid.GridControl()
        Me.VGrid_Imagen = New DevExpress.XtraGrid.Views.WinExplorer.WinExplorerView()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.cmb_ir = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chk_iva = New DevExpress.XtraEditors.CheckEdit()
        Me.txt_iva = New DevExpress.XtraEditors.TextEdit()
        Me.txt_por_iva = New DevExpress.XtraEditors.TextEdit()
        Me.txt_concepto = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Observaciones = New DevExpress.XtraEditors.TextEdit()
        Me.chk_ir = New DevExpress.XtraEditors.CheckEdit()
        Me.chk_imi = New DevExpress.XtraEditors.CheckEdit()
        Me.chk_automatico = New DevExpress.XtraEditors.CheckEdit()
        Me.cmb_banco = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridLookUpEdit2View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txt_IMI_Serie = New DevExpress.XtraEditors.TextEdit()
        Me.txt_ck = New DevExpress.XtraEditors.TextEdit()
        Me.txt_IR_Recibo = New DevExpress.XtraEditors.TextEdit()
        Me.txt_IMI_Recibo = New DevExpress.XtraEditors.TextEdit()
        Me.txt_IR = New DevExpress.XtraEditors.TextEdit()
        Me.txt_por_ir = New DevExpress.XtraEditors.TextEdit()
        Me.txt_IMI = New DevExpress.XtraEditors.TextEdit()
        Me.dpt_fecha = New DevExpress.XtraEditors.DateEdit()
        Me.txt_por_imi = New DevExpress.XtraEditors.TextEdit()
        Me.txt_Importe = New DevExpress.XtraEditors.TextEdit()
        Me.txt_No_Factura = New DevExpress.XtraEditors.TextEdit()
        Me.txt_telefono = New DevExpress.XtraEditors.TextEdit()
        Me.Txt_Cedula = New DevExpress.XtraEditors.TextEdit()
        Me.Txt_Ruc = New DevExpress.XtraEditors.TextEdit()
        Me.cmb_retenido = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txt_IR_serie = New DevExpress.XtraEditors.TextEdit()
        Me.cmb_oficina = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn17 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn18 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.TabbedControlGroup1 = New DevExpress.XtraLayout.TabbedControlGroup()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup8 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem7 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem34 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.panel_obs = New DevExpress.XtraLayout.LayoutControlItem()
        Me.panel_chk = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem39 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.panel_banco = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem40 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem12 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem10 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup6 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem33 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup7 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem35 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem36 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem37 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup5 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.OpenFile = New System.Windows.Forms.OpenFileDialog()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.txt_filename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk_pro_ir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk_pro_imi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk_pro_iva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Grid_Imagenes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VGrid_Imagen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_ir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk_iva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_iva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_por_iva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_concepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Observaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk_ir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk_imi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chk_automatico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_banco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit2View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IMI_Serie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_ck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IR_Recibo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IMI_Recibo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_por_ir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IMI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpt_fecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpt_fecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_por_imi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Importe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_No_Factura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_telefono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_Cedula.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Txt_Ruc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_retenido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_IR_serie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_oficina.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabbedControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.panel_obs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.panel_chk, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.panel_banco, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.txt_filename)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
        Me.LayoutControl1.Controls.Add(Me.chk_pro_ir)
        Me.LayoutControl1.Controls.Add(Me.chk_pro_imi)
        Me.LayoutControl1.Controls.Add(Me.chk_pro_iva)
        Me.LayoutControl1.Controls.Add(Me.TextEdit1)
        Me.LayoutControl1.Controls.Add(Me.btn_grande)
        Me.LayoutControl1.Controls.Add(Me.btn_mediano)
        Me.LayoutControl1.Controls.Add(Me.btn_lista)
        Me.LayoutControl1.Controls.Add(Me.btn_agregarimagen)
        Me.LayoutControl1.Controls.Add(Me.btn_eliminarimagen)
        Me.LayoutControl1.Controls.Add(Me.Grid_Imagenes)
        Me.LayoutControl1.Controls.Add(Me.cmb_ir)
        Me.LayoutControl1.Controls.Add(Me.chk_iva)
        Me.LayoutControl1.Controls.Add(Me.txt_iva)
        Me.LayoutControl1.Controls.Add(Me.txt_por_iva)
        Me.LayoutControl1.Controls.Add(Me.txt_concepto)
        Me.LayoutControl1.Controls.Add(Me.txt_Observaciones)
        Me.LayoutControl1.Controls.Add(Me.chk_ir)
        Me.LayoutControl1.Controls.Add(Me.chk_imi)
        Me.LayoutControl1.Controls.Add(Me.chk_automatico)
        Me.LayoutControl1.Controls.Add(Me.cmb_banco)
        Me.LayoutControl1.Controls.Add(Me.txt_IMI_Serie)
        Me.LayoutControl1.Controls.Add(Me.txt_ck)
        Me.LayoutControl1.Controls.Add(Me.txt_IR_Recibo)
        Me.LayoutControl1.Controls.Add(Me.txt_IMI_Recibo)
        Me.LayoutControl1.Controls.Add(Me.txt_IR)
        Me.LayoutControl1.Controls.Add(Me.txt_por_ir)
        Me.LayoutControl1.Controls.Add(Me.txt_IMI)
        Me.LayoutControl1.Controls.Add(Me.dpt_fecha)
        Me.LayoutControl1.Controls.Add(Me.txt_por_imi)
        Me.LayoutControl1.Controls.Add(Me.txt_Importe)
        Me.LayoutControl1.Controls.Add(Me.txt_No_Factura)
        Me.LayoutControl1.Controls.Add(Me.txt_telefono)
        Me.LayoutControl1.Controls.Add(Me.Txt_Cedula)
        Me.LayoutControl1.Controls.Add(Me.Txt_Ruc)
        Me.LayoutControl1.Controls.Add(Me.cmb_retenido)
        Me.LayoutControl1.Controls.Add(Me.txt_IR_serie)
        Me.LayoutControl1.Controls.Add(Me.cmb_oficina)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 40)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(3, 256, 453, 350)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1323, 576)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'txt_filename
        '
        Me.txt_filename.EditValue = ""
        Me.txt_filename.Enabled = False
        Me.txt_filename.Location = New System.Drawing.Point(145, 452)
        Me.txt_filename.MenuManager = Me.BarManager1
        Me.txt_filename.Name = "txt_filename"
        Me.txt_filename.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_filename.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.txt_filename.Properties.Appearance.Options.UseFont = True
        Me.txt_filename.Properties.Appearance.Options.UseForeColor = True
        Me.txt_filename.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.txt_filename.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_filename.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Blue
        Me.txt_filename.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_filename.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_filename.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_filename.Properties.ReadOnly = True
        Me.txt_filename.Size = New System.Drawing.Size(633, 26)
        Me.txt_filename.StyleController = Me.LayoutControl1
        Me.txt_filename.TabIndex = 38
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btn_regresar, Me.btn_guardar, Me.btn_anulado})
        Me.BarManager1.MainMenu = Me.Bar2
        Me.BarManager1.MaxItemId = 3
        '
        'Bar2
        '
        Me.Bar2.BarName = "Menú principal"
        Me.Bar2.DockCol = 0
        Me.Bar2.DockRow = 0
        Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btn_guardar), New DevExpress.XtraBars.LinkPersistInfo(Me.btn_regresar)})
        Me.Bar2.OptionsBar.MultiLine = True
        Me.Bar2.OptionsBar.UseWholeRow = True
        Me.Bar2.Text = "Menú principal"
        '
        'btn_guardar
        '
        Me.btn_guardar.Caption = "Registrar Retencion"
        Me.btn_guardar.Id = 1
        Me.btn_guardar.ImageOptions.Image = CType(resources.GetObject("btn_guardar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_guardar.Name = "btn_guardar"
        ToolTipTitleItem3.Appearance.Image = CType(resources.GetObject("resource.Image"), System.Drawing.Image)
        ToolTipTitleItem3.Appearance.Options.UseImage = True
        ToolTipTitleItem3.Image = CType(resources.GetObject("ToolTipTitleItem3.Image"), System.Drawing.Image)
        ToolTipTitleItem3.Text = "Guardar"
        ToolTipItem3.LeftIndent = 6
        ToolTipItem3.Text = "Guardar Retención" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        SuperToolTip3.Items.Add(ToolTipTitleItem3)
        SuperToolTip3.Items.Add(ToolTipItem3)
        Me.btn_guardar.SuperTip = SuperToolTip3
        '
        'btn_regresar
        '
        Me.btn_regresar.Caption = "Regresar"
        Me.btn_regresar.Id = 0
        Me.btn_regresar.ImageOptions.Image = CType(resources.GetObject("btn_regresar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_regresar.Name = "btn_regresar"
        Me.btn_regresar.Tag = "Impuestos & Rentenciones"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Manager = Me.BarManager1
        Me.barDockControlTop.Size = New System.Drawing.Size(1323, 40)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 616)
        Me.barDockControlBottom.Manager = Me.BarManager1
        Me.barDockControlBottom.Size = New System.Drawing.Size(1323, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 40)
        Me.barDockControlLeft.Manager = Me.BarManager1
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 576)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(1323, 40)
        Me.barDockControlRight.Manager = Me.BarManager1
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 576)
        '
        'btn_anulado
        '
        Me.btn_anulado.Caption = "BarButtonItem1"
        Me.btn_anulado.Id = 2
        Me.btn_anulado.ImageOptions.Image = CType(resources.GetObject("btn_anulado.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_anulado.Name = "btn_anulado"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.ImageOptions.Image = CType(resources.GetObject("SimpleButton2.ImageOptions.Image"), System.Drawing.Image)
        Me.SimpleButton2.Location = New System.Drawing.Point(11, 452)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(130, 26)
        Me.SimpleButton2.StyleController = Me.LayoutControl1
        Me.SimpleButton2.TabIndex = 37
        Me.SimpleButton2.Text = "Adjuntar"
        '
        'chk_pro_ir
        '
        Me.chk_pro_ir.Enabled = False
        Me.chk_pro_ir.Location = New System.Drawing.Point(1086, 106)
        Me.chk_pro_ir.MenuManager = Me.BarManager1
        Me.chk_pro_ir.Name = "chk_pro_ir"
        Me.chk_pro_ir.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!)
        Me.chk_pro_ir.Properties.Appearance.Options.UseFont = True
        Me.chk_pro_ir.Properties.Caption = "IR"
        Me.chk_pro_ir.Size = New System.Drawing.Size(42, 22)
        Me.chk_pro_ir.StyleController = Me.LayoutControl1
        Me.chk_pro_ir.TabIndex = 35
        '
        'chk_pro_imi
        '
        Me.chk_pro_imi.Enabled = False
        Me.chk_pro_imi.Location = New System.Drawing.Point(1132, 106)
        Me.chk_pro_imi.MenuManager = Me.BarManager1
        Me.chk_pro_imi.Name = "chk_pro_imi"
        Me.chk_pro_imi.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!)
        Me.chk_pro_imi.Properties.Appearance.Options.UseFont = True
        Me.chk_pro_imi.Properties.Caption = "IMI"
        Me.chk_pro_imi.Size = New System.Drawing.Size(180, 22)
        Me.chk_pro_imi.StyleController = Me.LayoutControl1
        Me.chk_pro_imi.TabIndex = 34
        '
        'chk_pro_iva
        '
        Me.chk_pro_iva.Enabled = False
        Me.chk_pro_iva.Location = New System.Drawing.Point(1031, 106)
        Me.chk_pro_iva.MenuManager = Me.BarManager1
        Me.chk_pro_iva.Name = "chk_pro_iva"
        Me.chk_pro_iva.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!)
        Me.chk_pro_iva.Properties.Appearance.Options.UseFont = True
        Me.chk_pro_iva.Properties.Caption = "IVA"
        Me.chk_pro_iva.Size = New System.Drawing.Size(51, 22)
        Me.chk_pro_iva.StyleController = Me.LayoutControl1
        Me.chk_pro_iva.TabIndex = 33
        '
        'TextEdit1
        '
        Me.TextEdit1.EditValue = "CONSTANCIAS"
        Me.TextEdit1.Enabled = False
        Me.TextEdit1.Location = New System.Drawing.Point(367, 302)
        Me.TextEdit1.MenuManager = Me.BarManager1
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.BackColor = System.Drawing.Color.PaleGoldenrod
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.TextEdit1.Properties.Appearance.Options.UseBackColor = True
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.Appearance.Options.UseForeColor = True
        Me.TextEdit1.Properties.Appearance.Options.UseTextOptions = True
        Me.TextEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.TextEdit1.Size = New System.Drawing.Size(188, 26)
        Me.TextEdit1.StyleController = Me.LayoutControl1
        Me.TextEdit1.TabIndex = 32
        '
        'btn_grande
        '
        Me.btn_grande.ImageOptions.Image = CType(resources.GetObject("btn_grande.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_grande.Location = New System.Drawing.Point(1163, 30)
        Me.btn_grande.Name = "btn_grande"
        Me.btn_grande.Size = New System.Drawing.Size(44, 47)
        Me.btn_grande.StyleController = Me.LayoutControl1
        Me.btn_grande.TabIndex = 31
        '
        'btn_mediano
        '
        Me.btn_mediano.ImageOptions.Image = CType(resources.GetObject("btn_mediano.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_mediano.Location = New System.Drawing.Point(1211, 30)
        Me.btn_mediano.Name = "btn_mediano"
        Me.btn_mediano.Size = New System.Drawing.Size(47, 47)
        Me.btn_mediano.StyleController = Me.LayoutControl1
        Me.btn_mediano.TabIndex = 30
        '
        'btn_lista
        '
        Me.btn_lista.ImageOptions.Image = CType(resources.GetObject("btn_lista.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_lista.Location = New System.Drawing.Point(1262, 30)
        Me.btn_lista.Name = "btn_lista"
        Me.btn_lista.Size = New System.Drawing.Size(56, 47)
        Me.btn_lista.StyleController = Me.LayoutControl1
        Me.btn_lista.TabIndex = 29
        '
        'btn_agregarimagen
        '
        Me.btn_agregarimagen.ImageOptions.Image = CType(resources.GetObject("btn_agregarimagen.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_agregarimagen.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.BottomCenter
        Me.btn_agregarimagen.Location = New System.Drawing.Point(5, 30)
        Me.btn_agregarimagen.Name = "btn_agregarimagen"
        Me.btn_agregarimagen.Size = New System.Drawing.Size(46, 47)
        Me.btn_agregarimagen.StyleController = Me.LayoutControl1
        Me.btn_agregarimagen.TabIndex = 28
        '
        'btn_eliminarimagen
        '
        Me.btn_eliminarimagen.ImageOptions.Image = CType(resources.GetObject("btn_eliminarimagen.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_eliminarimagen.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.BottomCenter
        Me.btn_eliminarimagen.Location = New System.Drawing.Point(55, 30)
        Me.btn_eliminarimagen.Name = "btn_eliminarimagen"
        Me.btn_eliminarimagen.Size = New System.Drawing.Size(46, 47)
        Me.btn_eliminarimagen.StyleController = Me.LayoutControl1
        Me.btn_eliminarimagen.TabIndex = 27
        '
        'Grid_Imagenes
        '
        Me.Grid_Imagenes.Location = New System.Drawing.Point(5, 81)
        Me.Grid_Imagenes.MainView = Me.VGrid_Imagen
        Me.Grid_Imagenes.MenuManager = Me.BarManager1
        Me.Grid_Imagenes.Name = "Grid_Imagenes"
        Me.Grid_Imagenes.Size = New System.Drawing.Size(1313, 490)
        Me.Grid_Imagenes.TabIndex = 25
        Me.Grid_Imagenes.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.VGrid_Imagen})
        '
        'VGrid_Imagen
        '
        Me.VGrid_Imagen.Appearance.ItemDescriptionHovered.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VGrid_Imagen.Appearance.ItemDescriptionHovered.ForeColor = System.Drawing.Color.Red
        Me.VGrid_Imagen.Appearance.ItemDescriptionHovered.Options.UseFont = True
        Me.VGrid_Imagen.Appearance.ItemDescriptionHovered.Options.UseForeColor = True
        Me.VGrid_Imagen.Appearance.ItemDescriptionNormal.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VGrid_Imagen.Appearance.ItemDescriptionNormal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.VGrid_Imagen.Appearance.ItemDescriptionNormal.Options.UseFont = True
        Me.VGrid_Imagen.Appearance.ItemDescriptionNormal.Options.UseForeColor = True
        Me.VGrid_Imagen.Appearance.ItemHovered.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VGrid_Imagen.Appearance.ItemHovered.ForeColor = System.Drawing.Color.Blue
        Me.VGrid_Imagen.Appearance.ItemHovered.Options.UseFont = True
        Me.VGrid_Imagen.Appearance.ItemHovered.Options.UseForeColor = True
        Me.VGrid_Imagen.Appearance.ItemNormal.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VGrid_Imagen.Appearance.ItemNormal.ForeColor = System.Drawing.Color.Blue
        Me.VGrid_Imagen.Appearance.ItemNormal.Options.UseFont = True
        Me.VGrid_Imagen.Appearance.ItemNormal.Options.UseForeColor = True
        Me.VGrid_Imagen.Appearance.ItemPressed.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VGrid_Imagen.Appearance.ItemPressed.ForeColor = System.Drawing.Color.Blue
        Me.VGrid_Imagen.Appearance.ItemPressed.Options.UseFont = True
        Me.VGrid_Imagen.Appearance.ItemPressed.Options.UseForeColor = True
        Me.VGrid_Imagen.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn14, Me.GridColumn15, Me.GridColumn16})
        Me.VGrid_Imagen.ColumnSet.DescriptionColumn = Me.GridColumn16
        Me.VGrid_Imagen.ColumnSet.ExtraLargeImageColumn = Me.GridColumn14
        Me.VGrid_Imagen.ColumnSet.ExtraLargeImageIndexColumn = Me.GridColumn14
        Me.VGrid_Imagen.ColumnSet.LargeImageColumn = Me.GridColumn14
        Me.VGrid_Imagen.ColumnSet.LargeImageIndexColumn = Me.GridColumn14
        Me.VGrid_Imagen.ColumnSet.MediumImageColumn = Me.GridColumn14
        Me.VGrid_Imagen.ColumnSet.MediumImageIndexColumn = Me.GridColumn14
        Me.VGrid_Imagen.ColumnSet.SmallImageColumn = Me.GridColumn14
        Me.VGrid_Imagen.ColumnSet.SmallImageIndexColumn = Me.GridColumn14
        Me.VGrid_Imagen.ColumnSet.TextColumn = Me.GridColumn16
        Me.VGrid_Imagen.GridControl = Me.Grid_Imagenes
        Me.VGrid_Imagen.Name = "VGrid_Imagen"
        Me.VGrid_Imagen.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateAllContent
        Me.VGrid_Imagen.OptionsView.Style = DevExpress.XtraGrid.Views.WinExplorer.WinExplorerViewStyle.ExtraLarge
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "Imagen"
        Me.GridColumn14.FieldName = "Imagen"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 0
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "RetencionID"
        Me.GridColumn15.FieldName = "RetencionID"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = True
        Me.GridColumn15.VisibleIndex = 0
        '
        'GridColumn16
        '
        Me.GridColumn16.Caption = "Nombre"
        Me.GridColumn16.FieldName = "Nombre"
        Me.GridColumn16.Name = "GridColumn16"
        Me.GridColumn16.Visible = True
        Me.GridColumn16.VisibleIndex = 0
        '
        'cmb_ir
        '
        Me.cmb_ir.EditValue = ""
        Me.cmb_ir.EnterMoveNextControl = True
        Me.cmb_ir.Location = New System.Drawing.Point(142, 242)
        Me.cmb_ir.MenuManager = Me.BarManager1
        Me.cmb_ir.Name = "cmb_ir"
        Me.cmb_ir.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_ir.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.cmb_ir.Properties.Appearance.Options.UseFont = True
        Me.cmb_ir.Properties.Appearance.Options.UseForeColor = True
        Me.cmb_ir.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_ir.Properties.NullText = ""
        Me.cmb_ir.Properties.PopupFormMinSize = New System.Drawing.Size(682, 0)
        Me.cmb_ir.Properties.PopupFormSize = New System.Drawing.Size(682, 0)
        Me.cmb_ir.Properties.View = Me.GridView1
        Me.cmb_ir.Size = New System.Drawing.Size(682, 26)
        Me.cmb_ir.StyleController = Me.LayoutControl1
        Me.cmb_ir.TabIndex = 23
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn9, Me.GridColumn10, Me.GridColumn11, Me.GridColumn12, Me.GridColumn13})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.RowHeight = 25
        '
        'GridColumn9
        '
        Me.GridColumn9.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn9.AppearanceCell.Options.UseFont = True
        Me.GridColumn9.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn9.AppearanceHeader.Options.UseFont = True
        Me.GridColumn9.Caption = "ID"
        Me.GridColumn9.FieldName = "ID"
        Me.GridColumn9.Name = "GridColumn9"
        '
        'GridColumn10
        '
        Me.GridColumn10.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn10.AppearanceCell.Options.UseFont = True
        Me.GridColumn10.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn10.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridColumn10.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn10.AppearanceHeader.Options.UseFont = True
        Me.GridColumn10.Caption = "Código Retención"
        Me.GridColumn10.FieldName = "IR_ActID"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 2
        '
        'GridColumn11
        '
        Me.GridColumn11.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn11.AppearanceCell.Options.UseFont = True
        Me.GridColumn11.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn11.AppearanceHeader.Options.UseFont = True
        Me.GridColumn11.Caption = "Codigo Retención"
        Me.GridColumn11.FieldName = "Cod_RetID"
        Me.GridColumn11.Name = "GridColumn11"
        '
        'GridColumn12
        '
        Me.GridColumn12.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn12.AppearanceCell.Options.UseFont = True
        Me.GridColumn12.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn12.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridColumn12.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn12.AppearanceHeader.Options.UseFont = True
        Me.GridColumn12.Caption = "Alicuota"
        Me.GridColumn12.DisplayFormat.FormatString = "{0:n0}"
        Me.GridColumn12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn12.FieldName = "Alicuota"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 1
        '
        'GridColumn13
        '
        Me.GridColumn13.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn13.AppearanceCell.Options.UseFont = True
        Me.GridColumn13.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn13.AppearanceHeader.Options.UseFont = True
        Me.GridColumn13.Caption = "Descripción"
        Me.GridColumn13.FieldName = "Descripcion"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 0
        Me.GridColumn13.Width = 300
        '
        'chk_iva
        '
        Me.chk_iva.EditValue = True
        Me.chk_iva.EnterMoveNextControl = True
        Me.chk_iva.Location = New System.Drawing.Point(344, 302)
        Me.chk_iva.MenuManager = Me.BarManager1
        Me.chk_iva.Name = "chk_iva"
        Me.chk_iva.Properties.Caption = ""
        Me.chk_iva.Size = New System.Drawing.Size(19, 19)
        Me.chk_iva.StyleController = Me.LayoutControl1
        Me.chk_iva.TabIndex = 9
        '
        'txt_iva
        '
        Me.txt_iva.EditValue = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txt_iva.EnterMoveNextControl = True
        Me.txt_iva.Location = New System.Drawing.Point(200, 302)
        Me.txt_iva.MenuManager = Me.BarManager1
        Me.txt_iva.Name = "txt_iva"
        Me.txt_iva.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_iva.Properties.Appearance.ForeColor = System.Drawing.Color.Red
        Me.txt_iva.Properties.Appearance.Options.UseFont = True
        Me.txt_iva.Properties.Appearance.Options.UseForeColor = True
        Me.txt_iva.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_iva.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_iva.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_iva.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_iva.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_iva.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_iva.Properties.Mask.EditMask = "n2"
        Me.txt_iva.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_iva.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_iva.Properties.ValidateOnEnterKey = True
        Me.txt_iva.Size = New System.Drawing.Size(140, 26)
        Me.txt_iva.StyleController = Me.LayoutControl1
        Me.txt_iva.TabIndex = 8
        '
        'txt_por_iva
        '
        Me.txt_por_iva.EditValue = New Decimal(New Integer() {1500, 0, 0, 131072})
        Me.txt_por_iva.EnterMoveNextControl = True
        Me.txt_por_iva.Location = New System.Drawing.Point(142, 302)
        Me.txt_por_iva.MenuManager = Me.BarManager1
        Me.txt_por_iva.Name = "txt_por_iva"
        Me.txt_por_iva.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_por_iva.Properties.Appearance.ForeColor = System.Drawing.Color.Red
        Me.txt_por_iva.Properties.Appearance.Options.UseFont = True
        Me.txt_por_iva.Properties.Appearance.Options.UseForeColor = True
        Me.txt_por_iva.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_por_iva.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_por_iva.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_por_iva.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_por_iva.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Red
        Me.txt_por_iva.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_por_iva.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_por_iva.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.txt_por_iva.Properties.ReadOnly = True
        Me.txt_por_iva.Size = New System.Drawing.Size(54, 26)
        Me.txt_por_iva.StyleController = Me.LayoutControl1
        Me.txt_por_iva.TabIndex = 7
        '
        'txt_concepto
        '
        Me.txt_concepto.EnterMoveNextControl = True
        Me.txt_concepto.Location = New System.Drawing.Point(142, 212)
        Me.txt_concepto.MenuManager = Me.BarManager1
        Me.txt_concepto.Name = "txt_concepto"
        Me.txt_concepto.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_concepto.Properties.Appearance.Options.UseFont = True
        Me.txt_concepto.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_concepto.Size = New System.Drawing.Size(682, 26)
        Me.txt_concepto.StyleController = Me.LayoutControl1
        Me.txt_concepto.TabIndex = 4
        '
        'txt_Observaciones
        '
        Me.txt_Observaciones.Location = New System.Drawing.Point(142, 422)
        Me.txt_Observaciones.MenuManager = Me.BarManager1
        Me.txt_Observaciones.Name = "txt_Observaciones"
        Me.txt_Observaciones.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Observaciones.Properties.Appearance.Options.UseFont = True
        Me.txt_Observaciones.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_Observaciones.Size = New System.Drawing.Size(636, 26)
        Me.txt_Observaciones.StyleController = Me.LayoutControl1
        Me.txt_Observaciones.TabIndex = 22
        '
        'chk_ir
        '
        Me.chk_ir.AutoSizeInLayoutControl = True
        Me.chk_ir.EditValue = True
        Me.chk_ir.EnterMoveNextControl = True
        Me.chk_ir.Location = New System.Drawing.Point(344, 362)
        Me.chk_ir.MenuManager = Me.BarManager1
        Me.chk_ir.Name = "chk_ir"
        Me.chk_ir.Properties.Caption = ""
        Me.chk_ir.Size = New System.Drawing.Size(19, 19)
        Me.chk_ir.StyleController = Me.LayoutControl1
        Me.chk_ir.TabIndex = 17
        '
        'chk_imi
        '
        Me.chk_imi.AutoSizeInLayoutControl = True
        Me.chk_imi.EditValue = True
        Me.chk_imi.EnterMoveNextControl = True
        Me.chk_imi.Location = New System.Drawing.Point(344, 332)
        Me.chk_imi.MenuManager = Me.BarManager1
        Me.chk_imi.Name = "chk_imi"
        Me.chk_imi.Properties.Caption = ""
        Me.chk_imi.Size = New System.Drawing.Size(19, 19)
        Me.chk_imi.StyleController = Me.LayoutControl1
        Me.chk_imi.TabIndex = 12
        '
        'chk_automatico
        '
        Me.chk_automatico.EditValue = True
        Me.chk_automatico.EnterMoveNextControl = True
        Me.chk_automatico.Location = New System.Drawing.Point(344, 272)
        Me.chk_automatico.Name = "chk_automatico"
        Me.chk_automatico.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chk_automatico.Properties.Appearance.Options.UseFont = True
        Me.chk_automatico.Properties.Caption = "Automaticos"
        Me.chk_automatico.Size = New System.Drawing.Size(864, 20)
        Me.chk_automatico.StyleController = Me.LayoutControl1
        Me.chk_automatico.TabIndex = 6
        '
        'cmb_banco
        '
        Me.cmb_banco.EnterMoveNextControl = True
        Me.cmb_banco.Location = New System.Drawing.Point(456, 392)
        Me.cmb_banco.Name = "cmb_banco"
        Me.cmb_banco.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_banco.Properties.Appearance.Options.UseFont = True
        Me.cmb_banco.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_banco.Properties.NullText = ""
        Me.cmb_banco.Properties.PopupFormMinSize = New System.Drawing.Size(470, 0)
        Me.cmb_banco.Properties.PopupFormSize = New System.Drawing.Size(470, 0)
        Me.cmb_banco.Properties.View = Me.GridLookUpEdit2View
        Me.cmb_banco.Size = New System.Drawing.Size(322, 26)
        Me.cmb_banco.StyleController = Me.LayoutControl1
        Me.cmb_banco.TabIndex = 21
        '
        'GridLookUpEdit2View
        '
        Me.GridLookUpEdit2View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn6, Me.GridColumn7, Me.GridColumn8})
        Me.GridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit2View.Name = "GridLookUpEdit2View"
        Me.GridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit2View.OptionsView.ShowGroupPanel = False
        Me.GridLookUpEdit2View.RowHeight = 25
        '
        'GridColumn6
        '
        Me.GridColumn6.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn6.AppearanceCell.Options.UseFont = True
        Me.GridColumn6.Caption = "GridColumn6"
        Me.GridColumn6.FieldName = "BancoID"
        Me.GridColumn6.Name = "GridColumn6"
        '
        'GridColumn7
        '
        Me.GridColumn7.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn7.AppearanceCell.Options.UseFont = True
        Me.GridColumn7.Caption = "Entidad Bancaria"
        Me.GridColumn7.FieldName = "Banco"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 0
        '
        'GridColumn8
        '
        Me.GridColumn8.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn8.AppearanceCell.Options.UseFont = True
        Me.GridColumn8.Caption = "Siglas"
        Me.GridColumn8.FieldName = "Siglas"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 1
        '
        'txt_IMI_Serie
        '
        Me.txt_IMI_Serie.EditValue = "NE"
        Me.txt_IMI_Serie.EnterMoveNextControl = True
        Me.txt_IMI_Serie.Location = New System.Drawing.Point(485, 332)
        Me.txt_IMI_Serie.Name = "txt_IMI_Serie"
        Me.txt_IMI_Serie.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IMI_Serie.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txt_IMI_Serie.Properties.Appearance.Options.UseFont = True
        Me.txt_IMI_Serie.Properties.Appearance.Options.UseForeColor = True
        Me.txt_IMI_Serie.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.txt_IMI_Serie.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IMI_Serie.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Red
        Me.txt_IMI_Serie.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_IMI_Serie.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_IMI_Serie.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_IMI_Serie.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_IMI_Serie.Size = New System.Drawing.Size(70, 26)
        Me.txt_IMI_Serie.StyleController = Me.LayoutControl1
        Me.txt_IMI_Serie.TabIndex = 14
        '
        'txt_ck
        '
        Me.txt_ck.EditValue = ""
        Me.txt_ck.EnterMoveNextControl = True
        Me.txt_ck.Location = New System.Drawing.Point(142, 392)
        Me.txt_ck.Name = "txt_ck"
        Me.txt_ck.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_ck.Properties.Appearance.Options.UseFont = True
        Me.txt_ck.Properties.MaxLength = 10
        Me.txt_ck.Size = New System.Drawing.Size(179, 26)
        Me.txt_ck.StyleController = Me.LayoutControl1
        Me.txt_ck.TabIndex = 20
        '
        'txt_IR_Recibo
        '
        Me.txt_IR_Recibo.EditValue = ""
        Me.txt_IR_Recibo.EnterMoveNextControl = True
        Me.txt_IR_Recibo.Location = New System.Drawing.Point(367, 362)
        Me.txt_IR_Recibo.Name = "txt_IR_Recibo"
        Me.txt_IR_Recibo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IR_Recibo.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.txt_IR_Recibo.Properties.Appearance.Options.UseFont = True
        Me.txt_IR_Recibo.Properties.Appearance.Options.UseForeColor = True
        Me.txt_IR_Recibo.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_IR_Recibo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_IR_Recibo.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.txt_IR_Recibo.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IR_Recibo.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Red
        Me.txt_IR_Recibo.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_IR_Recibo.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_IR_Recibo.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_IR_Recibo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_IR_Recibo.Size = New System.Drawing.Size(114, 26)
        Me.txt_IR_Recibo.StyleController = Me.LayoutControl1
        Me.txt_IR_Recibo.TabIndex = 18
        '
        'txt_IMI_Recibo
        '
        Me.txt_IMI_Recibo.EditValue = ""
        Me.txt_IMI_Recibo.EnterMoveNextControl = True
        Me.txt_IMI_Recibo.Location = New System.Drawing.Point(367, 332)
        Me.txt_IMI_Recibo.Name = "txt_IMI_Recibo"
        Me.txt_IMI_Recibo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IMI_Recibo.Properties.Appearance.ForeColor = System.Drawing.Color.Maroon
        Me.txt_IMI_Recibo.Properties.Appearance.Options.UseFont = True
        Me.txt_IMI_Recibo.Properties.Appearance.Options.UseForeColor = True
        Me.txt_IMI_Recibo.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_IMI_Recibo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_IMI_Recibo.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.txt_IMI_Recibo.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IMI_Recibo.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Red
        Me.txt_IMI_Recibo.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_IMI_Recibo.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_IMI_Recibo.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_IMI_Recibo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_IMI_Recibo.Size = New System.Drawing.Size(114, 26)
        Me.txt_IMI_Recibo.StyleController = Me.LayoutControl1
        Me.txt_IMI_Recibo.TabIndex = 13
        '
        'txt_IR
        '
        Me.txt_IR.EditValue = "0.00"
        Me.txt_IR.EnterMoveNextControl = True
        Me.txt_IR.Location = New System.Drawing.Point(200, 362)
        Me.txt_IR.Name = "txt_IR"
        Me.txt_IR.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IR.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.txt_IR.Properties.Appearance.Options.UseFont = True
        Me.txt_IR.Properties.Appearance.Options.UseForeColor = True
        Me.txt_IR.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_IR.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_IR.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_IR.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IR.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_IR.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_IR.Properties.Mask.EditMask = "n2"
        Me.txt_IR.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_IR.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_IR.Size = New System.Drawing.Size(140, 26)
        Me.txt_IR.StyleController = Me.LayoutControl1
        Me.txt_IR.TabIndex = 16
        '
        'txt_por_ir
        '
        Me.txt_por_ir.EditValue = "2.00"
        Me.txt_por_ir.EnterMoveNextControl = True
        Me.txt_por_ir.Location = New System.Drawing.Point(142, 362)
        Me.txt_por_ir.Name = "txt_por_ir"
        Me.txt_por_ir.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_por_ir.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.txt_por_ir.Properties.Appearance.Options.UseFont = True
        Me.txt_por_ir.Properties.Appearance.Options.UseForeColor = True
        Me.txt_por_ir.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_por_ir.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_por_ir.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.txt_por_ir.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_por_ir.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.txt_por_ir.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_por_ir.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_por_ir.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_por_ir.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_por_ir.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_por_ir.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_por_ir.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_por_ir.Properties.Mask.EditMask = "n2"
        Me.txt_por_ir.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_por_ir.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_por_ir.Properties.ReadOnly = True
        Me.txt_por_ir.Size = New System.Drawing.Size(54, 26)
        Me.txt_por_ir.StyleController = Me.LayoutControl1
        Me.txt_por_ir.TabIndex = 15
        '
        'txt_IMI
        '
        Me.txt_IMI.EditValue = "0.00"
        Me.txt_IMI.EnterMoveNextControl = True
        Me.txt_IMI.Location = New System.Drawing.Point(200, 332)
        Me.txt_IMI.Name = "txt_IMI"
        Me.txt_IMI.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IMI.Properties.Appearance.ForeColor = System.Drawing.Color.Maroon
        Me.txt_IMI.Properties.Appearance.Options.UseFont = True
        Me.txt_IMI.Properties.Appearance.Options.UseForeColor = True
        Me.txt_IMI.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_IMI.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_IMI.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_IMI.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IMI.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_IMI.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_IMI.Properties.Mask.EditMask = "n2"
        Me.txt_IMI.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_IMI.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_IMI.Size = New System.Drawing.Size(140, 26)
        Me.txt_IMI.StyleController = Me.LayoutControl1
        Me.txt_IMI.TabIndex = 11
        '
        'dpt_fecha
        '
        Me.dpt_fecha.EditValue = Nothing
        Me.dpt_fecha.EnterMoveNextControl = True
        Me.dpt_fecha.Location = New System.Drawing.Point(142, 152)
        Me.dpt_fecha.Name = "dpt_fecha"
        Me.dpt_fecha.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dpt_fecha.Properties.Appearance.Options.UseFont = True
        Me.dpt_fecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpt_fecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpt_fecha.Size = New System.Drawing.Size(198, 26)
        Me.dpt_fecha.StyleController = Me.LayoutControl1
        Me.dpt_fecha.TabIndex = 2
        '
        'txt_por_imi
        '
        Me.txt_por_imi.EditValue = "1.00"
        Me.txt_por_imi.EnterMoveNextControl = True
        Me.txt_por_imi.Location = New System.Drawing.Point(142, 332)
        Me.txt_por_imi.Name = "txt_por_imi"
        Me.txt_por_imi.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_por_imi.Properties.Appearance.ForeColor = System.Drawing.Color.Maroon
        Me.txt_por_imi.Properties.Appearance.Options.UseFont = True
        Me.txt_por_imi.Properties.Appearance.Options.UseForeColor = True
        Me.txt_por_imi.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_por_imi.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_por_imi.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.txt_por_imi.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_por_imi.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.txt_por_imi.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_por_imi.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_por_imi.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_por_imi.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.txt_por_imi.Properties.AppearanceReadOnly.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_por_imi.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.txt_por_imi.Properties.AppearanceReadOnly.Options.UseFont = True
        Me.txt_por_imi.Properties.Mask.EditMask = "n2"
        Me.txt_por_imi.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_por_imi.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_por_imi.Properties.ReadOnly = True
        Me.txt_por_imi.Size = New System.Drawing.Size(54, 26)
        Me.txt_por_imi.StyleController = Me.LayoutControl1
        Me.txt_por_imi.TabIndex = 10
        '
        'txt_Importe
        '
        Me.txt_Importe.EditValue = "0.00"
        Me.txt_Importe.EnterMoveNextControl = True
        Me.txt_Importe.Location = New System.Drawing.Point(142, 272)
        Me.txt_Importe.Name = "txt_Importe"
        Me.txt_Importe.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Importe.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.txt_Importe.Properties.Appearance.Options.UseFont = True
        Me.txt_Importe.Properties.Appearance.Options.UseForeColor = True
        Me.txt_Importe.Properties.Appearance.Options.UseTextOptions = True
        Me.txt_Importe.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txt_Importe.Properties.Mask.EditMask = "n2"
        Me.txt_Importe.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txt_Importe.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txt_Importe.Size = New System.Drawing.Size(198, 26)
        Me.txt_Importe.StyleController = Me.LayoutControl1
        Me.txt_Importe.TabIndex = 5
        '
        'txt_No_Factura
        '
        Me.txt_No_Factura.EditValue = ""
        Me.txt_No_Factura.EnterMoveNextControl = True
        Me.txt_No_Factura.Location = New System.Drawing.Point(142, 182)
        Me.txt_No_Factura.Name = "txt_No_Factura"
        Me.txt_No_Factura.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_No_Factura.Properties.Appearance.Options.UseFont = True
        Me.txt_No_Factura.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_No_Factura.Properties.MaxLength = 50
        Me.txt_No_Factura.Size = New System.Drawing.Size(682, 26)
        Me.txt_No_Factura.StyleController = Me.LayoutControl1
        Me.txt_No_Factura.TabIndex = 3
        '
        'txt_telefono
        '
        Me.txt_telefono.Enabled = False
        Me.txt_telefono.EnterMoveNextControl = True
        Me.txt_telefono.Location = New System.Drawing.Point(835, 106)
        Me.txt_telefono.Name = "txt_telefono"
        Me.txt_telefono.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_telefono.Properties.Appearance.Options.UseFont = True
        Me.txt_telefono.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.txt_telefono.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.txt_telefono.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_telefono.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_telefono.Size = New System.Drawing.Size(192, 30)
        Me.txt_telefono.StyleController = Me.LayoutControl1
        Me.txt_telefono.TabIndex = 3
        '
        'Txt_Cedula
        '
        Me.Txt_Cedula.EditValue = ""
        Me.Txt_Cedula.Enabled = False
        Me.Txt_Cedula.EnterMoveNextControl = True
        Me.Txt_Cedula.Location = New System.Drawing.Point(482, 106)
        Me.Txt_Cedula.Name = "Txt_Cedula"
        Me.Txt_Cedula.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Cedula.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.Txt_Cedula.Properties.Appearance.Options.UseFont = True
        Me.Txt_Cedula.Properties.Appearance.Options.UseForeColor = True
        Me.Txt_Cedula.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.Txt_Cedula.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.Txt_Cedula.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.Txt_Cedula.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.Txt_Cedula.Size = New System.Drawing.Size(218, 30)
        Me.Txt_Cedula.StyleController = Me.LayoutControl1
        Me.Txt_Cedula.TabIndex = 2
        '
        'Txt_Ruc
        '
        Me.Txt_Ruc.EditValue = ""
        Me.Txt_Ruc.Enabled = False
        Me.Txt_Ruc.EnterMoveNextControl = True
        Me.Txt_Ruc.Location = New System.Drawing.Point(142, 106)
        Me.Txt_Ruc.Name = "Txt_Ruc"
        Me.Txt_Ruc.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Txt_Ruc.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.Txt_Ruc.Properties.Appearance.Options.UseFont = True
        Me.Txt_Ruc.Properties.Appearance.Options.UseForeColor = True
        Me.Txt_Ruc.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.Txt_Ruc.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.Txt_Ruc.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.Txt_Ruc.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.Txt_Ruc.Size = New System.Drawing.Size(205, 30)
        Me.Txt_Ruc.StyleController = Me.LayoutControl1
        Me.Txt_Ruc.TabIndex = 1
        '
        'cmb_retenido
        '
        Me.cmb_retenido.EditValue = ""
        Me.cmb_retenido.EnterMoveNextControl = True
        Me.cmb_retenido.Location = New System.Drawing.Point(142, 72)
        Me.cmb_retenido.Name = "cmb_retenido"
        Me.cmb_retenido.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_retenido.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.cmb_retenido.Properties.Appearance.Options.UseFont = True
        Me.cmb_retenido.Properties.Appearance.Options.UseForeColor = True
        EditorButtonImageOptions1.Image = CType(resources.GetObject("EditorButtonImageOptions1.Image"), System.Drawing.Image)
        EditorButtonImageOptions2.Image = CType(resources.GetObject("EditorButtonImageOptions2.Image"), System.Drawing.Image)
        EditorButtonImageOptions3.Image = CType(resources.GetObject("EditorButtonImageOptions3.Image"), System.Drawing.Image)
        Me.cmb_retenido.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(EditorButtonImageOptions1, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, Nothing), New DevExpress.XtraEditors.Controls.EditorButton(EditorButtonImageOptions2, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, Nothing), New DevExpress.XtraEditors.Controls.EditorButton(EditorButtonImageOptions3, DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, Nothing)})
        Me.cmb_retenido.Properties.NullText = ""
        Me.cmb_retenido.Properties.PopupFormMinSize = New System.Drawing.Size(1200, 0)
        Me.cmb_retenido.Properties.PopupFormSize = New System.Drawing.Size(1200, 0)
        Me.cmb_retenido.Properties.View = Me.GridLookUpEdit1View
        Me.cmb_retenido.Size = New System.Drawing.Size(1170, 30)
        Me.cmb_retenido.StyleController = Me.LayoutControl1
        Me.cmb_retenido.TabIndex = 0
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn2, Me.GridColumn1, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5})
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowAutoFilterRow = True
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn2
        '
        Me.GridColumn2.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn2.AppearanceCell.ForeColor = System.Drawing.Color.Blue
        Me.GridColumn2.AppearanceCell.Options.UseFont = True
        Me.GridColumn2.AppearanceCell.Options.UseForeColor = True
        Me.GridColumn2.Caption = "RUC"
        Me.GridColumn2.FieldName = "RUC"
        Me.GridColumn2.Image = CType(resources.GetObject("GridColumn2.Image"), System.Drawing.Image)
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 0
        Me.GridColumn2.Width = 45
        '
        'GridColumn1
        '
        Me.GridColumn1.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn1.AppearanceCell.Options.UseFont = True
        Me.GridColumn1.Caption = "Retenido"
        Me.GridColumn1.FieldName = "Proveedor"
        Me.GridColumn1.Image = CType(resources.GetObject("GridColumn1.Image"), System.Drawing.Image)
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        Me.GridColumn1.Width = 200
        '
        'GridColumn3
        '
        Me.GridColumn3.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn3.AppearanceCell.Options.UseFont = True
        Me.GridColumn3.Caption = "Cedula"
        Me.GridColumn3.FieldName = "Cedula"
        Me.GridColumn3.Image = CType(resources.GetObject("GridColumn3.Image"), System.Drawing.Image)
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 45
        '
        'GridColumn4
        '
        Me.GridColumn4.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn4.AppearanceCell.Options.UseFont = True
        Me.GridColumn4.Caption = "Telefono"
        Me.GridColumn4.FieldName = "Telefono"
        Me.GridColumn4.Image = CType(resources.GetObject("GridColumn4.Image"), System.Drawing.Image)
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 3
        Me.GridColumn4.Width = 45
        '
        'GridColumn5
        '
        Me.GridColumn5.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn5.AppearanceCell.Options.UseFont = True
        Me.GridColumn5.Caption = " "
        Me.GridColumn5.FieldName = "ProveedorID"
        Me.GridColumn5.Image = CType(resources.GetObject("GridColumn5.Image"), System.Drawing.Image)
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 4
        Me.GridColumn5.Width = 49
        '
        'txt_IR_serie
        '
        Me.txt_IR_serie.EnterMoveNextControl = True
        Me.txt_IR_serie.Location = New System.Drawing.Point(485, 362)
        Me.txt_IR_serie.Name = "txt_IR_serie"
        Me.txt_IR_serie.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IR_serie.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.txt_IR_serie.Properties.Appearance.Options.UseFont = True
        Me.txt_IR_serie.Properties.Appearance.Options.UseForeColor = True
        Me.txt_IR_serie.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.White
        Me.txt_IR_serie.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_IR_serie.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Red
        Me.txt_IR_serie.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.txt_IR_serie.Properties.AppearanceDisabled.Options.UseFont = True
        Me.txt_IR_serie.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.txt_IR_serie.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_IR_serie.Size = New System.Drawing.Size(70, 26)
        Me.txt_IR_serie.StyleController = Me.LayoutControl1
        Me.txt_IR_serie.TabIndex = 19
        '
        'cmb_oficina
        '
        Me.cmb_oficina.EditValue = ""
        Me.cmb_oficina.EnterMoveNextControl = True
        Me.cmb_oficina.Location = New System.Drawing.Point(142, 36)
        Me.cmb_oficina.Name = "cmb_oficina"
        Me.cmb_oficina.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_oficina.Properties.Appearance.Options.UseFont = True
        Me.cmb_oficina.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.SpinDown)})
        Me.cmb_oficina.Properties.NullText = ""
        Me.cmb_oficina.Properties.PopupFormMinSize = New System.Drawing.Size(800, 0)
        Me.cmb_oficina.Properties.PopupFormSize = New System.Drawing.Size(800, 0)
        Me.cmb_oficina.Properties.View = Me.GridView2
        Me.cmb_oficina.Size = New System.Drawing.Size(1170, 26)
        Me.cmb_oficina.StyleController = Me.LayoutControl1
        Me.cmb_oficina.TabIndex = 0
        '
        'GridView2
        '
        Me.GridView2.Appearance.FocusedRow.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Blue
        Me.GridView2.Appearance.FocusedRow.Options.UseFont = True
        Me.GridView2.Appearance.FocusedRow.Options.UseForeColor = True
        Me.GridView2.Appearance.Row.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridView2.Appearance.Row.ForeColor = System.Drawing.Color.Blue
        Me.GridView2.Appearance.Row.Options.UseFont = True
        Me.GridView2.Appearance.Row.Options.UseForeColor = True
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn17, Me.GridColumn18})
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn17
        '
        Me.GridColumn17.Caption = "ID"
        Me.GridColumn17.FieldName = "OficinaID"
        Me.GridColumn17.Name = "GridColumn17"
        '
        'GridColumn18
        '
        Me.GridColumn18.Caption = "Oficina"
        Me.GridColumn18.FieldName = "Oficina"
        Me.GridColumn18.Name = "GridColumn18"
        Me.GridColumn18.Visible = True
        Me.GridColumn18.VisibleIndex = 0
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.TabbedControlGroup1})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1323, 576)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'TabbedControlGroup1
        '
        Me.TabbedControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.TabbedControlGroup1.Name = "TabbedControlGroup1"
        Me.TabbedControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.TabbedControlGroup1.SelectedTabPage = Me.LayoutControlGroup2
        Me.TabbedControlGroup1.SelectedTabPageIndex = 0
        Me.TabbedControlGroup1.Size = New System.Drawing.Size(1323, 576)
        Me.TabbedControlGroup1.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup2, Me.LayoutControlGroup5})
        Me.TabbedControlGroup1.Text = "Generales"
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.CaptionImage = CType(resources.GetObject("LayoutControlGroup2.CaptionImage"), System.Drawing.Image)
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup3, Me.LayoutControlGroup4})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(1317, 545)
        Me.LayoutControlGroup2.Text = "Generales"
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup8})
        Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 116)
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(1317, 429)
        Me.LayoutControlGroup3.Text = "Datos de Retención"
        Me.LayoutControlGroup3.TextVisible = False
        '
        'LayoutControlGroup8
        '
        Me.LayoutControlGroup8.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem10, Me.EmptySpaceItem1, Me.EmptySpaceItem3, Me.EmptySpaceItem5, Me.LayoutControlItem19, Me.EmptySpaceItem7, Me.LayoutControlItem7, Me.LayoutControlItem1, Me.LayoutControlItem20, Me.LayoutControlItem6, Me.LayoutControlItem23, Me.LayoutControlItem9, Me.LayoutControlItem12, Me.LayoutControlItem13, Me.LayoutControlItem11, Me.LayoutControlItem24, Me.LayoutControlItem18, Me.LayoutControlItem21, Me.LayoutControlItem15, Me.LayoutControlItem14, Me.LayoutControlItem25, Me.LayoutControlItem34, Me.LayoutControlItem17, Me.LayoutControlItem8, Me.panel_obs, Me.panel_chk, Me.LayoutControlItem39, Me.panel_banco, Me.LayoutControlItem40, Me.EmptySpaceItem12, Me.EmptySpaceItem10})
        Me.LayoutControlGroup8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup8.Name = "LayoutControlGroup8"
        Me.LayoutControlGroup8.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup8.Size = New System.Drawing.Size(1311, 423)
        Me.LayoutControlGroup8.TextVisible = False
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem10.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem10.Control = Me.dpt_fecha
        Me.LayoutControlItem10.Image = CType(resources.GetObject("LayoutControlItem10.Image"), System.Drawing.Image)
        Me.LayoutControlItem10.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem10.MaxSize = New System.Drawing.Size(333, 30)
        Me.LayoutControlItem10.MinSize = New System.Drawing.Size(333, 30)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(333, 30)
        Me.LayoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem10.Text = "Fecha"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(128, 19)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(333, 0)
        Me.EmptySpaceItem1.MaxSize = New System.Drawing.Size(868, 30)
        Me.EmptySpaceItem1.MinSize = New System.Drawing.Size(868, 30)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(972, 30)
        Me.EmptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = False
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(817, 30)
        Me.EmptySpaceItem3.MaxSize = New System.Drawing.Size(384, 30)
        Me.EmptySpaceItem3.MinSize = New System.Drawing.Size(384, 30)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(488, 30)
        Me.EmptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem5
        '
        Me.EmptySpaceItem5.AllowHotTrack = False
        Me.EmptySpaceItem5.Location = New System.Drawing.Point(817, 60)
        Me.EmptySpaceItem5.MaxSize = New System.Drawing.Size(384, 60)
        Me.EmptySpaceItem5.MinSize = New System.Drawing.Size(384, 60)
        Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Size = New System.Drawing.Size(488, 60)
        Me.EmptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.BestFitWeight = 5
        Me.LayoutControlItem19.Control = Me.chk_automatico
        Me.LayoutControlItem19.ImageToTextDistance = 2
        Me.LayoutControlItem19.Location = New System.Drawing.Point(333, 120)
        Me.LayoutControlItem19.MaxSize = New System.Drawing.Size(868, 30)
        Me.LayoutControlItem19.MinSize = New System.Drawing.Size(868, 30)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(972, 30)
        Me.LayoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem19.Text = "Calcular"
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem19.TextVisible = False
        '
        'EmptySpaceItem7
        '
        Me.EmptySpaceItem7.AllowHotTrack = False
        Me.EmptySpaceItem7.Location = New System.Drawing.Point(548, 150)
        Me.EmptySpaceItem7.MaxSize = New System.Drawing.Size(653, 90)
        Me.EmptySpaceItem7.MinSize = New System.Drawing.Size(653, 90)
        Me.EmptySpaceItem7.Name = "EmptySpaceItem7"
        Me.EmptySpaceItem7.Size = New System.Drawing.Size(757, 90)
        Me.EmptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem7.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem7.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem7.Control = Me.txt_Importe
        Me.LayoutControlItem7.Image = CType(resources.GetObject("LayoutControlItem7.Image"), System.Drawing.Image)
        Me.LayoutControlItem7.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem7.MaxSize = New System.Drawing.Size(333, 30)
        Me.LayoutControlItem7.MinSize = New System.Drawing.Size(333, 30)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(333, 30)
        Me.LayoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem7.Text = "Sub Total"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(128, 19)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem1.Control = Me.cmb_ir
        Me.LayoutControlItem1.CustomizationFormText = "Tipo de Servicios"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 90)
        Me.LayoutControlItem1.MaxSize = New System.Drawing.Size(817, 30)
        Me.LayoutControlItem1.MinSize = New System.Drawing.Size(817, 30)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(817, 30)
        Me.LayoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem1.Text = "Tipos de Servicios"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(128, 19)
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem20.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem20.Control = Me.txt_concepto
        Me.LayoutControlItem20.Image = CType(resources.GetObject("LayoutControlItem20.Image"), System.Drawing.Image)
        Me.LayoutControlItem20.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 60)
        Me.LayoutControlItem20.MaxSize = New System.Drawing.Size(817, 30)
        Me.LayoutControlItem20.MinSize = New System.Drawing.Size(817, 30)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(817, 30)
        Me.LayoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem20.Text = "Conceptos"
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(128, 19)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem6.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem6.Control = Me.txt_No_Factura
        Me.LayoutControlItem6.Image = CType(resources.GetObject("LayoutControlItem6.Image"), System.Drawing.Image)
        Me.LayoutControlItem6.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 30)
        Me.LayoutControlItem6.MaxSize = New System.Drawing.Size(817, 30)
        Me.LayoutControlItem6.MinSize = New System.Drawing.Size(817, 30)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(817, 30)
        Me.LayoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem6.Text = "Factura No"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(128, 19)
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem23.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem23.Control = Me.txt_por_iva
        Me.LayoutControlItem23.Image = CType(resources.GetObject("LayoutControlItem23.Image"), System.Drawing.Image)
        Me.LayoutControlItem23.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem23.Location = New System.Drawing.Point(0, 150)
        Me.LayoutControlItem23.MaxSize = New System.Drawing.Size(189, 30)
        Me.LayoutControlItem23.MinSize = New System.Drawing.Size(189, 30)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(189, 30)
        Me.LayoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem23.Text = "Impuesto IVA"
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(128, 19)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem9.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem9.Control = Me.txt_por_imi
        Me.LayoutControlItem9.Image = CType(resources.GetObject("LayoutControlItem9.Image"), System.Drawing.Image)
        Me.LayoutControlItem9.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 180)
        Me.LayoutControlItem9.MaxSize = New System.Drawing.Size(189, 30)
        Me.LayoutControlItem9.MinSize = New System.Drawing.Size(189, 30)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(189, 30)
        Me.LayoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem9.Text = "Retención IMI"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(128, 19)
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem12.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem12.Control = Me.txt_por_ir
        Me.LayoutControlItem12.Image = CType(resources.GetObject("LayoutControlItem12.Image"), System.Drawing.Image)
        Me.LayoutControlItem12.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 210)
        Me.LayoutControlItem12.MaxSize = New System.Drawing.Size(189, 30)
        Me.LayoutControlItem12.MinSize = New System.Drawing.Size(189, 30)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(189, 30)
        Me.LayoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem12.Text = "Retención IR"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(128, 19)
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem13.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem13.Control = Me.txt_IR
        Me.LayoutControlItem13.Location = New System.Drawing.Point(189, 210)
        Me.LayoutControlItem13.MaxSize = New System.Drawing.Size(144, 30)
        Me.LayoutControlItem13.MinSize = New System.Drawing.Size(144, 30)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(144, 30)
        Me.LayoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem13.TextVisible = False
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem11.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem11.Control = Me.txt_IMI
        Me.LayoutControlItem11.Location = New System.Drawing.Point(189, 180)
        Me.LayoutControlItem11.MaxSize = New System.Drawing.Size(144, 30)
        Me.LayoutControlItem11.MinSize = New System.Drawing.Size(144, 30)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(144, 30)
        Me.LayoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.txt_iva
        Me.LayoutControlItem24.Location = New System.Drawing.Point(189, 150)
        Me.LayoutControlItem24.MaxSize = New System.Drawing.Size(144, 30)
        Me.LayoutControlItem24.MinSize = New System.Drawing.Size(144, 30)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(144, 30)
        Me.LayoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem24.TextVisible = False
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.chk_imi
        Me.LayoutControlItem18.Location = New System.Drawing.Point(333, 180)
        Me.LayoutControlItem18.MaxSize = New System.Drawing.Size(23, 30)
        Me.LayoutControlItem18.MinSize = New System.Drawing.Size(23, 30)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(23, 30)
        Me.LayoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem18.TextVisible = False
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.chk_ir
        Me.LayoutControlItem21.Location = New System.Drawing.Point(333, 210)
        Me.LayoutControlItem21.MaxSize = New System.Drawing.Size(23, 30)
        Me.LayoutControlItem21.MinSize = New System.Drawing.Size(23, 30)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(23, 30)
        Me.LayoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem21.TextVisible = False
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem15.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem15.Control = Me.txt_IR_Recibo
        Me.LayoutControlItem15.Location = New System.Drawing.Point(356, 210)
        Me.LayoutControlItem15.MaxSize = New System.Drawing.Size(118, 30)
        Me.LayoutControlItem15.MinSize = New System.Drawing.Size(118, 30)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(118, 30)
        Me.LayoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem15.Text = "No"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem15.TextVisible = False
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem14.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem14.Control = Me.txt_IMI_Recibo
        Me.LayoutControlItem14.Location = New System.Drawing.Point(356, 180)
        Me.LayoutControlItem14.MaxSize = New System.Drawing.Size(118, 30)
        Me.LayoutControlItem14.MinSize = New System.Drawing.Size(118, 30)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(118, 30)
        Me.LayoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem14.Text = "No"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem14.TextVisible = False
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.chk_iva
        Me.LayoutControlItem25.Location = New System.Drawing.Point(333, 150)
        Me.LayoutControlItem25.MaxSize = New System.Drawing.Size(23, 30)
        Me.LayoutControlItem25.MinSize = New System.Drawing.Size(23, 30)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(23, 30)
        Me.LayoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem25.TextVisible = False
        '
        'LayoutControlItem34
        '
        Me.LayoutControlItem34.Control = Me.TextEdit1
        Me.LayoutControlItem34.Location = New System.Drawing.Point(356, 150)
        Me.LayoutControlItem34.MaxSize = New System.Drawing.Size(192, 30)
        Me.LayoutControlItem34.MinSize = New System.Drawing.Size(192, 30)
        Me.LayoutControlItem34.Name = "LayoutControlItem34"
        Me.LayoutControlItem34.Size = New System.Drawing.Size(192, 30)
        Me.LayoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem34.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem34.TextVisible = False
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.txt_IMI_Serie
        Me.LayoutControlItem17.Location = New System.Drawing.Point(474, 180)
        Me.LayoutControlItem17.MaxSize = New System.Drawing.Size(74, 30)
        Me.LayoutControlItem17.MinSize = New System.Drawing.Size(74, 30)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(74, 30)
        Me.LayoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem17.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.txt_IR_serie
        Me.LayoutControlItem8.CustomizationFormText = "LayoutControlItem17"
        Me.LayoutControlItem8.Location = New System.Drawing.Point(474, 210)
        Me.LayoutControlItem8.MaxSize = New System.Drawing.Size(74, 30)
        Me.LayoutControlItem8.MinSize = New System.Drawing.Size(74, 30)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(74, 30)
        Me.LayoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem8.Text = "LayoutControlItem17"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem8.TextVisible = False
        '
        'panel_obs
        '
        Me.panel_obs.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panel_obs.AppearanceItemCaption.Options.UseFont = True
        Me.panel_obs.Control = Me.txt_Observaciones
        Me.panel_obs.Location = New System.Drawing.Point(0, 270)
        Me.panel_obs.MaxSize = New System.Drawing.Size(0, 30)
        Me.panel_obs.MinSize = New System.Drawing.Size(180, 30)
        Me.panel_obs.Name = "panel_obs"
        Me.panel_obs.Size = New System.Drawing.Size(771, 30)
        Me.panel_obs.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.panel_obs.Text = "Observaciones"
        Me.panel_obs.TextSize = New System.Drawing.Size(128, 19)
        '
        'panel_chk
        '
        Me.panel_chk.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panel_chk.AppearanceItemCaption.Options.UseFont = True
        Me.panel_chk.Control = Me.txt_ck
        Me.panel_chk.Image = CType(resources.GetObject("panel_chk.Image"), System.Drawing.Image)
        Me.panel_chk.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.panel_chk.Location = New System.Drawing.Point(0, 240)
        Me.panel_chk.MaxSize = New System.Drawing.Size(0, 30)
        Me.panel_chk.MinSize = New System.Drawing.Size(185, 30)
        Me.panel_chk.Name = "panel_chk"
        Me.panel_chk.Size = New System.Drawing.Size(314, 30)
        Me.panel_chk.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.panel_chk.Text = "Cheques"
        Me.panel_chk.TextSize = New System.Drawing.Size(128, 19)
        '
        'LayoutControlItem39
        '
        Me.LayoutControlItem39.Control = Me.SimpleButton2
        Me.LayoutControlItem39.Location = New System.Drawing.Point(0, 300)
        Me.LayoutControlItem39.MaxSize = New System.Drawing.Size(134, 30)
        Me.LayoutControlItem39.MinSize = New System.Drawing.Size(134, 30)
        Me.LayoutControlItem39.Name = "LayoutControlItem39"
        Me.LayoutControlItem39.Size = New System.Drawing.Size(134, 30)
        Me.LayoutControlItem39.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem39.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem39.TextVisible = False
        '
        'panel_banco
        '
        Me.panel_banco.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panel_banco.AppearanceItemCaption.Options.UseFont = True
        Me.panel_banco.Control = Me.cmb_banco
        Me.panel_banco.Image = CType(resources.GetObject("panel_banco.Image"), System.Drawing.Image)
        Me.panel_banco.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.panel_banco.Location = New System.Drawing.Point(314, 240)
        Me.panel_banco.MaxSize = New System.Drawing.Size(0, 30)
        Me.panel_banco.MinSize = New System.Drawing.Size(185, 30)
        Me.panel_banco.Name = "panel_banco"
        Me.panel_banco.Size = New System.Drawing.Size(457, 30)
        Me.panel_banco.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.panel_banco.Text = "Entidad Banco"
        Me.panel_banco.TextSize = New System.Drawing.Size(128, 19)
        '
        'LayoutControlItem40
        '
        Me.LayoutControlItem40.Control = Me.txt_filename
        Me.LayoutControlItem40.Location = New System.Drawing.Point(134, 300)
        Me.LayoutControlItem40.MaxSize = New System.Drawing.Size(637, 30)
        Me.LayoutControlItem40.MinSize = New System.Drawing.Size(637, 30)
        Me.LayoutControlItem40.Name = "LayoutControlItem40"
        Me.LayoutControlItem40.Size = New System.Drawing.Size(637, 30)
        Me.LayoutControlItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem40.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem40.TextVisible = False
        '
        'EmptySpaceItem12
        '
        Me.EmptySpaceItem12.AllowHotTrack = False
        Me.EmptySpaceItem12.Location = New System.Drawing.Point(0, 330)
        Me.EmptySpaceItem12.MaxSize = New System.Drawing.Size(1201, 70)
        Me.EmptySpaceItem12.MinSize = New System.Drawing.Size(1201, 70)
        Me.EmptySpaceItem12.Name = "EmptySpaceItem12"
        Me.EmptySpaceItem12.Size = New System.Drawing.Size(1305, 87)
        Me.EmptySpaceItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem12.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem10
        '
        Me.EmptySpaceItem10.AllowHotTrack = False
        Me.EmptySpaceItem10.Location = New System.Drawing.Point(771, 240)
        Me.EmptySpaceItem10.MaxSize = New System.Drawing.Size(430, 90)
        Me.EmptySpaceItem10.MinSize = New System.Drawing.Size(430, 90)
        Me.EmptySpaceItem10.Name = "EmptySpaceItem10"
        Me.EmptySpaceItem10.Size = New System.Drawing.Size(534, 90)
        Me.EmptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem10.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlGroup4
        '
        Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup6, Me.LayoutControlGroup7})
        Me.LayoutControlGroup4.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup4.Name = "LayoutControlGroup4"
        Me.LayoutControlGroup4.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup4.Size = New System.Drawing.Size(1317, 116)
        Me.LayoutControlGroup4.Text = " Datos Generales del Retenido"
        Me.LayoutControlGroup4.TextVisible = False
        '
        'LayoutControlGroup6
        '
        Me.LayoutControlGroup6.AppearanceGroup.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup6.AppearanceGroup.Options.UseFont = True
        Me.LayoutControlGroup6.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup6.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlGroup6.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem33})
        Me.LayoutControlGroup6.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup6.Name = "LayoutControlGroup6"
        Me.LayoutControlGroup6.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup6.Size = New System.Drawing.Size(1311, 36)
        Me.LayoutControlGroup6.TextVisible = False
        '
        'LayoutControlItem33
        '
        Me.LayoutControlItem33.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem33.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem33.Control = Me.cmb_oficina
        Me.LayoutControlItem33.CustomizationFormText = "Retenido"
        Me.LayoutControlItem33.Image = CType(resources.GetObject("LayoutControlItem33.Image"), System.Drawing.Image)
        Me.LayoutControlItem33.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem33.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem33.Name = "LayoutControlItem33"
        Me.LayoutControlItem33.Size = New System.Drawing.Size(1305, 30)
        Me.LayoutControlItem33.Text = "Oficina"
        Me.LayoutControlItem33.TextSize = New System.Drawing.Size(128, 18)
        '
        'LayoutControlGroup7
        '
        Me.LayoutControlGroup7.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem26, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem35, Me.LayoutControlItem36, Me.LayoutControlItem37})
        Me.LayoutControlGroup7.Location = New System.Drawing.Point(0, 36)
        Me.LayoutControlGroup7.Name = "LayoutControlGroup7"
        Me.LayoutControlGroup7.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup7.Size = New System.Drawing.Size(1311, 74)
        Me.LayoutControlGroup7.TextVisible = False
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem26.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem26.BestFitWeight = 50
        Me.LayoutControlItem26.Control = Me.Txt_Ruc
        Me.LayoutControlItem26.Image = CType(resources.GetObject("LayoutControlItem26.Image"), System.Drawing.Image)
        Me.LayoutControlItem26.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem26.Location = New System.Drawing.Point(0, 34)
        Me.LayoutControlItem26.MaxSize = New System.Drawing.Size(0, 34)
        Me.LayoutControlItem26.MinSize = New System.Drawing.Size(185, 34)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(340, 34)
        Me.LayoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem26.Text = "RUC"
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(128, 18)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem3.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem3.BestFitWeight = 50
        Me.LayoutControlItem3.Control = Me.cmb_retenido
        Me.LayoutControlItem3.Image = CType(resources.GetObject("LayoutControlItem3.Image"), System.Drawing.Image)
        Me.LayoutControlItem3.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem3.MaxSize = New System.Drawing.Size(0, 34)
        Me.LayoutControlItem3.MinSize = New System.Drawing.Size(185, 34)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(1305, 34)
        Me.LayoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem3.Text = "Proveedor"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(128, 18)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem4.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem4.BestFitWeight = 50
        Me.LayoutControlItem4.Control = Me.Txt_Cedula
        Me.LayoutControlItem4.Image = CType(resources.GetObject("LayoutControlItem4.Image"), System.Drawing.Image)
        Me.LayoutControlItem4.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem4.Location = New System.Drawing.Point(340, 34)
        Me.LayoutControlItem4.MaxSize = New System.Drawing.Size(0, 34)
        Me.LayoutControlItem4.MinSize = New System.Drawing.Size(185, 34)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(353, 34)
        Me.LayoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem4.Text = "Cedula"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(128, 18)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem5.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem5.BestFitWeight = 50
        Me.LayoutControlItem5.Control = Me.txt_telefono
        Me.LayoutControlItem5.Image = CType(resources.GetObject("LayoutControlItem5.Image"), System.Drawing.Image)
        Me.LayoutControlItem5.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem5.Location = New System.Drawing.Point(693, 34)
        Me.LayoutControlItem5.MaxSize = New System.Drawing.Size(0, 34)
        Me.LayoutControlItem5.MinSize = New System.Drawing.Size(185, 34)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(327, 34)
        Me.LayoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem5.Text = "Telefono"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(128, 18)
        '
        'LayoutControlItem35
        '
        Me.LayoutControlItem35.Control = Me.chk_pro_iva
        Me.LayoutControlItem35.Location = New System.Drawing.Point(1020, 34)
        Me.LayoutControlItem35.MaxSize = New System.Drawing.Size(0, 34)
        Me.LayoutControlItem35.MinSize = New System.Drawing.Size(51, 34)
        Me.LayoutControlItem35.Name = "LayoutControlItem35"
        Me.LayoutControlItem35.Size = New System.Drawing.Size(55, 34)
        Me.LayoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem35.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem35.TextVisible = False
        Me.LayoutControlItem35.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'LayoutControlItem36
        '
        Me.LayoutControlItem36.Control = Me.chk_pro_imi
        Me.LayoutControlItem36.Location = New System.Drawing.Point(1121, 34)
        Me.LayoutControlItem36.MaxSize = New System.Drawing.Size(0, 34)
        Me.LayoutControlItem36.MinSize = New System.Drawing.Size(50, 34)
        Me.LayoutControlItem36.Name = "LayoutControlItem36"
        Me.LayoutControlItem36.Size = New System.Drawing.Size(184, 34)
        Me.LayoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem36.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem36.TextVisible = False
        Me.LayoutControlItem36.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'LayoutControlItem37
        '
        Me.LayoutControlItem37.Control = Me.chk_pro_ir
        Me.LayoutControlItem37.Location = New System.Drawing.Point(1075, 34)
        Me.LayoutControlItem37.MaxSize = New System.Drawing.Size(0, 34)
        Me.LayoutControlItem37.MinSize = New System.Drawing.Size(42, 34)
        Me.LayoutControlItem37.Name = "LayoutControlItem37"
        Me.LayoutControlItem37.Size = New System.Drawing.Size(46, 34)
        Me.LayoutControlItem37.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem37.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem37.TextVisible = False
        Me.LayoutControlItem37.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'LayoutControlGroup5
        '
        Me.LayoutControlGroup5.CaptionImage = CType(resources.GetObject("LayoutControlGroup5.CaptionImage"), System.Drawing.Image)
        Me.LayoutControlGroup5.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem28, Me.LayoutControlItem29, Me.LayoutControlItem30, Me.LayoutControlItem27, Me.LayoutControlItem31, Me.LayoutControlItem32, Me.EmptySpaceItem4})
        Me.LayoutControlGroup5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup5.Name = "LayoutControlGroup5"
        Me.LayoutControlGroup5.Size = New System.Drawing.Size(1317, 545)
        Me.LayoutControlGroup5.Text = "Soporte "
        Me.LayoutControlGroup5.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.Control = Me.Grid_Imagenes
        Me.LayoutControlItem28.Location = New System.Drawing.Point(0, 51)
        Me.LayoutControlItem28.MinSize = New System.Drawing.Size(104, 24)
        Me.LayoutControlItem28.Name = "LayoutControlItem28"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(1317, 494)
        Me.LayoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem28.TextVisible = False
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.btn_eliminarimagen
        Me.LayoutControlItem29.Location = New System.Drawing.Point(50, 0)
        Me.LayoutControlItem29.MinSize = New System.Drawing.Size(46, 42)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(50, 51)
        Me.LayoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem29.TextVisible = False
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.Control = Me.btn_agregarimagen
        Me.LayoutControlItem30.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem30.MinSize = New System.Drawing.Size(46, 42)
        Me.LayoutControlItem30.Name = "LayoutControlItem30"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(50, 51)
        Me.LayoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem30.TextVisible = False
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.Control = Me.btn_lista
        Me.LayoutControlItem27.Location = New System.Drawing.Point(1257, 0)
        Me.LayoutControlItem27.MinSize = New System.Drawing.Size(20, 24)
        Me.LayoutControlItem27.Name = "LayoutControlItem27"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(60, 51)
        Me.LayoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem27.TextVisible = False
        '
        'LayoutControlItem31
        '
        Me.LayoutControlItem31.Control = Me.btn_mediano
        Me.LayoutControlItem31.Location = New System.Drawing.Point(1206, 0)
        Me.LayoutControlItem31.MinSize = New System.Drawing.Size(20, 24)
        Me.LayoutControlItem31.Name = "LayoutControlItem31"
        Me.LayoutControlItem31.Size = New System.Drawing.Size(51, 51)
        Me.LayoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem31.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem31.TextVisible = False
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.Control = Me.btn_grande
        Me.LayoutControlItem32.Location = New System.Drawing.Point(1158, 0)
        Me.LayoutControlItem32.MinSize = New System.Drawing.Size(20, 24)
        Me.LayoutControlItem32.Name = "LayoutControlItem32"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(48, 51)
        Me.LayoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem32.TextVisible = False
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = False
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(100, 0)
        Me.EmptySpaceItem4.MinSize = New System.Drawing.Size(104, 24)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(1058, 51)
        Me.EmptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
        '
        'Frm_Retenciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1323, 616)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "Frm_Retenciones"
        Me.Text = "Tax Impuestos & Retenciones"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.txt_filename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk_pro_ir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk_pro_imi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk_pro_iva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Grid_Imagenes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VGrid_Imagen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_ir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk_iva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_iva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_por_iva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_concepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Observaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk_ir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk_imi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chk_automatico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_banco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit2View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IMI_Serie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_ck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IR_Recibo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IMI_Recibo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_por_ir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IMI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpt_fecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpt_fecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_por_imi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Importe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_No_Factura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_telefono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_Cedula.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Txt_Ruc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_retenido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_IR_serie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_oficina.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabbedControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.panel_obs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.panel_chk, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.panel_banco, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents cmb_retenido As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txt_telefono As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txt_Cedula As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Txt_Ruc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_IR_Recibo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_IMI_Recibo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_IR As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_por_ir As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_IMI As DevExpress.XtraEditors.TextEdit
    Friend WithEvents dpt_fecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txt_por_imi As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_Importe As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_No_Factura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_ck As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_IMI_Serie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cmb_banco As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridLookUpEdit2View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chk_automatico As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txt_IR_serie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
    Friend WithEvents btn_guardar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btn_regresar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents chk_ir As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chk_imi As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txt_Observaciones As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_concepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btn_anulado As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents txt_iva As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_por_iva As DevExpress.XtraEditors.TextEdit
    Friend WithEvents chk_iva As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents cmb_ir As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TabbedControlGroup1 As DevExpress.XtraLayout.TabbedControlGroup
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents panel_chk As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents panel_banco As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem7 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents panel_obs As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlGroup5 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents Grid_Imagenes As DevExpress.XtraGrid.GridControl
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents btn_agregarimagen As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_eliminarimagen As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents VGrid_Imagen As DevExpress.XtraGrid.Views.WinExplorer.WinExplorerView
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btn_grande As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_mediano As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_lista As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cmb_oficina As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlGroup6 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem33 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup7 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents GridColumn17 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn18 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup8 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents chk_pro_ir As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chk_pro_imi As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chk_pro_iva As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem35 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem36 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem37 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem34 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents OpenFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txt_filename As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem39 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem12 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem40 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem10 As DevExpress.XtraLayout.EmptySpaceItem
End Class
