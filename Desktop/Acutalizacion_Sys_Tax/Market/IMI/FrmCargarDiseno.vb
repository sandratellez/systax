Imports System
Imports System.IO
Imports System.Data.SqlClient
Imports System.Drawing
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraEditors


Public Class FrmCargarDiseno
    Inherits DevExpress.XtraEditors.XtraForm

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents XrDesignPanel1 As DevExpress.XtraReports.UserDesigner.XRDesignPanel
    Friend WithEvents XrDesignDockManager1 As DevExpress.XtraReports.UserDesigner.XRDesignDockManager
    Friend WithEvents DockPanel1 As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents DockPanel1_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cmbboolean As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cmbfecha As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cmbbordes As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cmbimpresa As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cmbcambio As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents XrDesignDockManager2 As DevExpress.XtraReports.UserDesigner.XRDesignDockManager
    Friend WithEvents PropertyGridDockPanel1 As DevExpress.XtraReports.UserDesigner.PropertyGridDockPanel
    Friend WithEvents hideContainerRight As DevExpress.XtraBars.Docking.AutoHideContainer
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents XrDesignBarManager1 As DevExpress.XtraReports.UserDesigner.XRDesignBarManager
    Friend WithEvents DesignBar1 As DevExpress.XtraReports.UserDesigner.DesignBar
    Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents CommandBarItem31 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem39 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem32 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents btn_guardar As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem40 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem41 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents BarSubItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents CommandBarItem37 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem38 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem34 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem35 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem36 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem42 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem43 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents BarSubItem3 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarReportTabButtonsListItem1 As DevExpress.XtraReports.UserDesigner.BarReportTabButtonsListItem
    Friend WithEvents BarSubItem4 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents XrBarToolbarsListItem1 As DevExpress.XtraReports.UserDesigner.XRBarToolbarsListItem
    Friend WithEvents BarSubItem5 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarDockPanelsListItem1 As DevExpress.XtraReports.UserDesigner.BarDockPanelsListItem
    Friend WithEvents BarSubItem6 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents CommandColorBarItem1 As DevExpress.XtraReports.UserDesigner.CommandColorBarItem
    Friend WithEvents CommandColorBarItem2 As DevExpress.XtraReports.UserDesigner.CommandColorBarItem
    Friend WithEvents BarSubItem7 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents CommandBarItem1 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem2 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem3 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents BarSubItem8 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents CommandBarItem4 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem5 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem6 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem7 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents BarSubItem9 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents CommandBarItem9 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem10 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem11 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem12 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem13 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem14 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem8 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents BarSubItem10 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents CommandBarItem15 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem16 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem17 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem18 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents BarSubItem11 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents CommandBarItem19 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem20 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem21 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem22 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents BarSubItem12 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents CommandBarItem23 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem24 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem25 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem26 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents BarSubItem13 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents CommandBarItem27 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem28 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents BarSubItem14 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents CommandBarItem29 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents CommandBarItem30 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents DesignBar2 As DevExpress.XtraReports.UserDesigner.DesignBar
    Friend WithEvents DesignBar3 As DevExpress.XtraReports.UserDesigner.DesignBar
    Friend WithEvents BarEditItem1 As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RecentlyUsedItemsComboBox1 As DevExpress.XtraReports.UserDesigner.RecentlyUsedItemsComboBox
    Friend WithEvents BarEditItem2 As DevExpress.XtraBars.BarEditItem
    Friend WithEvents DesignRepositoryItemComboBox1 As DevExpress.XtraReports.UserDesigner.DesignRepositoryItemComboBox
    Friend WithEvents DesignBar4 As DevExpress.XtraReports.UserDesigner.DesignBar
    Friend WithEvents DesignBar5 As DevExpress.XtraReports.UserDesigner.DesignBar
    Friend WithEvents BarStaticItem1 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents DesignBar6 As DevExpress.XtraReports.UserDesigner.DesignBar
    Friend WithEvents CommandBarItem44 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents XrZoomBarEditItem1 As DevExpress.XtraReports.UserDesigner.XRZoomBarEditItem
    Friend WithEvents DesignRepositoryItemComboBox2 As DevExpress.XtraReports.UserDesigner.DesignRepositoryItemComboBox
    Friend WithEvents CommandBarItem45 As DevExpress.XtraReports.UserDesigner.CommandBarItem
    Friend WithEvents btn_guardar_formato As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ToolBoxDockPanel1 As DevExpress.XtraReports.UserDesigner.ToolBoxDockPanel
    Friend WithEvents ToolBoxDockPanel1_Container As DevExpress.XtraReports.UserDesigner.DesignControlContainer
    Friend WithEvents XrDesignDockManager3 As DevExpress.XtraReports.UserDesigner.XRDesignDockManager
    Friend WithEvents ReportExplorerDockPanel1 As DevExpress.XtraReports.UserDesigner.ReportExplorerDockPanel
    Friend WithEvents ReportExplorerDockPanel1_Container As DevExpress.XtraReports.UserDesigner.DesignControlContainer
    Friend WithEvents FieldListDockPanel1 As DevExpress.XtraReports.UserDesigner.FieldListDockPanel
    Friend WithEvents FieldListDockPanel1_Container As DevExpress.XtraReports.UserDesigner.DesignControlContainer
    Friend WithEvents PropertyGridDockPanel2 As DevExpress.XtraReports.UserDesigner.PropertyGridDockPanel
    Friend WithEvents PropertyGridDockPanel2_Container As DevExpress.XtraReports.UserDesigner.DesignControlContainer
    Friend WithEvents GroupAndSortDockPanel1 As DevExpress.XtraReports.UserDesigner.GroupAndSortDockPanel
    Friend WithEvents GroupAndSortDockPanel1_Container As DevExpress.XtraReports.UserDesigner.DesignControlContainer
    Friend WithEvents PropertyGridDockPanel1_Container As DevExpress.XtraReports.UserDesigner.DesignControlContainer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCargarDiseno))
        Me.XrDesignPanel1 = New DevExpress.XtraReports.UserDesigner.XRDesignPanel()
        Me.XrDesignDockManager1 = New DevExpress.XtraReports.UserDesigner.XRDesignDockManager(Me.components)
        Me.DockPanel1 = New DevExpress.XtraBars.Docking.DockPanel()
        Me.DockPanel1_Container = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.cmbimpresa = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cmbboolean = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cmbfecha = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cmbbordes = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cmbcambio = New DevExpress.XtraEditors.LookUpEdit()
        Me.XrDesignDockManager2 = New DevExpress.XtraReports.UserDesigner.XRDesignDockManager(Me.components)
        Me.hideContainerRight = New DevExpress.XtraBars.Docking.AutoHideContainer()
        Me.PropertyGridDockPanel1 = New DevExpress.XtraReports.UserDesigner.PropertyGridDockPanel()
        Me.PropertyGridDockPanel1_Container = New DevExpress.XtraReports.UserDesigner.DesignControlContainer()
        Me.XrDesignBarManager1 = New DevExpress.XtraReports.UserDesigner.XRDesignBarManager(Me.components)
        Me.DesignBar1 = New DevExpress.XtraReports.UserDesigner.DesignBar()
        Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem()
        Me.CommandBarItem31 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem39 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem32 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.btn_guardar = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem40 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem41 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.btn_guardar_formato = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItem2 = New DevExpress.XtraBars.BarSubItem()
        Me.CommandBarItem37 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem38 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem34 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem35 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem36 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem42 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem43 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.BarSubItem3 = New DevExpress.XtraBars.BarSubItem()
        Me.BarReportTabButtonsListItem1 = New DevExpress.XtraReports.UserDesigner.BarReportTabButtonsListItem()
        Me.BarSubItem4 = New DevExpress.XtraBars.BarSubItem()
        Me.XrBarToolbarsListItem1 = New DevExpress.XtraReports.UserDesigner.XRBarToolbarsListItem()
        Me.BarSubItem5 = New DevExpress.XtraBars.BarSubItem()
        Me.BarDockPanelsListItem1 = New DevExpress.XtraReports.UserDesigner.BarDockPanelsListItem()
        Me.BarSubItem6 = New DevExpress.XtraBars.BarSubItem()
        Me.CommandColorBarItem1 = New DevExpress.XtraReports.UserDesigner.CommandColorBarItem()
        Me.CommandColorBarItem2 = New DevExpress.XtraReports.UserDesigner.CommandColorBarItem()
        Me.BarSubItem7 = New DevExpress.XtraBars.BarSubItem()
        Me.CommandBarItem1 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem2 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem3 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.BarSubItem8 = New DevExpress.XtraBars.BarSubItem()
        Me.CommandBarItem4 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem5 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem6 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem7 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.BarSubItem9 = New DevExpress.XtraBars.BarSubItem()
        Me.CommandBarItem9 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem10 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem11 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem12 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem13 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem14 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem8 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.BarSubItem10 = New DevExpress.XtraBars.BarSubItem()
        Me.CommandBarItem15 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem16 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem17 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem18 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.BarSubItem11 = New DevExpress.XtraBars.BarSubItem()
        Me.CommandBarItem19 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem20 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem21 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem22 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.BarSubItem12 = New DevExpress.XtraBars.BarSubItem()
        Me.CommandBarItem23 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem24 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem25 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem26 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.BarSubItem13 = New DevExpress.XtraBars.BarSubItem()
        Me.CommandBarItem27 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem28 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.BarSubItem14 = New DevExpress.XtraBars.BarSubItem()
        Me.CommandBarItem29 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.CommandBarItem30 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.DesignBar2 = New DevExpress.XtraReports.UserDesigner.DesignBar()
        Me.DesignBar3 = New DevExpress.XtraReports.UserDesigner.DesignBar()
        Me.BarEditItem1 = New DevExpress.XtraBars.BarEditItem()
        Me.RecentlyUsedItemsComboBox1 = New DevExpress.XtraReports.UserDesigner.RecentlyUsedItemsComboBox()
        Me.BarEditItem2 = New DevExpress.XtraBars.BarEditItem()
        Me.DesignRepositoryItemComboBox1 = New DevExpress.XtraReports.UserDesigner.DesignRepositoryItemComboBox()
        Me.DesignBar4 = New DevExpress.XtraReports.UserDesigner.DesignBar()
        Me.DesignBar5 = New DevExpress.XtraReports.UserDesigner.DesignBar()
        Me.BarStaticItem1 = New DevExpress.XtraBars.BarStaticItem()
        Me.DesignBar6 = New DevExpress.XtraReports.UserDesigner.DesignBar()
        Me.CommandBarItem44 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.XrZoomBarEditItem1 = New DevExpress.XtraReports.UserDesigner.XRZoomBarEditItem()
        Me.DesignRepositoryItemComboBox2 = New DevExpress.XtraReports.UserDesigner.DesignRepositoryItemComboBox()
        Me.CommandBarItem45 = New DevExpress.XtraReports.UserDesigner.CommandBarItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.XrDesignDockManager3 = New DevExpress.XtraReports.UserDesigner.XRDesignDockManager(Me.components)
        Me.ReportExplorerDockPanel1 = New DevExpress.XtraReports.UserDesigner.ReportExplorerDockPanel()
        Me.ReportExplorerDockPanel1_Container = New DevExpress.XtraReports.UserDesigner.DesignControlContainer()
        Me.FieldListDockPanel1 = New DevExpress.XtraReports.UserDesigner.FieldListDockPanel()
        Me.FieldListDockPanel1_Container = New DevExpress.XtraReports.UserDesigner.DesignControlContainer()
        Me.PropertyGridDockPanel2 = New DevExpress.XtraReports.UserDesigner.PropertyGridDockPanel()
        Me.PropertyGridDockPanel2_Container = New DevExpress.XtraReports.UserDesigner.DesignControlContainer()
        Me.GroupAndSortDockPanel1 = New DevExpress.XtraReports.UserDesigner.GroupAndSortDockPanel()
        Me.GroupAndSortDockPanel1_Container = New DevExpress.XtraReports.UserDesigner.DesignControlContainer()
        Me.ToolBoxDockPanel1 = New DevExpress.XtraReports.UserDesigner.ToolBoxDockPanel()
        Me.ToolBoxDockPanel1_Container = New DevExpress.XtraReports.UserDesigner.DesignControlContainer()
        CType(Me.XrDesignPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrDesignDockManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DockPanel1.SuspendLayout()
        Me.DockPanel1_Container.SuspendLayout()
        CType(Me.cmbimpresa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbboolean.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbfecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbbordes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbcambio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrDesignDockManager2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hideContainerRight.SuspendLayout()
        Me.PropertyGridDockPanel1.SuspendLayout()
        CType(Me.XrDesignBarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RecentlyUsedItemsComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DesignRepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DesignRepositoryItemComboBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrDesignDockManager3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ReportExplorerDockPanel1.SuspendLayout()
        Me.FieldListDockPanel1.SuspendLayout()
        Me.PropertyGridDockPanel2.SuspendLayout()
        Me.GroupAndSortDockPanel1.SuspendLayout()
        Me.ToolBoxDockPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'XrDesignPanel1
        '
        Me.XrDesignPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XrDesignPanel1.Location = New System.Drawing.Point(165, 115)
        Me.XrDesignPanel1.Name = "XrDesignPanel1"
        Me.XrDesignPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.XrDesignPanel1.Size = New System.Drawing.Size(468, 414)
        Me.XrDesignPanel1.TabIndex = 5
        '
        'XrDesignDockManager1
        '
        Me.XrDesignDockManager1.Form = Me
        Me.XrDesignDockManager1.ImageStream = CType(resources.GetObject("XrDesignDockManager1.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.XrDesignDockManager1.RootPanels.AddRange(New DevExpress.XtraBars.Docking.DockPanel() {Me.DockPanel1})
        Me.XrDesignDockManager1.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "System.Windows.Forms.StatusBar", "DevExpress.XtraBars.Ribbon.RibbonStatusBar", "DevExpress.XtraBars.Ribbon.RibbonControl"})
        Me.XrDesignDockManager1.XRDesignPanel = Me.XrDesignPanel1
        '
        'DockPanel1
        '
        Me.DockPanel1.Controls.Add(Me.DockPanel1_Container)
        Me.DockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.DockPanel1.ID = New System.Guid("7f603330-77d0-4783-bbec-f86161f4e7d5")
        Me.DockPanel1.Location = New System.Drawing.Point(633, 115)
        Me.DockPanel1.Name = "DockPanel1"
        Me.DockPanel1.OriginalSize = New System.Drawing.Size(195, 554)
        Me.DockPanel1.Size = New System.Drawing.Size(195, 414)
        Me.DockPanel1.Text = "Opciones del Texto"
        '
        'DockPanel1_Container
        '
        Me.DockPanel1_Container.Controls.Add(Me.cmbimpresa)
        Me.DockPanel1_Container.Controls.Add(Me.LabelControl1)
        Me.DockPanel1_Container.Controls.Add(Me.cmbboolean)
        Me.DockPanel1_Container.Controls.Add(Me.LabelControl2)
        Me.DockPanel1_Container.Controls.Add(Me.cmbfecha)
        Me.DockPanel1_Container.Controls.Add(Me.cmbbordes)
        Me.DockPanel1_Container.Controls.Add(Me.LabelControl3)
        Me.DockPanel1_Container.Controls.Add(Me.LabelControl4)
        Me.DockPanel1_Container.Controls.Add(Me.LabelControl5)
        Me.DockPanel1_Container.Controls.Add(Me.cmbcambio)
        Me.DockPanel1_Container.Location = New System.Drawing.Point(4, 23)
        Me.DockPanel1_Container.Name = "DockPanel1_Container"
        Me.DockPanel1_Container.Size = New System.Drawing.Size(187, 387)
        Me.DockPanel1_Container.TabIndex = 0
        '
        'cmbimpresa
        '
        Me.cmbimpresa.Location = New System.Drawing.Point(24, 155)
        Me.cmbimpresa.Name = "cmbimpresa"
        Me.cmbimpresa.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmbimpresa.Properties.NullText = ""
        Me.cmbimpresa.Properties.ShowFooter = False
        Me.cmbimpresa.Properties.ShowHeader = False
        Me.cmbimpresa.Size = New System.Drawing.Size(152, 20)
        Me.cmbimpresa.TabIndex = 2
        Me.cmbimpresa.Visible = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(24, 17)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Visible:"
        '
        'cmbboolean
        '
        Me.cmbboolean.EditValue = "Si"
        Me.cmbboolean.Location = New System.Drawing.Point(79, 13)
        Me.cmbboolean.Name = "cmbboolean"
        Me.cmbboolean.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmbboolean.Properties.Items.AddRange(New Object() {"Si", "No"})
        Me.cmbboolean.Size = New System.Drawing.Size(100, 20)
        Me.cmbboolean.TabIndex = 0
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(24, 52)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "Formato:"
        '
        'cmbfecha
        '
        Me.cmbfecha.EditValue = "Fecha Corta"
        Me.cmbfecha.Location = New System.Drawing.Point(80, 52)
        Me.cmbfecha.Name = "cmbfecha"
        Me.cmbfecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmbfecha.Properties.Items.AddRange(New Object() {"Fecha Corta", "Fecha Larga", "Dia", "Mes", "A�o", "Numero"})
        Me.cmbfecha.Size = New System.Drawing.Size(100, 20)
        Me.cmbfecha.TabIndex = 0
        '
        'cmbbordes
        '
        Me.cmbbordes.EditValue = "Todos"
        Me.cmbbordes.Location = New System.Drawing.Point(80, 86)
        Me.cmbbordes.Name = "cmbbordes"
        Me.cmbbordes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmbbordes.Properties.Items.AddRange(New Object() {"Todos", "Ninguno", "Superior", "Inferior", "Derecho", "Izquierdo"})
        Me.cmbbordes.Size = New System.Drawing.Size(100, 20)
        Me.cmbbordes.TabIndex = 0
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(24, 86)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "Bordes:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(24, 138)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl4.TabIndex = 1
        Me.LabelControl4.Text = "Moneda Impresa :"
        Me.LabelControl4.Visible = False
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(24, 190)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl5.TabIndex = 1
        Me.LabelControl5.Text = "Moneda Tipo Cambio :"
        Me.LabelControl5.Visible = False
        '
        'cmbcambio
        '
        Me.cmbcambio.Location = New System.Drawing.Point(24, 207)
        Me.cmbcambio.Name = "cmbcambio"
        Me.cmbcambio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmbcambio.Properties.NullText = ""
        Me.cmbcambio.Properties.ShowFooter = False
        Me.cmbcambio.Properties.ShowHeader = False
        Me.cmbcambio.Size = New System.Drawing.Size(152, 20)
        Me.cmbcambio.TabIndex = 2
        Me.cmbcambio.Visible = False
        '
        'XrDesignDockManager2
        '
        Me.XrDesignDockManager2.AutoHideContainers.AddRange(New DevExpress.XtraBars.Docking.AutoHideContainer() {Me.hideContainerRight})
        Me.XrDesignDockManager2.Form = Me
        Me.XrDesignDockManager2.ImageStream = CType(resources.GetObject("XrDesignDockManager2.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.XrDesignDockManager2.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "System.Windows.Forms.StatusBar", "DevExpress.XtraBars.Ribbon.RibbonStatusBar", "DevExpress.XtraBars.Ribbon.RibbonControl"})
        Me.XrDesignDockManager2.XRDesignPanel = Me.XrDesignPanel1
        '
        'hideContainerRight
        '
        Me.hideContainerRight.BackColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(239, Byte), Integer))
        Me.hideContainerRight.Controls.Add(Me.PropertyGridDockPanel1)
        Me.hideContainerRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.hideContainerRight.Location = New System.Drawing.Point(828, 115)
        Me.hideContainerRight.Name = "hideContainerRight"
        Me.hideContainerRight.Size = New System.Drawing.Size(22, 414)
        '
        'PropertyGridDockPanel1
        '
        Me.PropertyGridDockPanel1.Controls.Add(Me.PropertyGridDockPanel1_Container)
        Me.PropertyGridDockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.PropertyGridDockPanel1.ID = New System.Guid("b38d12c3-cd06-4dec-b93d-63a0088e495a")
        Me.PropertyGridDockPanel1.ImageIndex = 1
        Me.PropertyGridDockPanel1.Location = New System.Drawing.Point(0, 0)
        Me.PropertyGridDockPanel1.Name = "PropertyGridDockPanel1"
        Me.PropertyGridDockPanel1.OriginalSize = New System.Drawing.Size(0, 0)
        Me.PropertyGridDockPanel1.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.PropertyGridDockPanel1.SavedIndex = 0
        Me.PropertyGridDockPanel1.ShowCategories = False
        Me.PropertyGridDockPanel1.Size = New System.Drawing.Size(250, 478)
        Me.PropertyGridDockPanel1.Text = "Property Grid"
        Me.PropertyGridDockPanel1.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide
        Me.PropertyGridDockPanel1.XRDesignPanel = Me.XrDesignPanel1
        '
        'PropertyGridDockPanel1_Container
        '
        Me.PropertyGridDockPanel1_Container.Location = New System.Drawing.Point(3, 25)
        Me.PropertyGridDockPanel1_Container.Name = "PropertyGridDockPanel1_Container"
        Me.PropertyGridDockPanel1_Container.Size = New System.Drawing.Size(244, 450)
        Me.PropertyGridDockPanel1_Container.TabIndex = 0
        '
        'XrDesignBarManager1
        '
        Me.XrDesignBarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.DesignBar1, Me.DesignBar2, Me.DesignBar3, Me.DesignBar4, Me.DesignBar5, Me.DesignBar6})
        Me.XrDesignBarManager1.DockControls.Add(Me.barDockControlTop)
        Me.XrDesignBarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.XrDesignBarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.XrDesignBarManager1.DockControls.Add(Me.barDockControlRight)
        Me.XrDesignBarManager1.DockManager = Me.XrDesignDockManager3
        Me.XrDesignBarManager1.FontNameBox = Me.RecentlyUsedItemsComboBox1
        Me.XrDesignBarManager1.FontNameEdit = Me.BarEditItem1
        Me.XrDesignBarManager1.FontSizeBox = Me.DesignRepositoryItemComboBox1
        Me.XrDesignBarManager1.FontSizeEdit = Me.BarEditItem2
        Me.XrDesignBarManager1.Form = Me
        Me.XrDesignBarManager1.FormattingToolbar = Me.DesignBar3
        Me.XrDesignBarManager1.HintStaticItem = Me.BarStaticItem1
        Me.XrDesignBarManager1.ImageStream = CType(resources.GetObject("XrDesignBarManager1.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.XrDesignBarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarEditItem1, Me.BarEditItem2, Me.CommandBarItem1, Me.CommandBarItem2, Me.CommandBarItem3, Me.CommandColorBarItem1, Me.CommandColorBarItem2, Me.CommandBarItem4, Me.CommandBarItem5, Me.CommandBarItem6, Me.CommandBarItem7, Me.CommandBarItem8, Me.CommandBarItem9, Me.CommandBarItem10, Me.CommandBarItem11, Me.CommandBarItem12, Me.CommandBarItem13, Me.CommandBarItem14, Me.CommandBarItem15, Me.CommandBarItem16, Me.CommandBarItem17, Me.CommandBarItem18, Me.CommandBarItem19, Me.CommandBarItem20, Me.CommandBarItem21, Me.CommandBarItem22, Me.CommandBarItem23, Me.CommandBarItem24, Me.CommandBarItem25, Me.CommandBarItem26, Me.CommandBarItem27, Me.CommandBarItem28, Me.CommandBarItem29, Me.CommandBarItem30, Me.CommandBarItem31, Me.CommandBarItem32, Me.btn_guardar, Me.CommandBarItem34, Me.CommandBarItem35, Me.CommandBarItem36, Me.CommandBarItem37, Me.CommandBarItem38, Me.BarStaticItem1, Me.BarSubItem1, Me.BarSubItem2, Me.BarSubItem3, Me.BarReportTabButtonsListItem1, Me.BarSubItem4, Me.XrBarToolbarsListItem1, Me.BarSubItem5, Me.BarDockPanelsListItem1, Me.BarSubItem6, Me.BarSubItem7, Me.BarSubItem8, Me.BarSubItem9, Me.BarSubItem10, Me.BarSubItem11, Me.BarSubItem12, Me.BarSubItem13, Me.BarSubItem14, Me.CommandBarItem39, Me.CommandBarItem40, Me.CommandBarItem41, Me.CommandBarItem42, Me.CommandBarItem43, Me.CommandBarItem44, Me.XrZoomBarEditItem1, Me.CommandBarItem45, Me.btn_guardar_formato})
        Me.XrDesignBarManager1.LayoutToolbar = Me.DesignBar4
        Me.XrDesignBarManager1.MainMenu = Me.DesignBar1
        Me.XrDesignBarManager1.MaxItemId = 72
        Me.XrDesignBarManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RecentlyUsedItemsComboBox1, Me.DesignRepositoryItemComboBox1, Me.DesignRepositoryItemComboBox2})
        Me.XrDesignBarManager1.StatusBar = Me.DesignBar5
        Me.XrDesignBarManager1.Toolbar = Me.DesignBar2
        Me.XrDesignBarManager1.XRDesignPanel = Me.XrDesignPanel1
        Me.XrDesignBarManager1.ZoomItem = Me.XrZoomBarEditItem1
        '
        'DesignBar1
        '
        Me.DesignBar1.BarName = "Main Menu"
        Me.DesignBar1.DockCol = 0
        Me.DesignBar1.DockRow = 0
        Me.DesignBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.DesignBar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem2), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem3), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem6)})
        Me.DesignBar1.OptionsBar.MultiLine = True
        Me.DesignBar1.OptionsBar.UseWholeRow = True
        Me.DesignBar1.Text = "Main Menu"
        '
        'BarSubItem1
        '
        Me.BarSubItem1.Caption = "&File"
        Me.BarSubItem1.Id = 43
        Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem31), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem39), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem32), New DevExpress.XtraBars.LinkPersistInfo(Me.btn_guardar, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem40), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem41, True), New DevExpress.XtraBars.LinkPersistInfo(Me.btn_guardar_formato)})
        Me.BarSubItem1.Name = "BarSubItem1"
        '
        'CommandBarItem31
        '
        Me.CommandBarItem31.Caption = "&New"
        Me.CommandBarItem31.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.NewReport
        Me.CommandBarItem31.Hint = "Create a new blank report"
        Me.CommandBarItem31.Id = 34
        Me.CommandBarItem31.ImageIndex = 9
        Me.CommandBarItem31.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N))
        Me.CommandBarItem31.Name = "CommandBarItem31"
        '
        'CommandBarItem39
        '
        Me.CommandBarItem39.Caption = "New with &Wizard..."
        Me.CommandBarItem39.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.NewReportWizard
        Me.CommandBarItem39.Hint = "Create a new report using the Wizard"
        Me.CommandBarItem39.Id = 60
        Me.CommandBarItem39.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.W))
        Me.CommandBarItem39.Name = "CommandBarItem39"
        '
        'CommandBarItem32
        '
        Me.CommandBarItem32.Caption = "&Open..."
        Me.CommandBarItem32.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.OpenFile
        Me.CommandBarItem32.Hint = "Open a report"
        Me.CommandBarItem32.Id = 35
        Me.CommandBarItem32.ImageIndex = 10
        Me.CommandBarItem32.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O))
        Me.CommandBarItem32.Name = "CommandBarItem32"
        '
        'btn_guardar
        '
        Me.btn_guardar.Caption = "&Save"
        Me.btn_guardar.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.SaveFile
        Me.btn_guardar.Enabled = False
        Me.btn_guardar.Hint = "Save a report"
        Me.btn_guardar.Id = 36
        Me.btn_guardar.ImageIndex = 11
        Me.btn_guardar.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S))
        Me.btn_guardar.Name = "btn_guardar"
        '
        'CommandBarItem40
        '
        Me.CommandBarItem40.Caption = "Save &As..."
        Me.CommandBarItem40.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.SaveFileAs
        Me.CommandBarItem40.Enabled = False
        Me.CommandBarItem40.Hint = "Save a report with a new name"
        Me.CommandBarItem40.Id = 61
        Me.CommandBarItem40.Name = "CommandBarItem40"
        '
        'CommandBarItem41
        '
        Me.CommandBarItem41.Caption = "E&xit"
        Me.CommandBarItem41.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.[Exit]
        Me.CommandBarItem41.Hint = "Close the designer"
        Me.CommandBarItem41.Id = 62
        Me.CommandBarItem41.Name = "CommandBarItem41"
        '
        'btn_guardar_formato
        '
        Me.btn_guardar_formato.Caption = "Guardar Dise�o Origina"
        Me.btn_guardar_formato.Id = 71
        Me.btn_guardar_formato.Name = "btn_guardar_formato"
        '
        'BarSubItem2
        '
        Me.BarSubItem2.Caption = "&Edit"
        Me.BarSubItem2.Id = 44
        Me.BarSubItem2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem37, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem38), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem34, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem35), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem36), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem42), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem43, True)})
        Me.BarSubItem2.Name = "BarSubItem2"
        '
        'CommandBarItem37
        '
        Me.CommandBarItem37.Caption = "&Undo"
        Me.CommandBarItem37.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.Undo
        Me.CommandBarItem37.Enabled = False
        Me.CommandBarItem37.Hint = "Undo the last operation"
        Me.CommandBarItem37.Id = 40
        Me.CommandBarItem37.ImageIndex = 15
        Me.CommandBarItem37.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Z))
        Me.CommandBarItem37.Name = "CommandBarItem37"
        '
        'CommandBarItem38
        '
        Me.CommandBarItem38.Caption = "&Redo"
        Me.CommandBarItem38.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.Redo
        Me.CommandBarItem38.Enabled = False
        Me.CommandBarItem38.Hint = "Redo the last operation"
        Me.CommandBarItem38.Id = 41
        Me.CommandBarItem38.ImageIndex = 16
        Me.CommandBarItem38.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Y))
        Me.CommandBarItem38.Name = "CommandBarItem38"
        '
        'CommandBarItem34
        '
        Me.CommandBarItem34.Caption = "Cu&t"
        Me.CommandBarItem34.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.Cut
        Me.CommandBarItem34.Enabled = False
        Me.CommandBarItem34.Hint = "Delete the control and copy it to the clipboard"
        Me.CommandBarItem34.Id = 37
        Me.CommandBarItem34.ImageIndex = 12
        Me.CommandBarItem34.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X))
        Me.CommandBarItem34.Name = "CommandBarItem34"
        '
        'CommandBarItem35
        '
        Me.CommandBarItem35.Caption = "&Copy"
        Me.CommandBarItem35.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.Copy
        Me.CommandBarItem35.Enabled = False
        Me.CommandBarItem35.Hint = "Copy the control to the clipboard"
        Me.CommandBarItem35.Id = 38
        Me.CommandBarItem35.ImageIndex = 13
        Me.CommandBarItem35.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C))
        Me.CommandBarItem35.Name = "CommandBarItem35"
        '
        'CommandBarItem36
        '
        Me.CommandBarItem36.Caption = "&Paste"
        Me.CommandBarItem36.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.Paste
        Me.CommandBarItem36.Enabled = False
        Me.CommandBarItem36.Hint = "Add the control from the clipboard"
        Me.CommandBarItem36.Id = 39
        Me.CommandBarItem36.ImageIndex = 14
        Me.CommandBarItem36.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V))
        Me.CommandBarItem36.Name = "CommandBarItem36"
        '
        'CommandBarItem42
        '
        Me.CommandBarItem42.Caption = "&Delete"
        Me.CommandBarItem42.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.Delete
        Me.CommandBarItem42.Enabled = False
        Me.CommandBarItem42.Hint = "Delete the control"
        Me.CommandBarItem42.Id = 63
        Me.CommandBarItem42.Name = "CommandBarItem42"
        '
        'CommandBarItem43
        '
        Me.CommandBarItem43.Caption = "Select &All"
        Me.CommandBarItem43.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.SelectAll
        Me.CommandBarItem43.Enabled = False
        Me.CommandBarItem43.Hint = "Select all the controls in the document"
        Me.CommandBarItem43.Id = 64
        Me.CommandBarItem43.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A))
        Me.CommandBarItem43.Name = "CommandBarItem43"
        '
        'BarSubItem3
        '
        Me.BarSubItem3.Caption = "&View"
        Me.BarSubItem3.Id = 45
        Me.BarSubItem3.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarReportTabButtonsListItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem4, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem5, True)})
        Me.BarSubItem3.Name = "BarSubItem3"
        '
        'BarReportTabButtonsListItem1
        '
        Me.BarReportTabButtonsListItem1.Caption = "Tab Buttons"
        Me.BarReportTabButtonsListItem1.Id = 46
        Me.BarReportTabButtonsListItem1.Name = "BarReportTabButtonsListItem1"
        '
        'BarSubItem4
        '
        Me.BarSubItem4.Caption = "&Toolbars"
        Me.BarSubItem4.Id = 47
        Me.BarSubItem4.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.XrBarToolbarsListItem1)})
        Me.BarSubItem4.Name = "BarSubItem4"
        '
        'XrBarToolbarsListItem1
        '
        Me.XrBarToolbarsListItem1.Caption = "&Toolbars"
        Me.XrBarToolbarsListItem1.Id = 48
        Me.XrBarToolbarsListItem1.Name = "XrBarToolbarsListItem1"
        '
        'BarSubItem5
        '
        Me.BarSubItem5.Caption = "&Windows"
        Me.BarSubItem5.Id = 49
        Me.BarSubItem5.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarDockPanelsListItem1)})
        Me.BarSubItem5.Name = "BarSubItem5"
        '
        'BarDockPanelsListItem1
        '
        Me.BarDockPanelsListItem1.Caption = "&Windows"
        Me.BarDockPanelsListItem1.Id = 50
        Me.BarDockPanelsListItem1.Name = "BarDockPanelsListItem1"
        Me.BarDockPanelsListItem1.ShowCustomizationItem = False
        Me.BarDockPanelsListItem1.ShowDockPanels = True
        Me.BarDockPanelsListItem1.ShowToolbars = False
        '
        'BarSubItem6
        '
        Me.BarSubItem6.Caption = "Fo&rmat"
        Me.BarSubItem6.Id = 51
        Me.BarSubItem6.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.CommandColorBarItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandColorBarItem2), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem7, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem8), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem9, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem10), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem11, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem12), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem13, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem14, True)})
        Me.BarSubItem6.Name = "BarSubItem6"
        '
        'CommandColorBarItem1
        '
        Me.CommandColorBarItem1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown
        Me.CommandColorBarItem1.Caption = "For&eground Color"
        Me.CommandColorBarItem1.CloseSubMenuOnClick = False
        Me.CommandColorBarItem1.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.ForeColor
        Me.CommandColorBarItem1.Enabled = False
        Me.CommandColorBarItem1.Glyph = CType(resources.GetObject("CommandColorBarItem1.Glyph"), System.Drawing.Image)
        Me.CommandColorBarItem1.Hint = "Set the foreground color of the control"
        Me.CommandColorBarItem1.Id = 5
        Me.CommandColorBarItem1.Name = "CommandColorBarItem1"
        '
        'CommandColorBarItem2
        '
        Me.CommandColorBarItem2.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown
        Me.CommandColorBarItem2.Caption = "Bac&kground Color"
        Me.CommandColorBarItem2.CloseSubMenuOnClick = False
        Me.CommandColorBarItem2.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.BackColor
        Me.CommandColorBarItem2.Enabled = False
        Me.CommandColorBarItem2.Glyph = CType(resources.GetObject("CommandColorBarItem2.Glyph"), System.Drawing.Image)
        Me.CommandColorBarItem2.Hint = "Set the background color of the control"
        Me.CommandColorBarItem2.Id = 6
        Me.CommandColorBarItem2.Name = "CommandColorBarItem2"
        '
        'BarSubItem7
        '
        Me.BarSubItem7.Caption = "&Font"
        Me.BarSubItem7.Id = 52
        Me.BarSubItem7.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem1, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem2), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem3)})
        Me.BarSubItem7.Name = "BarSubItem7"
        '
        'CommandBarItem1
        '
        Me.CommandBarItem1.Caption = "&Bold"
        Me.CommandBarItem1.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.FontBold
        Me.CommandBarItem1.Enabled = False
        Me.CommandBarItem1.Hint = "Make the font bold"
        Me.CommandBarItem1.Id = 2
        Me.CommandBarItem1.ImageIndex = 0
        Me.CommandBarItem1.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.B))
        Me.CommandBarItem1.Name = "CommandBarItem1"
        '
        'CommandBarItem2
        '
        Me.CommandBarItem2.Caption = "&Italic"
        Me.CommandBarItem2.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.FontItalic
        Me.CommandBarItem2.Enabled = False
        Me.CommandBarItem2.Hint = "Make the font italic"
        Me.CommandBarItem2.Id = 3
        Me.CommandBarItem2.ImageIndex = 1
        Me.CommandBarItem2.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.I))
        Me.CommandBarItem2.Name = "CommandBarItem2"
        '
        'CommandBarItem3
        '
        Me.CommandBarItem3.Caption = "&Underline"
        Me.CommandBarItem3.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.FontUnderline
        Me.CommandBarItem3.Enabled = False
        Me.CommandBarItem3.Hint = "Underline the font"
        Me.CommandBarItem3.Id = 4
        Me.CommandBarItem3.ImageIndex = 2
        Me.CommandBarItem3.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.U))
        Me.CommandBarItem3.Name = "CommandBarItem3"
        '
        'BarSubItem8
        '
        Me.BarSubItem8.Caption = "&Justify"
        Me.BarSubItem8.Id = 53
        Me.BarSubItem8.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem4, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem5), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem6), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem7)})
        Me.BarSubItem8.Name = "BarSubItem8"
        '
        'CommandBarItem4
        '
        Me.CommandBarItem4.Caption = "&Left"
        Me.CommandBarItem4.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.JustifyLeft
        Me.CommandBarItem4.Enabled = False
        Me.CommandBarItem4.Hint = "Align the control's text to the left"
        Me.CommandBarItem4.Id = 7
        Me.CommandBarItem4.ImageIndex = 5
        Me.CommandBarItem4.Name = "CommandBarItem4"
        '
        'CommandBarItem5
        '
        Me.CommandBarItem5.Caption = "&Center"
        Me.CommandBarItem5.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.JustifyCenter
        Me.CommandBarItem5.Enabled = False
        Me.CommandBarItem5.Hint = "Align the control's text to the center"
        Me.CommandBarItem5.Id = 8
        Me.CommandBarItem5.ImageIndex = 6
        Me.CommandBarItem5.Name = "CommandBarItem5"
        '
        'CommandBarItem6
        '
        Me.CommandBarItem6.Caption = "&Rights"
        Me.CommandBarItem6.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.JustifyRight
        Me.CommandBarItem6.Enabled = False
        Me.CommandBarItem6.Hint = "Align the control's text to the right"
        Me.CommandBarItem6.Id = 9
        Me.CommandBarItem6.ImageIndex = 7
        Me.CommandBarItem6.Name = "CommandBarItem6"
        '
        'CommandBarItem7
        '
        Me.CommandBarItem7.Caption = "&Justify"
        Me.CommandBarItem7.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.JustifyJustify
        Me.CommandBarItem7.Enabled = False
        Me.CommandBarItem7.Hint = "Justify the control's text"
        Me.CommandBarItem7.Id = 10
        Me.CommandBarItem7.ImageIndex = 8
        Me.CommandBarItem7.Name = "CommandBarItem7"
        '
        'BarSubItem9
        '
        Me.BarSubItem9.Caption = "&Align"
        Me.BarSubItem9.Id = 54
        Me.BarSubItem9.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem9, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem10), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem11), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem12, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem13), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem14), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem8, True)})
        Me.BarSubItem9.Name = "BarSubItem9"
        '
        'CommandBarItem9
        '
        Me.CommandBarItem9.Caption = "&Lefts"
        Me.CommandBarItem9.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.AlignLeft
        Me.CommandBarItem9.Enabled = False
        Me.CommandBarItem9.Hint = "Left align the selected controls"
        Me.CommandBarItem9.Id = 12
        Me.CommandBarItem9.ImageIndex = 18
        Me.CommandBarItem9.Name = "CommandBarItem9"
        '
        'CommandBarItem10
        '
        Me.CommandBarItem10.Caption = "&Centers"
        Me.CommandBarItem10.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.AlignVerticalCenters
        Me.CommandBarItem10.Enabled = False
        Me.CommandBarItem10.Hint = "Align the centers of the selected controls vertically"
        Me.CommandBarItem10.Id = 13
        Me.CommandBarItem10.ImageIndex = 19
        Me.CommandBarItem10.Name = "CommandBarItem10"
        '
        'CommandBarItem11
        '
        Me.CommandBarItem11.Caption = "&Rights"
        Me.CommandBarItem11.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.AlignRight
        Me.CommandBarItem11.Enabled = False
        Me.CommandBarItem11.Hint = "Right align the selected controls"
        Me.CommandBarItem11.Id = 14
        Me.CommandBarItem11.ImageIndex = 20
        Me.CommandBarItem11.Name = "CommandBarItem11"
        '
        'CommandBarItem12
        '
        Me.CommandBarItem12.Caption = "&Tops"
        Me.CommandBarItem12.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.AlignTop
        Me.CommandBarItem12.Enabled = False
        Me.CommandBarItem12.Hint = "Align the tops of the selected controls"
        Me.CommandBarItem12.Id = 15
        Me.CommandBarItem12.ImageIndex = 21
        Me.CommandBarItem12.Name = "CommandBarItem12"
        '
        'CommandBarItem13
        '
        Me.CommandBarItem13.Caption = "&Middles"
        Me.CommandBarItem13.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.AlignHorizontalCenters
        Me.CommandBarItem13.Enabled = False
        Me.CommandBarItem13.Hint = "Align the centers of the selected controls horizontally"
        Me.CommandBarItem13.Id = 16
        Me.CommandBarItem13.ImageIndex = 22
        Me.CommandBarItem13.Name = "CommandBarItem13"
        '
        'CommandBarItem14
        '
        Me.CommandBarItem14.Caption = "&Bottoms"
        Me.CommandBarItem14.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.AlignBottom
        Me.CommandBarItem14.Enabled = False
        Me.CommandBarItem14.Hint = "Align the bottoms of the selected controls"
        Me.CommandBarItem14.Id = 17
        Me.CommandBarItem14.ImageIndex = 23
        Me.CommandBarItem14.Name = "CommandBarItem14"
        '
        'CommandBarItem8
        '
        Me.CommandBarItem8.Caption = "to &Grid"
        Me.CommandBarItem8.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.AlignToGrid
        Me.CommandBarItem8.Enabled = False
        Me.CommandBarItem8.Hint = "Align the positions of the selected controls to the grid"
        Me.CommandBarItem8.Id = 11
        Me.CommandBarItem8.ImageIndex = 17
        Me.CommandBarItem8.Name = "CommandBarItem8"
        '
        'BarSubItem10
        '
        Me.BarSubItem10.Caption = "&Make Same Size"
        Me.BarSubItem10.Id = 55
        Me.BarSubItem10.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem15, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem16), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem17), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem18)})
        Me.BarSubItem10.Name = "BarSubItem10"
        '
        'CommandBarItem15
        '
        Me.CommandBarItem15.Caption = "&Width"
        Me.CommandBarItem15.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.SizeToControlWidth
        Me.CommandBarItem15.Enabled = False
        Me.CommandBarItem15.Hint = "Make the selected controls have the same width"
        Me.CommandBarItem15.Id = 18
        Me.CommandBarItem15.ImageIndex = 24
        Me.CommandBarItem15.Name = "CommandBarItem15"
        '
        'CommandBarItem16
        '
        Me.CommandBarItem16.Caption = "Size to Gri&d"
        Me.CommandBarItem16.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.SizeToGrid
        Me.CommandBarItem16.Enabled = False
        Me.CommandBarItem16.Hint = "Size the selected controls to the grid"
        Me.CommandBarItem16.Id = 19
        Me.CommandBarItem16.ImageIndex = 25
        Me.CommandBarItem16.Name = "CommandBarItem16"
        '
        'CommandBarItem17
        '
        Me.CommandBarItem17.Caption = "&Height"
        Me.CommandBarItem17.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.SizeToControlHeight
        Me.CommandBarItem17.Enabled = False
        Me.CommandBarItem17.Hint = "Make the selected controls have the same height"
        Me.CommandBarItem17.Id = 20
        Me.CommandBarItem17.ImageIndex = 26
        Me.CommandBarItem17.Name = "CommandBarItem17"
        '
        'CommandBarItem18
        '
        Me.CommandBarItem18.Caption = "&Both"
        Me.CommandBarItem18.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.SizeToControl
        Me.CommandBarItem18.Enabled = False
        Me.CommandBarItem18.Hint = "Make the selected controls the same size"
        Me.CommandBarItem18.Id = 21
        Me.CommandBarItem18.ImageIndex = 27
        Me.CommandBarItem18.Name = "CommandBarItem18"
        '
        'BarSubItem11
        '
        Me.BarSubItem11.Caption = "&Horizontal Spacing"
        Me.BarSubItem11.Id = 56
        Me.BarSubItem11.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem19, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem20), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem21), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem22)})
        Me.BarSubItem11.Name = "BarSubItem11"
        '
        'CommandBarItem19
        '
        Me.CommandBarItem19.Caption = "Make &Equal"
        Me.CommandBarItem19.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.HorizSpaceMakeEqual
        Me.CommandBarItem19.Enabled = False
        Me.CommandBarItem19.Hint = "Make the spacing between the selected controls equal"
        Me.CommandBarItem19.Id = 22
        Me.CommandBarItem19.ImageIndex = 28
        Me.CommandBarItem19.Name = "CommandBarItem19"
        '
        'CommandBarItem20
        '
        Me.CommandBarItem20.Caption = "&Increase"
        Me.CommandBarItem20.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.HorizSpaceIncrease
        Me.CommandBarItem20.Enabled = False
        Me.CommandBarItem20.Hint = "Increase the spacing between the selected controls"
        Me.CommandBarItem20.Id = 23
        Me.CommandBarItem20.ImageIndex = 29
        Me.CommandBarItem20.Name = "CommandBarItem20"
        '
        'CommandBarItem21
        '
        Me.CommandBarItem21.Caption = "&Decrease"
        Me.CommandBarItem21.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.HorizSpaceDecrease
        Me.CommandBarItem21.Enabled = False
        Me.CommandBarItem21.Hint = "Decrease the spacing between the selected controls"
        Me.CommandBarItem21.Id = 24
        Me.CommandBarItem21.ImageIndex = 30
        Me.CommandBarItem21.Name = "CommandBarItem21"
        '
        'CommandBarItem22
        '
        Me.CommandBarItem22.Caption = "&Remove"
        Me.CommandBarItem22.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.HorizSpaceConcatenate
        Me.CommandBarItem22.Enabled = False
        Me.CommandBarItem22.Hint = "Remove the spacing between the selected controls"
        Me.CommandBarItem22.Id = 25
        Me.CommandBarItem22.ImageIndex = 31
        Me.CommandBarItem22.Name = "CommandBarItem22"
        '
        'BarSubItem12
        '
        Me.BarSubItem12.Caption = "&Vertical Spacing"
        Me.BarSubItem12.Id = 57
        Me.BarSubItem12.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem23, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem24), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem25), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem26)})
        Me.BarSubItem12.Name = "BarSubItem12"
        '
        'CommandBarItem23
        '
        Me.CommandBarItem23.Caption = "Make &Equal"
        Me.CommandBarItem23.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.VertSpaceMakeEqual
        Me.CommandBarItem23.Enabled = False
        Me.CommandBarItem23.Hint = "Make the spacing between the selected controls equal"
        Me.CommandBarItem23.Id = 26
        Me.CommandBarItem23.ImageIndex = 32
        Me.CommandBarItem23.Name = "CommandBarItem23"
        '
        'CommandBarItem24
        '
        Me.CommandBarItem24.Caption = "&Increase"
        Me.CommandBarItem24.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.VertSpaceIncrease
        Me.CommandBarItem24.Enabled = False
        Me.CommandBarItem24.Hint = "Increase the spacing between the selected controls"
        Me.CommandBarItem24.Id = 27
        Me.CommandBarItem24.ImageIndex = 33
        Me.CommandBarItem24.Name = "CommandBarItem24"
        '
        'CommandBarItem25
        '
        Me.CommandBarItem25.Caption = "&Decrease"
        Me.CommandBarItem25.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.VertSpaceDecrease
        Me.CommandBarItem25.Enabled = False
        Me.CommandBarItem25.Hint = "Decrease the spacing between the selected controls"
        Me.CommandBarItem25.Id = 28
        Me.CommandBarItem25.ImageIndex = 34
        Me.CommandBarItem25.Name = "CommandBarItem25"
        '
        'CommandBarItem26
        '
        Me.CommandBarItem26.Caption = "&Remove"
        Me.CommandBarItem26.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.VertSpaceConcatenate
        Me.CommandBarItem26.Enabled = False
        Me.CommandBarItem26.Hint = "Remove the spacing between the selected controls"
        Me.CommandBarItem26.Id = 29
        Me.CommandBarItem26.ImageIndex = 35
        Me.CommandBarItem26.Name = "CommandBarItem26"
        '
        'BarSubItem13
        '
        Me.BarSubItem13.Caption = "&Center in Form"
        Me.BarSubItem13.Id = 58
        Me.BarSubItem13.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem27, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem28)})
        Me.BarSubItem13.Name = "BarSubItem13"
        '
        'CommandBarItem27
        '
        Me.CommandBarItem27.Caption = "&Horizontally"
        Me.CommandBarItem27.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.CenterHorizontally
        Me.CommandBarItem27.Enabled = False
        Me.CommandBarItem27.Hint = "Horizontally center the selected controls within a band"
        Me.CommandBarItem27.Id = 30
        Me.CommandBarItem27.ImageIndex = 36
        Me.CommandBarItem27.Name = "CommandBarItem27"
        '
        'CommandBarItem28
        '
        Me.CommandBarItem28.Caption = "&Vertically"
        Me.CommandBarItem28.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.CenterVertically
        Me.CommandBarItem28.Enabled = False
        Me.CommandBarItem28.Hint = "Vertically center the selected controls within a band"
        Me.CommandBarItem28.Id = 31
        Me.CommandBarItem28.ImageIndex = 37
        Me.CommandBarItem28.Name = "CommandBarItem28"
        '
        'BarSubItem14
        '
        Me.BarSubItem14.Caption = "&Order"
        Me.BarSubItem14.Id = 59
        Me.BarSubItem14.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem29, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem30)})
        Me.BarSubItem14.Name = "BarSubItem14"
        '
        'CommandBarItem29
        '
        Me.CommandBarItem29.Caption = "&Bring to Front"
        Me.CommandBarItem29.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.BringToFront
        Me.CommandBarItem29.Enabled = False
        Me.CommandBarItem29.Hint = "Bring the selected controls to the front"
        Me.CommandBarItem29.Id = 32
        Me.CommandBarItem29.ImageIndex = 38
        Me.CommandBarItem29.Name = "CommandBarItem29"
        '
        'CommandBarItem30
        '
        Me.CommandBarItem30.Caption = "&Send to Back"
        Me.CommandBarItem30.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.SendToBack
        Me.CommandBarItem30.Enabled = False
        Me.CommandBarItem30.Hint = "Move the selected controls to the back"
        Me.CommandBarItem30.Id = 33
        Me.CommandBarItem30.ImageIndex = 39
        Me.CommandBarItem30.Name = "CommandBarItem30"
        '
        'DesignBar2
        '
        Me.DesignBar2.BarName = "Toolbar"
        Me.DesignBar2.DockCol = 0
        Me.DesignBar2.DockRow = 1
        Me.DesignBar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.DesignBar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem31), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem32), New DevExpress.XtraBars.LinkPersistInfo(Me.btn_guardar), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem34, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem35), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem36), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem37, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem38)})
        Me.DesignBar2.Text = "Toolbar"
        '
        'DesignBar3
        '
        Me.DesignBar3.BarName = "Formatting Toolbar"
        Me.DesignBar3.DockCol = 0
        Me.DesignBar3.DockRow = 2
        Me.DesignBar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.DesignBar3.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarEditItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.BarEditItem2), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem2), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem3), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandColorBarItem1, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandColorBarItem2), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem4, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem5), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem6), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem7)})
        Me.DesignBar3.Text = "Formatting Toolbar"
        '
        'BarEditItem1
        '
        Me.BarEditItem1.Caption = "Font Name"
        Me.BarEditItem1.Edit = Me.RecentlyUsedItemsComboBox1
        Me.BarEditItem1.Hint = "Font Name"
        Me.BarEditItem1.Id = 0
        Me.BarEditItem1.Name = "BarEditItem1"
        Me.BarEditItem1.Width = 120
        '
        'RecentlyUsedItemsComboBox1
        '
        Me.RecentlyUsedItemsComboBox1.AutoHeight = False
        Me.RecentlyUsedItemsComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RecentlyUsedItemsComboBox1.DropDownRows = 12
        Me.RecentlyUsedItemsComboBox1.Name = "RecentlyUsedItemsComboBox1"
        '
        'BarEditItem2
        '
        Me.BarEditItem2.Caption = "Font Size"
        Me.BarEditItem2.Edit = Me.DesignRepositoryItemComboBox1
        Me.BarEditItem2.Hint = "Font Size"
        Me.BarEditItem2.Id = 1
        Me.BarEditItem2.Name = "BarEditItem2"
        Me.BarEditItem2.Width = 55
        '
        'DesignRepositoryItemComboBox1
        '
        Me.DesignRepositoryItemComboBox1.AutoHeight = False
        Me.DesignRepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DesignRepositoryItemComboBox1.Name = "DesignRepositoryItemComboBox1"
        '
        'DesignBar4
        '
        Me.DesignBar4.BarName = "Layout Toolbar"
        Me.DesignBar4.DockCol = 0
        Me.DesignBar4.DockRow = 3
        Me.DesignBar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.DesignBar4.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem8), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem9, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem10), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem11), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem12, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem13), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem14), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem15, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem16), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem17), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem18), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem19, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem20), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem21), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem22), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem23, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem24), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem25), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem26), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem27, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem28), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem29, True), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem30)})
        Me.DesignBar4.Text = "Layout Toolbar"
        '
        'DesignBar5
        '
        Me.DesignBar5.BarName = "Status Bar"
        Me.DesignBar5.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.DesignBar5.DockCol = 0
        Me.DesignBar5.DockRow = 0
        Me.DesignBar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.DesignBar5.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem1)})
        Me.DesignBar5.OptionsBar.AllowQuickCustomization = False
        Me.DesignBar5.OptionsBar.DrawDragBorder = False
        Me.DesignBar5.OptionsBar.UseWholeRow = True
        Me.DesignBar5.Text = "Status Bar"
        '
        'BarStaticItem1
        '
        Me.BarStaticItem1.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.BarStaticItem1.Id = 42
        Me.BarStaticItem1.Name = "BarStaticItem1"
        Me.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near
        Me.BarStaticItem1.Width = 32
        '
        'DesignBar6
        '
        Me.DesignBar6.BarName = "Zoom Toolbar"
        Me.DesignBar6.DockCol = 1
        Me.DesignBar6.DockRow = 1
        Me.DesignBar6.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.DesignBar6.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem44), New DevExpress.XtraBars.LinkPersistInfo(Me.XrZoomBarEditItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.CommandBarItem45)})
        Me.DesignBar6.Offset = 257
        Me.DesignBar6.Text = "Zoom Toolbar"
        '
        'CommandBarItem44
        '
        Me.CommandBarItem44.Caption = "Zoom Out"
        Me.CommandBarItem44.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.ZoomOut
        Me.CommandBarItem44.Enabled = False
        Me.CommandBarItem44.Hint = "Zoom out the design surface"
        Me.CommandBarItem44.Id = 65
        Me.CommandBarItem44.ImageIndex = 40
        Me.CommandBarItem44.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Subtract))
        Me.CommandBarItem44.Name = "CommandBarItem44"
        '
        'XrZoomBarEditItem1
        '
        Me.XrZoomBarEditItem1.Caption = "Zoom"
        Me.XrZoomBarEditItem1.Edit = Me.DesignRepositoryItemComboBox2
        Me.XrZoomBarEditItem1.Enabled = False
        Me.XrZoomBarEditItem1.Hint = "Select or input the zoom factor"
        Me.XrZoomBarEditItem1.Id = 66
        Me.XrZoomBarEditItem1.Name = "XrZoomBarEditItem1"
        Me.XrZoomBarEditItem1.Width = 70
        '
        'DesignRepositoryItemComboBox2
        '
        Me.DesignRepositoryItemComboBox2.AutoComplete = False
        Me.DesignRepositoryItemComboBox2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DesignRepositoryItemComboBox2.Name = "DesignRepositoryItemComboBox2"
        '
        'CommandBarItem45
        '
        Me.CommandBarItem45.Caption = "Zoom In"
        Me.CommandBarItem45.Command = DevExpress.XtraReports.UserDesigner.ReportCommand.ZoomIn
        Me.CommandBarItem45.Enabled = False
        Me.CommandBarItem45.Hint = "Zoom in the design surface"
        Me.CommandBarItem45.Id = 67
        Me.CommandBarItem45.ImageIndex = 41
        Me.CommandBarItem45.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Add))
        Me.CommandBarItem45.Name = "CommandBarItem45"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(850, 115)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 529)
        Me.barDockControlBottom.Size = New System.Drawing.Size(850, 25)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 115)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 414)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(850, 115)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 414)
        '
        'XrDesignDockManager3
        '
        Me.XrDesignDockManager3.Form = Me
        Me.XrDesignDockManager3.HiddenPanels.AddRange(New DevExpress.XtraBars.Docking.DockPanel() {Me.ReportExplorerDockPanel1, Me.FieldListDockPanel1, Me.PropertyGridDockPanel2, Me.GroupAndSortDockPanel1})
        Me.XrDesignDockManager3.ImageStream = CType(resources.GetObject("XrDesignDockManager3.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.XrDesignDockManager3.MenuManager = Me.XrDesignBarManager1
        Me.XrDesignDockManager3.RootPanels.AddRange(New DevExpress.XtraBars.Docking.DockPanel() {Me.ToolBoxDockPanel1})
        Me.XrDesignDockManager3.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "DevExpress.XtraBars.StandaloneBarDockControl", "System.Windows.Forms.StatusBar", "DevExpress.XtraBars.Ribbon.RibbonStatusBar", "DevExpress.XtraBars.Ribbon.RibbonControl"})
        Me.XrDesignDockManager3.XRDesignPanel = Me.XrDesignPanel1
        '
        'ReportExplorerDockPanel1
        '
        Me.ReportExplorerDockPanel1.Controls.Add(Me.ReportExplorerDockPanel1_Container)
        Me.ReportExplorerDockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill
        Me.ReportExplorerDockPanel1.ID = New System.Guid("fb3ec6cc-3b9b-4b9c-91cf-cff78c1edbf1")
        Me.ReportExplorerDockPanel1.ImageIndex = 2
        Me.ReportExplorerDockPanel1.Location = New System.Drawing.Point(3, 29)
        Me.ReportExplorerDockPanel1.Name = "ReportExplorerDockPanel1"
        Me.ReportExplorerDockPanel1.OriginalSize = New System.Drawing.Size(0, 0)
        Me.ReportExplorerDockPanel1.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Fill
        Me.ReportExplorerDockPanel1.SavedIndex = 0
        Me.ReportExplorerDockPanel1.SavedParent = Me.FieldListDockPanel1
        Me.ReportExplorerDockPanel1.SavedTabbed = True
        Me.ReportExplorerDockPanel1.Size = New System.Drawing.Size(244, 158)
        Me.ReportExplorerDockPanel1.Text = "Report Explorer"
        Me.ReportExplorerDockPanel1.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        Me.ReportExplorerDockPanel1.XRDesignPanel = Me.XrDesignPanel1
        '
        'ReportExplorerDockPanel1_Container
        '
        Me.ReportExplorerDockPanel1_Container.Location = New System.Drawing.Point(0, 0)
        Me.ReportExplorerDockPanel1_Container.Name = "ReportExplorerDockPanel1_Container"
        Me.ReportExplorerDockPanel1_Container.Size = New System.Drawing.Size(244, 158)
        Me.ReportExplorerDockPanel1_Container.TabIndex = 0
        '
        'FieldListDockPanel1
        '
        Me.FieldListDockPanel1.Controls.Add(Me.FieldListDockPanel1_Container)
        Me.FieldListDockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill
        Me.FieldListDockPanel1.ID = New System.Guid("faf69838-a93f-4114-83e8-d0d09cc5ce95")
        Me.FieldListDockPanel1.ImageIndex = 0
        Me.FieldListDockPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FieldListDockPanel1.Name = "FieldListDockPanel1"
        Me.FieldListDockPanel1.OriginalSize = New System.Drawing.Size(0, 0)
        Me.FieldListDockPanel1.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Fill
        Me.FieldListDockPanel1.SavedIndex = 0
        Me.FieldListDockPanel1.SavedParent = Me.PropertyGridDockPanel2
        Me.FieldListDockPanel1.Size = New System.Drawing.Size(250, 212)
        Me.FieldListDockPanel1.Text = "Field List"
        Me.FieldListDockPanel1.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        Me.FieldListDockPanel1.XRDesignPanel = Me.XrDesignPanel1
        '
        'FieldListDockPanel1_Container
        '
        Me.FieldListDockPanel1_Container.Location = New System.Drawing.Point(3, 29)
        Me.FieldListDockPanel1_Container.Name = "FieldListDockPanel1_Container"
        Me.FieldListDockPanel1_Container.Size = New System.Drawing.Size(244, 180)
        Me.FieldListDockPanel1_Container.TabIndex = 0
        '
        'PropertyGridDockPanel2
        '
        Me.PropertyGridDockPanel2.Controls.Add(Me.PropertyGridDockPanel2_Container)
        Me.PropertyGridDockPanel2.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.PropertyGridDockPanel2.ID = New System.Guid("b38d12c3-cd06-4dec-b93d-63a0088e495a")
        Me.PropertyGridDockPanel2.ImageIndex = 1
        Me.PropertyGridDockPanel2.Location = New System.Drawing.Point(600, 105)
        Me.PropertyGridDockPanel2.Name = "PropertyGridDockPanel2"
        Me.PropertyGridDockPanel2.OriginalSize = New System.Drawing.Size(0, 0)
        Me.PropertyGridDockPanel2.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right
        Me.PropertyGridDockPanel2.SavedIndex = 0
        Me.PropertyGridDockPanel2.Size = New System.Drawing.Size(250, 424)
        Me.PropertyGridDockPanel2.Text = "Property Grid"
        Me.PropertyGridDockPanel2.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        Me.PropertyGridDockPanel2.XRDesignPanel = Me.XrDesignPanel1
        '
        'PropertyGridDockPanel2_Container
        '
        Me.PropertyGridDockPanel2_Container.Location = New System.Drawing.Point(3, 29)
        Me.PropertyGridDockPanel2_Container.Name = "PropertyGridDockPanel2_Container"
        Me.PropertyGridDockPanel2_Container.Size = New System.Drawing.Size(244, 392)
        Me.PropertyGridDockPanel2_Container.TabIndex = 0
        '
        'GroupAndSortDockPanel1
        '
        Me.GroupAndSortDockPanel1.Controls.Add(Me.GroupAndSortDockPanel1_Container)
        Me.GroupAndSortDockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom
        Me.GroupAndSortDockPanel1.ID = New System.Guid("4bab159e-c495-4d67-87dc-f4e895da443e")
        Me.GroupAndSortDockPanel1.ImageIndex = 4
        Me.GroupAndSortDockPanel1.Location = New System.Drawing.Point(165, 369)
        Me.GroupAndSortDockPanel1.Name = "GroupAndSortDockPanel1"
        Me.GroupAndSortDockPanel1.OriginalSize = New System.Drawing.Size(0, 0)
        Me.GroupAndSortDockPanel1.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Bottom
        Me.GroupAndSortDockPanel1.SavedIndex = 1
        Me.GroupAndSortDockPanel1.Size = New System.Drawing.Size(685, 160)
        Me.GroupAndSortDockPanel1.Text = "Group and Sort"
        Me.GroupAndSortDockPanel1.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        Me.GroupAndSortDockPanel1.XRDesignPanel = Me.XrDesignPanel1
        '
        'GroupAndSortDockPanel1_Container
        '
        Me.GroupAndSortDockPanel1_Container.Location = New System.Drawing.Point(3, 29)
        Me.GroupAndSortDockPanel1_Container.Name = "GroupAndSortDockPanel1_Container"
        Me.GroupAndSortDockPanel1_Container.Size = New System.Drawing.Size(679, 128)
        Me.GroupAndSortDockPanel1_Container.TabIndex = 0
        '
        'ToolBoxDockPanel1
        '
        Me.ToolBoxDockPanel1.Controls.Add(Me.ToolBoxDockPanel1_Container)
        Me.ToolBoxDockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left
        Me.ToolBoxDockPanel1.ID = New System.Guid("161a5a1a-d9b9-4f06-9ac4-d0c3e507c54f")
        Me.ToolBoxDockPanel1.ImageIndex = 3
        Me.ToolBoxDockPanel1.Location = New System.Drawing.Point(0, 115)
        Me.ToolBoxDockPanel1.Name = "ToolBoxDockPanel1"
        Me.ToolBoxDockPanel1.OriginalSize = New System.Drawing.Size(165, 414)
        Me.ToolBoxDockPanel1.Size = New System.Drawing.Size(165, 414)
        Me.ToolBoxDockPanel1.Text = "Tool Box"
        Me.ToolBoxDockPanel1.XRDesignPanel = Me.XrDesignPanel1
        '
        'ToolBoxDockPanel1_Container
        '
        Me.ToolBoxDockPanel1_Container.Location = New System.Drawing.Point(4, 23)
        Me.ToolBoxDockPanel1_Container.Name = "ToolBoxDockPanel1_Container"
        Me.ToolBoxDockPanel1_Container.Size = New System.Drawing.Size(157, 387)
        Me.ToolBoxDockPanel1_Container.TabIndex = 0
        '
        'FrmCargarDiseno
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(850, 554)
        Me.Controls.Add(Me.XrDesignPanel1)
        Me.Controls.Add(Me.ToolBoxDockPanel1)
        Me.Controls.Add(Me.DockPanel1)
        Me.Controls.Add(Me.hideContainerRight)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "FrmCargarDiseno"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.XrDesignPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrDesignDockManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DockPanel1.ResumeLayout(False)
        Me.DockPanel1_Container.ResumeLayout(False)
        Me.DockPanel1_Container.PerformLayout()
        CType(Me.cmbimpresa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbboolean.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbfecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbbordes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbcambio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrDesignDockManager2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hideContainerRight.ResumeLayout(False)
        Me.PropertyGridDockPanel1.ResumeLayout(False)
        CType(Me.XrDesignBarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RecentlyUsedItemsComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DesignRepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DesignRepositoryItemComboBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrDesignDockManager3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ReportExplorerDockPanel1.ResumeLayout(False)
        Me.FieldListDockPanel1.ResumeLayout(False)
        Me.PropertyGridDockPanel2.ResumeLayout(False)
        Me.GroupAndSortDockPanel1.ResumeLayout(False)
        Me.ToolBoxDockPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private _Codigos As Integer
    Public Tipo As Integer = 0

    Public Property Codigos()
        Get
            Return _Codigos
        End Get
        Set(ByVal Value)
            _Codigos = Value
        End Set

    End Property


    Private Sub FrmConfiguraCheque_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'GetMonedasList(cmbimpresa)
        'GetMonedasList(cmbcambio)

        Dim DT As DataTable = Rutinas_SGC.obtener_datos("SELECT * FROM Formatos Where Tipo = " & Tipo & "")

        Try
            'cmbimpresa.EditValue = DT.Rows(0)("Impresa")
            'cmbcambio.EditValue = DT.Rows(0)("Cambio")
        Catch ex As Exception

        End Try
        Dim Cadena As String
        Dim Temp As Boolean = False

        Try
            Dim Data() As Byte
            If Tipo = 0 Then
                Data = CType(Rutinas_SGC.obtener_datos("SELECT Formato FROM Formatos WHERE Tipo = " & Tipo & "").Rows(0).Item(0), Byte())
                cmbimpresa.Visible = False
                cmbcambio.Visible = False
                LabelControl4.Visible = False
                LabelControl5.Visible = False
            Else
                Data = CType(Rutinas_SGC.obtener_datos("SELECT Formato FROM Formatos WHERE Tipo = " & Tipo & "").Rows(0).Item(0), Byte())
            End If

            Dim Tamano As Integer
            Tamano = Data.GetUpperBound(0)
            Cadena = Application.StartupPath & "\RptFormato.repx"
            If File.Exists(Cadena) Then Kill(Cadena)
            Dim Archivo As New FileStream(Cadena, FileMode.OpenOrCreate, FileAccess.Write)
            Archivo.Write(Data, 0, Tamano)
            Archivo.Close()
            Temp = True
        Catch ex As Exception
        End Try

        Dim Rep As XtraReport = Nothing

        Rep = New Rpt_impuesto_IMI

        
        If Temp Then
            Try
                Rep.LoadLayout(Application.StartupPath & "\RptFormato.repx")
            Catch ex1 As Exception
                MsgBox(ex1.Message)
            End Try
        End If
        Me.XrDesignPanel1.OpenReport(Rep)
        Me.XrDesignPanel1.Show()
    End Sub

    Private Sub btn_guardar_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_guardar.ItemClick
        If File.Exists(Application.StartupPath & "\RptFormato.repx") Then
            Kill(Application.StartupPath & "\RptFormato.repx")
        End If

        Me.XrDesignPanel1.FileName = Application.StartupPath & "\RptFormato.repx"
        Me.XrDesignPanel1.SaveReport()

        Dim cn As New SqlConnection(Rutinas_SGC.CadenaConexion())
        Dim cmd As New SqlCommand("sp_GuardarFormato", cn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim Data As New SqlParameter("@Formato", SqlDbType.VarBinary)
        Dim _Tipo As New SqlParameter("@Tipo", SqlDbType.Int)
        'Dim Impresa As New SqlParameter("@Impresa", SqlDbType.NVarChar)
        'Dim Cambio As New SqlParameter("@Cambio", SqlDbType.NVarChar)
        'Dim Empresa As New SqlParameter("@Empresa", SqlDbType.NVarChar)

        Dim Archivo As New FileStream(Application.StartupPath & "\RptFormato.repx", _
                            FileMode.Open, FileAccess.Read)

        Dim ArchivoData(Archivo.Length() - 1) As Byte
        Archivo.Read(ArchivoData, 0, ArchivoData.Length)
        Archivo.Close()

        Data.Value = ArchivoData
        'Codigo.Value = Codigos
        _Tipo.Value = Tipo
        'Impresa.Value = IsNull(cmbimpresa.EditValue, MonedaBase)
        'Cambio.Value = IsNull(cmbcambio.EditValue, MonedaBase)
        'Empresa.Value = EmpresaActual

        cmd.Parameters.Add(Data)
        'cmd.Parameters.Add(Codigo)
        cmd.Parameters.Add(_Tipo)
        'cmd.Parameters.Add(Impresa)
        'cmd.Parameters.Add(Cambio)
        'cmd.Parameters.Add(Empresa)

        Try
            cn.Open()
            cmd.ExecuteNonQuery()
            cn.Close()
            XtraMessageBox.Show("Se Guardo Satisfactoriamente...", "Aviso Importante...", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch Ex As Exception
            cn.Close()
            MsgBox(Ex.Message)
        Finally
            Kill(Application.StartupPath & "\RptFormato.repx")
        End Try

    End Sub

    Private Sub cmdoriginal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_guardar_formato.ItemClick
        Dim Rep As New Rpt_impuesto_IMI
        Me.XrDesignPanel1.OpenReport(Rep)
    End Sub
    End Class
