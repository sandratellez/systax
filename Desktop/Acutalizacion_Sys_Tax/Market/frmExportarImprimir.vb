Imports System
Imports DevExpress.Utils
Imports DevExpress.Export
Imports Microsoft.VisualBasic
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPivotGrid
Imports DevExpress.Web.ASPxPivotGrid


Public Class frmExportarImprimir
    Dim Opt As Integer
    Private GridP As Object

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Public Sub Mostrar(ByRef Grid As Object)
        GridP = Grid
        Me.ShowDialog()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_exportar.ItemClick


    End Sub
    Private Sub btn_regresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_regresar.ItemClick

        Me.Close()
    End Sub


    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Opt = 1
        Exportar(Opt)
    End Sub

    Private Sub SimpleButton6_Click(sender As Object, e As EventArgs) Handles SimpleButton6.Click
        Opt = 2
        Exportar(Opt)
    End Sub

    Sub Exportar(Opt As Integer)
        Dim Extension As String = String.Empty

        If Opt = 1 Then Extension = "*.xls"
        If Opt = 2 Then Extension = "*.xlsx"
        If Opt = 3 Then Extension = "*.pdf"
        If Opt = 4 Then Extension = "*.html"
        If Opt = 5 Then Extension = "*.rtf"
        If Opt = 6 Then Extension = "*.txt"


        If Opt <> 6 Then
            Guardar.DefaultExt = Extension
            Guardar.Filter = "(" & Extension & ")|" & Extension

            If Guardar.ShowDialog = DialogResult.OK Then


                If Extension = "*.xls" Or Extension = "*.xlsx" Then



                    'Si es un grid
                    If TypeOf (GridP) Is DevExpress.XtraGrid.GridControl Then _
                        CType(GridP, DevExpress.XtraGrid.GridControl).ExportToXlsx(Guardar.FileName, New XlsxExportOptionsEx With {.ExportType = ExportType.WYSIWYG})

                    'Si es un Pivot
                    If TypeOf (GridP) Is DevExpress.XtraPivotGrid.PivotGridControl Then
                        Dim iPivot As DevExpress.XtraPivotGrid.PivotGridControl

                        Dim options As XlsxExportOptionsEx
                        options = New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG}
                        options.SheetName = "Dinamico"

                        iPivot = CType(GridP, DevExpress.XtraPivotGrid.PivotGridControl)

                        iPivot.OptionsView.ShowColumnHeaders = False
                        iPivot.OptionsView.ShowDataHeaders = False
                        iPivot.OptionsView.ShowFilterHeaders = False


                        Try
                            iPivot.ExportToXlsx(Guardar.FileName, options)
                        Catch ex As Exception
                            iPivot.ExportToXlsx(Guardar.FileName)
                        End Try


                        iPivot.OptionsView.ShowColumnHeaders = True
                        iPivot.OptionsView.ShowDataHeaders = True
                        iPivot.OptionsView.ShowFilterHeaders = True

                    End If

                    'Si es un Arbol
                    If TypeOf (GridP) Is DevExpress.XtraTreeList.TreeList Then _
                        CType(GridP, DevExpress.XtraTreeList.TreeList).ExportToXlsx(Guardar.FileName, New XlsxExportOptionsEx With {.ExportType = ExportType.WYSIWYG})

                    'Si es una Vista
                    If TypeOf (GridP) Is DevExpress.XtraGrid.Views.Grid.GridView Then _
                        CType(GridP, DevExpress.XtraGrid.Views.Grid.GridView).ExportToXlsx(Guardar.FileName, New XlsxExportOptionsEx With {.ExportType = ExportType.WYSIWYG})

                    'Dim iGrid As DevExpress.XtraGrid.GridControl
                    'Dim iPivot As DevExpress.XtraPivotGrid.PivotGridControl

                    'Try
                    '    iGrid = CType(GridP, DevExpress.XtraGrid.GridControl)
                    '    iGrid.ExportToXlsx(Guardar.FileName, New XlsxExportOptionsEx With {.ExportType = ExportType.WYSIWYG})
                    'Catch ex As Exception

                    '    Dim options As XlsxExportOptionsEx
                    '    options = New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG}
                    '    options.SheetName = "Dinamico"

                    '    iPivot = CType(GridP, DevExpress.XtraPivotGrid.PivotGridControl)

                    '    iPivot.OptionsView.ShowColumnHeaders = False
                    '    iPivot.OptionsView.ShowDataHeaders = False

                    '    iPivot.ExportToXlsx(Guardar.FileName, options)
                    'End Try

                    'If  CType(GridP,DevExpress.XtraGrid.GridControl) is 
                    'If GridP Is DevExpress.XtraGrid.GridControl then GridP.ExportToXlsx(Guardar.FileName, xl) Then
                    'CType(GridP, DevExpress.XtraPivotGrid.PivotGridControl).ExportToXlsx(Guardar.FileName, DevExpress.XtraPrinting.TextExportMode.Value)

                End If




                If Opt = 3 Then GridP.ExportToPdf(Guardar.FileName)
                If Opt = 4 Then GridP.ExportToHtml(Guardar.FileName)
                If Opt = 5 Then GridP.ExportToRtf(Guardar.FileName)
                If Opt = 6 Then GridP.ExportToText(Guardar.FileName)

                If MessageBox.Show("� Abrir Archivo?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                    Process.Start(Guardar.FileName)
                End If
            End If
            Me.Close()
        Else
            Me.Close()
            GridP.ShowPrintPreview()
        End If
    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        Opt = 3
        Exportar(Opt)
    End Sub

    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles SimpleButton3.Click
        Opt = 4
        Exportar(Opt)
    End Sub

    Private Sub SimpleButton5_Click(sender As Object, e As EventArgs) Handles SimpleButton5.Click
        Opt = 5
        Exportar(Opt)
    End Sub

    Private Sub SimpleButton4_Click(sender As Object, e As EventArgs) Handles SimpleButton4.Click
        Opt = 6
        Exportar(Opt)
    End Sub
End Class
