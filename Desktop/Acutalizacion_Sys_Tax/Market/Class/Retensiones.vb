﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class Retensiones

    Public Shared Function GetListRetencionImpuestosTOTAL(FechaDesde As String, FechaHasta As String) As DataTable
        Dim dt As New DataTable

        Try

            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("GetListRetencionImpuestosTOTAL", conexion)

            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("FechaDesde", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("FechaDesde").Value = FechaDesde

            cmd.SelectCommand.Parameters.Add("FechaHasta", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("FechaHasta").Value = FechaHasta

            cmd.Fill(dt)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return dt

    End Function


    Public Shared Function GetListRetencionImpuestos(FechaDesde As String, FechaHasta As String, UsuarioID As String, Impuesto As String) As DataTable
        Dim dt As New DataTable

        Try

            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("GetListRetencionImpuestos", conexion)

            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("FechaDesde", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("FechaDesde").Value = FechaDesde

            cmd.SelectCommand.Parameters.Add("FechaHasta", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("FechaHasta").Value = FechaHasta

            cmd.SelectCommand.Parameters.Add("UsuarioID", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("UsuarioID").Value = UsuarioID

            cmd.SelectCommand.Parameters.Add("Impuesto", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("Impuesto").Value = Impuesto

            cmd.Fill(dt)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return dt

    End Function


    Public Shared Function GetLis(FechaDesde As String, FechaHasta As String, UsuarioID As String) As DataTable
        Dim dt As New DataTable

        Try

            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("GetListRetencion", conexion)

            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("FechaDesde", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("FechaDesde").Value = FechaDesde

            cmd.SelectCommand.Parameters.Add("FechaHasta", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("FechaHasta").Value = FechaHasta

            cmd.SelectCommand.Parameters.Add("UsuarioID", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("UsuarioID").Value = UsuarioID

            cmd.Fill(dt)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return dt

    End Function

    Public Shared Function Unique(RetencionID As Integer) As DataTable
        Dim dt As New DataTable

        Try
            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("Unique", conexion)
            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("RetencionID", SqlDbType.Int)
            cmd.SelectCommand.Parameters("RetencionID").Value = RetencionID

            cmd.Fill(dt)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return dt

    End Function


    Public Shared Function Anular(ID As Integer, Usuario As String, Motivo_Anula As String)

        Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
        Dim cmd As New SqlCommand("AnularRetencion_ejem", conexion)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID
            cmd.Parameters.Add("@usuario", SqlDbType.VarChar).Value = Usuario
            cmd.Parameters.Add("@motivo_anula", SqlDbType.VarChar).Value = Motivo_Anula
            conexion.Open()
            cmd.ExecuteNonQuery()
            conexion.Close()

        Catch ex As Exception

            conexion.Close()
            MsgBox(ex.Message)

        End Try
    End Function
    Public Shared Function GetConstanciaUnique(TipoRetencion As String, Serie As String, ID As String) As DataTable
        Dim dt As New DataTable

        Try

            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("GetConstanciaUnique", conexion)

            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("TipoRetencion", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("TipoRetencion").Value = TipoRetencion

            cmd.SelectCommand.Parameters.Add("Serie", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("Serie").Value = Serie

            cmd.SelectCommand.Parameters.Add("ID", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("ID").Value = ID

            cmd.Fill(dt)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return dt

    End Function
    Public Shared Function GetListRetencionImpuestosFULL(FechaDesde As String, FechaHasta As String, Impuesto As String, OficinaID As String) As DataTable
        Dim dt As New DataTable

        Try

            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("GetListRetencionImpuestosFULL", conexion)

            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("FechaDesde", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("FechaDesde").Value = FechaDesde

            cmd.SelectCommand.Parameters.Add("FechaHasta", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("FechaHasta").Value = FechaHasta

            cmd.SelectCommand.Parameters.Add("Impuesto", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("Impuesto").Value = Impuesto

            cmd.SelectCommand.Parameters.Add("OficinaID", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("OficinaID").Value = OficinaID

            cmd.Fill(dt)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return dt

    End Function
    Public Shared Function EliminarAdjunto(
                            ID As Integer
                                )

        Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
        Dim cmd As New SqlCommand("EliminarAdjuntado", conexion)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            cmd.Parameters.Add("ID", SqlDbType.Int)
            cmd.Parameters("ID").Value = ID

            conexion.Open()
            cmd.ExecuteNonQuery()
            conexion.Close()

        Catch ex As Exception

            conexion.Close()
            MsgBox(ex.Message)

        End Try
    End Function


End Class


