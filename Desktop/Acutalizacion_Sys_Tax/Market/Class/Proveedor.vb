﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class Proveedor

    Public Shared Function GetlistProveedorImagen(ProveedorID As String) As DataTable
        Dim dt As New DataTable

        Try
            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("GetlistProveedorImagen", conexion)
            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("ProveedorID", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("ProveedorID").Value = ProveedorID


            cmd.Fill(dt)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return dt
    End Function
    Public Shared Function GetList(ProveedorID As String) As DataTable
        Dim dt As New DataTable

        Try
            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("GetListProveedor", conexion)
            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("ProveedorID", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("ProveedorID").Value = ProveedorID


            cmd.Fill(dt)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return dt
    End Function

    Public Shared Function GetListActivos() As DataTable
        Dim dt As New DataTable

        Try
            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("GetListAgenteActivos", conexion)
            cmd.SelectCommand.CommandType = CommandType.StoredProcedure


            cmd.Fill(dt)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return dt
    End Function

    Public Shared Function IU(conexion As SqlConnection, transaccion As SqlTransaction, ByRef ProveedorID As Integer, Proveedor As String, Ruc As String, Celular As String, Cedula As String, Accion As String, Activo As Boolean, iva As Boolean, ir As Boolean, imi As Boolean)

        Dim cmd As New SqlCommand("IU_Proveedor", conexion)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("Proveedor", SqlDbType.NVarChar)
        cmd.Parameters("Proveedor").Value = Proveedor

        cmd.Parameters.Add("RUC", SqlDbType.NVarChar)
        cmd.Parameters("RUC").Value = Ruc

        cmd.Parameters.Add("Cedula", SqlDbType.NVarChar)
        cmd.Parameters("Cedula").Value = Cedula

        cmd.Parameters.Add("Celular", SqlDbType.NVarChar)
        cmd.Parameters("Celular").Value = Celular

        cmd.Parameters.Add("Activo", SqlDbType.Bit)
        cmd.Parameters("Activo").Value = Activo

        cmd.Parameters.Add("Accion", SqlDbType.NVarChar)
        cmd.Parameters("Accion").Value = Accion

        cmd.Parameters.Add("iva", SqlDbType.Bit)
        cmd.Parameters("iva").Value = iva

        cmd.Parameters.Add("ir", SqlDbType.Bit)
        cmd.Parameters("ir").Value = ir

        cmd.Parameters.Add("imi", SqlDbType.Bit)
        cmd.Parameters("imi").Value = imi

        cmd.Parameters.Add("Usuario", SqlDbType.NVarChar)
        cmd.Parameters("Usuario").Value = Usuario_

        Dim _ProveedorID As SqlParameter = New SqlParameter("ProveedorID", SqlDbType.Int)
        _ProveedorID.Direction = ParameterDirection.Output
        cmd.Parameters.Add(_ProveedorID)

        cmd.Transaction = transaccion
        cmd.ExecuteNonQuery()
        ProveedorID = _ProveedorID.Value

    End Function

    Public Shared Function Edit_Proveedor(conexion As SqlConnection, transaccion As SqlTransaction, ProveedorID As Integer, Proveedor As String, Ruc As String, Celular As String, Cedula As String, Accion As String, Activo As Boolean, iva As Boolean, ir As Boolean, imi As Boolean)

        Dim cmd As New SqlCommand("Edit_Proveedor", conexion)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("Proveedor", SqlDbType.NVarChar)
        cmd.Parameters("Proveedor").Value = Proveedor

        cmd.Parameters.Add("RUC", SqlDbType.NVarChar)
        cmd.Parameters("RUC").Value = Ruc

        cmd.Parameters.Add("Cedula", SqlDbType.NVarChar)
        cmd.Parameters("Cedula").Value = Cedula

        cmd.Parameters.Add("Celular", SqlDbType.NVarChar)
        cmd.Parameters("Celular").Value = Celular

        cmd.Parameters.Add("Activo", SqlDbType.Bit)
        cmd.Parameters("Activo").Value = Activo

        cmd.Parameters.Add("ProveedorID", SqlDbType.Int)
        cmd.Parameters("ProveedorID").Value = ProveedorID

        cmd.Parameters.Add("iva", SqlDbType.Bit)
        cmd.Parameters("iva").Value = iva

        cmd.Parameters.Add("ir", SqlDbType.Bit)
        cmd.Parameters("ir").Value = ir

        cmd.Parameters.Add("imi", SqlDbType.Bit)
        cmd.Parameters("imi").Value = imi

        cmd.Parameters.Add("Usuario", SqlDbType.NVarChar)
        cmd.Parameters("Usuario").Value = Usuario_

        cmd.Transaction = transaccion
        cmd.ExecuteNonQuery()


    End Function


    Public Shared Function IUDetalles(conexion As SqlConnection, transaccion As SqlTransaction, ProveedorID As Integer, ProveedorImagen As DataTable)

        '   Agregar Oficinas a los Usuario

        For i As Integer = 0 To ProveedorImagen.Rows.Count - 1
            Dim _cmd As New SqlCommand("IUDetalleProveedorImagen", conexion)
            _cmd.CommandType = CommandType.StoredProcedure

            _cmd.Parameters.Add("ProveedorID", SqlDbType.Int)
            _cmd.Parameters("ProveedorID").Value = ProveedorID

            _cmd.Parameters.Add("TipoID", SqlDbType.Int)
            _cmd.Parameters("TipoID").Value = ProveedorImagen.Rows(i).Item("TipoID")

            _cmd.Parameters.Add("Imagen", SqlDbType.Image)
            _cmd.Parameters("Imagen").Value = ConvertToByteArray(ProveedorImagen.Rows(i).Item("Imagen"))

            _cmd.Transaction = transaccion
            _cmd.ExecuteNonQuery()

        Next


    End Function

    Public Shared Function ConvertToByteArray(ByVal value As Bitmap) As Byte()

        Dim bitmapBytes As Byte()
        Using stream As New System.IO.MemoryStream
            value.Save(stream, value.RawFormat)
            bitmapBytes = stream.ToArray
        End Using
        Return bitmapBytes

    End Function

    'Public Shared Function ConvertToByteArray(ByVal value As Byte) As Bitmap

    '    Dim bitmapBytes As Bitmap
    '    Using stream As New System.IO.MemoryStream
    '        value.Save(stream, value.RawFormat)
    '        bitmapBytes = stream.to
    '    End Using
    '    Return bitmapBytes

    'End Function



    Public Shared Function GetlistAgenteCombox(ByRef Combox As DevExpress.XtraEditors.GridLookUpEdit)


        Combox.Properties.DataSource = Rutinas_SGC.obtener_datos("GetlistAgenteCombox")
        Combox.Properties.ValueMember = "AgenteID"
        Combox.Properties.DisplayMember = "RazonSocial"
        Combox.Properties.PopulateViewColumns()

        Combox.Properties.View.Columns(0).Visible = False
        Combox.Properties.View.Columns(1).Caption = "Agente de Mercado"

        Combox.Properties.View.BestFitColumns()

        Combox.Properties.PopupFormMinSize = New Point(Combox.Size.Width + 50, 0)
        Combox.Properties.ShowFooter = True
        Combox.Text = String.Empty


    End Function

    Public Shared Function DeleteProveedorImage(ProveedorID As Integer, TipoID As Integer)
        Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
        Dim cmd As New SqlCommand("DeleteProveedorImage", conexion)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            cmd.Parameters.Add("ProveedorID", SqlDbType.Int)
            cmd.Parameters("ProveedorID").Value = ProveedorID

            cmd.Parameters.Add("TipoID", SqlDbType.Int)
            cmd.Parameters("TipoID").Value = TipoID

            conexion.Open()
            cmd.ExecuteNonQuery()
            conexion.Close()

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)

        End Try

    End Function
End Class

