﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class Talonario
    Public Shared Function GetLisTalonario(TipoID As String, OficinaID As String) As DataTable
        Dim dt As New DataTable

        Try
            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("GetLisTalonario", conexion)
            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("TipoID", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("TipoID").Value = TipoID

            cmd.SelectCommand.Parameters.Add("OficinaID", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("OficinaID").Value = OficinaID

            cmd.Fill(dt)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return dt

    End Function
    Public Shared Function GetlistTalonarioAll(ID As String, UsuarioID As String) As DataTable
        Dim dt As New DataTable

        Try
            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("GetlistTalonarioAll", conexion)
            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("ID", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("ID").Value = ID

            cmd.SelectCommand.Parameters.Add("UsuarioID", SqlDbType.NVarChar)
            cmd.SelectCommand.Parameters("UsuarioID").Value = UsuarioID


            cmd.Fill(dt)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return dt

    End Function

    'Public Shared Function IUBancos(BancoID As Integer, Banco As String, Siglas As String, entidad As String, Accion As String)

    '    Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
    '    Dim cmd As New SqlCommand("IUBancos", conexion)
    '    cmd.CommandType = CommandType.StoredProcedure

    '    Try
    '        cmd.Parameters.Add("BancoID", SqlDbType.Int)
    '        cmd.Parameters("BancoID").Value = BancoID

    '        cmd.Parameters.Add("Banco", SqlDbType.NVarChar)
    '        cmd.Parameters("Banco").Value = Banco

    '        cmd.Parameters.Add("Siglas", SqlDbType.NVarChar)
    '        cmd.Parameters("Siglas").Value = Siglas

    '        cmd.Parameters.Add("Accion", SqlDbType.NVarChar)
    '        cmd.Parameters("Accion").Value = Accion

    '        cmd.Parameters.Add("Entidad", SqlDbType.NVarChar)
    '        cmd.Parameters("Entidad").Value = entidad

    '        conexion.Open()
    '        cmd.ExecuteNonQuery()
    '        conexion.Close()

    '    Catch ex As Exception

    '        conexion.Close()
    '        MsgBox(ex.Message)
    '    End Try



    'End Function


End Class


