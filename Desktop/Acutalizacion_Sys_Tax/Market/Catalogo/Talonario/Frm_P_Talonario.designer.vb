﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Frm_P_Talonario
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Frm_P_Talonario))
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridFormatRule1 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue1 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.DockManager1 = New DevExpress.XtraBars.Docking.DockManager(Me.components)
        Me.DockPanel1 = New DevExpress.XtraBars.Docking.DockPanel()
        Me.DockPanel1_Container = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.btn_regresar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_exportar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_eliminar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_editar = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_nuevo = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.GridBanco = New DevExpress.XtraGrid.GridControl()
        Me.GridViewBanco = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DockPanel1.SuspendLayout()
        Me.DockPanel1_Container.SuspendLayout()
        CType(Me.GridBanco, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewBanco, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Activo"
        Me.GridColumn7.FieldName = "Activo"
        Me.GridColumn7.Image = CType(resources.GetObject("GridColumn7.Image"), System.Drawing.Image)
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 8
        Me.GridColumn7.Width = 80
        '
        'DockManager1
        '
        Me.DockManager1.Form = Me
        Me.DockManager1.RootPanels.AddRange(New DevExpress.XtraBars.Docking.DockPanel() {Me.DockPanel1})
        Me.DockManager1.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "DevExpress.XtraBars.StandaloneBarDockControl", "System.Windows.Forms.StatusBar", "System.Windows.Forms.MenuStrip", "System.Windows.Forms.StatusStrip", "DevExpress.XtraBars.Ribbon.RibbonStatusBar", "DevExpress.XtraBars.Ribbon.RibbonControl"})
        '
        'DockPanel1
        '
        Me.DockPanel1.Controls.Add(Me.DockPanel1_Container)
        Me.DockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left
        Me.DockPanel1.ID = New System.Guid("08cce836-1e5d-4383-9ba2-6948a4b8bd47")
        Me.DockPanel1.Location = New System.Drawing.Point(0, 0)
        Me.DockPanel1.Name = "DockPanel1"
        Me.DockPanel1.OriginalSize = New System.Drawing.Size(107, 200)
        Me.DockPanel1.Size = New System.Drawing.Size(107, 369)
        '
        'DockPanel1_Container
        '
        Me.DockPanel1_Container.Controls.Add(Me.btn_regresar)
        Me.DockPanel1_Container.Controls.Add(Me.btn_exportar)
        Me.DockPanel1_Container.Controls.Add(Me.btn_eliminar)
        Me.DockPanel1_Container.Controls.Add(Me.btn_editar)
        Me.DockPanel1_Container.Controls.Add(Me.btn_nuevo)
        Me.DockPanel1_Container.Location = New System.Drawing.Point(4, 23)
        Me.DockPanel1_Container.Name = "DockPanel1_Container"
        Me.DockPanel1_Container.Size = New System.Drawing.Size(98, 342)
        Me.DockPanel1_Container.TabIndex = 0
        '
        'btn_regresar
        '
        Me.btn_regresar.ImageOptions.Image = CType(resources.GetObject("btn_regresar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_regresar.Location = New System.Drawing.Point(3, 173)
        Me.btn_regresar.Name = "btn_regresar"
        Me.btn_regresar.Size = New System.Drawing.Size(94, 38)
        Me.btn_regresar.TabIndex = 18
        Me.btn_regresar.Tag = "Salir De Listado Talonario"
        Me.btn_regresar.Text = "Regresar"
        '
        'btn_exportar
        '
        Me.btn_exportar.ImageOptions.Image = CType(resources.GetObject("btn_exportar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_exportar.Location = New System.Drawing.Point(3, 131)
        Me.btn_exportar.Name = "btn_exportar"
        Me.btn_exportar.Size = New System.Drawing.Size(94, 38)
        Me.btn_exportar.TabIndex = 17
        Me.btn_exportar.Tag = "Exportar Talonario"
        Me.btn_exportar.Text = "Exportar"
        '
        'btn_eliminar
        '
        Me.btn_eliminar.ImageOptions.Image = CType(resources.GetObject("btn_eliminar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_eliminar.Location = New System.Drawing.Point(3, 89)
        Me.btn_eliminar.Name = "btn_eliminar"
        Me.btn_eliminar.Size = New System.Drawing.Size(94, 38)
        Me.btn_eliminar.TabIndex = 16
        Me.btn_eliminar.Tag = "Eliminar Talonario"
        Me.btn_eliminar.Text = "Eliminar"
        '
        'btn_editar
        '
        Me.btn_editar.ImageOptions.Image = CType(resources.GetObject("btn_editar.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_editar.Location = New System.Drawing.Point(3, 47)
        Me.btn_editar.Name = "btn_editar"
        Me.btn_editar.Size = New System.Drawing.Size(94, 38)
        Me.btn_editar.TabIndex = 15
        Me.btn_editar.Tag = "Editar  Talonario"
        Me.btn_editar.Text = "Editar"
        '
        'btn_nuevo
        '
        Me.btn_nuevo.ImageOptions.Image = CType(resources.GetObject("btn_nuevo.ImageOptions.Image"), System.Drawing.Image)
        Me.btn_nuevo.Location = New System.Drawing.Point(3, 3)
        Me.btn_nuevo.Name = "btn_nuevo"
        Me.btn_nuevo.Size = New System.Drawing.Size(94, 38)
        Me.btn_nuevo.TabIndex = 14
        Me.btn_nuevo.Tag = "Crear Nuevo Talonario"
        Me.btn_nuevo.Text = "Nuevo"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Appearance.Options.UseForeColor = True
        Me.LabelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl1.Location = New System.Drawing.Point(107, 0)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(137, 16)
        Me.LabelControl1.TabIndex = 3
        Me.LabelControl1.Text = "Listado de Talonarios"
        '
        'GridBanco
        '
        Me.GridBanco.Dock = System.Windows.Forms.DockStyle.Fill
        GridLevelNode1.RelationName = "Level1"
        Me.GridBanco.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridBanco.Location = New System.Drawing.Point(107, 16)
        Me.GridBanco.MainView = Me.GridViewBanco
        Me.GridBanco.Name = "GridBanco"
        Me.GridBanco.Size = New System.Drawing.Size(978, 353)
        Me.GridBanco.TabIndex = 4
        Me.GridBanco.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewBanco})
        '
        'GridViewBanco
        '
        Me.GridViewBanco.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
        Me.GridViewBanco.Appearance.GroupRow.Options.UseForeColor = True
        Me.GridViewBanco.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn2, Me.GridColumn4, Me.GridColumn6, Me.GridColumn7, Me.GridColumn1, Me.GridColumn3, Me.GridColumn5, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10})
        Me.GridViewBanco.CustomizationFormBounds = New System.Drawing.Rectangle(694, 316, 210, 172)
        GridFormatRule1.ApplyToRow = True
        GridFormatRule1.Column = Me.GridColumn7
        GridFormatRule1.Name = "Format0"
        FormatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Silver
        FormatConditionRuleValue1.Appearance.Options.UseForeColor = True
        FormatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue1.Value1 = False
        GridFormatRule1.Rule = FormatConditionRuleValue1
        Me.GridViewBanco.FormatRules.Add(GridFormatRule1)
        Me.GridViewBanco.GridControl = Me.GridBanco
        Me.GridViewBanco.Name = "GridViewBanco"
        Me.GridViewBanco.OptionsBehavior.AutoExpandAllGroups = True
        Me.GridViewBanco.OptionsBehavior.Editable = False
        Me.GridViewBanco.OptionsBehavior.ReadOnly = True
        Me.GridViewBanco.OptionsFind.AlwaysVisible = True
        Me.GridViewBanco.OptionsFind.FindNullPrompt = "Enter para buscar el texto"
        Me.GridViewBanco.OptionsView.ColumnAutoWidth = False
        Me.GridViewBanco.OptionsView.ShowFooter = True
        Me.GridViewBanco.OptionsView.ShowGroupPanel = False
        Me.GridViewBanco.RowHeight = 21
        Me.GridViewBanco.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn2
        '
        Me.GridColumn2.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridColumn2.AppearanceCell.Options.UseFont = True
        Me.GridColumn2.Caption = "Tipo Talonario"
        Me.GridColumn2.FieldName = "TipoTalonario_"
        Me.GridColumn2.Image = CType(resources.GetObject("GridColumn2.Image"), System.Drawing.Image)
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsFilter.AllowFilter = False
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        Me.GridColumn2.Width = 225
        '
        'GridColumn4
        '
        Me.GridColumn4.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn4.AppearanceCell.Options.UseFont = True
        Me.GridColumn4.Caption = " ID"
        Me.GridColumn4.FieldName = "OficinaID"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Width = 50
        '
        'GridColumn6
        '
        Me.GridColumn6.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridColumn6.AppearanceCell.Options.UseFont = True
        Me.GridColumn6.Caption = "Oficina"
        Me.GridColumn6.FieldName = "Oficina"
        Me.GridColumn6.Image = CType(resources.GetObject("GridColumn6.Image"), System.Drawing.Image)
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 1
        Me.GridColumn6.Width = 218
        '
        'GridColumn1
        '
        Me.GridColumn1.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridColumn1.AppearanceCell.Options.UseFont = True
        Me.GridColumn1.Caption = "Inicio"
        Me.GridColumn1.FieldName = "Inicio"
        Me.GridColumn1.Image = CType(resources.GetObject("GridColumn1.Image"), System.Drawing.Image)
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 4
        Me.GridColumn1.Width = 90
        '
        'GridColumn3
        '
        Me.GridColumn3.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridColumn3.AppearanceCell.Options.UseFont = True
        Me.GridColumn3.Caption = "Actual"
        Me.GridColumn3.FieldName = "consecutivo"
        Me.GridColumn3.Image = CType(resources.GetObject("GridColumn3.Image"), System.Drawing.Image)
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 7
        Me.GridColumn3.Width = 90
        '
        'GridColumn5
        '
        Me.GridColumn5.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridColumn5.AppearanceCell.Options.UseFont = True
        Me.GridColumn5.Caption = "Cantidad"
        Me.GridColumn5.FieldName = "Cantidad"
        Me.GridColumn5.Image = CType(resources.GetObject("GridColumn5.Image"), System.Drawing.Image)
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 6
        Me.GridColumn5.Width = 90
        '
        'GridColumn8
        '
        Me.GridColumn8.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridColumn8.AppearanceCell.Options.UseFont = True
        Me.GridColumn8.Caption = "Fecha Entrega"
        Me.GridColumn8.FieldName = "Fecha"
        Me.GridColumn8.Image = CType(resources.GetObject("GridColumn8.Image"), System.Drawing.Image)
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 3
        Me.GridColumn8.Width = 103
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "ID"
        Me.GridColumn9.FieldName = "ID"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 0
        '
        'GridColumn10
        '
        Me.GridColumn10.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridColumn10.AppearanceCell.Options.UseFont = True
        Me.GridColumn10.Caption = "Serie"
        Me.GridColumn10.FieldName = "Serie"
        Me.GridColumn10.Image = CType(resources.GetObject("GridColumn10.Image"), System.Drawing.Image)
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 5
        Me.GridColumn10.Width = 92
        '
        'Frm_P_Talonario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1085, 369)
        Me.Controls.Add(Me.GridBanco)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.DockPanel1)
        Me.Name = "Frm_P_Talonario"
        Me.Tag = "Formulario Talonarios"
        Me.Text = "Talonarios"
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DockPanel1.ResumeLayout(False)
        Me.DockPanel1_Container.ResumeLayout(False)
        CType(Me.GridBanco, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewBanco, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DockManager1 As DevExpress.XtraBars.Docking.DockManager
    Friend WithEvents DockPanel1 As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents DockPanel1_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents btn_regresar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_exportar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_eliminar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_editar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_nuevo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridBanco As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewBanco As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
End Class
