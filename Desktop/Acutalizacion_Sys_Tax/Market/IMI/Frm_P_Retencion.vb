﻿Imports System
Imports System.IO
Imports System.Data.SqlClient
Imports System.Drawing
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraEditors
Imports System.Linq


Public Class Frm_P_Retencion

    Public fecha1 As New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
    Public fecha2 As New DateTime
    Dim A As New Auditoria
    Private Sub btn_nuevo_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_nuevo.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Nuevo", "Impuestos Retenciones", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.Frm_Retenciones.Accion = "NEW"
        My.Forms.Frm_Retenciones.MdiParent = Me.MdiParent
        My.Forms.Frm_Retenciones.Show()

    End Sub

    Private Sub Frm_P_Retencion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadata()
    End Sub

    Public Sub loadata()

        fecha1 = New DateTime(Date.Now.Year, Date.Now.Month, 1)
        fecha2 = fecha1.AddMonths(1).AddDays(-1)

        Me.lbl_periodo.Caption = Me.fecha1.ToShortDateString + " - " + fecha2.ToShortDateString
        Me.GridRetencion.DataSource = Retensiones.GetLis(fecha1.ToShortDateString, fecha2.ToShortDateString, Usuario_)

    End Sub


    Private Sub btn_filtro_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_filtro.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Buscar", "Filtrar Fecha", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.frm_filtros.dtp_fechadesde.EditValue = CDate(fecha1)
        My.Forms.frm_filtros.dtp_fechahasta.EditValue = CDate(fecha2)

        My.Forms.frm_filtros.StartPosition = FormStartPosition.CenterScreen
        My.Forms.frm_filtros.WindowState = FormWindowState.Normal
        If My.Forms.frm_filtros.ShowDialog() = Windows.Forms.DialogResult.Yes Then
            Me.lbl_periodo.Caption = CDate(My.Forms.frm_filtros.dtp_fechadesde.EditValue).ToShortDateString + " - " + CDate(My.Forms.frm_filtros.dtp_fechahasta.EditValue).ToShortDateString
            Me.GridRetencion.DataSource = Retensiones.GetLis(CDate(My.Forms.frm_filtros.dtp_fechadesde.EditValue).ToShortDateString, CDate(My.Forms.frm_filtros.dtp_fechahasta.EditValue).ToShortDateString, Usuario_)
        End If

    End Sub



    Private Sub btn_imprimir_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_imprimir.ItemClick

    End Sub

    Private Sub BarButtonItem6_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem6.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Regresar", "Regresando", e.Item.Caption + "|" + e.Item.Tag)
        Me.Close()
    End Sub



    Private Sub btn_editar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_editar.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Editar", "Editar Retencion ", e.Item.Caption + "|" + e.Item.Tag)
        If MessageBox.Show("Orden #" & Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "OficinaUNI") & " - " & Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "Proveedor"), "Editar Retención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        My.Forms.Frm_Retenciones.ID = Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "RetencionID")
        My.Forms.Frm_Retenciones.Accion = "EDIT"
        My.Forms.Frm_Retenciones.MdiParent = Me.MdiParent
        My.Forms.Frm_Retenciones.Show()


    End Sub

    Private Sub btn_anulado_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_anulado.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Anular", "Anular Retencion ", e.Item.Caption + "|" + e.Item.Tag)
        Dim motivo_anula As String = Nothing
        If MessageBox.Show("Anular Orden #" & Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "OficinaUNI") & " - " & Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "Proveedor"), "* * * ANULAR * * *", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Try



            My.Forms.Motivo_Anula.StartPosition = FormStartPosition.CenterScreen
            My.Forms.Motivo_Anula.WindowState = FormWindowState.Normal
            If My.Forms.Motivo_Anula.ShowDialog() = Windows.Forms.DialogResult.Yes Then
                motivo_anula = My.Forms.Motivo_Anula.motivo
                Retensiones.Anular(Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "RetencionID"), Usuario_, motivo_anula)
                loadata()
            End If

            'Retensiones.Anular(Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "RetencionID"), Usuario_, motivo_anula)
            'loadata()
            'motivo_anula = ""
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Exportar", "Listado IR ", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.frm_IR_Exportar.lbl_periodo.Caption = Me.lbl_periodo.Caption
        My.Forms.frm_IR_Exportar.GridRetencion.DataSource = Retensiones.GetListRetencionImpuestos(fecha1.ToShortDateString, fecha2.ToShortDateString, Usuario_, "IR")
        My.Forms.frm_IR_Exportar.MdiParent = Me.MdiParent
        My.Forms.frm_IR_Exportar.Show()

    End Sub

    Private Sub btn_exportar_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_exportar.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Exportar", "Listado IMI", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.frm_IMI_Exportar.lbl_periodo.Caption = Me.lbl_periodo.Caption
        My.Forms.frm_IMI_Exportar.GridRetencion.DataSource = Retensiones.GetListRetencionImpuestos(fecha1.ToShortDateString, fecha2.ToShortDateString, Usuario_, "IMI")
        My.Forms.frm_IMI_Exportar.MdiParent = Me.MdiParent
        My.Forms.frm_IMI_Exportar.Show()

    End Sub

    Private Sub BarButtonItem4_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem4.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Exportar", "Listado IVA ", e.Item.Caption + "|" + e.Item.Tag)
        My.Forms.frm_IVA_Exportar.lbl_periodo.Caption = Me.lbl_periodo.Caption
        My.Forms.frm_IVA_Exportar.GridRetencion.DataSource = Retensiones.GetListRetencionImpuestos(fecha1.ToShortDateString, fecha2.ToShortDateString, Usuario_, "IVA")
        My.Forms.frm_IVA_Exportar.MdiParent = Me.MdiParent
        My.Forms.frm_IVA_Exportar.Show()

    End Sub

    Private Sub BarButtonItem5_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_openadjunto.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Exportar", "Ver Retencion ", e.Item.Caption + "|" + e.Item.Tag)
        Open()
    End Sub

    Sub Open()

         If Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "Adjunto") = "Sí" Then

            If MessageBox.Show("Abrir adjunto #" & Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "OficinaUNI") & " - " & Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "Proveedor"), "Aviso Importante", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If

            Try
                Process.Start(Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "FilePath"))
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error en la vista previa del documento", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End Try

        End If
    End Sub


    Private Sub BarButtonItem7_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem7.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Anular", "Anular Archivo Adjunto", e.Item.Caption + "|" + e.Item.Tag)
        If MessageBox.Show("Elimiar adjunto # " & Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "OficinaUNI") & " - " & Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "Proveedor"), "Aviso Importante", MessageBoxButtons.YesNo, MessageBoxIcon.Stop) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        If Me.GridViewRetension.RowCount = 0 Then
            Exit Sub
        End If

        Try
            ' File.Open(Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "FilePath"), FileMode.Open)
            If File.Exists(Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "FilePath")) Then
                Kill(Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "FilePath"))
            End If

            Retensiones.EliminarAdjunto(Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "RetencionID"))
            loadata()
            'My.Computer.FileSystem.DeleteFile(Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "FilePath"))

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error al Eliminar Adjunto")
        End Try

    End Sub

    Private Sub BarButtonItem8_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem8.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Exportar", "Listado General", e.Item.Caption + "|" + e.Item.Tag)
        If MessageBox.Show("Exportar Listado General", "Aviso Importante", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim f As New frmExportarImprimir
            f.Mostrar(Me.GridRetencion)
        End If

    End Sub

    Private Sub btn_print_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btn_print.ItemClick
        A.InsertarAuditoria(Usuario_, "Click Imprimir", "Imprimir Retencion ", e.Item.Caption + "|" + e.Item.Tag)
        If MessageBox.Show("Orden #" & Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "OficinaUNI") & " - " & Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "Proveedor"), "Imprimir Retención", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim DT_Facturas As DataTable = Retensiones.Unique(Me.GridViewRetension.GetRowCellValue(Me.GridViewRetension.FocusedRowHandle, "RetencionID"))
        Dim Ds As New DataSet

        DT_Facturas.TableName = "Rpt_impuesto_IMI"
        Ds.Tables.Add(DT_Facturas)




        Dim Cadena As String
        Dim Temp As Boolean = False
        Dim Data() As Byte

        Try
            Data = CType(Rutinas_SGC.obtener_datos("SELECT Formato FROM Formatos WHERE Tipo = 0").Rows(0).Item(0), Byte())
            Temp = True
            Dim Tamano As Integer
            Tamano = Data.GetUpperBound(0)

            Cadena = Application.StartupPath & "\Rpt_impuesto_IMI.repx"
            If File.Exists(Cadena) Then Kill(Cadena)
            Dim Archivo As New FileStream(Cadena, FileMode.OpenOrCreate, FileAccess.Write)
            Archivo.Write(Data, 0, Tamano)
            Archivo.Close()

        Catch ex As Exception
            Temp = False
        End Try

        Dim rpt As New XtraReport

        If Temp = True Then
            rpt = XtraReport.FromFile(Application.StartupPath & "\Rpt_impuesto_IMI.repx", True)
        Else
            rpt = New Rpt_impuesto_IMI
        End If

        'DtTotalizado.TableName = "Totalizado"
        'Ds.Tables.Add(DtTotalizado)

        'Dim DT_datos As DataTable = Rutinas_SGC.obtener_datos("sp_listado_aviso_corte '2','" & Me.txt_mayor.EditValue & "','" & Me.cmb_ruta.EditValue & "'")
        'Dim DT_empresa As DataTable = Rutinas_SGC.obtener_datos("Select * from empresa")

        'Dim Ds As New DataSet

        'Ds.Tables.Add(DT_datos)
        'Ds.Tables.Add(DT_empresa)

        Ds.WriteXml(Application.StartupPath & "\xml\Rpt_impuesto_IMI.xml", XmlWriteMode.WriteSchema)

        '            Dim rpt As New XR_Aviso_Corte
        'rpt.XmlDataPath = Application.StartupPath & "\xml\rpt_aviso_corte.xml"
        'rpt.ShowPreview()

        Dim mdi_rpt As New mdi_reporte
        mdi_rpt.PrintControl1.PrintingSystem = rpt.PrintingSystem
        rpt.XmlDataPath = Application.StartupPath & "\xml\Rpt_impuesto_IMI.xml"
        rpt.DataSource = Ds
        rpt.CreateDocument()

        mdi_rpt.MdiParent = Me.MdiParent
        mdi_rpt.Show()
        mdi_rpt.WindowState = FormWindowState.Maximized
        mdi_rpt = Nothing
        rpt = Nothing

    End Sub


    Private Sub Link_adjunto_Click(sender As Object, e As EventArgs) Handles Link_adjunto.Click
        Open()
    End Sub

End Class