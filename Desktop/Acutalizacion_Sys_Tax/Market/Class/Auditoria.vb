﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Net

Public Class Auditoria
    Public Function InsertarAuditoria(Usuario As String, Evento As String, Tabla As String, Pista As String) As Boolean

        Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
        Dim cmd As New SqlCommand("AuditoriaIU", conexion)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            cmd.Parameters.Add("Usuario", SqlDbType.NVarChar)
            cmd.Parameters("Usuario").Value = Usuario

            cmd.Parameters.Add("Evento", SqlDbType.NVarChar)
            cmd.Parameters("Evento").Value = Evento

            'cmd.Parameters.Add("NIC", SqlDbType.NVarChar)
            'cmd.Parameters("NIC").Value = NIC

            cmd.Parameters.Add("Tabla", SqlDbType.NVarChar)
            cmd.Parameters("Tabla").Value = Tabla

            'Dim mIpHostEntry As IPHostEntry
            'mIpHostEntry = Dns.GetHostEntry(My.Computer.Name)

            Dim IPMachine As String
            IPMachine = Dns.GetHostEntry(My.Computer.Name).AddressList.FirstOrDefault(Function(i) i.AddressFamily = Sockets.AddressFamily.InterNetwork).ToString()

            cmd.Parameters.Add("Pista", SqlDbType.VarChar)
            cmd.Parameters("Pista").Value = Pista + " - " + ", desde el IP : " + IPMachine + " - Nombre del Equipo : " + My.Computer.Name

            conexion.Open()
            cmd.ExecuteNonQuery()
            conexion.Close()

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)

        End Try
        Return False
    End Function


    Public Shared Function Mostrar_datos_Auditoria(fecha1 As Date, fecha2 As Date) As DataTable
        Dim dt As New DataTable

        Try
            Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
            Dim cmd As New SqlDataAdapter("mostrar_datos_auditoria", conexion)
            cmd.SelectCommand.CommandType = CommandType.StoredProcedure

            cmd.SelectCommand.Parameters.Add("@FechaDesde", SqlDbType.NVarChar).Value = fecha1
            cmd.SelectCommand.Parameters.Add("@FechaHasta", SqlDbType.NVarChar).Value = fecha2

            cmd.Fill(dt)
            Return dt

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return dt

    End Function

    'Public Shared Function Auditoria(Usuario As String, Descripcion As String) As Boolean

    '    Dim conexion As New SqlConnection(Rutinas_SGC.CadenaConexion())
    '    Dim coman As New SqlCommand
    '    coman = New SqlCommand("Insert into Auditoria (Usuario,descripcion) values ('" & Usuario & "','" & Descripcion & "')", conexion)
    '    coman.Connection.Open()
    '    coman.ExecuteNonQuery()
    '    coman.Connection.Close()
    '    Return True
    'End Function

End Class