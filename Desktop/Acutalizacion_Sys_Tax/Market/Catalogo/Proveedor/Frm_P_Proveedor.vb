﻿Public Class Frm_P_Proveedor
    '  Dim Banco As New Banco
    Dim A As New Auditoria

    Private Sub Frm_Banco_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Cargar()
    End Sub

    Public Function Cargar()
        Me.GridBanco.DataSource = Proveedor.GetList("%")
    End Function

    Private Sub btn_nuevo_Click(sender As Object, e As EventArgs) Handles btn_nuevo.Click
        A.InsertarAuditoria(Usuario_, "Click Nuevo", "Listado Proveedor", sender.text + "|" + sender.tag)
        My.Forms.Frm_IU_Proveedor.Accion = "NEW"
        My.Forms.Frm_IU_Proveedor.MdiParent = Me.MdiParent
        My.Forms.Frm_IU_Proveedor.Show()

    End Sub

    Private Sub btn_editar_Click(sender As Object, e As EventArgs) Handles btn_editar.Click
        A.InsertarAuditoria(Usuario_, "Click Editar", "Listado Proveedor", sender.text + "|" + sender.tag)
        If MessageBox.Show("Editar " & Me.GridViewBanco.GetRowCellValue(Me.GridViewBanco.FocusedRowHandle, "Proveedor"), "Aviso Importante", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        If Me.GridViewBanco.GetRowCellValue(Me.GridViewBanco.FocusedRowHandle, "ProveedorID") <> Nothing Then

            If Me.GridViewBanco.RowCount > 0 Then

                My.Forms.Frm_IU_Proveedor.Accion = "EDIT"
                My.Forms.Frm_IU_Proveedor.ID = Me.GridViewBanco.GetRowCellValue(Me.GridViewBanco.FocusedRowHandle, "ProveedorID")
                My.Forms.Frm_IU_Proveedor.MdiParent = Me.MdiParent
                My.Forms.Frm_IU_Proveedor.Show()

            End If
        End If

    End Sub

    Private Sub btn_exportar_Click(sender As Object, e As EventArgs) Handles btn_exportar.Click
        A.InsertarAuditoria(Usuario_, "Click Exportar", "Listado Proveedor", sender.text + "|" + sender.tag)
        Dim f As New frmExportarImprimir
        f.Mostrar(Me.GridViewBanco)
    End Sub


    Private Sub btn_regresar_Click(sender As Object, e As EventArgs) Handles btn_regresar.Click
        A.InsertarAuditoria(Usuario_, "Click Regresar", "Listado Proveedor", sender.text + "|" + sender.tag)
        Me.Close()

    End Sub
    Private Sub GridBanco_DoubleClick(sender As Object, e As EventArgs) Handles GridBanco.DoubleClick

        If MessageBox.Show("Editar " & Me.GridViewBanco.GetRowCellValue(Me.GridViewBanco.FocusedRowHandle, "Proveedor"), "Aviso Importante", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        If Me.GridViewBanco.GetRowCellValue(Me.GridViewBanco.FocusedRowHandle, "ProveedorID") <> Nothing Then

            If Me.GridViewBanco.RowCount > 0 Then

                My.Forms.Frm_IU_Proveedor.Accion = "EDIT"
                My.Forms.Frm_IU_Proveedor.ID = Me.GridViewBanco.GetRowCellValue(Me.GridViewBanco.FocusedRowHandle, "ProveedorID")
                My.Forms.Frm_IU_Proveedor.MdiParent = Me.MdiParent
                My.Forms.Frm_IU_Proveedor.Show()

            End If
        End If

    End Sub

 
End Class