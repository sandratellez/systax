﻿Public Class Motivo_Anula
    Public motivo As String
    Private Sub BtnCancelar_Click(sender As Object, e As EventArgs) Handles BtnCancelar.Click
        Me.DialogResult = Windows.Forms.DialogResult.No
    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        If EditMotivo_anula.Text = "" Or EditMotivo_anula.Text = " " Then
            MessageBox.Show("Escriba el motivo de anulacion")
            Exit Sub
        End If
        Me.DialogResult = Windows.Forms.DialogResult.Yes
    End Sub

    Private Sub Motivo_Anula_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If EditMotivo_anula.Text = "" Then
            MessageBox.Show("Escriba el motivo de anulacion")
            Exit Sub
        End If
        motivo = EditMotivo_anula.Text

    End Sub

    Private Sub Motivo_Anula_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        motivo = Nothing
        EditMotivo_anula.Text = Nothing
    End Sub
End Class