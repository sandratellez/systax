<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_ausr
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_ausr))
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.BtnRegresar = New System.Windows.Forms.Button()
        Me.btnguardar = New System.Windows.Forms.Button()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.opt_duplicado = New DevExpress.XtraEditors.ToggleSwitch()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.opt_servicios = New DevExpress.XtraEditors.ToggleSwitch()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.opt_tarifas = New DevExpress.XtraEditors.ToggleSwitch()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.opt_rectificar = New DevExpress.XtraEditors.ToggleSwitch()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.opt_anular = New DevExpress.XtraEditors.ToggleSwitch()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.opt_cliente = New DevExpress.XtraEditors.ToggleSwitch()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.opt_arqueo = New DevExpress.XtraEditors.ToggleSwitch()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.opt_ac_sesion = New DevExpress.XtraEditors.ToggleSwitch()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.opt_ac_fecha = New DevExpress.XtraEditors.ToggleSwitch()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.opt_estado = New DevExpress.XtraEditors.ToggleSwitch()
        Me.dpt_baja = New DevExpress.XtraEditors.DateEdit()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cmb_rol = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.dpt_fecha_ingreso = New DevExpress.XtraEditors.DateEdit()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.pic_foto = New DevExpress.XtraEditors.PictureEdit()
        Me.txtconfirmar = New System.Windows.Forms.TextBox()
        Me.txtcontraseña = New System.Windows.Forms.TextBox()
        Me.txtusr = New System.Windows.Forms.TextBox()
        Me.txt_telefono = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_celular = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_cedula = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtapellidos = New System.Windows.Forms.TextBox()
        Me.txtnombre = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.gridData = New DevExpress.XtraGrid.GridControl()
        Me.v_fabrica = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.vProductores = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.btn_remove = New DevExpress.XtraEditors.SimpleButton()
        Me.btn_add = New DevExpress.XtraEditors.SimpleButton()
        Me.g_oficinasAsignadas = New DevExpress.XtraGrid.GridControl()
        Me.v_oficinasasignadas = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn21 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn22 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCheckEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.GridView5 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn25 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn26 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn27 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn28 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.g_oficinadisponibles = New DevExpress.XtraGrid.GridControl()
        Me.v_oficinasdisponibles = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCheckEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn17 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn18 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn19 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn20 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Open = New System.Windows.Forms.OpenFileDialog()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.opt_duplicado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.opt_servicios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.opt_tarifas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.opt_rectificar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.opt_anular.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.opt_cliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.opt_arqueo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.opt_ac_sesion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.opt_ac_fecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.opt_estado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpt_baja.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpt_baja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmb_rol.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpt_fecha_ingreso.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dpt_fecha_ingreso.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_foto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.gridData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.v_fabrica, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.vProductores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.g_oficinasAsignadas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.v_oficinasasignadas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.g_oficinadisponibles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.v_oficinasdisponibles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.BtnRegresar)
        Me.GroupControl4.Controls.Add(Me.btnguardar)
        Me.GroupControl4.Dock = System.Windows.Forms.DockStyle.Left
        Me.GroupControl4.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl4.LookAndFeel.SkinName = "Black"
        Me.GroupControl4.LookAndFeel.UseDefaultLookAndFeel = False
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(150, 451)
        Me.GroupControl4.TabIndex = 67
        Me.GroupControl4.Text = "Opciones"
        '
        'BtnRegresar
        '
        Me.BtnRegresar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnRegresar.ForeColor = System.Drawing.Color.Black
        Me.BtnRegresar.Image = CType(resources.GetObject("BtnRegresar.Image"), System.Drawing.Image)
        Me.BtnRegresar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnRegresar.Location = New System.Drawing.Point(8, 76)
        Me.BtnRegresar.Name = "BtnRegresar"
        Me.BtnRegresar.Size = New System.Drawing.Size(136, 45)
        Me.BtnRegresar.TabIndex = 55
        Me.BtnRegresar.Text = "&Regresar"
        Me.BtnRegresar.UseVisualStyleBackColor = True
        '
        'btnguardar
        '
        Me.btnguardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnguardar.Image = CType(resources.GetObject("btnguardar.Image"), System.Drawing.Image)
        Me.btnguardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnguardar.Location = New System.Drawing.Point(8, 26)
        Me.btnguardar.Name = "btnguardar"
        Me.btnguardar.Size = New System.Drawing.Size(136, 45)
        Me.btnguardar.TabIndex = 54
        Me.btnguardar.Tag = "Boton de Guardar Usuario"
        Me.btnguardar.Text = "&Guardar"
        Me.btnguardar.UseVisualStyleBackColor = True
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(150, 0)
        Me.XtraTabControl1.LookAndFeel.SkinName = "Black"
        Me.XtraTabControl1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(1155, 451)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.GroupBox2)
        Me.XtraTabPage1.Image = CType(resources.GetObject("XtraTabPage1.Image"), System.Drawing.Image)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(1149, 420)
        Me.XtraTabPage1.Text = "Datos Generales"
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.opt_duplicado)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.opt_servicios)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.opt_tarifas)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.opt_rectificar)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.opt_anular)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.opt_cliente)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.opt_arqueo)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.opt_ac_sesion)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.opt_ac_fecha)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.opt_estado)
        Me.GroupBox2.Controls.Add(Me.dpt_baja)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.cmb_rol)
        Me.GroupBox2.Controls.Add(Me.dpt_fecha_ingreso)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.pic_foto)
        Me.GroupBox2.Controls.Add(Me.txtconfirmar)
        Me.GroupBox2.Controls.Add(Me.txtcontraseña)
        Me.GroupBox2.Controls.Add(Me.txtusr)
        Me.GroupBox2.Controls.Add(Me.txt_telefono)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.txt_celular)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.txt_cedula)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.txtapellidos)
        Me.GroupBox2.Controls.Add(Me.txtnombre)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Location = New System.Drawing.Point(5, 2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1122, 621)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos Generales del Usuario"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Black
        Me.Label21.Location = New System.Drawing.Point(725, 318)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(109, 13)
        Me.Label21.TabIndex = 529
        Me.Label21.Tag = "Actualizar Cliente"
        Me.Label21.Text = "Duplicado de Factura"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label21.Visible = False
        '
        'opt_duplicado
        '
        Me.opt_duplicado.EditValue = True
        Me.opt_duplicado.Location = New System.Drawing.Point(841, 313)
        Me.opt_duplicado.Name = "opt_duplicado"
        Me.opt_duplicado.Properties.OffText = "No"
        Me.opt_duplicado.Properties.OnText = "Si"
        Me.opt_duplicado.Size = New System.Drawing.Size(95, 24)
        Me.opt_duplicado.TabIndex = 528
        Me.opt_duplicado.Visible = False
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(724, 288)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(110, 13)
        Me.Label19.TabIndex = 527
        Me.Label19.Tag = "Actualizar Cliente"
        Me.Label19.Text = "Servicios Comerciales"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label19.Visible = False
        '
        'opt_servicios
        '
        Me.opt_servicios.EditValue = True
        Me.opt_servicios.Location = New System.Drawing.Point(841, 283)
        Me.opt_servicios.Name = "opt_servicios"
        Me.opt_servicios.Properties.OffText = "No"
        Me.opt_servicios.Properties.OnText = "Si"
        Me.opt_servicios.Size = New System.Drawing.Size(95, 24)
        Me.opt_servicios.TabIndex = 526
        Me.opt_servicios.Visible = False
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Black
        Me.Label20.Location = New System.Drawing.Point(748, 258)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(86, 13)
        Me.Label20.TabIndex = 525
        Me.Label20.Tag = "Actualizar Cliente"
        Me.Label20.Text = "Pliego de Tarifas"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label20.Visible = False
        '
        'opt_tarifas
        '
        Me.opt_tarifas.EditValue = True
        Me.opt_tarifas.Location = New System.Drawing.Point(841, 253)
        Me.opt_tarifas.Name = "opt_tarifas"
        Me.opt_tarifas.Properties.OffText = "No"
        Me.opt_tarifas.Properties.OnText = "Si"
        Me.opt_tarifas.Size = New System.Drawing.Size(95, 24)
        Me.opt_tarifas.TabIndex = 524
        Me.opt_tarifas.Visible = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Black
        Me.Label18.Location = New System.Drawing.Point(782, 228)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(52, 13)
        Me.Label18.TabIndex = 523
        Me.Label18.Tag = "Actualizar Cliente"
        Me.Label18.Text = "Rectificar"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label18.Visible = False
        '
        'opt_rectificar
        '
        Me.opt_rectificar.EditValue = True
        Me.opt_rectificar.Location = New System.Drawing.Point(841, 223)
        Me.opt_rectificar.Name = "opt_rectificar"
        Me.opt_rectificar.Properties.OffText = "No"
        Me.opt_rectificar.Properties.OnText = "Si"
        Me.opt_rectificar.Size = New System.Drawing.Size(95, 24)
        Me.opt_rectificar.TabIndex = 522
        Me.opt_rectificar.Visible = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Black
        Me.Label17.Location = New System.Drawing.Point(760, 198)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(74, 13)
        Me.Label17.TabIndex = 521
        Me.Label17.Tag = "Actualizar Cliente"
        Me.Label17.Text = "Anular Recibo"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label17.Visible = False
        '
        'opt_anular
        '
        Me.opt_anular.EditValue = True
        Me.opt_anular.Location = New System.Drawing.Point(841, 193)
        Me.opt_anular.Name = "opt_anular"
        Me.opt_anular.Properties.OffText = "No"
        Me.opt_anular.Properties.OnText = "Si"
        Me.opt_anular.Size = New System.Drawing.Size(95, 24)
        Me.opt_anular.TabIndex = 520
        Me.opt_anular.Visible = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(746, 168)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(88, 13)
        Me.Label16.TabIndex = 519
        Me.Label16.Tag = "Actualizar Cliente"
        Me.Label16.Text = "Actualizar Cliente"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label16.Visible = False
        '
        'opt_cliente
        '
        Me.opt_cliente.EditValue = True
        Me.opt_cliente.Location = New System.Drawing.Point(841, 163)
        Me.opt_cliente.Name = "opt_cliente"
        Me.opt_cliente.Properties.OffText = "No"
        Me.opt_cliente.Properties.OnText = "Si"
        Me.opt_cliente.Size = New System.Drawing.Size(95, 24)
        Me.opt_cliente.TabIndex = 518
        Me.opt_cliente.Visible = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(739, 138)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(95, 13)
        Me.Label15.TabIndex = 517
        Me.Label15.Text = "Arqueo de Factura"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label15.Visible = False
        '
        'opt_arqueo
        '
        Me.opt_arqueo.EditValue = True
        Me.opt_arqueo.Location = New System.Drawing.Point(841, 133)
        Me.opt_arqueo.Name = "opt_arqueo"
        Me.opt_arqueo.Properties.OffText = "No"
        Me.opt_arqueo.Properties.OnText = "Si"
        Me.opt_arqueo.Size = New System.Drawing.Size(95, 24)
        Me.opt_arqueo.TabIndex = 516
        Me.opt_arqueo.Visible = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(717, 108)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(117, 13)
        Me.Label14.TabIndex = 515
        Me.Label14.Text = "Apetura y Cierre Sesión"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label14.Visible = False
        '
        'opt_ac_sesion
        '
        Me.opt_ac_sesion.EditValue = True
        Me.opt_ac_sesion.Location = New System.Drawing.Point(841, 103)
        Me.opt_ac_sesion.Name = "opt_ac_sesion"
        Me.opt_ac_sesion.Properties.OffText = "No"
        Me.opt_ac_sesion.Properties.OnText = "Si"
        Me.opt_ac_sesion.Size = New System.Drawing.Size(95, 24)
        Me.opt_ac_sesion.TabIndex = 514
        Me.opt_ac_sesion.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(719, 78)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(115, 13)
        Me.Label13.TabIndex = 513
        Me.Label13.Text = "Apetura y Cierre Fecha"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label13.Visible = False
        '
        'opt_ac_fecha
        '
        Me.opt_ac_fecha.EditValue = True
        Me.opt_ac_fecha.Location = New System.Drawing.Point(841, 73)
        Me.opt_ac_fecha.Name = "opt_ac_fecha"
        Me.opt_ac_fecha.Properties.OffText = "No"
        Me.opt_ac_fecha.Properties.OnText = "Si"
        Me.opt_ac_fecha.Size = New System.Drawing.Size(95, 24)
        Me.opt_ac_fecha.TabIndex = 512
        Me.opt_ac_fecha.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(360, 328)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(37, 13)
        Me.Label7.TabIndex = 511
        Me.Label7.Text = "Activo"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'opt_estado
        '
        Me.opt_estado.EditValue = True
        Me.opt_estado.Location = New System.Drawing.Point(403, 324)
        Me.opt_estado.Name = "opt_estado"
        Me.opt_estado.Properties.OffText = "No"
        Me.opt_estado.Properties.OnText = "Si"
        Me.opt_estado.Size = New System.Drawing.Size(95, 24)
        Me.opt_estado.TabIndex = 11
        '
        'dpt_baja
        '
        Me.dpt_baja.EditValue = Nothing
        Me.dpt_baja.Location = New System.Drawing.Point(403, 297)
        Me.dpt_baja.Name = "dpt_baja"
        Me.dpt_baja.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dpt_baja.Properties.Appearance.Options.UseFont = True
        Me.dpt_baja.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.dpt_baja.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpt_baja.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dpt_baja.Size = New System.Drawing.Size(315, 22)
        Me.dpt_baja.TabIndex = 10
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(368, 300)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(31, 13)
        Me.Label12.TabIndex = 58
        Me.Label12.Text = "Baja:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmb_rol
        '
        Me.cmb_rol.EditValue = ""
        Me.cmb_rol.EnterMoveNextControl = True
        Me.cmb_rol.Location = New System.Drawing.Point(403, 180)
        Me.cmb_rol.Name = "cmb_rol"
        Me.cmb_rol.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmb_rol.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.cmb_rol.Properties.Appearance.Options.UseFont = True
        Me.cmb_rol.Properties.Appearance.Options.UseForeColor = True
        Me.cmb_rol.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.cmb_rol.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cmb_rol.Properties.DisplayMember = "Nombre"
        Me.cmb_rol.Properties.LookAndFeel.SkinName = "Black"
        Me.cmb_rol.Properties.LookAndFeel.UseDefaultLookAndFeel = False
        Me.cmb_rol.Properties.NullText = ""
        Me.cmb_rol.Properties.PopupFormMinSize = New System.Drawing.Size(300, 0)
        Me.cmb_rol.Properties.ValueMember = "Codigo"
        Me.cmb_rol.Properties.View = Me.GridView4
        Me.cmb_rol.Size = New System.Drawing.Size(315, 22)
        Me.cmb_rol.TabIndex = 6
        '
        'GridView4
        '
        Me.GridView4.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn7, Me.GridColumn8, Me.GridColumn10})
        Me.GridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView4.OptionsView.EnableAppearanceEvenRow = True
        Me.GridView4.OptionsView.EnableAppearanceOddRow = True
        Me.GridView4.OptionsView.ShowAutoFilterRow = True
        Me.GridView4.OptionsView.ShowGroupPanel = False
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Cód_Rol"
        Me.GridColumn7.FieldName = "cod_rol"
        Me.GridColumn7.MinWidth = 50
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.OptionsColumn.ReadOnly = True
        Me.GridColumn7.Width = 50
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Rol"
        Me.GridColumn8.FieldName = "rol"
        Me.GridColumn8.MinWidth = 300
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 0
        Me.GridColumn8.Width = 300
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "c"
        Me.GridColumn10.FieldName = "c"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.OptionsColumn.ReadOnly = True
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 1
        '
        'dpt_fecha_ingreso
        '
        Me.dpt_fecha_ingreso.EditValue = Nothing
        Me.dpt_fecha_ingreso.Location = New System.Drawing.Point(403, 271)
        Me.dpt_fecha_ingreso.Name = "dpt_fecha_ingreso"
        Me.dpt_fecha_ingreso.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dpt_fecha_ingreso.Properties.Appearance.Options.UseFont = True
        Me.dpt_fecha_ingreso.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.dpt_fecha_ingreso.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dpt_fecha_ingreso.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dpt_fecha_ingreso.Size = New System.Drawing.Size(315, 22)
        Me.dpt_fecha_ingreso.TabIndex = 9
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(354, 274)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(45, 13)
        Me.Label8.TabIndex = 56
        Me.Label8.Text = "Ingreso:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pic_foto
        '
        Me.pic_foto.Cursor = System.Windows.Forms.Cursors.Default
        Me.pic_foto.Location = New System.Drawing.Point(6, 19)
        Me.pic_foto.Name = "pic_foto"
        Me.pic_foto.Properties.PictureStoreMode = DevExpress.XtraEditors.Controls.PictureStoreMode.ByteArray
        Me.pic_foto.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze
        Me.pic_foto.Properties.ZoomAccelerationFactor = 1.0R
        Me.pic_foto.Size = New System.Drawing.Size(319, 351)
        Me.pic_foto.TabIndex = 67
        '
        'txtconfirmar
        '
        Me.txtconfirmar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtconfirmar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtconfirmar.ForeColor = System.Drawing.Color.Black
        Me.txtconfirmar.Location = New System.Drawing.Point(403, 245)
        Me.txtconfirmar.MaxLength = 10
        Me.txtconfirmar.Name = "txtconfirmar"
        Me.txtconfirmar.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtconfirmar.Size = New System.Drawing.Size(315, 22)
        Me.txtconfirmar.TabIndex = 8
        '
        'txtcontraseña
        '
        Me.txtcontraseña.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtcontraseña.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcontraseña.ForeColor = System.Drawing.Color.Black
        Me.txtcontraseña.Location = New System.Drawing.Point(403, 219)
        Me.txtcontraseña.MaxLength = 10
        Me.txtcontraseña.Name = "txtcontraseña"
        Me.txtcontraseña.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtcontraseña.Size = New System.Drawing.Size(315, 22)
        Me.txtcontraseña.TabIndex = 7
        '
        'txtusr
        '
        Me.txtusr.BackColor = System.Drawing.Color.Gainsboro
        Me.txtusr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtusr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtusr.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtusr.ForeColor = System.Drawing.Color.Black
        Me.txtusr.Location = New System.Drawing.Point(403, 49)
        Me.txtusr.MaxLength = 10
        Me.txtusr.Name = "txtusr"
        Me.txtusr.Size = New System.Drawing.Size(112, 22)
        Me.txtusr.TabIndex = 0
        '
        'txt_telefono
        '
        Me.txt_telefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_telefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_telefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_telefono.ForeColor = System.Drawing.Color.Black
        Me.txt_telefono.Location = New System.Drawing.Point(589, 153)
        Me.txt_telefono.MaxLength = 50
        Me.txt_telefono.Name = "txt_telefono"
        Me.txt_telefono.Size = New System.Drawing.Size(129, 22)
        Me.txt_telefono.TabIndex = 5
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(521, 155)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(62, 16)
        Me.Label11.TabIndex = 63
        Me.Label11.Text = "Telefono"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt_celular
        '
        Me.txt_celular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_celular.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_celular.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_celular.ForeColor = System.Drawing.Color.Black
        Me.txt_celular.Location = New System.Drawing.Point(403, 153)
        Me.txt_celular.MaxLength = 50
        Me.txt_celular.Name = "txt_celular"
        Me.txt_celular.Size = New System.Drawing.Size(112, 22)
        Me.txt_celular.TabIndex = 4
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(360, 157)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(39, 13)
        Me.Label10.TabIndex = 61
        Me.Label10.Text = "Celular"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt_cedula
        '
        Me.txt_cedula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cedula.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txt_cedula.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_cedula.ForeColor = System.Drawing.Color.Black
        Me.txt_cedula.Location = New System.Drawing.Point(403, 127)
        Me.txt_cedula.MaxLength = 50
        Me.txt_cedula.Name = "txt_cedula"
        Me.txt_cedula.Size = New System.Drawing.Size(315, 22)
        Me.txt_cedula.TabIndex = 3
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(339, 131)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(60, 13)
        Me.Label9.TabIndex = 59
        Me.Label9.Text = "No. Cedula"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtapellidos
        '
        Me.txtapellidos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtapellidos.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtapellidos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtapellidos.ForeColor = System.Drawing.Color.Black
        Me.txtapellidos.Location = New System.Drawing.Point(403, 101)
        Me.txtapellidos.MaxLength = 50
        Me.txtapellidos.Name = "txtapellidos"
        Me.txtapellidos.Size = New System.Drawing.Size(315, 22)
        Me.txtapellidos.TabIndex = 2
        '
        'txtnombre
        '
        Me.txtnombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtnombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtnombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnombre.ForeColor = System.Drawing.Color.Black
        Me.txtnombre.Location = New System.Drawing.Point(403, 75)
        Me.txtnombre.MaxLength = 50
        Me.txtnombre.Name = "txtnombre"
        Me.txtnombre.Size = New System.Drawing.Size(315, 22)
        Me.txtnombre.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(329, 183)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(70, 13)
        Me.Label6.TabIndex = 54
        Me.Label6.Text = "Rol Asignado"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(356, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 37
        Me.Label1.Text = "Usuario"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(339, 222)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 38
        Me.Label2.Text = "Contraseña"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(339, 249)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 13)
        Me.Label3.TabIndex = 39
        Me.Label3.Text = "Contraseña"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(350, 105)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 13)
        Me.Label5.TabIndex = 41
        Me.Label5.Text = "Apellidos"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(350, 79)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 13)
        Me.Label4.TabIndex = 40
        Me.Label4.Text = "Nombres"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.gridData)
        Me.XtraTabPage2.Image = CType(resources.GetObject("XtraTabPage2.Image"), System.Drawing.Image)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.PageVisible = False
        Me.XtraTabPage2.Size = New System.Drawing.Size(1149, 420)
        Me.XtraTabPage2.Text = "Configuraciónes"
        '
        'gridData
        '
        Me.gridData.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gridData.Location = New System.Drawing.Point(5, 3)
        Me.gridData.LookAndFeel.SkinName = "Black"
        Me.gridData.MainView = Me.v_fabrica
        Me.gridData.Name = "gridData"
        Me.gridData.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.gridData.Size = New System.Drawing.Size(512, 616)
        Me.gridData.TabIndex = 386
        Me.gridData.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.v_fabrica, Me.vProductores})
        '
        'v_fabrica
        '
        Me.v_fabrica.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_fabrica.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_fabrica.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.v_fabrica.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.v_fabrica.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.v_fabrica.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.v_fabrica.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(154, Byte), Integer), CType(CType(190, Byte), Integer), CType(CType(243, Byte), Integer))
        Me.v_fabrica.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.v_fabrica.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_fabrica.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.v_fabrica.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.v_fabrica.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.v_fabrica.Appearance.Empty.BackColor = System.Drawing.Color.White
        Me.v_fabrica.Appearance.Empty.Options.UseBackColor = True
        Me.v_fabrica.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.EvenRow.Options.UseBackColor = True
        Me.v_fabrica.Appearance.EvenRow.Options.UseForeColor = True
        Me.v_fabrica.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_fabrica.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_fabrica.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.v_fabrica.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.v_fabrica.Appearance.FilterCloseButton.Options.UseForeColor = True
        Me.v_fabrica.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(62, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.v_fabrica.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White
        Me.v_fabrica.Appearance.FilterPanel.Options.UseBackColor = True
        Me.v_fabrica.Appearance.FilterPanel.Options.UseForeColor = True
        Me.v_fabrica.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.v_fabrica.Appearance.FixedLine.Options.UseBackColor = True
        Me.v_fabrica.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
        Me.v_fabrica.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.FocusedCell.Options.UseBackColor = True
        Me.v_fabrica.Appearance.FocusedCell.Options.UseForeColor = True
        Me.v_fabrica.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(197, Byte), Integer))
        Me.v_fabrica.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.v_fabrica.Appearance.FocusedRow.Options.UseBackColor = True
        Me.v_fabrica.Appearance.FocusedRow.Options.UseForeColor = True
        Me.v_fabrica.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_fabrica.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.FooterPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.v_fabrica.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_fabrica.Appearance.FooterPanel.Options.UseBackColor = True
        Me.v_fabrica.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.v_fabrica.Appearance.FooterPanel.Options.UseFont = True
        Me.v_fabrica.Appearance.FooterPanel.Options.UseForeColor = True
        Me.v_fabrica.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_fabrica.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_fabrica.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.GroupButton.Options.UseBackColor = True
        Me.v_fabrica.Appearance.GroupButton.Options.UseBorderColor = True
        Me.v_fabrica.Appearance.GroupButton.Options.UseForeColor = True
        Me.v_fabrica.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_fabrica.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_fabrica.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.GroupFooter.Options.UseBackColor = True
        Me.v_fabrica.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.v_fabrica.Appearance.GroupFooter.Options.UseForeColor = True
        Me.v_fabrica.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(62, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.v_fabrica.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.GroupPanel.Options.UseBackColor = True
        Me.v_fabrica.Appearance.GroupPanel.Options.UseForeColor = True
        Me.v_fabrica.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_fabrica.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_fabrica.Appearance.GroupRow.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.v_fabrica.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.GroupRow.Options.UseBackColor = True
        Me.v_fabrica.Appearance.GroupRow.Options.UseBorderColor = True
        Me.v_fabrica.Appearance.GroupRow.Options.UseFont = True
        Me.v_fabrica.Appearance.GroupRow.Options.UseForeColor = True
        Me.v_fabrica.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_fabrica.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_fabrica.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_fabrica.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.v_fabrica.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.v_fabrica.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.v_fabrica.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(106, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_fabrica.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.v_fabrica.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.v_fabrica.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.v_fabrica.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(99, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(196, Byte), Integer))
        Me.v_fabrica.Appearance.HorzLine.Options.UseBackColor = True
        Me.v_fabrica.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.v_fabrica.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.OddRow.Options.UseBackColor = True
        Me.v_fabrica.Appearance.OddRow.Options.UseForeColor = True
        Me.v_fabrica.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.v_fabrica.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.v_fabrica.Appearance.Preview.Options.UseBackColor = True
        Me.v_fabrica.Appearance.Preview.Options.UseForeColor = True
        Me.v_fabrica.Appearance.Row.BackColor = System.Drawing.Color.White
        Me.v_fabrica.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.v_fabrica.Appearance.Row.Options.UseBackColor = True
        Me.v_fabrica.Appearance.Row.Options.UseForeColor = True
        Me.v_fabrica.Appearance.RowSeparator.BackColor = System.Drawing.Color.White
        Me.v_fabrica.Appearance.RowSeparator.Options.UseBackColor = True
        Me.v_fabrica.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(217, Byte), Integer))
        Me.v_fabrica.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
        Me.v_fabrica.Appearance.SelectedRow.Options.UseBackColor = True
        Me.v_fabrica.Appearance.SelectedRow.Options.UseForeColor = True
        Me.v_fabrica.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(99, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(196, Byte), Integer))
        Me.v_fabrica.Appearance.VertLine.Options.UseBackColor = True
        Me.v_fabrica.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn15, Me.GridColumn1, Me.GridColumn2, Me.GridColumn6})
        Me.v_fabrica.GridControl = Me.gridData
        Me.v_fabrica.Name = "v_fabrica"
        Me.v_fabrica.OptionsView.EnableAppearanceEvenRow = True
        Me.v_fabrica.OptionsView.EnableAppearanceOddRow = True
        Me.v_fabrica.OptionsView.ShowAutoFilterRow = True
        Me.v_fabrica.OptionsView.ShowFooter = True
        Me.v_fabrica.OptionsView.ShowGroupPanel = False
        Me.v_fabrica.OptionsView.ShowIndicator = False
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "CajaID"
        Me.GridColumn15.FieldName = "CajaID"
        Me.GridColumn15.Image = CType(resources.GetObject("GridColumn15.Image"), System.Drawing.Image)
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = True
        Me.GridColumn15.VisibleIndex = 0
        Me.GridColumn15.Width = 67
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Nombre de la Caja"
        Me.GridColumn1.FieldName = "CajaNombre"
        Me.GridColumn1.Image = CType(resources.GetObject("GridColumn1.Image"), System.Drawing.Image)
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        Me.GridColumn1.Width = 142
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Nombre PC"
        Me.GridColumn2.FieldName = "CajaPC"
        Me.GridColumn2.Image = CType(resources.GetObject("GridColumn2.Image"), System.Drawing.Image)
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        Me.GridColumn2.Width = 147
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Asignar"
        Me.GridColumn6.FieldName = "Asignar"
        Me.GridColumn6.Image = CType(resources.GetObject("GridColumn6.Image"), System.Drawing.Image)
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 3
        Me.GridColumn6.Width = 93
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Caption = "Check"
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'vProductores
        '
        Me.vProductores.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray
        Me.vProductores.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray
        Me.vProductores.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray
        Me.vProductores.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.vProductores.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.vProductores.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.vProductores.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray
        Me.vProductores.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray
        Me.vProductores.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro
        Me.vProductores.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.vProductores.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.vProductores.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.vProductores.Appearance.Empty.BackColor = System.Drawing.Color.DimGray
        Me.vProductores.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal
        Me.vProductores.Appearance.Empty.Options.UseBackColor = True
        Me.vProductores.Appearance.EvenRow.BackColor = System.Drawing.Color.White
        Me.vProductores.Appearance.EvenRow.Options.UseBackColor = True
        Me.vProductores.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray
        Me.vProductores.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray
        Me.vProductores.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.vProductores.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.vProductores.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray
        Me.vProductores.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
        Me.vProductores.Appearance.FilterPanel.Options.UseBackColor = True
        Me.vProductores.Appearance.FilterPanel.Options.UseForeColor = True
        Me.vProductores.Appearance.FocusedRow.BackColor = System.Drawing.Color.Black
        Me.vProductores.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.vProductores.Appearance.FocusedRow.Options.UseBackColor = True
        Me.vProductores.Appearance.FocusedRow.Options.UseForeColor = True
        Me.vProductores.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray
        Me.vProductores.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray
        Me.vProductores.Appearance.FooterPanel.Options.UseBackColor = True
        Me.vProductores.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.vProductores.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver
        Me.vProductores.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver
        Me.vProductores.Appearance.GroupButton.Options.UseBackColor = True
        Me.vProductores.Appearance.GroupButton.Options.UseBorderColor = True
        Me.vProductores.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver
        Me.vProductores.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver
        Me.vProductores.Appearance.GroupFooter.Options.UseBackColor = True
        Me.vProductores.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.vProductores.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray
        Me.vProductores.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White
        Me.vProductores.Appearance.GroupPanel.Options.UseBackColor = True
        Me.vProductores.Appearance.GroupPanel.Options.UseForeColor = True
        Me.vProductores.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver
        Me.vProductores.Appearance.GroupRow.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.vProductores.Appearance.GroupRow.Options.UseBackColor = True
        Me.vProductores.Appearance.GroupRow.Options.UseFont = True
        Me.vProductores.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray
        Me.vProductores.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray
        Me.vProductores.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.vProductores.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.vProductores.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray
        Me.vProductores.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.vProductores.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray
        Me.vProductores.Appearance.HorzLine.Options.UseBackColor = True
        Me.vProductores.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke
        Me.vProductores.Appearance.OddRow.Options.UseBackColor = True
        Me.vProductores.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro
        Me.vProductores.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray
        Me.vProductores.Appearance.Preview.Options.UseBackColor = True
        Me.vProductores.Appearance.Preview.Options.UseForeColor = True
        Me.vProductores.Appearance.Row.BackColor = System.Drawing.Color.White
        Me.vProductores.Appearance.Row.Options.UseBackColor = True
        Me.vProductores.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray
        Me.vProductores.Appearance.RowSeparator.Options.UseBackColor = True
        Me.vProductores.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray
        Me.vProductores.Appearance.SelectedRow.Options.UseBackColor = True
        Me.vProductores.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray
        Me.vProductores.Appearance.VertLine.Options.UseBackColor = True
        Me.vProductores.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn3, Me.GridColumn4, Me.GridColumn11, Me.GridColumn12})
        Me.vProductores.GridControl = Me.gridData
        Me.vProductores.Name = "vProductores"
        Me.vProductores.OptionsView.EnableAppearanceEvenRow = True
        Me.vProductores.OptionsView.EnableAppearanceOddRow = True
        Me.vProductores.OptionsView.ShowAutoFilterRow = True
        Me.vProductores.OptionsView.ShowGroupPanel = False
        Me.vProductores.PaintStyleName = "UltraFlat"
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "No"
        Me.GridColumn3.FieldName = "Nucleo_No"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 0
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Nombre"
        Me.GridColumn4.FieldName = "Nucleo_Nombre"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 1
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Dirección"
        Me.GridColumn11.FieldName = "Nucleo_Direccion"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 2
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "GridColumn4"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 3
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.btn_remove)
        Me.XtraTabPage3.Controls.Add(Me.btn_add)
        Me.XtraTabPage3.Controls.Add(Me.g_oficinasAsignadas)
        Me.XtraTabPage3.Controls.Add(Me.g_oficinadisponibles)
        Me.XtraTabPage3.Image = CType(resources.GetObject("XtraTabPage3.Image"), System.Drawing.Image)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(1149, 420)
        Me.XtraTabPage3.Text = "Oficinas"
        '
        'btn_remove
        '
        Me.btn_remove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_remove.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_remove.Appearance.Options.UseFont = True
        Me.btn_remove.Location = New System.Drawing.Point(505, 258)
        Me.btn_remove.Name = "btn_remove"
        Me.btn_remove.Size = New System.Drawing.Size(75, 59)
        Me.btn_remove.TabIndex = 390
        Me.btn_remove.Text = "<<"
        '
        'btn_add
        '
        Me.btn_add.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_add.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_add.Appearance.Options.UseFont = True
        Me.btn_add.Location = New System.Drawing.Point(505, 182)
        Me.btn_add.Name = "btn_add"
        Me.btn_add.Size = New System.Drawing.Size(75, 59)
        Me.btn_add.TabIndex = 389
        Me.btn_add.Text = ">>"
        '
        'g_oficinasAsignadas
        '
        Me.g_oficinasAsignadas.Dock = System.Windows.Forms.DockStyle.Right
        Me.g_oficinasAsignadas.Location = New System.Drawing.Point(586, 0)
        Me.g_oficinasAsignadas.LookAndFeel.SkinName = "Black"
        Me.g_oficinasAsignadas.MainView = Me.v_oficinasasignadas
        Me.g_oficinasAsignadas.Name = "g_oficinasAsignadas"
        Me.g_oficinasAsignadas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit3})
        Me.g_oficinasAsignadas.Size = New System.Drawing.Size(563, 420)
        Me.g_oficinasAsignadas.TabIndex = 388
        Me.g_oficinasAsignadas.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.v_oficinasasignadas, Me.GridView5})
        '
        'v_oficinasasignadas
        '
        Me.v_oficinasasignadas.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasasignadas.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_oficinasasignadas.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.v_oficinasasignadas.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(154, Byte), Integer), CType(CType(190, Byte), Integer), CType(CType(243, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasasignadas.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_oficinasasignadas.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.v_oficinasasignadas.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.Empty.BackColor = System.Drawing.Color.White
        Me.v_oficinasasignadas.Appearance.Empty.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.EvenRow.BackColor = System.Drawing.Color.White
        Me.v_oficinasasignadas.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasasignadas.Appearance.EvenRow.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.EvenRow.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasasignadas.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_oficinasasignadas.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.v_oficinasasignadas.Appearance.FilterCloseButton.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(62, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White
        Me.v_oficinasasignadas.Appearance.FilterPanel.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.FilterPanel.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.FixedLine.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.FocusedCell.BackColor = System.Drawing.Color.Blue
        Me.v_oficinasasignadas.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasasignadas.Appearance.FocusedCell.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.FocusedCell.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.FocusedRow.BackColor = System.Drawing.Color.Blue
        Me.v_oficinasasignadas.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.v_oficinasasignadas.Appearance.FocusedRow.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.FocusedRow.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.FooterPanel.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.v_oficinasasignadas.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasasignadas.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_oficinasasignadas.Appearance.FooterPanel.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.v_oficinasasignadas.Appearance.FooterPanel.Options.UseFont = True
        Me.v_oficinasasignadas.Appearance.FooterPanel.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.FooterPanel.Options.UseTextOptions = True
        Me.v_oficinasasignadas.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.v_oficinasasignadas.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasasignadas.Appearance.GroupButton.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.GroupButton.Options.UseBorderColor = True
        Me.v_oficinasasignadas.Appearance.GroupButton.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasasignadas.Appearance.GroupFooter.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.v_oficinasasignadas.Appearance.GroupFooter.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(62, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.GroupPanel.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.GroupPanel.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.GroupRow.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.v_oficinasasignadas.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasasignadas.Appearance.GroupRow.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.GroupRow.Options.UseBorderColor = True
        Me.v_oficinasasignadas.Appearance.GroupRow.Options.UseFont = True
        Me.v_oficinasasignadas.Appearance.GroupRow.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasasignadas.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_oficinasasignadas.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.v_oficinasasignadas.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Blue
        Me.v_oficinasasignadas.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.HorzLine.BackColor = System.Drawing.Color.White
        Me.v_oficinasasignadas.Appearance.HorzLine.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.v_oficinasasignadas.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasasignadas.Appearance.OddRow.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.OddRow.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.v_oficinasasignadas.Appearance.Preview.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.Preview.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.Row.BackColor = System.Drawing.Color.White
        Me.v_oficinasasignadas.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasasignadas.Appearance.Row.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.Row.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.RowSeparator.BackColor = System.Drawing.Color.White
        Me.v_oficinasasignadas.Appearance.RowSeparator.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.SelectedRow.BackColor = System.Drawing.Color.Blue
        Me.v_oficinasasignadas.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
        Me.v_oficinasasignadas.Appearance.SelectedRow.Options.UseBackColor = True
        Me.v_oficinasasignadas.Appearance.SelectedRow.Options.UseForeColor = True
        Me.v_oficinasasignadas.Appearance.VertLine.BackColor = System.Drawing.Color.White
        Me.v_oficinasasignadas.Appearance.VertLine.Options.UseBackColor = True
        Me.v_oficinasasignadas.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn21, Me.GridColumn22})
        Me.v_oficinasasignadas.GridControl = Me.g_oficinasAsignadas
        Me.v_oficinasasignadas.Name = "v_oficinasasignadas"
        Me.v_oficinasasignadas.OptionsBehavior.Editable = False
        Me.v_oficinasasignadas.OptionsBehavior.ReadOnly = True
        Me.v_oficinasasignadas.OptionsView.EnableAppearanceEvenRow = True
        Me.v_oficinasasignadas.OptionsView.EnableAppearanceOddRow = True
        Me.v_oficinasasignadas.OptionsView.ShowFooter = True
        Me.v_oficinasasignadas.OptionsView.ShowGroupPanel = False
        Me.v_oficinasasignadas.OptionsView.ShowIndicator = False
        Me.v_oficinasasignadas.RowHeight = 25
        '
        'GridColumn21
        '
        Me.GridColumn21.Caption = "OficinaID"
        Me.GridColumn21.FieldName = "OficinaID"
        Me.GridColumn21.Image = CType(resources.GetObject("GridColumn21.Image"), System.Drawing.Image)
        Me.GridColumn21.Name = "GridColumn21"
        Me.GridColumn21.Width = 67
        '
        'GridColumn22
        '
        Me.GridColumn22.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn22.AppearanceCell.Options.UseFont = True
        Me.GridColumn22.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn22.AppearanceHeader.Options.UseFont = True
        Me.GridColumn22.Caption = "Oficina Asignadas"
        Me.GridColumn22.FieldName = "Oficina"
        Me.GridColumn22.Image = CType(resources.GetObject("GridColumn22.Image"), System.Drawing.Image)
        Me.GridColumn22.Name = "GridColumn22"
        Me.GridColumn22.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "Oficina", "{0}")})
        Me.GridColumn22.Visible = True
        Me.GridColumn22.VisibleIndex = 0
        Me.GridColumn22.Width = 142
        '
        'RepositoryItemCheckEdit3
        '
        Me.RepositoryItemCheckEdit3.AutoHeight = False
        Me.RepositoryItemCheckEdit3.Caption = "Check"
        Me.RepositoryItemCheckEdit3.Name = "RepositoryItemCheckEdit3"
        '
        'GridView5
        '
        Me.GridView5.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray
        Me.GridView5.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray
        Me.GridView5.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray
        Me.GridView5.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.GridView5.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.GridView5.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.GridView5.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray
        Me.GridView5.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray
        Me.GridView5.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro
        Me.GridView5.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.GridView5.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.GridView5.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.GridView5.Appearance.Empty.BackColor = System.Drawing.Color.DimGray
        Me.GridView5.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal
        Me.GridView5.Appearance.Empty.Options.UseBackColor = True
        Me.GridView5.Appearance.EvenRow.BackColor = System.Drawing.Color.White
        Me.GridView5.Appearance.EvenRow.Options.UseBackColor = True
        Me.GridView5.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray
        Me.GridView5.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray
        Me.GridView5.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.GridView5.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.GridView5.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray
        Me.GridView5.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
        Me.GridView5.Appearance.FilterPanel.Options.UseBackColor = True
        Me.GridView5.Appearance.FilterPanel.Options.UseForeColor = True
        Me.GridView5.Appearance.FocusedRow.BackColor = System.Drawing.Color.Black
        Me.GridView5.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.GridView5.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView5.Appearance.FocusedRow.Options.UseForeColor = True
        Me.GridView5.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray
        Me.GridView5.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray
        Me.GridView5.Appearance.FooterPanel.Options.UseBackColor = True
        Me.GridView5.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.GridView5.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver
        Me.GridView5.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver
        Me.GridView5.Appearance.GroupButton.Options.UseBackColor = True
        Me.GridView5.Appearance.GroupButton.Options.UseBorderColor = True
        Me.GridView5.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver
        Me.GridView5.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver
        Me.GridView5.Appearance.GroupFooter.Options.UseBackColor = True
        Me.GridView5.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.GridView5.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray
        Me.GridView5.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White
        Me.GridView5.Appearance.GroupPanel.Options.UseBackColor = True
        Me.GridView5.Appearance.GroupPanel.Options.UseForeColor = True
        Me.GridView5.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver
        Me.GridView5.Appearance.GroupRow.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.GridView5.Appearance.GroupRow.Options.UseBackColor = True
        Me.GridView5.Appearance.GroupRow.Options.UseFont = True
        Me.GridView5.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray
        Me.GridView5.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray
        Me.GridView5.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.GridView5.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.GridView5.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray
        Me.GridView5.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView5.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray
        Me.GridView5.Appearance.HorzLine.Options.UseBackColor = True
        Me.GridView5.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke
        Me.GridView5.Appearance.OddRow.Options.UseBackColor = True
        Me.GridView5.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro
        Me.GridView5.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray
        Me.GridView5.Appearance.Preview.Options.UseBackColor = True
        Me.GridView5.Appearance.Preview.Options.UseForeColor = True
        Me.GridView5.Appearance.Row.BackColor = System.Drawing.Color.White
        Me.GridView5.Appearance.Row.Options.UseBackColor = True
        Me.GridView5.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray
        Me.GridView5.Appearance.RowSeparator.Options.UseBackColor = True
        Me.GridView5.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray
        Me.GridView5.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridView5.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray
        Me.GridView5.Appearance.VertLine.Options.UseBackColor = True
        Me.GridView5.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn25, Me.GridColumn26, Me.GridColumn27, Me.GridColumn28})
        Me.GridView5.GridControl = Me.g_oficinasAsignadas
        Me.GridView5.Name = "GridView5"
        Me.GridView5.OptionsView.EnableAppearanceEvenRow = True
        Me.GridView5.OptionsView.EnableAppearanceOddRow = True
        Me.GridView5.OptionsView.ShowAutoFilterRow = True
        Me.GridView5.OptionsView.ShowGroupPanel = False
        Me.GridView5.PaintStyleName = "UltraFlat"
        '
        'GridColumn25
        '
        Me.GridColumn25.Caption = "No"
        Me.GridColumn25.FieldName = "Nucleo_No"
        Me.GridColumn25.Name = "GridColumn25"
        Me.GridColumn25.Visible = True
        Me.GridColumn25.VisibleIndex = 0
        '
        'GridColumn26
        '
        Me.GridColumn26.Caption = "Nombre"
        Me.GridColumn26.FieldName = "Nucleo_Nombre"
        Me.GridColumn26.Name = "GridColumn26"
        Me.GridColumn26.Visible = True
        Me.GridColumn26.VisibleIndex = 1
        '
        'GridColumn27
        '
        Me.GridColumn27.Caption = "Dirección"
        Me.GridColumn27.FieldName = "Nucleo_Direccion"
        Me.GridColumn27.Name = "GridColumn27"
        Me.GridColumn27.Visible = True
        Me.GridColumn27.VisibleIndex = 2
        '
        'GridColumn28
        '
        Me.GridColumn28.Caption = "GridColumn4"
        Me.GridColumn28.Name = "GridColumn28"
        Me.GridColumn28.Visible = True
        Me.GridColumn28.VisibleIndex = 3
        '
        'g_oficinadisponibles
        '
        Me.g_oficinadisponibles.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.g_oficinadisponibles.Location = New System.Drawing.Point(1, 0)
        Me.g_oficinadisponibles.LookAndFeel.SkinName = "Black"
        Me.g_oficinadisponibles.MainView = Me.v_oficinasdisponibles
        Me.g_oficinadisponibles.Name = "g_oficinadisponibles"
        Me.g_oficinadisponibles.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit2})
        Me.g_oficinadisponibles.Size = New System.Drawing.Size(498, 421)
        Me.g_oficinadisponibles.TabIndex = 387
        Me.g_oficinadisponibles.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.v_oficinasdisponibles, Me.GridView2})
        '
        'v_oficinasdisponibles
        '
        Me.v_oficinasdisponibles.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasdisponibles.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_oficinasdisponibles.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.v_oficinasdisponibles.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(154, Byte), Integer), CType(CType(190, Byte), Integer), CType(CType(243, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasdisponibles.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_oficinasdisponibles.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.v_oficinasdisponibles.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.Empty.BackColor = System.Drawing.Color.White
        Me.v_oficinasdisponibles.Appearance.Empty.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.EvenRow.BackColor = System.Drawing.Color.White
        Me.v_oficinasdisponibles.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasdisponibles.Appearance.EvenRow.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.EvenRow.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasdisponibles.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_oficinasdisponibles.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.v_oficinasdisponibles.Appearance.FilterCloseButton.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(62, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White
        Me.v_oficinasdisponibles.Appearance.FilterPanel.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.FilterPanel.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.FixedLine.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.FocusedCell.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasdisponibles.Appearance.FocusedCell.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.FocusedCell.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasdisponibles.Appearance.FocusedRow.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.FocusedRow.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.FooterPanel.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.v_oficinasdisponibles.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasdisponibles.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_oficinasdisponibles.Appearance.FooterPanel.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.v_oficinasdisponibles.Appearance.FooterPanel.Options.UseFont = True
        Me.v_oficinasdisponibles.Appearance.FooterPanel.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.FooterPanel.Options.UseTextOptions = True
        Me.v_oficinasdisponibles.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.v_oficinasdisponibles.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasdisponibles.Appearance.GroupButton.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.GroupButton.Options.UseBorderColor = True
        Me.v_oficinasdisponibles.Appearance.GroupButton.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasdisponibles.Appearance.GroupFooter.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.v_oficinasdisponibles.Appearance.GroupFooter.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(62, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.GroupPanel.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.GroupPanel.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.GroupRow.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.v_oficinasdisponibles.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasdisponibles.Appearance.GroupRow.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.GroupRow.Options.UseBorderColor = True
        Me.v_oficinasdisponibles.Appearance.GroupRow.Options.UseFont = True
        Me.v_oficinasdisponibles.Appearance.GroupRow.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasdisponibles.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.v_oficinasdisponibles.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.v_oficinasdisponibles.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.HorzLine.BackColor = System.Drawing.Color.White
        Me.v_oficinasdisponibles.Appearance.HorzLine.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.v_oficinasdisponibles.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasdisponibles.Appearance.OddRow.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.OddRow.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.Preview.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.Preview.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.Row.BackColor = System.Drawing.Color.Silver
        Me.v_oficinasdisponibles.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.v_oficinasdisponibles.Appearance.Row.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.Row.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.RowSeparator.BackColor = System.Drawing.Color.White
        Me.v_oficinasdisponibles.Appearance.RowSeparator.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.v_oficinasdisponibles.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
        Me.v_oficinasdisponibles.Appearance.SelectedRow.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Appearance.SelectedRow.Options.UseForeColor = True
        Me.v_oficinasdisponibles.Appearance.VertLine.BackColor = System.Drawing.Color.White
        Me.v_oficinasdisponibles.Appearance.VertLine.Options.UseBackColor = True
        Me.v_oficinasdisponibles.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn5, Me.GridColumn13})
        Me.v_oficinasdisponibles.GridControl = Me.g_oficinadisponibles
        Me.v_oficinasdisponibles.Name = "v_oficinasdisponibles"
        Me.v_oficinasdisponibles.OptionsBehavior.Editable = False
        Me.v_oficinasdisponibles.OptionsBehavior.ReadOnly = True
        Me.v_oficinasdisponibles.OptionsView.EnableAppearanceEvenRow = True
        Me.v_oficinasdisponibles.OptionsView.EnableAppearanceOddRow = True
        Me.v_oficinasdisponibles.OptionsView.ShowFooter = True
        Me.v_oficinasdisponibles.OptionsView.ShowGroupPanel = False
        Me.v_oficinasdisponibles.OptionsView.ShowIndicator = False
        Me.v_oficinasdisponibles.RowHeight = 25
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "ID"
        Me.GridColumn5.FieldName = "OficinaID"
        Me.GridColumn5.Image = CType(resources.GetObject("GridColumn5.Image"), System.Drawing.Image)
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Width = 83
        '
        'GridColumn13
        '
        Me.GridColumn13.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn13.AppearanceCell.Options.UseFont = True
        Me.GridColumn13.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn13.AppearanceHeader.Options.UseFont = True
        Me.GridColumn13.Caption = "Oficinas Disponibles"
        Me.GridColumn13.FieldName = "Oficina"
        Me.GridColumn13.Image = CType(resources.GetObject("GridColumn13.Image"), System.Drawing.Image)
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "Oficina", "{0}")})
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 0
        Me.GridColumn13.Width = 372
        '
        'RepositoryItemCheckEdit2
        '
        Me.RepositoryItemCheckEdit2.AutoHeight = False
        Me.RepositoryItemCheckEdit2.Caption = "Check"
        Me.RepositoryItemCheckEdit2.Name = "RepositoryItemCheckEdit2"
        '
        'GridView2
        '
        Me.GridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray
        Me.GridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray
        Me.GridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray
        Me.GridView2.Appearance.ColumnFilterButton.Options.UseBackColor = True
        Me.GridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = True
        Me.GridView2.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.GridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray
        Me.GridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray
        Me.GridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro
        Me.GridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
        Me.GridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
        Me.GridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
        Me.GridView2.Appearance.Empty.BackColor = System.Drawing.Color.DimGray
        Me.GridView2.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal
        Me.GridView2.Appearance.Empty.Options.UseBackColor = True
        Me.GridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.White
        Me.GridView2.Appearance.EvenRow.Options.UseBackColor = True
        Me.GridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray
        Me.GridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray
        Me.GridView2.Appearance.FilterCloseButton.Options.UseBackColor = True
        Me.GridView2.Appearance.FilterCloseButton.Options.UseBorderColor = True
        Me.GridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray
        Me.GridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
        Me.GridView2.Appearance.FilterPanel.Options.UseBackColor = True
        Me.GridView2.Appearance.FilterPanel.Options.UseForeColor = True
        Me.GridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.Black
        Me.GridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.GridView2.Appearance.FocusedRow.Options.UseBackColor = True
        Me.GridView2.Appearance.FocusedRow.Options.UseForeColor = True
        Me.GridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray
        Me.GridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray
        Me.GridView2.Appearance.FooterPanel.Options.UseBackColor = True
        Me.GridView2.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.GridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver
        Me.GridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver
        Me.GridView2.Appearance.GroupButton.Options.UseBackColor = True
        Me.GridView2.Appearance.GroupButton.Options.UseBorderColor = True
        Me.GridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver
        Me.GridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver
        Me.GridView2.Appearance.GroupFooter.Options.UseBackColor = True
        Me.GridView2.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.GridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray
        Me.GridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White
        Me.GridView2.Appearance.GroupPanel.Options.UseBackColor = True
        Me.GridView2.Appearance.GroupPanel.Options.UseForeColor = True
        Me.GridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver
        Me.GridView2.Appearance.GroupRow.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.GridView2.Appearance.GroupRow.Options.UseBackColor = True
        Me.GridView2.Appearance.GroupRow.Options.UseFont = True
        Me.GridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray
        Me.GridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray
        Me.GridView2.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.GridView2.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.GridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray
        Me.GridView2.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray
        Me.GridView2.Appearance.HorzLine.Options.UseBackColor = True
        Me.GridView2.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke
        Me.GridView2.Appearance.OddRow.Options.UseBackColor = True
        Me.GridView2.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro
        Me.GridView2.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray
        Me.GridView2.Appearance.Preview.Options.UseBackColor = True
        Me.GridView2.Appearance.Preview.Options.UseForeColor = True
        Me.GridView2.Appearance.Row.BackColor = System.Drawing.Color.White
        Me.GridView2.Appearance.Row.Options.UseBackColor = True
        Me.GridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray
        Me.GridView2.Appearance.RowSeparator.Options.UseBackColor = True
        Me.GridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray
        Me.GridView2.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridView2.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray
        Me.GridView2.Appearance.VertLine.Options.UseBackColor = True
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn17, Me.GridColumn18, Me.GridColumn19, Me.GridColumn20})
        Me.GridView2.GridControl = Me.g_oficinadisponibles
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.EnableAppearanceEvenRow = True
        Me.GridView2.OptionsView.EnableAppearanceOddRow = True
        Me.GridView2.OptionsView.ShowAutoFilterRow = True
        Me.GridView2.OptionsView.ShowGroupPanel = False
        Me.GridView2.PaintStyleName = "UltraFlat"
        '
        'GridColumn17
        '
        Me.GridColumn17.Caption = "No"
        Me.GridColumn17.FieldName = "Nucleo_No"
        Me.GridColumn17.Name = "GridColumn17"
        Me.GridColumn17.Visible = True
        Me.GridColumn17.VisibleIndex = 0
        '
        'GridColumn18
        '
        Me.GridColumn18.Caption = "Nombre"
        Me.GridColumn18.FieldName = "Nucleo_Nombre"
        Me.GridColumn18.Name = "GridColumn18"
        Me.GridColumn18.Visible = True
        Me.GridColumn18.VisibleIndex = 1
        '
        'GridColumn19
        '
        Me.GridColumn19.Caption = "Dirección"
        Me.GridColumn19.FieldName = "Nucleo_Direccion"
        Me.GridColumn19.Name = "GridColumn19"
        Me.GridColumn19.Visible = True
        Me.GridColumn19.VisibleIndex = 2
        '
        'GridColumn20
        '
        Me.GridColumn20.Caption = "GridColumn4"
        Me.GridColumn20.Name = "GridColumn20"
        Me.GridColumn20.Visible = True
        Me.GridColumn20.VisibleIndex = 3
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "c"
        Me.GridColumn9.FieldName = "c"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 1
        '
        'Open
        '
        Me.Open.Filter = "JPG|*.JPG"
        Me.Open.Title = "Fonde del Sistema"
        '
        'frm_ausr
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1305, 451)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.GroupControl4)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frm_ausr"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Usuarios"
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.opt_duplicado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.opt_servicios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.opt_tarifas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.opt_rectificar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.opt_anular.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.opt_cliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.opt_arqueo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.opt_ac_sesion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.opt_ac_fecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.opt_estado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpt_baja.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpt_baja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmb_rol.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpt_fecha_ingreso.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dpt_fecha_ingreso.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_foto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.gridData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.v_fabrica, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.vProductores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        CType(Me.g_oficinasAsignadas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.v_oficinasasignadas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.g_oficinadisponibles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.v_oficinasdisponibles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents BtnRegresar As System.Windows.Forms.Button
    Friend WithEvents btnguardar As System.Windows.Forms.Button
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtconfirmar As System.Windows.Forms.TextBox
    Friend WithEvents txtcontraseña As System.Windows.Forms.TextBox
    Friend WithEvents dpt_baja As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents dpt_fecha_ingreso As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtusr As System.Windows.Forms.TextBox
    Friend WithEvents txt_telefono As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txt_celular As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txt_cedula As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtapellidos As System.Windows.Forms.TextBox
    Friend WithEvents txtnombre As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents pic_foto As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents cmb_rol As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Open As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents opt_estado As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gridData As DevExpress.XtraGrid.GridControl
    Friend WithEvents v_fabrica As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents vProductores As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents opt_ac_sesion As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents opt_ac_fecha As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents opt_arqueo As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents opt_cliente As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents opt_rectificar As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents opt_anular As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents opt_servicios As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents opt_tarifas As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents opt_duplicado As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents btn_remove As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn_add As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents g_oficinasAsignadas As DevExpress.XtraGrid.GridControl
    Friend WithEvents v_oficinasasignadas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn21 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn22 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents GridView5 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn25 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn26 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn27 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn28 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents g_oficinadisponibles As DevExpress.XtraGrid.GridControl
    Friend WithEvents v_oficinasdisponibles As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn17 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn18 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn19 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn20 As DevExpress.XtraGrid.Columns.GridColumn
End Class
